#!usr/bin/env python3

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from mod.funk_run import Run
from mod.funk_manip import muon_data, weather_data, combine_data
from mod.funk_plt import heatmap, show

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (165/72, 165/72)

pd.set_option('display.max_rows', 10)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


cuts = fcn.cuts.select('dtlim' , 'riselim', 'Slim', 'tsiglim')
trig = False
run  = Run.configs('v35', trig=trig, cuts=cuts)
# run = Run.configs('v35', trig=True)


df_pmt	= run.get_df('out_closed')
muons 	= muon_data()
weather = weather_data()
# embed()
df_all 	= combine_data(df_pmt, muons, weather)

labels  = [
	'rates',
	'T1', 'T2', #'T2m', 'T200m',
	'p', #'p2',
	'vcoinc', 'hcoinc'
	]

correls = df_all[labels].corr().iloc[1:,:-1]
mask 		= np.ones_like(correls, dtype=bool)
mask[np.triu_indices_from(mask, k=1)] = False


# #  somehow copy paste this into embed works better for tight-layout
plt.figure()
im, cbar = heatmap(
	correls, mask=mask, vmin=-1, vmax=1,
	fmt='%.2f', annot_kwargs={'fontsize': 'smaller'},
	cbar_kwargs={'pad':0, 'aspect': 20, 'shrink': 0.855},
	xticklabels=['$r_\mathrm{ev}$', '$T_1$', '$T_2$', '$P$', '$r_\mu^\mathrm{v}$'],
	yticklabels=['$T_1$', '$T_2$', '$P$', '$r_\mu^\mathrm{v}$', '$r_\mu^\mathrm{b}$']
)
im.axes.tick_params(top=False, right=False)
cbar.ax.tick_params(labelsize='smaller', length=2)


show()
# show(save='./plt/12/v35_outclosedCorrelation', pad=0.05)

embed()