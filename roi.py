#!usr/bin/env python3

import numpy as np
import pandas as pd
import scipy.stats as st
import matplotlib.pyplot as plt
import matplotlib.tight_layout as tl
import mod.funk_consts as fcn 

from IPython import embed
from matplotlib.colors import LogNorm
from matplotlib.patches import Rectangle
from matplotlib.transforms import Bbox
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from mod.funk_run import Run, Flasher
from mod.funk_manip import apply_cuts
from mod.funk_plt import show, order_legend, group_legend, add_toplegend

plt.rcParams.update(fcn.rcDefense)
# plt.rcParams.update(fcn.rcThesis)
# plt.rcParams.update(fcn.rcPaper)
# plt.rcParams.update(fcn.rcPoS)

# plt.rcParams.update(fcn.rcPoster)
# plt.rcParams['figure.figsize'] = (550/72, 350/72)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

plt.rcParams['figure.figsize'] = (200/72, 174/72)

def std_gaussian_entropy(h):
	""" standard deviation of a gaussian signal as function of
			the differential information entropy
	"""
	return np.exp(h) / np.sqrt( 2*np.pi*np.e )


cuts = fcn.cuts.select('dtlim')
run  = Run.raw_configs(
				run_v='v35', npy='auto',
				cols=['S', 'h', 'q', 'tvar', 'rise'],
				cuts=cuts
			 )

df = run.get_df('out_open').copy() # do not copy if out of memory
df['h'] = 10**df['h'].values
df.rename(columns={'h': 'H'}, inplace=True)


fig, ax = plt.subplots()
ax.tick_params(right=False)

xdata = df['S'].values
ydata = df['tsig'].values
hist 	= ax.hist2d(
					xdata, ydata, bins=100,
					range=[(1, 5), (0, 30)],
					cmap='viridis', norm=LogNorm(),
					label='all pulses'
				)
cbar 	= fig.colorbar(
					mappable=hist[3], pad=0, aspect=30,
					shrink=1., fraction=0.1,
					orientation='vertical', ticklocation='left'
				)
cbar.ax.tick_params(length=2, labelsize='smaller')
cbar.ax.yaxis.set_minor_locator(plt.NullLocator())


Srange = np.linspace(df['S'].min(), df['S'].max())
ax.plot(
	Srange, std_gaussian_entropy(Srange),
	color='k', ls='--', lw=0.6, label='Gaussian\nshape'
)


Slim 	 	= fcn.cuts['Slim']
tsiglim = fcn.cuts['tsiglim']
ax.add_patch(
	Rectangle(
		xy=(Slim[0], tsiglim[0]),
		width= Slim[1]-Slim[0],
		height=tsiglim[1]-tsiglim[0],
		fill=False, edgecolor='red',
		ls='-', lw=1.2, label='ROI'
))


# ax.set_xlabel('$S$')
ax.set_xlabel('Information entropy')
ax.set_xlim(1, 5)
# ax.set_ylabel('$\sigma_t$/ns')
ax.set_ylabel('Pulse width/ns')
ax.set_ylim(0, 30)
ax.legend(
	*order_legend('Gaussian'), loc='upper left',
	fontsize='smaller', handlelength=1.4, frameon=False
)

fig.tight_layout(pad=0.1)
rect = tl.get_tight_layout_figure(
					plt.gcf(), [plt.gca()], tl.get_subplotspec_list([plt.gca()]),
					tl.get_renderer(plt.gcf()), pad=0.05
				)
bbox = ax.get_position()
# fig.savefig('./plt/12/v35_roi')
fig.savefig('../talks/src/phd_defense/figs/v35_roi')
plt.close()

# # this is wrong if data is cutted cause second pulse might be selected
# # ntriggers = df[df['pulse']==1]['pulse'].sum()

print(
	'before cuts: ntriggers={:,}, npulses={:,}'
	.format(len(df.groupby(['time', 'capture']).head(1)), len(df))
)

df2 = apply_cuts(df, fcn.cuts.select('riselim', 'Slim', 'tsiglim'))
print(
	'after cuts : ntriggers={:,}, npulses={:,}'
	.format(len(df2.groupby(['time', 'capture']).head(1)), len(df2))
)

# show()
# embed()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # mini-plots for flasher (thesis)

# plt.rcParams['figure.figsize'] = (130/72, 174/72)

# def get_df(dic, key):
	
# 	df = dic[key].copy()

# 	df['h'] 	 = 10**df['h'].values
# 	df['q'] 	 = 10**df['q'] / 1e8 
# 	df['tvar'] = np.sqrt(df['tvar'].values)
# 	df.rename(
# 		columns={'h': 'H', 'q': 'Q', 'tvar': 'tsig'},
# 		inplace=True
# 	)

# 	return df

# run = Flasher('v08f', split=True)
# dic = run.data
# for key in tuple(dic.keys()):
# 	dic[ key[-7:-3] ] = dic.pop(key)


# fig, (ax0, ax1) = plt.subplots(2,1, sharex=True, gridspec_kw={'hspace': 0})
# ax0.tick_params(right=False, bottom=False)
# ax1.tick_params(right=False, top=False)

# df_spe 	= get_df(dic, 'l126')
# xdata = df_spe['S'].values
# ydata = df_spe['tsig'].values
# hist 	= ax0.hist2d(
# 					xdata, ydata, bins=100,
# 					range=[(1, 5), (0, 30)],
# 					cmap='viridis', norm=LogNorm()
# 				)
# cax0 	= inset_axes(
# 					ax0,  width='5%', height='75%',
# 					loc='center right', borderpad=0,
# 				)
# cax0.tick_params(
# 	axis='both', direction='in', length=2,
# 	left=False, labelleft=False,
# 	bottom=False, labelbottom=False,
# 	top=False, labeltop=False,
# 	labelsize='smaller'
# )
# cbar 	= fig.colorbar(
# 					mappable=hist[3], cax=cax0,
# 					orientation='vertical',
# 					ticklocation='right',
# 					ticks=[1, 10, 100]
# 				)
# cax0.yaxis.set_minor_locator(plt.NullLocator())



# df_high = get_df(dic, 'l170')
# xdata = df_high['S'].values
# ydata = df_high['tsig'].values
# hist  = ax1.hist2d(
# 	xdata, ydata, bins=100,
# 	range=[(1, 5), (0, 30)],
# 	cmap='viridis', norm=LogNorm()
# )

# cax1 	= inset_axes(
# 					ax1,  width='4.9%', height='75%',
# 					loc='center right', borderpad=0,
# 				)
# cax1.tick_params(
# 	axis='both', direction='in', length=2,
# 	left=False, labelleft=False,
# 	bottom=False, labelbottom=False,
# 	top=False, labeltop=False,
# 	labelsize='smaller'
# )
# cbar 	= fig.colorbar(
# 					mappable=hist[3], cax=cax1,
# 					orientation='vertical',
# 					ticklocation='right',
# 					ticks=[1, 10, 100]
# 				)
# cax1.yaxis.set_minor_locator(plt.NullLocator())


# ax0.set_ylabel('$\sigma_t$/ns')
# ax0.set_yticks([5, 15, 25])
# ax0.set_ylim(0, 30)

# ax1.set_ylabel('$\sigma_t$/ns')
# ax1.set_yticks([5, 15, 25])
# ax1.set_ylim(0, 30)
# ax1.set_xlabel('$S$')
# ax1.set_xticks([2, 3, 4])
# ax1.set_xlim(1, 5)

# fig.subplots_adjust(
# 	# these are from rect, calculated above 
# 	# top=0.974367816091954, bottom=0.16183908045977013,
# 	top=0.965, bottom=0.17,
# 	left=0.22, right=0.88,
# 	wspace=0.1
# )
# # fig.tight_layout(pad=0.5)

# fig.savefig('./plt/12/flasher_roi')
# plt.close()

# show()
# embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # mini-plots for flasher (paper)

# plt.rcParams['figure.figsize'] = (246/72, 0.5*246/72)

# def get_df(dic, key):
	
# 	df = dic[key].copy()

# 	df['h'] 	 = 10**df['h'].values
# 	df['q'] 	 = 10**df['q'] / 1e8 
# 	df['tvar'] = np.sqrt(df['tvar'].values)
# 	df.rename(
# 		columns={'h': 'H', 'q': 'Q', 'tvar': 'tsig'},
# 		inplace=True
# 	)

# 	return df

# run = Flasher('v08f', split=True)
# dic = run.data
# for key in tuple(dic.keys()):
# 	dic[ key[-7:-3] ] = dic.pop(key)


# fig, (ax0, ax1) = plt.subplots(1,2, sharey=True)

# df_spe 	= get_df(dic, 'l126')
# xdata = df_spe['S'].values
# ydata = df_spe['tsig'].values
# ax0.hist2d(
# 	xdata, ydata, bins=100,
# 	range=[(1, 5), (0, 30)],
# 	cmap='BuPu', norm=LogNorm()
# )


# df_high = get_df(dic, 'l170') # l178
# xdata = df_high['S'].values
# ydata = df_high['tsig'].values
# ax1.hist2d(
# 	xdata, ydata, bins=100,
# 	range=[(1, 5), (0, 30)],
# 	cmap='BuPu', norm=LogNorm()
# )

# ax0.set_xticks(range(1, 6))
# ax0.set_yticks(range(0, 35, 10))
# ax0.tick_params(axis='y', right=True)
# ax0.set_ylabel('pulse duration/ns')
# ax1.set_xticks(range(1, 6))
# plt.figtext(0.39, 0.02, 'information entropy')

# fig.subplots_adjust(
# 	left=0.121, bottom=0.22, top=0.95, right=0.98, wspace=0.1
# )
# fig.savefig('./plt/8/flasher_roi')
# # show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # charge distribution before/after cuts

# plt.rcParams['figure.figsize'] = (180/72, 174/72)

# run  = Run.raw_configs(
# 				run_v='v35', npy='auto',
# 				cols=['S', 'h', 'q', 'tvar', 'rise'],
# 				cuts=fcn.cuts.select('dtlim')
# 			 )

# df = run.get_df('out_open').copy()
# df['h'] = 10**df['h'].values
# df.rename(columns={'h' : 'H'}, inplace=True)
# data = df['q'].values # np.power(df['q'].values, 10) / 1e8

# cuts = fcn.cuts.select('riselim', 'Slim', 'tsiglim')
# df2  = apply_cuts(df, cuts)
# data2 = df2['q'].values # np.power(df2['q'].values, 10) / 1e8


# bins 		= 100
# qrange 	= (6,10)
# density = False

# plt.hist(
# 	data, bins, qrange, density,
# 	histtype='step', lw=1, color='k',
# 	label='all pulses'
# )

# plt.hist(
# 	data2, bins, qrange, density,
# 	histtype='step', lw=1, color='crimson',
# 	label='events'
# )

# plt.xlabel('$q=\lg(Q/e)$')
# plt.xticks([6, 7, 8, 9, 10])
# plt.yscale('log')
# add_toplegend(labels=['all', 'events'], ncol=2)

# plt.tight_layout(pad=0.05)
# plt.savefig('./plt/12/v35_qDistn')
# plt.close()

# # # show()
# embed()