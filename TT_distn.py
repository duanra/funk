#!usr/bin/env python3

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from iminuit import Minuit
from itertools import chain
from IPython import embed

from mod.cymodule.TT_distn import get_mask
from mod.funk_manip import apply_cuts
from mod.funk_plt import show, order_legend, add_toplegend
from mod.funk_utils import timing, handle_warnings, Namespace
from mod.funk_ROOTy import TH1Fitter, TH1Wrapper, plot_th1

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (220/72, 150/72)

# plt.rcParams.update(fcn.rcPaper)
# plt.rcParams.update(fcn.rcPoS)

# plt.rcParams.update(fcn.rcPoster)
# plt.rcParams['figure.figsize'] = ( 550/72, 350/72 )

np.set_printoptions(linewidth=150)
pd.set_option('display.max_rows', 5)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


@timing
def get_deltas(df):
	""" get interarrival-times of triggers """

	_, counts = np.unique(df.index.values, return_counts=True)
	ttags 		= np.split(df['ttag'].values, np.cumsum(counts)[:-1])

	n 	 = len(df) - len(counts)
	ret  = np.fromiter(
					chain.from_iterable( np.diff(ttag) for ttag in ttags ),
					dtype=float, count=n
					)

	# # no-dots in loop: .values, .iloc .loc extremely slow!!!!
	# time = df.index.unique().values
	# ttag = df.ttag
	# n 	 = len(df) - len(time)
	# ret  = np.fromiter(
	# 				chain.from_iterable( np.diff(ttag[t].values) for t in time ),
	# 				dtype=float, count=n
	# 				)

	return ret


@timing
def selective_count(df, deadtime=0.1):
	""" implement counting dead time [secs],
			pulse-cuts can be done externally
	"""

	if deadtime is None or deadtime==0:
		return df

	else:

		_, counts = np.unique(df.index.values, return_counts=True)
		ttags 		= np.split(df['ttag'].values, np.cumsum(counts)[:-1])
		idxs 			= get_mask(ttags, deadtime)

		return df.iloc[idxs]


@handle_warnings
def expon_cdf(rvs, lam, deadtime=None):
	""" lam is the mean of (poissonian) event rate """

	if deadtime is not None:
		rvs -= deadtime
		# np.exp(-lam*deadtime) - np.exp(-lam*rvs)

	return 1 - np.exp(-lam*rvs)


class Deltas:

	def __init__(self, df, deadtime=None):

		self.__name__ 	 = 'deltas' # -required in TH1Fitter
		self.df_original = df
		self.nbins 			 = 100
		self.density 		 = True

		self.update(deadtime)


	def __call__(self, x, deadtime, N):
		""" for fitting purpose """

		if self.deadtime != deadtime:
			self.update(deadtime)

		i 	= self.th1.FindBin(x)
		ret = N * self.th1.GetBinContent(i)
		
		print(deadtime, ret)
		return ret


	def update(self, deadtime):

		df 		 = selective_count(self.df_original, deadtime)
		deltas = get_deltas(df)
		lam 	 = df.groupby('time').size().mean() / 60

		if deadtime is not None:
			lam /= (1 - lam * deadtime) # correction

		self.deadtime 	 = deadtime
		self.nevents 		 = len(df)
		self.lam 				 = lam
		self.transformed = expon_cdf(deltas, lam, deadtime)
		self.th1 				 = TH1Wrapper(
													self.transformed,
													self.nbins,
													0, 1,
													self.density)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# cuts obtained from caen data are somewhat not applicable here
# (see distributions below)
cuts = Namespace([
	('riselim', (0,)),
	('Slim', (1.719, 3.951)), #(2.08, 3.56)),
	('tsiglim', (1.624, 12.481)) #(3.34, 10.44))
])


plt.figure('ttagDistn')

# df = pd.read_pickle('./data/timetag/pulse_ttag_1536585179.sdf')
df = pd.read_pickle('./data/timetag/pulse_ttag_1535701375.sdf') # full

deltas = Deltas(df)
plot_th1(
	deltas.th1, endlines=True, fillprops=True,
	color='k', lw=1, label='all pulses'
)

df_wcuts = apply_cuts(df, cuts)
deltas_wcuts = Deltas(df_wcuts)
plot_th1(
	deltas_wcuts.th1, endlines=True, fillprops=None,
	color='crimson', lw=1, label='events'
)

# poissonian process
bce = np.array([ deltas.th1.GetBinCenter(i)
								 for i in range(1, deltas.th1.GetNbinsX()+1) ])
th1_poisson = TH1Wrapper(bce, 100, 0, 1, True)
plot_th1(
	th1_poisson, endlines=True, fillprops=dict(alpha=0.2),
	color='silver', lw=1.5, ls='--',
	label='Poi', zorder=0
)

plt.xlabel(r"$\tilde{t}$") # '$\Theta$'
plt.yscale('log')
plt.ylim(0.5, 30)
add_toplegend(labels=['all', 'events', 'Poi'], ncol=3, lw=2)

plt.tight_layout(pad=0.05)
plt.savefig('./plt/12/caen_ttagDistn')
plt.close()



plt.figure('ttagDistn_wDeadtime')

deltas_wdt1 = Deltas(df_wcuts, deadtime=1e-6)
nfrac1 = deltas_wdt1.nevents / deltas_wcuts.nevents
print(nfrac1)
plot_th1(
	deltas_wdt1.th1, endlines=True, fillprops=None,
	color='blue', lw=1,
	label=r'$t_\mathrm{dead} = 1\,\upmu$s'
)

deltas_wdt2 = Deltas(df_wcuts, deadtime=1e-3)
nfrac = deltas_wdt2.nevents / deltas_wcuts.nevents
print(nfrac)
plot_th1(
	deltas_wdt2.th1, endlines=True, fillprops=True,
	color='green', lw=1,
	label=r'$t_\mathrm{dead} = 1\,$ms'
)

plt.xlabel(r"$\tilde{t}$")
plt.yscale('log')
plt.ylim(0.5, 30)
add_toplegend(labels=['upmu', 'ms'], ncol=2, lw=2)

# plt.tight_layout(pad=0.05)
# from TT_distn2
rect = {
	'bottom': 0.1844,
	'top': 0.8830127886467943,
	'left': 0.13800933577387356,
	'right': 0.9999999999999997
}
plt.subplots_adjust(**rect)

plt.savefig('./plt/12/caen_ttagDistn_wDeadtime')
plt.close()


# show()
# embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# deadtime_fitter = TH1Fitter(th1_poisson, deltas_wdt)
# deadtime_fitter.set_parameters(30e-3, 1)
# deadtime_fitter.set_parlimits((1e-3, 1), (0,1))
# deadtime_fitter.fix_parameters(1)
# deadtime_fitter.fit('R')
# pickle_dump( deadtime_fitter.fitted, 'deadtime_fitted.sdic', './log' )

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # distns and cuts
# from matplotlib.colors import LogNorm
# from matplotlib.patches import Rectangle


# df 	 = pd.read_pickle('./data/timetag/pulse_ttag_1536585179.sdf')
# # df = pd.read_pickle('./data/timetag/pulse_ttag_1535701375.sdf') # full

# # df = pd.read_pickle('./data/timetag/pulse_ttag_13-26.sdf')
# # counts = df.groupby('time').size().values
# # time 	 = df.index.unique().values
# # trunc  = counts[:600].sum() # truncate 10 hours out
# # df = df.iloc[trunc:]


# # define cuts
# df2 = df.copy()
# df2 = apply_cuts(df2, cuts=Namespace(riselim=(0,)))

# S  = df2['S'].values
# S_mean, S_std = np.mean(S), np.std(S, ddof=1)

# tsig = df2['tsig'].values
# tsig = tsig[(tsig>=3) & (tsig<=11)] # preliminary cuts, hard truncation
# tsig_mean, tsig_std = np.mean(tsig), np.std(tsig, ddof=1)


# cuts_caen = Namespace({
# 	'Slim'		: (round(S_mean-3*S_std, 3), round(S_mean+3*S_std, 3)),
# 	'tsiglim'	: (round(tsig_mean-3*tsig_std, 3), round(tsig_mean+3*tsig_std, 3))
# })

# cuts_pico = fcn.cuts.select('Slim', 'tsiglim')	


# def add_patch(cuts, **kwargs):

# 	Slim 		= cuts.Slim
# 	tsiglim = cuts.tsiglim

# 	plt.gca().add_patch(
# 		Rectangle(
# 			xy = (Slim[0], tsiglim[0]),
# 			width = Slim[1] - Slim[0],
# 			height = tsiglim[1] - tsiglim[0],
# 			fill = False, ls='--', **kwargs
# 	))

# plt.hist2d(
# 	df['S'].values, df['tsig'].values, 100,
# 	range=[(1,5), (0, 50)],
# 	cmap='Greys', norm=LogNorm()
# )

# add_patch(cuts_caen, edgecolor='r')
# add_patch(cuts_pico, edgecolor='b')

# plt.xlabel('entropy')
# plt.ylabel('pulse duration')
# plt.tight_layout(pad=0.05)

# print('cuts_caen: ', cuts_caen)
# print('cuts_pico: ', cuts_pico)

# df3 = apply_cuts(df2, cuts=cuts_caen)
# print('cuts_caen efficiency: {:.2%}'.format(len(df3)/len(df)))

# show()
# embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # TODO: clean up, but probably not useful anymore

# @handle_warnings
# def pareto2_tr(y, A, n, theta, lam):
# 	""" probability of Y=F(X) where F is the CDF of expon(lam) and X~pareto2
# 			with y in [0,1), i.e apply expon_cdf transform of pareto2 rvs
# 	"""

# 	ret = np.exp( 
# 					np.log(A) + np.log(n) + np.log(theta) - np.log(lam) - np.log(1-y)
# 					- (n+1) * np.log( 1 - theta/lam * np.log(1-y) )
# 				)
# 	# ret = n*theta / (lam*(1-y)) * np.power(1 - theta/lam * np.log(1-y), -n-1)

# 	return ret 


# def powerlaw(y, B, k):
# 	from scipy.stats import powerlaw

# 	return B * powerlaw.pdf(y, k, loc=0, scale=1)


# def model(y, A, n, theta, lam, B, k):

# 	return pareto2_tr(y, A, n, theta, lam) + powerlaw(y, B, k)


# # model fitting
# fitter_tp2 = TH1Fitter(th1, pareto2_tr, xlow=0.15)
# mu  	 = np.mean(deltas)
# var 	 = np.var(deltas, ddof=1)
# n0 		 = 2*var / (var-mu**2)
# theta0 = (var-mu**2) / (var*mu+mu**3)
# fitter_tp2.set_parameters(1, n0, theta0, lam)
# fitter_tp2.set_parlimits((0,10), (2,50), (0,10), None)
# fitter_tp2.fix_parameters('lam')
# fitter_tp2.fit('R')
# fitter_tp2.plot(
# 	hprops=dict(endlines=True, fillprops=True, label='data', zorder=2),
# 	fprops=dict(lw=2, label='pareto2', zorder=1)
# 	)


# fitter_pw = TH1Fitter(th1, powerlaw, xlow=th1.GetBinCenter(2), xup=0.2)
# fitter_pw.set_parameters(1, 1)
# fitter_pw.set_parlimits(None, (0,10))
# fitter_pw.fit('R')
# fitter_pw.plot(hprops=False, fprops=dict(color='r', lw=2, ls=':'))