#!usr/bin/env python3

import numpy as np
import pandas as pd
from iminuit import Minuit
import matplotlib.pyplot as plt

from scipy.interpolate import interp1d
from scipy.fftpack import ifft
from scipy.stats import binned_statistic, pearsonr
from matplotlib.colors import LogNorm

from funk_manip import reset_configs
from funk_plt import cornertext
from funk_stats import fourier, power_spectrum, bin_centers
from funk_utils import posix2utc, timing, Namespace

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

POS = {
				(0,1) : 'out_open'	,
				(0,0) : 'out_closed',
				(1,1) : 'in_open'		,
				(1,0) : 'in_closed'	
			}


PMT = Namespace([
				('alpha', 0.9223), # \pm 0.0148
				('tau', 54.1247),  # \pm 1.1443
				('base', 96.5996)  # \pm 0.3945
			])


SPE = Namespace([
				('dtlim', (59.9, 60.1)),
				('riselim', (0,)),
				('Slim', (2.400, 3.266)), # (2.305, 3.317)),
				('tsiglim', (2.066, 6.061)) # (1.769, 6.163)),
			])


# event excess in response in/open
EPS_11 = (0.0032, 0.0006)


def get_data(run, begin=None, Dt=None):
	""" run: instance of funk_run.Run
			begin/Dt: timedelta object
	"""

	df = reset_configs(run.configs)\
				[['time', 'in', 'open', 'dt', 'counts', 'T1', 'T2', 'p']]
	df = df.assign(
					timedelta = posix2utc(df['time']) - \
											posix2utc(df['time'].iloc[0])
					)

	if (begin is not None) and (Dt is not None):
		ret = df[ (df['timedelta']>= begin) & (df['timedelta'] <= begin+Dt) ]

	elif begin is not None:
		ret = df[ (df['timedelta']>= begin) ]

	elif Dt is not None:
		ret = df[ (df['timedelta'] <= Dt) ]

	else:
		ret = df

	ret['T1'] = ret['T1'].interpolate()
	ret['T2'] = ret['T2'].interpolate()
	ret['p'] 	= ret['p'].interpolate()

	return ret.iloc[:len(ret)-len(ret)%4]\
						.reset_index(drop=True)



def dB(arr, ref_value):
	return 10 * np.log10(arr / ref_value)


def hertz2cpd(arr):
	""" cpd: cycles per day """
	return np.asarray(arr) * (60 * 60 * 24)


def cpd2hertz(arr):
	return np.asarray(arr) / (60 * 60 * 24)



def get_trend(xs, ts, flow=None, fup=None):
	""" get iffts of harmonics with frequencies between flow and fup
			xs: sampled values, ts: sampling timestamps
			freqs unit is cpd
	"""

	dt = np.mean(np.diff(ts))
	Fs = 1 / dt

	freqs, fftvals = fourier(xs, Fs, window=None, amplitude=False,
													 normed=False, onesided=False)
	
	DC 		= np.abs(fftvals[0])
	N 		= len(fftvals)

	Fs 		= hertz2cpd(Fs).round(8)
	freqs = hertz2cpd(freqs).round(8) # round for comparison
	fabs  = np.abs(freqs)

	fcut 	= (fabs[1] if flow is None else flow,
					 max(fabs) if fup is None else fup)
	ixcut = (fabs >= fcut[0]) & (fabs <= fcut[1])

	fftvals_	= fftvals.copy()
	fftvals_[ np.logical_not(ixcut) ] = 0

	ifftvals_ = np.real(ifft( fftvals_ ))

	# trend = interp1d(ts, ifftvals_, kind='quadratic',
	# 									fill_value='extrapolate')

	return ifftvals_



def get_power_spectrum(xs, ts):
	""" xs: sampled values, ts: sampling timestamps """

	dt = np.mean(np.diff(ts))
	Fs = 1 / dt # sampling frequency

	freqs, powers = power_spectrum(xs, Fs)

	Fs 			 = hertz2cpd(Fs)	
	freqs 	 = hertz2cpd(freqs[1:])
	powers 	 = dB(powers[1:], powers[0]) # relative to DC component

	return freqs, powers, Fs



class TrueSignal:
	""" correct for historical pile-up to recover the instantaneous signal
			events are evaluated at the middle each time-interval
	"""

	def __init__(self, data, alpha, tau, base):

		seq 	 	= list(map(tuple, data[['in', 'open']].iloc[:4].values))
		n 			= len(data)
		tintvls = [ (i, i+1) for i in range(n) ]

		self.dt 		 = 1
		self.seq 		 = seq
		self.midtime = np.mean(tintvls, axis=1)
		self.tintvls = tintvls

		self.events  = data['counts'].values
		self.params  = Namespace(alpha=alpha, tau=tau, base=base)

		self.signal  = np.zeros(n)
		self.history = np.zeros(n)
		self.xneg 	 = 0


	@classmethod
	def reconstruct(cls, data, alpha, tau, base, print_level=0):
		""" more like an alternative __new__, than __init__ """
		model = cls(data, alpha, tau, base)
		model._reconstruct(print_level)

		return model


	def _initial_values(self, print_level=0):

		alpha 	= self.params.alpha
		tau 		= self.params.tau
		base 		= self.params.base
		dt 			= self.dt
		dt_half = dt / 2

		# fit initial values
		ndebut 			 = 4*10
		events_debut = self.events[:ndebut]
		xneg = 50
		xs 	 = 100 * np.ones(ndebut)
		ix0  = min(self.seq.index((0,0)), self.seq.index((1,0)))
		xs[ix0::2] = 50

		parnames = ['xneg'] + ['x%d'%i for i in range(ndebut)]
		fitarg 	 = Namespace(
			[ ('xneg', xneg) ] +
			[ ('x%d'%i, xs[i]) for i in range(ndebut) ] +
			[ ('limit_xneg', (0, None)) ] +
			[ ('limit_x%d'%i, (0, None)) for i in range(ndebut) ] +
			[ ('error_xneg', 0.1) ] +
			[ ('error_x%d'%i, 0.1) for i in range(ndebut) ] +
			[ ('fix_x%d'%i, False) for i in range(ix0, ndebut, 2)] +	
			[ ('errordef', 1) ]
			)

		def lsq(*args):

			xneg, *xs = args
			ys = np.zeros(ndebut)
			hs = np.zeros(ndebut)

			coeff = 1 + alpha * (1 - np.exp(-dt_half/tau))
			ys[0] = base + xs[0]*coeff + alpha*xneg * np.exp(-dt_half/tau)

			for i in range(1, ndebut):
				hs[i] = hs[i-1] * np.exp(-dt/tau) +\
								xs[i-1] * np.exp(-dt_half/tau) * (1 - np.exp(-dt/tau))
				hneg 	= xneg * np.exp(-(i+dt_half)/tau) 
				ys[i] = base + xs[i]*coeff + alpha * (hneg + hs[i])

			# error can be assumed to be constant or proportional to poissonian
			return np.sum( (events_debut - ys)**2 )


		print('fitting initial values::')
		mfit = Minuit(lsq, forced_parameters=parnames,
									print_level=print_level, **fitarg)
		timing(mfit.migrad)()
		# mfit.minos('xneg')
		# mfit.minos('x0')

		self.xneg 		 = mfit.values['xneg']
		self.signal[0] = mfit.values['x0']


	def _reconstruct(self, print_level=0):

		self._initial_values(print_level)

		nevents = len(self.events)
		alpha 	= self.params.alpha
		tau 		= self.params.tau
		base 		= self.params.base
		dt 			= self.dt
		dt_half = dt / 2
		den 		= 1 + alpha * (1 - np.exp(-dt_half/tau))

		for i in range(1, nevents):

			yi 	 = self.events[i] - base
			hi 	 = self.history[i-1] * np.exp(-dt/tau) +\
					 	 self.signal[i-1] * np.exp(-dt_half/tau) * (1 - np.exp(-dt/tau))
			hneg = self.xneg *  np.exp(-(i+dt_half)/tau) 
			xi 	 = (yi  - alpha * (hi + hneg)) / den

			self.history[i] = hi
			self.signal[i] 	= xi

		print('::reconstruction done.')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


def profile_plot(x, y, bins_x, bins_y, statistic='stderr', legend=False):

	def flinear(a, b):
		return np.sum( (y - (a*(x-np.mean(x)) + b))**2 )

	if statistic=='std':
		statistic = lambda z: np.std(z, ddof=1)
	else: # default to this
		statistic = lambda z: np.std(z, ddof=1) / np.sqrt(len(z))


	y_mean, x_edg, *_ = binned_statistic(x, y, 'mean', bins_x)
	y_errbar, *_ 			= binned_statistic(x, y, statistic, bins_x)

	fit = Minuit(flinear, pedantic=False)
	fit.migrad()
	fitarg = fit.fitarg


	hist = plt.hist2d(
					x, y, bins=(bins_x, bins_y),
					cmap='viridis', norm=LogNorm()
				 )

	plt.colorbar(
		mappable=hist[3], pad=0, aspect=30, shrink=1,
		fraction=0.10, orientation='vertical', ticklocation='left'
		)

	plt.errorbar(
		bin_centers(x_edg), y_mean, y_errbar,
		fmt='o', ms=3, capsize=2, elinewidth=2,
		color='linen', mfc='k'
		)

	plt.plot(
		x, fitarg['a']*x + fitarg['b'] - fitarg['a']*np.mean(x),
		ls='-', color='k'
		)

	if legend:
		cornertext(
			'slope = {:.3f} $\pm$ {:.3f}\ncorr   = {:.3f}'
			.format(fitarg['a'], fitarg['error_a'], pearsonr(x,y)[0]),
			loc='upper left', frameon=True
			)
	else:
		print('slope: {:.5f} +- {:.5f}'.format(fitarg['a'], fitarg['error_a']))
		print('pearsonr:', pearsonr(x,y))