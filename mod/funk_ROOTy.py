""" various utilities / interfaces when working with pyROOT """

import numpy as np
import matplotlib.pyplot as plt

from contextlib import suppress
from inspect import signature
from tabulate import tabulate
from functools import wraps
from funk_utils import fmt, positive_index, Namespace 
from funk_stats import bin_centers, bin_widths

from root_numpy import hist2array, fill_hist
from ROOT import Double, TF1, TH1D, gRandom

from IPython import embed

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def TF1_signature(func):
	""" decorator for python function to pass on ROOT TF1 constructor 
			note: first arg of func must be the variable, then
						params are passed as *args. **kwargs are ignored
	"""
	@wraps(func)

	def wrapper(x, *pars):
		if len(pars):
			 # PyDoubleBuffer doesn't support index slicing
			pars_ = np.frombuffer(*pars)
			return func(x[0], *pars_)

		else:
			return func(x[0])

	return wrapper


def get_parameters(tf1):

	return np.frombuffer(tf1.GetParameters(), count=tf1.GetNpar())


def get_fitresults(tf1):
	""" tf1 is a TF1 root object """

	nparams = tf1.GetNpar()

	ret = Namespace([
		('nparams', nparams),
		('nfreeparams', tf1.GetNumberFreeParameters()),
		('nfitpoints', tf1.GetNumberFitPoints ()),
		('ndf', tf1.GetNDF()),
		('chi2', tf1.GetChisquare()),
		('prob', tf1.GetProb()),
		# ('formula', str(tf1.GetExpFormula())),
		('parnames', [tf1.GetParName(i) for i in range(nparams)]),
		('fitparams', np.frombuffer(tf1.GetParameters(), count=nparams)),
		('fiterrors', np.frombuffer(tf1.GetParErrors(), count=nparams))
		])
		
	for i in range(nparams):

		ret[tf1.GetParName(i)] = tf1.GetParameter(i)
		ret['error_'+tf1.GetParName(i)] = tf1.GetParError(i)

	return ret


def get_rchi2(tf1):

	chi2  = tf1.GetChisquare()
	ndf   = tf1.GetNDF()

	return chi2 / ndf
	# return 'reduced chi2: {} / {}'.format(fmt(chi2), fmt(ndf))


def get_histogram(th1, return_yerr=False):
	""" simple wrapper for unpacking the return of root_numpy.hist2array """

	yval, (xedg, ) = hist2array(th1, include_overflow=False, return_edges=True)
	
	if return_yerr:
		yerr = np.array([ th1.GetBinError(i) for i in range(th1.GetNbinsX()) ])
		return yval, yerr, xedg

	else:
		return yval, xedg


def gen_from(func, parameters, low, up, size=10, Npx=500):
	""" func must be a valid python function for TF1_signature parser,
			increase Npy if ratio up/low too large
			parameters: None or tuple of the parameter values
	"""

	tf1  = TF1(
		func.__name__, TF1_signature(func), low, up,
		len(parameters) if parameters is not None else 0 )
	
	if parameters is None:
		pass

	elif len(parameters)==1:
		tf1.SetParameter(0, parameters[0])
	
	else:
		tf1.SetParameters(*parameters)
	
	tf1.SetNpx(Npx)
	# somehow the default value in TF1 constructor is different (see docs)
	gRandom.SetSeed(0)
	
	return np.array([ tf1.GetRandom() for i in range(size) ], ndmin=1)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def add_fitlegend(
			tf1, loc='upper right', frameon=True, color='silver', alpha=0.2,
			parnames=None, numfmt=None, ax=None ):

	""" parnames (list): support latex-like syntax, e.g ['N_1', '\\mu']
			numfmt : formatter of the fit parameter/error values
			
			return AnchoredText object
	"""
			
	from matplotlib.lines import Line2D
	from matplotlib.offsetbox import AnchoredText # mpl v3.0

	# from matplotlib import MatplotlibDeprecationWarning
	# from warnings import catch_warnings, filterwarnings
	# with catch_warnings():
	# 	filterwarnings('ignore', category=MatplotlibDeprecationWarning)
	# 	from mpl_toolkits.axes_grid.anchored_artists import AnchoredText


	if ax is None: ax = plt.gca()

	fitted = get_fitresults(tf1)

	if parnames is None: parnames = fitted.parnames

	if numfmt is None: numfmt = fmt # default formatter in funk_utils

	fitvalues  = [ fmt(x) for x in fitted.fitparams ]
	fiterrors  = [ fmt(x) for x in fitted.fiterrors ]
	rchi2label = '$\\chi^2$/ndf'
	rchi2val 	 = '{:.3f}'.format(get_rchi2(tf1))


	txt 			 = 'fit parameters' # title

	boxlength  = max(
					len(txt),
					*map(len, map(lambda l: ''.join(l), 
												zip(parnames, fitvalues, fiterrors)) ),
					len(rchi2val) + 4
					) + 5 # for spacing left and right

	boxlength += 1 # to account for the \n at end of each txt line
	if boxlength%2: boxlength += 1 # allow properly centering title


	txt  = '{}{}{}\n'.format(' '*((boxlength-len(txt))//2+1), txt, '')

	for i, (parname, fvalue, ferror) in\
			enumerate(zip(parnames, fitvalues, fiterrors)):
		
		spacing = ' '*(boxlength-len(parname)-len(fvalue)-len(ferror)-1)
		txt += '{}{}{} $\\pm$ {}\n'\
					 .format(parname, spacing, fvalue, ferror)

	# mpl bug: don't put \n at the end of the last string
	txt += '{}{}{}'.format(
					rchi2label, ' '*(boxlength-4-len(rchi2val)), rchi2val )


	anchor = AnchoredText(
						txt, loc=loc, frameon=frameon, 
						prop=dict(family='monospace', linespacing=1.8) )
	anchor.patch.set_boxstyle('round', pad=0., rounding_size=0.1)
	anchor.patch.set_color(color)
	anchor.patch.set_alpha(alpha)
	ax.add_artist(anchor)


	# purely aestetic
	def _get_box_coords(*args):

		canvas  = plt.gcf().canvas
		# corners = anchor.get_window_extent(canvas.get_renderer()).corners()
		# a, b  = corners[1], corners[3]
		# toAxesCoords = ax.transAxes.inverted()
		# a, b 	= toAxesCoords.transform([a, b])

		corners = anchor.get_window_extent(canvas.get_renderer())\
							 			.transformed(ax.transAxes.inverted())\
							 			.corners()
		a, b  = corners[1], corners[3]
		dy = (a[1] - corners[0][1]) / (len(txt.split('\n'))+2)
		xAxes = (a[0], b[0])
		yAxes = (a[1]-dy, b[1]-dy)

		return xAxes, yAxes
	
	xAxes, yAxes = _get_box_coords()
	line = Line2D(xAxes, yAxes, color='k', ls='--', lw=1,
										 transform=ax.transAxes, zorder=100)
	ax.add_line(line)

	def _redraw_line(*args):

		new_xAxes, new_yAxes = _get_box_coords(*args)
		line.set_xdata(new_xAxes) # note that transform is not overridden
		line.set_ydata(new_yAxes)

	plt.gcf().canvas.mpl_connect('resize_event', _redraw_line)

	return anchor


def plot_tf1(tf1, xs=None, yscale=1, ax=None, **kwargs):
	""" kwargs go to plot """

	kwargs.setdefault('color', 'red')
	kwargs.setdefault('lw', 1)

	if ax is None:
		ax = plt.gca()

	if xs is None:
		xs = np.linspace(tf1.GetXmin(), tf1.GetXmax(), tf1.GetNpx())

	elif isinstance(xs, tuple):
		xs = np.linspace(*xs, tf1.GetNpx())

	else: # assume iterator on xs is passed
		pass

	ys = np.array([ tf1.Eval(x) for x in xs ])
	
	ax.plot(xs, yscale * ys, **kwargs)


def plot_th1(th1, endlines=False, fillprops=None, ax=None, **kwargs):
	""" fillprops: dict or True, named args for ax.bar
 			kwargs are common named args to ax,step, ax.hlines, ax.vlines
 	"""
	
	kwargs.setdefault('color', 'midnightblue')
	kwargs.setdefault('lw', 1)
	kwargs['linestyle'] = kwargs.pop('ls', '-') # mpl bug for ax.step

	if ax is None:
		ax = plt.gca()

	hist, bedg = get_histogram(th1)
	bce = bin_centers(bedg)

	ax.step(bce, hist, where='mid', **kwargs)

	if 'label' in kwargs.keys(): # remove label redundancy
		del kwargs['label']
	
	# fix behaviour of ax.step
	kwargs['linestyles'] = kwargs.pop('linestyle')
	ax.hlines(hist[0], bedg[0], bce[0], **kwargs)
	ax.hlines(hist[-1], bce[-1], bedg[-1], **kwargs)
	
	# purely aestetic
	if endlines:
		ax.vlines(bedg[0], 0, hist[0], **kwargs) 
		ax.vlines(bedg[-1], 0, hist[-1], **kwargs)

	if fillprops is not None:
		if fillprops is True:
			fillprops = {'color': kwargs['color'], 'alpha': 0.3}
		else:
			fillprops.setdefault('color', kwargs['color'])
			fillprops.setdefault('alpha', 0.3)
		bwi = bin_widths(bedg)
		ax.bar(bce, hist, bwi, **fillprops)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


class TH1Wrapper(TH1D):
	""" simple wrapper for TH1 constructor, use TH1D by default """

	counter = 0

	def __init__(
			self, data, nbins, xlow, xup, density=False,
			name=None, title=None ):

		if name is None:
			name  = 'hist%d'%TH1Wrapper.counter

		if title is None:
			title = 'hist%d'%TH1Wrapper.counter

		super(TH1Wrapper, self).__init__(name, title, nbins, xlow, xup)
		fill_hist(self, data)

		self.norm = self.Integral('width')

		if density: self.Scale( 1 / self.norm )

		TH1Wrapper.counter += 1


class TH1Fitter:
	""" do not derive from TF1/TH1 base class
			TODO: write a set_config(dict) method to config fitter at once
	"""

	def __init__(self, th1, func, xlow=None, xup=None):
		""" func: fitting model, valid python function for TF1_signature parser
							parnames are infered from func signature
		"""

		if xlow is None:
			xlow = th1.GetBinCenter(1)

		if xup is None:
			xup = th1.GetBinCenter(th1.GetNbinsX())

		func_sig = signature(func)
		parnames = list(func_sig.parameters.keys())[1:] # 0 is the variable

		tf1 = TF1(
				func.__name__,
				TF1_signature(func),
				xlow, xup, len(parnames)
				)

		tf1.SetParNames(*parnames)

		self.th1 			 = th1
		self.tf1 			 = tf1
		self.xlow 		 = xlow
		self.xup 			 = xup
		self.parnames  = parnames
		self.fixedpars = []
		self.fitted 	 = None


	def _pindex(self, p):

		return positive_index(p, len(self.parnames)) if isinstance(p, int)\
					 else self.parnames.index(p)


	def set_parameters(self, *args, **kwargs):

		if len(args):
			self.tf1.SetParameters(*args)

		elif len(kwargs):
			for p in kwargs.items():
				self.tf1.SetParameter(self._pindex(p[0]), p[1])

		else:
			return


	def set_parlimits(self, *args, **kwargs):
		""" args: list of tuples or kwargs: {parname: (low, up)} """

		if len(args):
			for i, p in enumerate(args):
				if p is not None:
					self.tf1.SetParLimits(i, *p)

		elif len(kwargs):
			for p in kwargs.items():
				if p[1] is not None:
					self.tf1.SetParLimits(self._pindex(p[0]), *p[1])

		else:
			pass


	def fix_parameters(self, *args):
		""" args: parnames (str) or integer index (can be negative) """

		for p in args:
			i = self._pindex(p)
			self.tf1.FixParameter(i, self.tf1.GetParameter(i))
			self.fixedpars.append(self.parnames[i])


	def release_parameters(self, *args):

		for p in args:
			i = self._pindex(p)
			self.tf1.ReleaseParameter(i)
	
			with suppress(ValueError):
				# can also be called to reset parlimits even par is not fixed
				self.fixedpars.remove(self.parnames[i])


	def get_parameters(self, *args):

		if not(len(args)): args = self.parnames
		ret  = Namespace()
		pars = get_parameters(self.tf1)

		for p in args:
			i = self._pindex(p)
			setattr(ret, self.parnames[i], pars[i])

		return ret


	def get_parlimits(self, *args):

		if not(len(args)): args = self.parnames
		ret = Namespace()

		for p in args:
			i = self._pindex(p)
			pmin, pmax = Double(0), Double(0)
			self.tf1.GetParLimits(i, pmin, pmax)

			pmin, pmax = float(pmin), float(pmax) # simple safety
			lim = None if (pmin==0)&(pmax==0) else (pmin, pmax)
			setattr(ret, self.parnames[i], lim)

		return ret


	def get_fitresults(self):

		ret = get_fitresults(self.tf1)
		ret.update( fixedpars=self.fixedpars )
		ret.update( parlimits=list(self.get_parlimits().items()) )

		return ret


	def get_rchi2(self):

		return get_rchi2(self.tf1)


	def print_status(self, **kwargs):
		""" kwargs go to tabulate """

		kwargs.setdefault('numalign', 'right')
		kwargs.setdefault('tablefmt', 'grid')

		status = self.get_fitresults()

		out  	 = [
				status.fitparams.tolist(),
				status.fiterrors.tolist(),
				[ lim for (p, lim) in status.parlimits ],
				[ (True if p in self.fixedpars else None) for p in self.parnames ]
				]

		table  = tabulate(
				zip(*out), # transpose
				headers=['', 'fitparams', 'fiterrors', 'parlimits', 'fixed'],
				showindex=self.parnames, **kwargs
				)

		print(table)


	def fit(self, option=''):
		""" option: str, see root docs """

		self.th1.Fit(self.tf1, option+'0')
		self.fitted = self.get_fitresults()


	def plot(self, hprops={}, fprops={}, legend=False):
		""" hprops, fprops: dict or False """

		if hprops is not False:
			plot_th1(self.th1, **hprops)

		if fprops is not False:
			xs = fprops.pop('xs', 'range')

			if xs=='range':
				xs = None # fall back to tf1 range
			elif xs=='full':
				xs = ( self.th1.GetBinCenter(1),
							 self.th1.GetBinCenter(self.th1.GetNbinsX()) )
			else:
				pass

			plot_tf1(self.tf1, xs, **fprops)
		
		if legend:
			add_fitlegend(self.tf1)