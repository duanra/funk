#!usr/bin/env python3
import numpy as np
import scipy.stats as st
from scipy.interpolate import interp1d
from sys import exit

from funk_utils import isnumber

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def dirac(x, a=0):
	x = np.asarray(x)
	return (x == a).astype(int)


def pgf_poisson(s, mu):
	""" probability generating function of poisson distribution"""
	return np.exp(mu*(s-1.))


def cdfinv_expon(x, lam):
	""" inverse of cdf """
	return - np.log(1-x) / lam 


def polya(x, mu, a):
	""" continious version of the negative binomial
				mean: mu
				sig : mu/sqrt(a)
	"""
	from scipy.special import gamma
	
	return np.power(a*x/mu, a) * np.exp(-a*x/mu) / (x*gamma(a))


def pareto2(x, n, theta):
	""" type II pareto distn
			n and theta correspond to the shape and scale params of the gamma 
	"""
	A = np.log(n) + np.log(theta) - (n+1)*np.log(1 + theta*x)

	return np.exp(A)
	# return n*theta * np.power(1 + theta*x, -n-1)


def cdf_pareto2(x, n, theta):
	return 1 - np.power(1 + theta*x, -n)


def cdfinv_pareto2(x, n, theta):
	return 1/theta * (np.power(1-x, -1/n) -1)


def poi_branch(x, lam, k, scalex=1.):
	""" Galtson-Watson branching process with poissonian multiplication
			lam : paramater of the poisson distribution
			k 	: number of stages
			TO CHECK: lam should be the total gain
	"""
	from scipy.special import iv 

	try:
		if lam<=1 and scalex==1:
			raise ValueError(
							'ValueError',
							'funk_stats.poi_branch(): lam=arg must be > 1'
						)
		if k<=1:
			raise ValueError(
							'ValueError', 
							'funk_stats.poi_branch(): k=arg must be > 1'
						)

	except ValueError as va:
		print('[{}] {}'.format(*va.args))

	else:
		# b = 0.5 * (lam-1) / (np.power(lam, 1/k) - 1)
		b 	= 0.5 * (lam - np.power(scalex, -1)) / (np.power(scalex*lam, 1/k) - 1)

		def _fn(x):
			if x<0:
				ret = 0
			elif x==0:
				ret = np.exp(-lam/b)
			else:
				ret = np.exp(-lam/b) * (
								dirac(x) +
								np.exp(-x/b) * np.sqrt(lam/x)/b * iv(1, 2*np.sqrt(lam*x)/b)
							)
			return ret

		if isnumber(x):
			return _fn(x)

		else:
			return np.array([_fn(xval) for xval in x])


# import matplotlib.pyplot as plt
# k    = 11
# G    = 1e6
# mu 	 = np.power(G, 1/k)
# Gsig = np.sqrt( G*(G-1)/(mu-1) )

# x = np.linspace(1, 3*G, num=1000)
# plt.plot(x, poi_branch(x, G, k), label='poi_branch')
# plt.plot(x, polya(x, G, (G/Gsig)**2), label='polya')
# # plt.plot(x, st.norm.pdf(x, loc=G, scale=Gsig), label='gauss')
# plt.ticklabel_format(axis='both', style='sci',
# 										 scilimits=(0,0), useMathText=True)
# plt.grid(ls=':')
# plt.legend()
# plt.show()
# plt.close()




def negbinom(*args, **kwargs):
	""" reparametrization of nbinom in scipy
				mu  = n (1-p) / p
				var = n (1-p) / p**2 = mu + mu**2 / n
			with n : number of success (stopping parameter)
					 p : probablity of a single success
		
		 return frozen pmf
	"""
	def _convert_to_np(mu, sig):
		""" n: number of success (stopping parameter)
				p: probability of single success
				case of mu>var (i.e p>1) are handled in stats class
		"""
		var = sig**2
		# if mu==var:
		if np.isclose(mu, var, rtol=1e-10) or mu==0:
			return 1, 1 # p=1, 1-p=0 --> prob=1 if x=0, else prob=0
		else:
			n = mu**2 / (var - mu)
			p = mu / var # n / (n + mu)
			return n, p

	if args:
		if len(args)==2:
			mu, sig = args
			loc = 0
		elif len(args)==3:
			mu, sig, loc = args
		else:
			print('[ArgError] funk_stats.negbinom() takes'\
												'at most 3 positional arguments')
			sys.exit(0)

	else:
		mu  = kwargs['mu']
		sig = kwargs['sig']
		loc = kwargs.get('loc', 0)

	return st.nbinom(*_convert_to_np(mu, sig), loc=loc)


# def _convert_to_np(mu, sig):
# 	var = sig**2
# 	return mu**2/(var-mu), mu/var
# print(_convert_to_np(400, 60))

# def _convert_to_musig(n, p):
# 	return n*(1-p)/p, np.sqrt(n*(1-p)/p**2) 
# print(_convert_to_musig(38.8, 0.088))
# p = negbinom(*_convert_to_musig(0.4, 0.4))
# print(p.pmf(np.arange(5)))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class TwoPoisson:
	""" calculate the probability p(n) of observing n events,
			this is a special case of compound poisson process:
			sum of n i.d.d rdv ~ poi(mu2) such that n ~ poi(mu1)
					y = x1 + ... + xn
					mu1 <--> g_N(s)  and  mu2 <--> g_X(s)
			the iteration stops when sum(p)~1

			notice that pmf are frozen within class instance for
			unchanged parameters and can be updated via pmf method
			(desirable for manipulation with ROOT)
	"""

	def __init__(self):
		self._pmf = None
		self._mu1 = None
		self._mu2 = None

	def __call__(self, mu1, mu2, norm_tol=1e-3):
		from cymodule.compound_poisson import two_poisson	
		print('TwoPoisson: called with mu1={} and mu2={}'.format(mu1, mu2))

		if norm_tol>1e-2:
			print('[Exception] TwoPoisson: called with norm_tol={} '\
						'(value is too big)'.format(norm_tol))
			exit(0)

		p = two_poisson(mu1, mu2, norm_tol) # freeze
		self._pmf = interp1d(np.arange(len(p)), p)
		self._mu1 = mu1
		self._mu2 = mu2  
		self._xlow = 0 
		self._xup  = len(p)-1

		return self # need to repoint to the class instance

	# def __str__(self):
	# 	return str(self._pmf)

	def pmf(self, x, *args):
		""" args are mu1, mu2, optionally norm_tol """
		def _get_prob(x):
			if not(x-int(x)) and self._xlow <= x <= self._xup:
				return self._pmf(x)
			else:
				return 0

		if self._pmf is None and not(len(args)):
			print('[Exception] TwoPoisson has no frozen pmf')
			exit(0)
		elif len(args) and (self._mu1, self._mu2) != args[:2]:
			self.__call__(*args)
		else:
			pass

		if hasattr(x, '__getitem__'):
			return np.array([_get_prob(xval) for xval in x])
		else:
			return _get_prob(x)


two_poisson = TwoPoisson()


# print(two_poisson.pmf(10, 10, 5))
# print(two_poisson.pmf(3, 10, 5))
# p = two_poisson(10,10)
# print(p.pmf(7))
# print(p.pmf([-2, 0.1, 1000]))
# print(p.pmf(3, 10,20))
# print(p.pmf(5))

# TODO: write a decorator to reeze pdf in a more generic way


# def two_poisson(x, mu1, mu2, norm_tol=1e-3):
# 	""" return the probability p(n) of observing n events,
# 			this is a special case of compound poisson process:
# 			sum of n i.d.d rdv ~ poi(mu2) such that n ~ poi(mu1)
# 					y = x1 + ... + xn
# 					mu1 <--> g_N(s)  and  mu2 <--> g_X(s)
# 			the iteration stops when sum(p)~1
# 	"""
# 	p = two_poisson(mu1, mu2, norm_tol)
# 	xmin, xmax = 0, len(p)-1

# 	def _check(xval):
# 		return not(bool(xval - int(xval))) and xmin <= xval <= xmax

# 	def _get_prob(xval): # p is normalised
# 		return p[int(xval)] if _check(xval) else 0
	
# 	if hasattr(x, '__getitem__'): # note: dict would also return true
# 		return np.array([_get_prob(xval) for xval in x])
# 	else:
# 		return _get_prob(x)

	##### this is now compiled in cython
	# pX_frozen = st.poisson(mu2)
	# pX = array('d', [pX_frozen.pmf(0)])
	# p2 = array('d', [pgf_poisson(pgf_poisson(0, mu2), mu1)])
	# n  = 1

	# while(True):
	# 	s = 0
	# 	pX.append(pX_frozen.pmf(n))
	# 	for i in range(0, n):
	# 		s += (n-i)*p2[i]* pX[n-i]
	# 	p2.append(mu1*s/n)

	# 	if np.isclose(np.sum(p2), 1., atol=norm_tol): 
	# 		break
	# 	n += 1

	# x = np.arange(len(p2))
	# return x, interp1d(x, p2, bounds_error=False, fill_value=0.)


# # analytical version
# def two_poisson_sym(n, mu1, mu2):
# 	import sympy as sym
# 	from scipy.special import factorial

# 	x, m1, m2 = sym.symbols('x, m1, m2')
# 	def g(x, m1, m2):
# 		return sym.exp(m1*( sym.exp(m2*(x-1)) -1 ))

# 	return sym.diff(g(x, m1, m2),x,n)\
# 				 		.subs(zip([x,m1,m2], [0,mu1,mu2]))\
# 				 		.evalf() / factorial(n)
# print(two_poisson_sym(10,2,2))




#!!!!!!!!!!!!!!!!!!!! TODO: FIND WHAT'S WRONG !!!!!!!!!!!!!!!!!!!!

# import numpy as np
# from scipy.stats import rv_discrete
# from scipy import special
# from scipy.special import entr, logsumexp, betaln, gammaln as gamln
# from numpy import floor, ceil, log, exp, sqrt, log1p, expm1, tanh, cosh, sinh

# class NegBinom_gen(rv_discrete):
# 	""" reparametrization of the nbinom_gen distribution in scipy.stats """

# 	@staticmethod
# 	def _convert_to_np(mu, var):
# 		""" n: number of success (stopping parameter)
# 				p: probability of single success
# 		"""
# 		n = mu**2 / (var - mu)
# 		p = mu / var # n / (n + mu)
# 		return n, p

# 	def __call__(self, mu, var, loc=0):
# 		return self.freeze(*self._convert_to_np(mu, var), loc=loc)
	
# 	def _rvs(self, mu, var):
# 		n, p = self._convert_to_np(mu, var)
# 		return self._random_state.negative_binomial(n, p, self._size)

# 	def _argcheck(self, mu, var):
# 		n, p = self._convert_to_np(mu, var)
# 		return (n > 0) & (p >= 0) & (p <= 1)

# 	def _pmf(self, x, mu, var):
# 		# nbinom.pmf(k) = choose(k+n-1, n-1) * p**n * (1-p)**k
# 		n, p = self._convert_to_np(mu, var)
# 		return exp(self._logpmf(x, n, p))

# 	def _logpmf(self, x, mu, var):
# 		n, p = self._convert_to_np(mu, var)
# 		coeff = gamln(n+x) - gamln(x+1) - gamln(n)
# 		return coeff + n*log(p) + special.xlog1py(x, -p)

# 	def _cdf(self, x, mu, var):
# 		n, p = self._convert_to_np(mu, var)
# 		k = floor(x)
# 		return special.betainc(n, k+1, p)

# 	def _sf_skip(self, x, mu, var):
# 		# skip because special.nbdtrc doesn't work for 0<n<1
# 		n, p = self._convert_to_np(mu, var)
# 		k = floor(x)
# 		return special.nbdtrc(k, n, p)

# 	def _ppf(self, q, mu, var):
# 		n, p = self._convert_to_np(mu, var)
# 		vals = ceil(special.nbdtrik(q, n, p))
# 		vals1 = (vals-1).clip(0.0, np.inf)
# 		temp = self._cdf(vals1, n, p)
# 		return np.where(temp >= q, vals1, vals)

# 	def _stats(self, mu, var):
# 		n, p = self._convert_to_np(mu, var)
# 		Q = 1.0 / p
# 		P = Q - 1.0
# 		mu = n*P
# 		var = n*P*Q
# 		g1 = (Q+P)/sqrt(n*P*Q)
# 		g2 = (1.0 + 6*P*Q) / (n*P*Q)
# 		return mu, var, g1, g2

# negbinom = NegBinom_gen()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# import scipy.stats as st

# p = negbinom(10, 15)
# print(p.pmf(5))
# print(p.rvs(10))
# # print(negbinom.pmf(50,10,15))

# def _convert_to_np(mu, var):
# 	""" n: number of success (stopping parameter)
# 			p: probability of single success
# 	"""
# 	n = mu**2 / (var - mu)
# 	p = mu / var # n / (n + mu)
# 	return n, p

# print(_convert_to_np(10,15))

# p = st.nbinom(*_convert_to_np(10,15))
# print(p.pmf(5))

# # print(st.nbinom(p=.5, n=3).pmf(5))