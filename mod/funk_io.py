#!usr/bin/env python3
import os
import sys
import numpy as np
import subprocess as sub
import warnings
import funk_consts as fcn
# from bz2 import BZ2File
from datetime import datetime, timedelta
from natsort import natsorted
from tempfile import TemporaryFile

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def find_all_files(path, exclude=None, pattern='.root',
									 combi=False, recursive=False,
									 on_error=False):
	""" exclude: None or a string or a list of strings
			pattern: '' (for all) or a string or a list of strings
			combi: apply for all(pattern) as one, otherwise
			both exclude and pattern are not exclusive, i.e any(exclude) and any(pattern)
			on_error: exit if error/exception/warning occurs else continue 
	"""
	try:
		if not os.path.exists(path):
			raise IOError('IOError', 'funk_io.find_all_files(): the directory '\
																+ path + ' does not exist')
	
	except IOError as io:
		print('[{}] {}'.format(*io.args))
		if on_error: sys.exit(0)
	
	else:
		res = []
		if isinstance(exclude, str): exclude = [exclude]
		if isinstance(pattern, str): pattern = [pattern]
		
		for root, dirs, files in os.walk(path):
			for file in files:
				if not(exclude is not None and any([x in file for x in exclude])):
					take = all([x in file for x in pattern]) if combi\
								 else any([x in file for x in pattern])
					if take:
						res.append(os.path.join(root, file))
			if not recursive: break
		
		if len(res)==0:
			print('[Warning] funk_io.find_all_files(): found no matches\n'\
						'\tpath = {}\n'\
						'\tpattern = {}\n'\
						'\texclude = {}\n'.format(path, pattern, exclude))
			if on_error: sys.exit(0)
		
		return sorted(res)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def parse_idxs(idxs, select, quiet=False):
	""" parser for file index/keys (e.g npy's in npz) """	
	ix = natsorted(idxs)
	if select=='all':
		if not quiet: print('file(s): {} to {}'.format(ix[0], ix[-1]))

	elif isinstance(select, str):
		ix = [x for x in ix if select in x] # look for pattern
		if not quiet: print('file(s): {}'.format(ix))

	elif isinstance(select, list) and\
			 all([isinstance(x, str) for x in select]):
		ix = [x for w in select for x in ix if w in x]
		if not quiet: print('file(s): {}'.format(ix))

	else: # integer-based numbering 
		def _posix(select): # positive index (starting from 0)
			n = len(ix)			
			if isinstance(select, str):
				if select=='begin':
					ret = 0
				elif select=='end':
					ret = n-1
				else: 
					pass
			
			elif isinstance(select, int):
				if select<0:
					ret = range(n-1,0,-1)[np.abs(select)-1]
				elif select>=n:
					if not quiet: print('[Info] file idx={}>=len(idxs)'.format(select))
					ret = n-1
				else:
					ret = select
			
			else:
				print('[ArgError] funk_io.parse_idxs(): check select=arg')
				sys.exit(0)

			return ret

		if isinstance(select, tuple) and len(select)==2:
			ix = [ix[i] for i in range(_posix(select[0]), _posix(select[1])+1)]
			if not quiet: print('file(s): {} to {}'.format(ix[0], ix[-1]))
		
		elif isinstance(select, list) and\
				 all([isinstance(x, int) for x in select]):
			ix = [ix[_posix(i)] for i in select]
			if not quiet: print('file(s): {}'.format(ix))
		
		else: # single integer
			ix = [ix[_posix(select)]]
			if not quiet: print('file(s): {}'.format(ix[0]))

	return iter(ix)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def npz_to_ascii(file, buff=False, outf='auto',
								 rows=(0,5), which=1, label=''):
	""" used for dumping pmt_run data
			file: npz file (pmt run)
			rows: 0 or (from,to) or (from, 'end')
			buff: write the output into the buffer
			outf: write the output into a file, 'auto' or 'path/to/outfile'
			which: 0 or n-th npy file
	"""
	try:
		if not os.path.isfile(file):
			raise IOError('IOError', 'funk_io.npz_to_ascii(): the file '\
																+ file + ' is missing')
		fname = '{}{}.txt'.format(file[:-4], '_'+str(which) if which else '')\
							if outf=='auto' else outf
		if not(buff) and os.path.isfile(fname):
			raise Exception('Exception', 'funk_io.npz_to_ascii(): the file '\
																		+ fname + ' already exists')
	
	except IOError as io:
		# print(io.errno, io.strerror)
		print('[{}] {}'.format(*io.args))
		# sys.exit(0)
	
	except Exception as ex:
		print('[{}] {}'.format(*ex.args))
		# sys.exit(0)
	
	else:
		npz  = np.load(file)	
		ix 	 = parse_idxs(npz.files, which)
		data = np.concatenate([npz[i] for i in ix], axis=0)

		if isinstance(rows, (tuple,	list)):
			data = data[slice(*rows)]

		if label=='pmt':
			label = ' '.join([fcn.dtype_pmt[i][0] for i in range(len(fcn.dtype_pmt))])
		
		if buff:
			if label: print(label)
			for row in data:
				line = ' '.join([str(fcn.dtype_pmt[i][1](row[i]))
													for i in range(len(fcn.dtype_pmt))])
				print(line)

		else:
			print('dumping', fname)
			with open(fname, 'w') as outf:
				if label: outf.write(label+'\n')
				for row in data:
					line = ' '.join([str(fcn.dtype_pmt[i][1](row[i]))
														for i in range(len(fcn.dtype_pmt))])
					outf.write(line+'\n')

# file = '/media/naud/DATA/Funk/data/pmt_run/3.5-1.5-7/v06_(-4,3.5,1.5,7).npz'
# file = '/home/tmp/data/Funk/data/pmt_run/3.5-1.5-7/v17_(-4,3.5,1.5,7).npz'
# npz_to_ascii(file, buff=True, outf='auto', rows=(0,5), which=0, label='pmt')

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def ascii_to_npz(path, outf='auto', extension='.log.bz2',
								 combi=False, recursive=False, 
								 skip_header=0, delimiter=None,
								 usecols=None, dtype=None):
	""" used for dumping muon/sensor/weather data
			cols: None, int or list of int
			skip_header: int
			dtype: None or explicit dtypes
	"""
	try:
		fnames = find_all_files(path, exclude=None, pattern=extension, combi=combi,
														recursive=recursive, on_error=True)
		if outf=='auto': outf = path + 'out.npz'
		if os.path.isfile(outf):
			raise Exception('Exception', 'funk_io.ascii_to_npz(): the file '\
																		+ outf + ' already exists')
	
	except Exception as ex:
		print('[{}] {}'.format(*ex.args))
		sys.exit(0)

	else:
		print('compressing', outf)
		dic = {}
		for f in fnames:
			try:
				dic[os.path.basename(f)[:-len(extension)]]\
						= np.genfromtxt(f, usecols=usecols, delimiter=delimiter,
														skip_header=skip_header, invalid_raise=True,
														dtype=dtype, encoding=None) # need encoding (py3)

			except Exception as ex:
				if 'Some errors were detected' in str(ex):
					print('[Exception] np.genfromtxt(): '
								'some errors were detected, '
								'skipping', f)
				else:
					raise

		# py3 / numpy.savez (open as 'wb', not 'w' only)
		# see https://stackoverflow.com/questions/5512811/\
		# 		builtins-typeerror-must-be-str-not-bytes
		f = open(outf, 'wb')
		np.savez_compressed(f, **dic)
		f.close()

# ascii_to_npz(path=fcn.prefix_muon, outf='auto',
# 						 extension='.log.bz2', skip_header=0,
# 						 usecols=np.arange(4), dtype=fcn.dtype_muon)

# ascii_to_npz(path=fcn.prefix_weather, outf='auto',
# 						 extension='.csv', skip_header=1, delimiter=',',
# 						 usecols=np.arange(6), dtype=fcn.dtype_weather)

# ascii_to_npz(path=fcn.prefix_sensors, outf='auto',
# 						 extension='.log.bz2', skip_header=0,
# 						 usecols=np.arange(1,6), dtype=fcn.dtype_sensors)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def load_npz(file, cols='all', npy='all', quiet=False):
	""" used for pmt_run data
			file: npz file
			cols: 'all' or list of the field names
			npy: 'all' or n-th npy file or list', can use 'begin'/'end' in list
			return a concantenated structured array with dtype = dtype_pmt
	
			keep lazy loading till return/call
	"""
	try:
		if not os.path.isfile(file):
			raise IOError('IOError', 'funk_io.load_npz(): the file '\
																+ file + ' is missing.')

	except IOError as io:
		print('[{}] {}'.format(*io.args))
		# sys.exit(0)

	else:
		npz = np.load(file)
		ix 	= parse_idxs(npz.files, npy, quiet)
		""" 
				for creating a pandas dataframe from an array:
				very important that 'file' is a structured array if dtype enable
				(with explicit dtype given in dump_wrapper.genfromtxt())
				otherwise, waste of time and memory to reformat with
				rec.fromarrays(array.T, dtype) or whatever
				structured arrays are faster compared to record arrays
				but only accept list of tuples as input
		"""

		if cols=='all':
			data = np.concatenate([npz[i] for i in ix], axis=0)
		else:
			data = np.concatenate([npz[i][cols] for i in ix], axis=0)
		
		return data

# import pandas as pd
# # file = '/media/naud/DATA/Funk/data/pmt_run/3.5-1.5-7/v06_(-4,3.5,1.5,7).npz'
# file = '/home/tmp/data/Funk/data/pmt_run/3.5-1.5-7/v17_(-4,3.5,1.5,7).npz'
# print(
# 	pd.DataFrame(
# 		load_npz(file, cols=['time'], npy=195)
# 		)
# 	)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def load_data(path, npz_file=None, dtype=None, usecols=None, 
							stack=('01-07-2017', '31-07-2017'),
							on_error=False):
	"""	used for muon or temperature data
			path: prefix_muon or prefix_sensors
			stack: 'all', str date (dd-mm-yy) or tuple of str dates (from, to)
			dtype, usecols: args of np.genfromtxt
			load npz_file/txt_files and return a structured array with explicit dtypes
	"""
	if not (isinstance(stack, (str, tuple, list))):
		print('[ArgError] funk_io.load_muon_data(): check arg=stack')
		sys.exit(0)
	
	try:
		def parse_date(date): # convert str date to datetime object
			def try_fmt(date):
				if date=='begin' or date=='end':
					return date
				for fmt in ("%d-%m-%Y", "%d/%m/%Y", "%d.%m.%Y", "%d %m %Y"):
					try:
						return datetime.strptime(date, fmt)
					except ValueError:
						pass
				raise ValueError('ValueError', 'parse_date(): invalid date format')

			if isinstance(date, str):
				return try_fmt(date)
			elif isinstance(date, (tuple,list)):
				return [try_fmt(x) for x in date]
			else:
				print('[ArgError] parse_date(): argument must be string or tuple/list')
				sys.exit(0)
		
		if stack!='all':
			stack = parse_date(stack)
	
	except ValueError as ve:
		print('[{}] {}'.format(*ve.args))
		sys.exit(0)
	
	else:
		def select_infiles(infiles, s=(0,)):
			# s: slice of the str_filename which contains the date 
			ret, i = [], 0
			if isinstance(stack, (tuple, list)):
				while i<len(infiles):
					if ((True if stack[0]=='begin'\
											else parse_date(infiles[i][slice(*s)])>=stack[0]) and
							(True if stack[1]=='end'\
											else parse_date(infiles[i][slice(*s)])<=stack[1])):
						ret.append(infiles[i])
						i+=1
					else:
						i+=1
			else:
				while i<len(infiles):
					if parse_date(infiles[i][slice(*s)])==stack:
						ret.append(infiles[i])
						break
					else:
						i+=1

			if len(ret)==0:
				print(
					'[Warning] funk_io.load_data(): '\
					'no {} data found for the selected date(s)'\
					.format('muon' if 'muon' in npz_file else 'temperature'
					))
				if on_error:
					sys.exit(0)
				else:
					return infiles
			return ret

		def log(infiles, s=(0,)): #s: slice of the str which contains the date 
			return '{} to {}'\
							.format(
									parse_date(infiles[0][slice(*s)]).strftime("%Y-%m-%d"),
								 	parse_date(infiles[-1][slice(*s)]).strftime("%Y-%m-%d")
							)
		
		if npz_file:
			npz = find_all_files(path, exclude=None, pattern=npz_file,
													 recursive=False, on_error=True)
			infiles = np.load(*npz)
			# sorting by date, somehow need a double sorting
			keys = sorted(infiles.keys(), key=lambda x: parse_date(x[-10:]))
			if stack!='all': keys = select_infiles(keys, s=(-10,None))
			
			print('loading ' + npz[0] + ':', log(keys, s=(-10,None)))
			return np.concatenate([infiles[key] for key in keys], axis=0)			
		
		else:
			infiles = sorted(find_all_files(path, exclude=None, pattern='.log.bz2',
																			recursive=False, on_error=True),
											 key=lambda x: parse_date(x[-18:-8])) #sorting by date		
			if stack!='all': infiles = select_infiles(infiles, s=(-18,-8))
			
			print('loading', path, log(infiles, s=(-18,-8)))
			
			out_arr = np.empty((0,), dtype=dtype)
			for infile in infiles:
				try: 
					temp_arr = np.genfromtxt(infile, usecols=usecols,
																	 dtype=dtype, invalid_raise=True)
		
				except Exception as ex:
					if 'Some errors were detected' in str(ex):
						print('[Exception] np.genfromtxt(): '\
									'some errors were detected, '\
									'skipping', f)
					else:
						raise
		
				else:
					out_arr = np.concatenate((out_arr, temp_arr))

			return out_arr

# import pandas as pd
# print(
# 	pd.DataFrame(
# 		load_data(fcn.prefix_muon, npz_file='muons.npz',
# 							dtype=fcn.dtype_muon, usecols=np.arange(4), 
# 							stack=('01-07-2017', '31-07-2017'), on_error=False)
# 		)
# 	)
# print(
# 		pd.DataFrame(
# 			load_data(fcn.prefix_sensors, npz_file='sensors.npz',
# 								dtype=fcn.dtype_sensors, usecols=np.arange(1,6),
# 								stack='all', on_error=False)
# 		)
# 	)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def load_weather(path, npz_file=None):
	# , stack=('01-07-2017', '31-07-2017'), on_error=False):
	
	if npz_file:
		npz = sorted(find_all_files(path, exclude=None, pattern=npz_file,
																recursive=False, on_error=True))[0]
		infiles = np.load(npz)
		def _f(d):
			return datetime.strptime(d, '%Y-%m-%d')\
										 .strftime('%d-%m-%Y')

		print('loading {}: {} to {}'\
					.format(npz,
									_f(infiles['2017']['cettime'][0][0:10]),
									_f(infiles['2018']['cettime'][-1][0:10])))

		return np.concatenate([infiles[k] for k in sorted(infiles.keys())])
				
	else:
		outarr = np.empty((0,), dtype=dtype_weather)
		infiles = sorted(find_all_files(path, exclude=None, pattern='.csv',
																		recursive=False, on_error=True))

		for infile in infiles:
			print('loading', infile)
			try:
				tmparr = np.genfromtxt(infile, delimiter=',', skip_header=1,
															 usecols=np.arange(6), dtype=dtype_weather,
															 invalid_raise=True)
			
			except Exception as ex:
				if 'Some errors were detected' in str(ex):
					print('[Exception] np.genfromtxt(): '\
								'some errors were detected, '\
								'skipping', f)
				else:
					outarr = np.concatenate((outarr, tmparr))

		return outarr

# import pandas as pd
# print(
# 	pd.DataFrame(
# 		load_weather(fcn.prefix_weather, stack='all', npz_file='weather.npz')
# 		)
# 	)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
