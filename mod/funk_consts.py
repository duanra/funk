#!usr/bin/env python3
import os
import sys
import numpy as np
from funk_utils import Namespace

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

if 'cr' in os.environ['HOME']:
	prefix = "/cr/users/arnaud/Repos/git/funk/data/"	# office
	# prefix = "/home/tmp/data/Funk/data/"	# office
else:
	prefix = "/media/naud/DATA/Funk/data/"	# home

pulse_par 			= "(-4,-8,1.5,7)"
# pulse_par 			= "(-4,3.5,1.5,7)"
# pulse_par 			= "(-4,10,5.5,100)"

prefix_pmt 			= prefix + 'pmt_run/'\
												 + '-'.join(pulse_par[1:-1].split(',')[1:]) + '/'
prefix_flasher 	= prefix + 'flasher_run/'\
												 + '-'.join(pulse_par[1:-1].split(',')[1:]) + '/'
prefix_muon 		= prefix + 'muon_monitor/'
prefix_sensors 	= prefix + 'temperatures/'
prefix_weather 	= prefix + 'weather_data/'
prefix_configs 	= prefix + 'configs_data/'


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


cuts = Namespace([
	('dtlim', (59.9, 60.1)),
	('qlim', (7.6, 8.4)),
	('Slim', (2.400, 3.266)), # (2.305, 3.317)),
	('tsiglim', (2.066, 6.061)), # (1.769, 6.163)),
	('riselim', (0,)),
	('tplim', (280., 300.)) # for flasher only
])

# dt_cut = (59.99, 60.01)
# q_cut = (7.8, 8.4) # spe
# q_cut = (8.4,15) # high-q


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


A4p = (.95*8.27, .95*11.69) # in inches
A4l	= (.95*11.69, .95*8.27)


rcPaper = {
	# tex document \columnwidth = 246pt and \textwidth = 510pt
	# save with plt.tight_layout(pad=0)
	'figure.figsize' 			: ( 246/72, 0.6*246/72 ),
	'savefig.format'			: 'pdf',
	'text.latex.preamble'	: [
			# temporary fix to https://github.com/matplotlib/matplotlib/issues/9118
			# r'\PassOptionsToPackage{full}{textcomp}',
			# r'\usepackage{newtxtext,newtxmath}',
			r'\usepackage{newpxtext,newpxmath}',
			r'\usepackage{upgreek}',
			r'\usepackage{amssymb}'
		],
	'text.usetex'					: True,
	'font.family'					: 'serif',
	'font.size'						:	9, #10
	'axes.labelsize'			: 9, #10
	'legend.fontsize'			: 9, #10
	'xtick.labelsize'			: 9,
	'ytick.labelsize'			: 9,
	'axes.grid'						: True,
	'grid.linestyle'			: ':',
	'grid.linewidth'			: 0.4,
}


rcPoS = {
	# tex document \textwidth = 430.2 pt, 
	'figure.figsize' 			: ( 0.48*430.2/72, 0.65*0.48*430.2/72 ),
	'savefig.format'			: 'pdf',
	'text.latex.preamble'	: [
			r'\usepackage{newpxtext,newpxmath}',
			r'\usepackage{upgreek}',
		],
	'text.usetex'					: True,
	'font.family'					: 'serif',
	'font.size'						:	10.95,
	'axes.labelsize'			: 10.95,
	'legend.fontsize'			: 10.95,
	'xtick.labelsize'			: 10,
	'ytick.labelsize'			: 10,	
}


rcPoster = {
	'savefig.format'			: 'pdf',
	'font.size'						: 30,
	'legend.fontsize'			: 'smaller',
	'text.latex.preamble'	: [
			r'\usepackage{times}',
			r'\usepackage{newpxmath}',
		],
	'text.usetex'					: True,
}


rcThesis = {
	# tex document \textwidth = 360pt, \linewdith = 340pt (for figure)
	'figure.figsize'			: (0.78*360/72, 0.64*360/72),
	'savefig.format'			: 'pdf',
	'text.latex.preamble'	: [
			r'\usepackage{newpxtext,newpxmath}',
			r'\usepackage{upgreek}',
			r'\usepackage{amssymb}',
			# r'\usepackage{color}'
		],
	'text.usetex'					: True,
	'font.family'					: 'serif',
	'font.size'						:	10,
	'axes.labelsize'			: 10,
	'legend.fontsize'			: 10,
	#
	'xtick.labelsize'			: 10,
	'xtick.direction'			: 'in',
	'xtick.top'						: True,
	#
	'ytick.labelsize'			: 10,
	'ytick.direction'			: 'in',
	'ytick.right'					: True,
}


fontsize_beamer = 12
rcDefense = {
	# tex document \textwidth = 360pt, \linewdith = 340pt (for figure)
	# 'figure.figsize'			: (0.78*360/72, 0.64*360/72),
	'savefig.format'			: 'png',#'eps',
	'savefig.dpi'					: 300,
	'text.latex.preamble'	: [
			r'\usepackage{newpxtext,newpxmath}',
			r'\usepackage{upgreek}',
			r'\usepackage{amssymb}',
			# r'\usepackage{color}'
		],
	'text.usetex'					: True,
	'font.family'					: 'serif',
	'font.size'						:	fontsize_beamer,
	'axes.labelsize'			: fontsize_beamer,
	'legend.fontsize'			: fontsize_beamer,
	#
	'xtick.labelsize'			: fontsize_beamer,
	'xtick.direction'			: 'in',
	'xtick.top'						: True,
	#
	'ytick.labelsize'			: fontsize_beamer,
	'ytick.direction'			: 'in',
	'ytick.right'					: True,
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


positions = {
	'in_open'			: (1,1),
	'in_closed'		: (1,0),
	'out_open'		: (0,1),
	'out_closed'	: (0,0)
}

diffs = {
	'0001'	: 'out_closed - out_open',
	'0010'	: 'out_closed - in_closed',
	'0011'	: 'out_closed - in_open',
	'0100'	: 'out_open - out_closed',
	'0110'	: 'out_open - in_closed',
	'0111'	: 'out_open - in_open',
	'1000'	: 'in_closed - out_closed',
	'1001'	: 'in_closed - out_open',
	'1011'	: 'in_closed - in_open',
	'1100'	: 'in_open - out_closed',
	'1101'	: 'in_open - out_open',
	'1110'	: 'in_open - in_closed'
}

color = {
	'in_open'		: '#1f77b4',
	'out_open'	: 'saddlebrown',
	'in_closed'	: 'black',
	'out_closed': 'gray',
	'1101'			: 'midnightblue',
	'1000'			: 'firebrick', 
	'1110'			: 'black',
	'0100'			: 'gray',
	'T1'				: 'green',
	'T2'				: 'olive',
	'p'					: 'blue',
	'p2'				: 'blue',
	'T200m'			: 'darkslategray',
	'muons'			: 'palevioletred',#'#d62728',
	'vcoinc'		: 'palevioletred',
	'vmuons'		: 'palevioletred',#use this in the future
	'hmuons'		: 'green', # to be changed
	'default'		: '#3d3d3d',
	}

old_runs = [
	'v06', 'v09', 'v10',
	'v11', 'v12', 'v16',
	'v17', 'v18', 'v19',
	'v20', 'v21'
	]

new_runs = [
	'v22', 'v23',
	'v24', 'v25'
	]

ctr_runs = [
	'v26', 'v27', 'v28',
	'v29', 'v30', 'v31'
	]

# 'v08' : wrong trigger threshold
# 'v11' : all closed (shutter glued to the lid)

# 'v31'	: closed with a metal lid
# 'v32'	: normal run with new shutter
# 'v33'	: test
# 'v34'	: open-open measurement
# 'v35'	: normal run
# 'v36'	: closed with metal lid

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

dtype_pmt = [
	('time', np.uint), ('dt', np.float), ('in', np.uint8), ('open', np.uint8),
	('event', np.uint16), ('capture', np.uint), ('pulse', np.uint16),
	('T1', np.float), ('T2', np.float), ('p', np.float),
	('S', np.float), ('h', np.float), ('q', np.float),
	('Qneg', np.float), ('aop', np.float), ('depth', np.float),
	('rise', np.float), ('tp', np.float), ('tvar', np.float), ('tlen', np.float),
	('bmode', np.float), ('bmean', np.float), ('bstd', np.float)
	]

dtype_muon = [
	('timestamp', np.float),
	('top', np.int),
	('vcoinc', np.int),
	('hcoinc', np.int)
	]

# ignore the first column in the temperatures data
dtype_sensors = [
	('T2', np.float),
	('T1', np.float),
	('p', np.float),
	('T3',  np.float),
	('timestamp', np.float)
	]

dtype_weather = [
	('cettime', '|U19'), # py3: dump unicode, py2: |S19
	('T2m', np.float),
	('T200m', np.float),
	('swdr',  np.float),
	('swur', np.float),
	('p2', np.float)
	]

dtype_flasher = [
	('event', np.uint), ('pulse', np.uint16),
	('S', np.float), ('h', np.float), ('q', np.float),
	('Qneg', np.float), ('aop', np.float), ('depth', np.float),
	('rise', np.float), ('tp', np.float), ('tvar', np.float), ('tlen', np.float),
	('bmode', np.float), ('bmean', np.float), ('bstd', np.float)
	]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #