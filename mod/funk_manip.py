#!usr/bin/env python3
import os
import sys
import numpy as np
import pandas as pd
import funk_utils as fus
import funk_consts as fcn
import funk_io as fio

from IPython import embed
from datetime import datetime, timedelta
from natsort import natsorted
from scipy.interpolate import interp1d

Namespace = fus.Namespace

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def parse_which(which, on_error=False, quiet=False):
	if which=='all':
		ret = ['in_closed', 'out_closed', 'in_open', 'out_open']
	
	elif which=='open':
		ret = ['in_open', 'out_open']

	elif which=='closed':
		ret = ['in_closed', 'out_closed']

	elif which=='in':
		ret = ['in_open', 'in_closed']

	elif which=='out':
		ret = ['out_open', 'out_closed']
	
	elif isinstance(which, str) and\
			 which in fcn.positions.keys():
		ret = [which]
	
	elif isinstance(which, list) and\
			 all([x in list(fcn.positions.keys()) for x in which]):
		ret = which
	
	else:
		if not quiet:
			print('[ArgError] funk_manip.parse_which(): '\
						'check which=arg')
		if on_error: sys.exit(0)
		ret = None

	return ret


def parse_runs(run, prefix_pmt=fcn.prefix_pmt, pulse_par=fcn.pulse_par,
							 on_error=False, quiet=False):
	""" run: name of the run, string/list """

	# note os.path.isfile() will look in current path if no path given
	if isinstance(run, str) and os.path.isfile(run):
		ret = [run] 

	elif isinstance(run, list) and all([os.path.isfile(x) for x in run]):
		ret = run 
	
	elif isinstance(run, str): # and run!=all:
		ret = fio.find_all_files(prefix_pmt,
														 recursive=False,
														 pattern=run+'_'+pulse_par+'.npz',
														 on_error=on_error) 

	elif isinstance(run, list):
		ret = fio.find_all_files(prefix_pmt,
													 recursive=False,
													 pattern=[x+'_'+pulse_par+'.npz' for x in run],
													 combi=False,
													 on_error=on_error) 

	else:
		if not quiet:
			print('[ArgError] funk_manip.parse_runs(): '\
						'check run=arg')
		if on_error: sys.exit(0)
		ret = None

	return ret


def parse_diff(diff, on_error=False, quiet=False):
	if diff==4:
		ret = [
			'1101', # 'in_open - out_open',
			'1000', # 'in_closed - out_closed',
			'1110', # 'in_open - in_closed'
			'0100' 	# 'out_open - out_closed',
			]
	
	elif diff==6:
		ret = [
			'0100', # 'out_open - out_closed',
			'0010', # 'out_closed - in_closed',
			'1100', # 'in_open - out_closed',
			'1110', # 'in_open - in_closed'
			'0111', # 'out_open - in_open',
			'0110' 	# 'out_open - in_closed',
			]
	
	elif isinstance(diff, str) and\
			 (diff in fcn.diffs.keys() or diff in fcn.diffs.values()):
		ret = [diff]

	elif isinstance(diff, (tuple, list)) and\
			 all([x in fcn.diffs.keys() or\
			 			x in fcn.diffs.values() for x in diff]):		
		ret = diff

	#make pairs of diffs out of which (positions)
	elif isinstance(diff, (tuple,list)) and\
			 len(diff)>1 and\
			 all([x in fcn.positions.keys() for x in diff]): 
		
		from operator import itemgetter
		X = itemgetter(*diff)(fcn.positions)
		pairs = [x for i in range(len(X))
								for x in zip([X[i]
									for j in range(len(X[i+1:]))], X[i+1:])]
		diff_which = {}

		for key, val in zip([''.join(str(x) for x in pairs[i][0])\
												 + ''.join(str(x) for x in pairs[i][1])\
												 	for i in range(len(pairs))],
												[' - '.join(x) for x in [\
														itemgetter(*pairs[i])\
														({val:key for key, val in fcn.positions.items()})\
																				for i in range(len(pairs))]]):

			if key in ['0100', '0010', '1100', '0110', '1101', '1110']:
				diff_which[key] = val
			elif ''.join((key[2:],key[:2])) in\
					 ['0100', '0010', '1100', '0110', '1101', '1110']:
				diff_which[''.join((key[2:],key[:2]))]\
						= ' - '.join(val.split(' - ')[::-1])
			else:
				pass

		ret = diff_which.keys()

	else:
		if not quiet:
			print('[ArgError] funk_manip.parse_diff(): '\
						'check diff=arg')
		if on_error: sys.exit(0)
		ret = None

	return ret

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def apply_cuts(df, cuts=Namespace()):
	""" note: sequence of multiple cuts are ordered and are applied separately """

	def cut(df_, lim):
	
		n = len(df_)
		
		if lim[0]=='dtlim':
			# basic cleaning: remove the last cycle if incomplete
			# print df_.groupby(['in', 'open']).head(1)

			df_t 	= df_[['time', 'dt']]
			tlast = df_t.drop_duplicates(subset='time', keep='last').index
			tlast_split = [tlast[0+i:4+i] for i in range(0, len(tlast)-4+1, 4)]
			
			if df_.index[-1] != tlast_split[-1][-1]:
				print('[Info] removing last imcomplete cycle')
				df_ = df_.loc[:tlast_split[-1][-1]]
			
			if lim[1]:
				dtlim = lim[1]
	
				# do some basic check first, cause this is expensive
				if len(df_t[(df_t['dt']>=dtlim[0]) & (df_t['dt']<=dtlim[1])]) != n:
					print('[Info] cutting on dt: {}'.format(dtlim))
					tfirst = df_t.drop_duplicates(subset='time', keep='first').index
					tfirst_split = [tfirst[0+i:4+i] for i in range(0, len(tfirst)-4+1, 4)]
					
					for (x,y) in zip(tfirst_split, tlast_split):
						if any((df_t.loc[x, 'dt']<dtlim[0]) | (df_t.loc[x, 'dt']>dtlim[1])):
							df_.drop(range(x[0], y[-1]+1), inplace=True)
					
					print('       thrown {:.4}% of the data'.format((n-len(df_))/n*100))
					n = len(df_)
	
				else:
					pass

		elif lim[0]=='hlim' and lim[1]:

			hlim = lim[1]
			print('[Info] cutting on h: {}'.format(hlim))
			df_ = df_[(df_['h']>=hlim[0]) & (df_['h']<=hlim[1])]
			print('       thrown {:.4}% of the data'.format((n-len(df_))/n*100))
			n = len(df_)

		elif lim[0]=='Hlim' and lim[1]:

			Hlim = lim[1]
			print('[Info] cutting on H: {}'.format(Hlim))
			df_ = df_[(df_['H']>=Hlim[0]) & (df_['H']<=Hlim[1])]
			print('       thrown {:.4}% of the data'.format((n-len(df_))/n*100))
			n = len(df_)

		elif lim[0]=='qlim' and lim[1]:

			qlim = lim[1]
			print('[Info] cutting on q: {}'.format(qlim))
			df_ = df_[(df_['q']>=qlim[0]) & (df_['q']<=qlim[1])]
			print('       thrown {:.4}% of the data'.format((n-len(df_))/n*100))
			n = len(df_)

		elif lim[0]=='Slim' and lim[1]:
	
			Slim = lim[1]
			print('[Info] cutting on S: {}'.format(Slim))
			df_ = df_[(df_['S']>=Slim[0]) & (df_['S']<=Slim[1])]
			print('       thrown {:.4}% of the data'.format((n-len(df_))/n*100))
			n = len(df_)

		elif lim[0]=='tsiglim' and lim[1]:
			
			tsiglim = lim[1]
			print('[Info] cutting on tsig: {}'.format(tsiglim))
			df_ = df_[(df_['tsig']>=tsiglim[0]) & (df_['tsig']<=tsiglim[1])]
			print('       thrown {:.4}% of the data'.format((n-len(df_))/n*100))
			n = len(df_)

		elif lim[0]=='riselim' and lim[1]:
			
			riselim = lim[1]
			print('[Info] cutting on rise: {}'.format(riselim))
			df_ = df_[(df_['rise']>riselim[0])] # strict
			print('       thrown {:.4}% of the data'.format((n-len(df_))/n*100))
			n = len(df_)

		elif lim[0]=='tplim' and lim[1]:
			
			tplim = lim[1]
			print('[Info] cutting on tp: {}'.format(tplim))
			df_ = df_[(df_['tp']>=tplim[0]) & (df_['tp']<=tplim[1])]
			print('       thrown {:.4}% of the data'.format((n-len(df_))/n*100))
			n = len(df_)

		else:
			
			print('[Info] {} is {}: cut not implemented'.format(*lim))

		return df_


	if cuts is None:
		return df

	else:
		df_ = df.copy()
		
		for lim in cuts.items():
			df_ = cut(df_, lim)

		return df_

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def load_raw(infile, cols=[], npy='auto', cuts=Namespace()):
	""" return a pandas.groupby object, grouping the measurement configuration
			cols: additional label, e.g [q, s]
	"""

	_cols = cols.copy()

	for (i,c) in enumerate(_cols):
		if c=='tsig': _cols[i]='tvar'

	if cuts.hlim and 'h' not in _cols: _cols.append('h')
	if cuts.qlim and 'q' not in _cols: _cols.append('q')
	if cuts.Slim and 'S' not in _cols: _cols.append('S')
	if cuts.tsiglim and 'tvar' not in _cols: _cols.append('tvar')
	if cuts.riselim and 'rise' not in _cols: _cols.append('rise')

	labels = ['time', 'dt', 'in', 'open', 'event', 'capture', 'pulse']
	labels.extend([c for c in fus.iterover(_cols) if c not in labels])
	
	if npy=='auto':
		run_v = os.path.basename(infile)[:3]
		npy 	= (10, 'end') if run_v in ['v26', 'v27', 'v30', 'v31'] else 'all'

	print('loading raw configs::')
	print('labels:', labels)

	df = pd.DataFrame(fio.load_npz(infile, cols=labels, npy=npy))

	if 'tvar' in df.columns:
		df.loc[:,'tvar'] = np.sqrt(df['tvar'])
		df.rename(columns={'tvar': 'tsig'}, inplace=True)

	if 'p' in df.columns:
		df['p'] /= 10**2 # [hPa]

	print('::end')

	df 			= apply_cuts(df, cuts=cuts)
	configs = df.groupby(['in', 'open']) 

	return check_configs(configs, infile)


def check_configs(configs, infile):
	# TODO:
	# 	- check for v33	
	# 	- care about combining adjacent timing in redo_configs
	# 		--> but would be huge mess for load_raw

	run_v = os.path.basename(infile).split('_')[0]

	if run_v=='v31':
		configs = redo_configs(configs, mode=0)

	elif run_v=='v34':
		configs = redo_configs(configs, mode=2)

	elif run_v in ['v26', 'v27', 'v28', 'v29', 'v30']:
		configs = redo_configs(configs, mode=1)
		
	else:
		pass

	return configs


def redo_configs(configs, mode):
	""" return a configs-like object
			mode:
				0 pmt window completely closed
					--> combine all four positions (dt=1mn)
				1 pmt at fixed position 'in'
					--> combine 'in' and 'out' data (dt=2mn)
				2 shutter always open
					--> combine 'open' and 'closed' data (dt=2mn)

			TODO: when configs is raw --> sort_values('time') does not work
	"""
	cols = configs.head(1).columns
	cols = list(filter(lambda s: (s!='in') and (s!='open'), cols))

	if mode==0:
		dic =\
		{
			(1,0)		: configs.apply(lambda g: g.reset_index())\
											 .reset_index(drop=True)\
											 .sort_values('time')\
											 .set_index('index')[cols],
			(1,1)		: pd.DataFrame([np.nan for i in range(len(cols))],
				 											dtype=float, index=cols).T,
			(0,0)		: pd.DataFrame([np.nan for i in range(len(cols))],
				 											dtype=float, index=cols).T,
			(0,1)		: pd.DataFrame([np.nan for i in range(len(cols))],
				 											dtype=float, index=cols).T,	
		}


	elif mode==1:
		dic =\
		{
			(1,1)		: pd.concat([
												configs.get_group(fcn.positions['in_open']),
												configs.get_group(fcn.positions['out_open'])
												], axis=0).sort_values('time')[cols],
			(1,0)		: pd.concat([
												configs.get_group(fcn.positions['in_closed']),
												configs.get_group(fcn.positions['out_closed'])
												], axis=0).sort_values('time')[cols],
			(0,1)		: pd.DataFrame([np.nan for i in range(len(cols))],
				 											dtype=float, index=cols).T,
			(0,0)		: pd.DataFrame([np.nan for i in range(len(cols))],
				 											dtype=float, index=cols).T,
		}


	elif mode==2:
		dic =\
		{
			(1,1)		: pd.concat([
												configs.get_group(fcn.positions['in_open']),
												configs.get_group(fcn.positions['in_closed'])
												], axis=0).sort_values('time')[cols],
			(0,1)		: pd.concat([
												configs.get_group(fcn.positions['out_open']),
												configs.get_group(fcn.positions['out_closed'])
												], axis=0).sort_values('time')[cols],
			(1,0)		: pd.DataFrame([np.nan for i in range(len(cols))],
				 											dtype=float, index=cols).T,
			(0,0)		: pd.DataFrame([np.nan for i in range(len(cols))],
				 											dtype=float, index=cols).T,
		}
		


	else:
		print('[Warning] funk_manip.redo_configs(): check arg=mode')
		sys.exit(0)

	# caution: nan values will upcast numeric dtypes to float
	return 	pd.concat([dic[k] for k in dic.keys()], axis=0, keys=dic.keys())\
						.reset_index(level=(0,1))\
						.rename(columns={'level_0': 'in', 'level_1': 'open'})\
						.groupby(['in', 'open'])

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def load_counts(infile, npy='auto', trig=False, spu=False,
								cuts=Namespace()):
	"""	return a pandas.groupby object, grouping the measurement configuration
			structured as follow
				["time", "in", "open", "dt", "counts", "rates", "T1", "T2", "p"]
	"""		
	cols 	= ['time', 'dt', 'in', 'open', 'event', 'capture', 'pulse',
					 'T1', 'T2', 'p', # env data (inside bunker)
					 'S', 'q', 'rise', 'tvar'] # only used for event selection

	if npy=='auto':
		run_v = os.path.basename(infile)[:3]
		npy 	= (10, 'end') if run_v in ['v26', 'v27', 'v30', 'v31'] else 'all'

	print('loading configs::')
	df = pd.DataFrame(fio.load_npz(infile, cols=cols, npy=npy))
	df.loc[:,'tvar'] = np.sqrt(df['tvar'])
	df.rename(columns={'tvar': 'tsig'}, inplace=True)
	df['p'] /= 10**2 # [hPa]
	print('::end')

	df = apply_cuts(df, cuts=cuts)

	if trig:
		print('[Info] counting triggers')
		counts = df.groupby(['time', 'capture'])\
							 .head(1)\
							 .groupby('time')\
							 .size().values

	else:
		print('[Info] counting pulses')
		if spu:
			n = len(df)
			print('[Info] removing traces with more than one pulses')
			captures = df.groupby(['time', 'capture'])\
									 .size()\
									 .reset_index(name='counts')
			captures = captures[captures['counts']==1]
			counts 	 = captures['counts'].groupby(captures['time'])\
																	 .sum().values
			print('       thrown {:.4} %% of the data'\
						.format((n-np.sum(counts))/n*100))

		else:
			counts   = df.groupby(['time']).size().values

	# average env data over dt=60sec
	events 	= df.groupby(['time'])
	env 		= events.mean()[['T1', 'T2', 'p']]
	events 	= events.head(1)[['time', 'in', 'open', 'dt']]
	rates 	= counts / events['dt'].values
	err_rates = np.sqrt(rates / events['dt'].values)

	# lost of precision after averaging floats --> set np.round()
	configs = events.assign(counts 		= counts,
													rates  		= rates,
													err_rates = err_rates,
													T1 				= np.round(env['T1'].values, decimals=5),
													T2 				= np.round(env['T2'].values, decimals=5),
													p  				= np.round(env['p'].values, decimals=5))\
									.groupby(['in', 'open'])
	
	# return configs
	return check_configs(configs, infile)


def reset_configs(configs):
	""" reset index of configs and return df """
	# cols = configs.head(1).columns

	return configs.apply((lambda g: g.reset_index()))\
								.reset_index(drop=True)\
								.sort_values('index')\
								.set_index('index')

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def get_tbounds(configs):
	return (configs.head(1)["time"].min(), configs.tail(1)["time"].max())


def get_df(configs, key):
	"""	if raw_configs or configsX, key must be in positions and not diffs """
	def _get_rate(configs, key):
		df_ = configs.get_group(fcn.positions[key])
		return df_

	def _get_diff(configs, key):
		key = ((int(key[0]),int(key[1])),
					 (int(key[2]),int(key[3])))
		
		df_0 = configs.get_group(key[0])
		df_1 = configs.get_group(key[1])
		
		if len(df_0)!=len(df_1):
			print('[Warning] funk_manip.diff_rates() got different len(dfs)\n'\
						'          run apply_cuts() on dt or check data')
			sys.exit(0)

		else:
			df_  = pd.DataFrame({
								'time'	: (df_0['time'].values + df_1['time'].values)/2,
								'dtdiff': (df_0['dt'].values - df_1['dt'].values),
								'counts': (df_0['counts'].values - df_1['counts'].values),
								'rates'	: df_0['rates'].values - df_1['rates'].values,
								'err_rates'	: np.sqrt(df_0['err_rates'].values**2\
																			+ df_1['err_rates'].values**2),
								'T1'		:	(df_0['T1'].values + df_1['T1'].values)/2,
								'T2'		:	(df_0['T2'].values + df_1['T2'].values)/2, 
								'p'			:	(df_0['p'].values + df_1['p'].values)/2,
								})

		return	df_[['time', 'dtdiff', 'counts', 'rates', 'err_rates',\
								 'T1', 'T2', 'p']]

	try:
		key = parse_which(key, on_error=True, quiet=True)[0]
		return _get_rate(configs, key)
	except:
		try:
			key = parse_diff(key, on_error=True, quiet=True)[0]
			return _get_diff(configs, key)
		except:
			print('[ArgError] funk_manip.get_df(): check args')
			sys.exit(0)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def rates(configs, which='in_closed'):
	""" which: see funk_consts.positions """
	which = parse_which(which)
	dic = {}
	for pos in which:
		dic[pos] = configs.get_group(fcn.positions[pos])\
							 [['time', 'dt', 'counts', 'rates', 'err_rates',\
							 	 'T1', 'T2', 'p']]

	return dic

def diff_rates(configs, diff=4):
	""" diff: see funk_consts.diffs """
	keys	= parse_diff(diff)
	dic 	= {}
	
	for key in keys:
		x 	= [(int(key[0]),int(key[1])), (int(key[2]),int(key[3]))]
		df_0 = configs.get_group(x[0])
		df_1 = configs.get_group(x[1])

		if len(df_0)!=len(df_1):
			print('[Warning] funk_manip.diff_rates() got different len(dfs)\n'\
						'          run apply_cuts() on dt or check data')
			# sys.exit(0)

			# should not be needed anymore but keep warning for now!
			# if len(df_0) > len(df_1):
			# 	df_0 = df_0.iloc[:len(df_1)-len(df_0)]
			# else:
			# 	df_1 = df_1.iloc[:len(df_0)-len(df_1)]

		dic[key] = pd.DataFrame({
					'time'	: (df_0['time'].values + df_1['time'].values)/2,
					'dtdiff': (df_0['dt'].values - df_1['dt'].values),
					'counts': (df_0['counts'].values - df_1['counts'].values),
					'rates'	: df_0['rates'].values - df_1['rates'].values,
					'err_rates'	: np.sqrt(df_0['err_rates'].values**2 +\
																df_1['err_rates'].values**2),
					'T1'		:	(df_0['T1'].values + df_1['T1'].values)/2,
					'T2'		:	(df_0['T2'].values + df_1['T2'].values)/2, 
					'p'			:	(df_0['p'].values + df_1['p'].values)/2,
					})[['time', 'dtdiff', 'counts', 'rates', 'err_rates',\
							'T1', 'T2', 'p']]

	return dic

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def muon_data(path=fcn.prefix_muon, stack='all', split=False, on_error=False):
	""" split: (in minutes) split discontinued measurement """

	muon_counts = pd.DataFrame(fio.load_data(
									path=path, npz_file='muons.npz', dtype=fcn.dtype_muon,
									usecols=np.arange(4), stack=stack, on_error=on_error
									))
	
	df = muon_counts.iloc[1:].reset_index(drop=True) - muon_counts.iloc[:-1]
	df.rename(columns={'timestamp': 'dt'}, inplace=True)
	df = df.assign(time=muon_counts.loc[1:, 'timestamp'].values)

	df = pd.DataFrame({
					'time'		: df['time'].values, 
					'dt'			: df['dt'].values,
					'top'			: df['top'].values / df['dt'].values,
					'vcoinc'	: df['vcoinc'].values / df['dt'].values,
					'hcoinc'	: df['hcoinc'].values / df['dt'].values
			 })[['time', 'dt', 'top', 'vcoinc', 'hcoinc']]
	
	# quality cuts: counter may reset at some point
	# 							or the High voltage need to be adjusted
	df = df[(df['dt']>=57) & (df['dt']<=63) &\
					(df['top']>50) &\
					(df['vcoinc']>10) &\
					(df['hcoinc']>0)].reset_index(drop=True)
	
	if split:
		if split is True: split=15 # default value
		print('splitting muon measurements - timedelta ',
					timedelta(minutes=split))
		cond = seconds=df['time'].iloc[1:].values - df['time'].iloc[:-1]
		cond = (df['time'].iloc[1:].values - df['time'].iloc[:-1]) > (split*60)
		df_splitted = np.split(df, np.nonzero(cond)[0]+1, axis=0)

		if len(df_splitted[-1])==0: del df_splitted[-1]
		dic = dict(zip(['mes_'+str(i) for i in range(len(df_splitted))],
									 df_splitted))

		print('got:', natsorted(dic.keys()))
		return dic

	else:
		return df


def weather_data(path=fcn.prefix_weather):

	df = pd.DataFrame(fio.load_weather(path=path, npz_file='weather.npz'))

	df['utctime'] = pd.Series([
										datetime.strptime(x, '%Y-%m-%d %H:%M:%S')
										for x in df['cettime'].values
									]) - timedelta(hours=1)

	df['time'] 		= pd.Series([
										(d - datetime(1970, 1, 1)).total_seconds()
										for d in df['utctime']
							 		]) # do not use array, numpy recasting

	return df[['time', 'T2m', 'T200m', 'p2', 'swdr', 'swur']]



def sensors_data(path=fcn.prefix_sensors, stack='all', on_error=False):

	df = pd.DataFrame(fio.load_data(
								path=path, npz_file='sensors.npz', dtype=fcn.dtype_sensors,
								usecols=np.arange(1,6), stack=stack, on_error=on_error
								))
	df.rename(columns={'timestamp': 'time'}, inplace=True)
	df['p'] /= 10**2 # [hPa]

	return df


# import matplotlib.pyplot as plt
# df = sensors_data()
# plt.plot(fus.posix2utc(df['time']), df['T1'].values)
# plt.show()


def select_data(df_env, select=(1505397501, 1507470327), on_error=False):
	""" time bounds must be within the time duration of the measurement """
	
	ti, tf = df_env['time'].iat[0], df_env['time'].iat[-1]

	if (select[0] >= ti) and (select[1] <= tf):
		ix0 = np.abs(df_env['time'].values - select[0]).argmin()
		ix1 = np.abs(df_env['time'].values - select[1]).argmin()
	
	elif (select[0] < ti) and (select[1] <= tf):
		ix0 = 0
		ix1 = np.abs(df_env['time'].values - select[1]).argmin()
	
	elif (select[0] >= ti) and (select[1] > tf):
		ix0 = np.abs(df_env['time'].values - select[0]).argmin()
		ix1 = len(df_env)

	elif (select[0] < ti) and (select[1] > tf):
		ix0 = 0
		ix1 = len(df_env)

	else:
		print('[Warning] funk_manip.select_data(): timestamps out of bounds')
		# return empty df with the same data structure
		return df_env.iloc[0:0] if not on_error else sys.exit(0)

	df_ = df_env.iloc[ix0:ix1+1].reset_index(drop=True)
	print('selecting {} data: from {} to {}'.format(
				'weather' if 'T2m' in df_env.columns else 'muon',
				*fus.posix2utc(df_['time'].iat[0], df_['time'].iat[-1])))
	
	return df_

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def combine_data(funk_pmt, muons=None, weather=None):
	""" all inputs are dataframes """

	funk_pmt_ = funk_pmt.reset_index(drop=True)
	tbounds 	= (funk_pmt_['time'].iat[0], funk_pmt_['time'].iat[-1])
	
	# effective acceptance of the 2-fold muon paddles, in cm^2 sr
	# see estimate/muon_fluxes/acceptance_telescope.py)
	A_eff 		= 3055.37
	
	print(
		'combining: funk_pmt, {}, {} data'.format(
		'muon' if muons is not None else '',
		'weather' if weather is not None else '')
	)

	if muons is not None:

		muons_ = select_data(muons, select=(tbounds[0]-600, tbounds[1]+600))

		if len(muons_)>1:

			vcoinc = interp1d(muons_['time'].values, muons_['vcoinc'].values)
			hcoinc = interp1d(muons_['time'].values, muons_['hcoinc'].values)
			top 	 = interp1d(muons_['time'].values, muons_['top'].values)

			muons_ = pd.DataFrame({
								'vcoinc'	: vcoinc(funk_pmt_['time'].values),
								'hcoinc'	: hcoinc(funk_pmt_['time'].values),
								'top'			: top(funk_pmt_['time'].values),
							 })

			if muons_.isna().values.any():
				# still useful for plotting but dropna for correlation purpose
				print('[Warning] nan values in muon data')

		else:
			muons_ = muons_.iloc[0:0]
			print('[Info] no muon data available')
			# need explicit float dtype, otherwise 'int' fucked up with np.nanmean
			# (nan treated as weird float object)			
			# muons_ = pd.DataFrame(
			# 					{}, columns=['vcoinc', 'hcoinc', 'top'],
			# 					dtype=np.float
			# 				 )


	if weather is not None:

		weather_ = select_data(weather, select=(tbounds[0]-600, tbounds[1]+600))

		if len(weather_)>1: # note: interp1d can handle nan values

			T2m 		 = interp1d(weather_['time'].values, weather_['T2m'].values) 
			T200m 	 = interp1d(weather_['time'].values, weather_['T200m'].values)
			p2 			 = interp1d(weather_['time'].values, weather_['p2'].values)

			weather_ = pd.DataFrame({
									'T2m'		: T2m(funk_pmt_['time'].values),
									'T200m'	: T200m(funk_pmt_['time'].values),
									'p2'		: p2(funk_pmt_['time'].values),
								 })

			if weather_.isna().values.any():
				print('[Warning] nan values in weather data')

		else:
			weather_ = weather_.iloc[0:0]
			print('[Info] no weather data available')
			# weather_ = pd.DataFrame(
			# 						{}, columns=['T2m', 'T200m', 'p2'],
			# 						dtype=np.float
			# 					 )


	# note: appending series to deal with missing data
	df = pd.DataFrame({
				'time'			: funk_pmt_['time'],
				'rates'			: funk_pmt_['rates'],
				'err_rates'	: funk_pmt_['err_rates'],
				'T1'				: funk_pmt_['T1'],
				'T2'				: funk_pmt_['T2'],
				'p'					: funk_pmt_['p'],
				'vcoinc'		: muons_['vcoinc'],
				'hcoinc'		: muons_['hcoinc'],
				'top'				: muons_['top'],
				'Iv'				: muons_['vcoinc'] / A_eff, # per s/cm^2/sr 
				'T2m'				: weather_['T2m'],
				'T200m'			: weather_['T200m'],
				'p2'				: weather_['p2'],
			 })[['time', 'rates', 'err_rates', 'T1', 'T2', 'p',
					 'vcoinc', 'hcoinc', 'top', 'Iv',
					 'T2m', 'T200m', 'p2']]

	return df


def get_muons(label='vcoinc', tperiod=None, timestamps=None, muons=None):
	""" tperiod 		tuple of utcdatetime 
			timestamps	listlike --> interpolate 
	"""

	if muons is None:
		muons	= muon_data()
	
	if tperiod is not None:
		tbounds = fus.utc2posix(tperiod)
		muons 	= select_data(muons, select=(tbounds[0]-600, tbounds[1]+600),
													on_error=False)

	if timestamps is not None:
		rates 	= interp1d(muons['time'].values, muons[label].values)
		ret 		= pd.DataFrame({
								'time'	: timestamps,
								'rates'	:	rates(timestamps)	
								})

	else:
		ret 		= pd.DataFrame({
								'time' 	: muons['time'].values,
								'rates' : muons[label].values
								})

	if ret.isna().values.any():
		print('[Warning] nan values in vmuon data')

	if len(ret)>1:
		return ret
	else:
		print('[Info] no muon data available')	
		return pd.DataFrame({}, columns=['time', 'rates'],
												dtype=np.float)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def stats_summary(df):
	""" use pd.set_option('display.width', 150)
					pd.set_option('display.max_columns', None)
			before printing output
	"""
	# labels = list(
	# 						filter(lambda s: (s!='time') and\
	# 														 (s!='err_rates') and\
	# 														 (s!='in') and\
	# 														 (s!='open'),
	# 								 	 df.columns)
	# 						)

	try:
		df_ = df[['dt', 'counts', 'rates', 'T1', 'T2', 'p']]
	except:
		df_ = df[['dtdiff', 'rates', 'T1', 'T2', 'p']]

	ret = pd.DataFrame({
					# 'count'	 : df_.count(), # ignore nan
					'mean'	 : df_.mean(),
					'std'		 : df_.std(),
					'min'		 : df_.min(),
					'max'		 : df_.max(),
					'median' : df_.median(),
 					})

	n 		 = df_.count().iloc[0] # ignore nan
	stderr = ret['std'] / np.sqrt(n) # np.sqrt(ret['count'])

	# replace with poissonian expectation for counts/rates
	stderr['rates'] = np.sqrt(np.sum(df['err_rates']**2)) / len(df)

	if 'counts' in stderr.index:
		# counts can be negative for diff and would need two counts
		# to get the poissonian error so ignore this for diff
		stderr['counts'] = np.sqrt(np.sum(df['counts'])) / len(df)

	return ret.assign(stderr=stderr)\
				 [['mean', 'stderr', 'std', 'min', 'median', 'max']]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def load_configs(infile, trig=False, spu=False, cuts=Namespace(),
								 pulse_par=fcn.pulse_par):
	""" works in tandem with dump_configs.py """
	
	fpath = fcn.prefix_configs +\
					'-'.join(pulse_par[1:-1].split(',')[1:]) + '/'
	run_v = os.path.basename(infile)[:3]
	cpars = '_'.join((x[0][:-3]+'cut' for x in list(cuts.items())\
												if x[0]!='dtlim' and x[1]))

	fname = run_v + '_configs' +\
					('_trig' if trig else '') +\
					('_spu' if spu else '') +\
					('_%s'%cpars if cpars else '')
	fconfigs = os.path.join(fpath, fname)

	if os.path.exists(fconfigs):
		return fus.pickle_load(fname, fpath)

	else:
		print('[Info] configs file missing, run dump_configs.py to load faster')
		return load_counts(infile, trig=trig, spu=spu, cuts=cuts)


def load_configsX(run_v, var, trig=False, spu=False, cuts=Namespace(),
									pulse_par=fcn.pulse_par):
	""" works in tandem with dump_configs.py """
	fpath = fcn.prefix_configs +\
				 '-'.join(pulse_par[1:-1].split(',')[1:]) + '/'
	cpars = '_'.join((x[0][:-3]+'cut' for x in list(cuts.items())\
												if x[0]!='dtlim' and x[1]))

	fname = run_v + '_%s'%var +\
					('_trig' if trig else '') +\
					('_spu' if spu else '') +\
					('_%s'%cpars if cpars else '')
	fX = os.path.join(fpath, fname)

	if os.path.exists(fX):
		return fus.pickle_load(fname, fpath)

	else:
		print('[Exception] the file {} is missing'.format(fX))
		print('            run dump_configs.py --cols')


def load_rawX(run_v, var, cuts=Namespace(), pulse_par=fcn.pulse_par):
	""" works in tandem with dump_rawX.py """
	fpath = fcn.prefix_configs +\
					'-'.join(pulse_par[1:-1].split(',')[1:]) + '/'
	cpars = '_'.join((x[0][:-3]+'cut' for x in list(cuts.items())\
												if x[0]!='dtlim' and x[1]))
	fname = run_v + '_raw%s'%var +\
				('_%s'%cpars if cpars else '')
	fX = os.path.join(fpath, fname)

	if os.path.exists(fX):
		return fus.pickle_load(fname, fpath)

	else:
		print('[Exception] the file {} is missing'.format(fX))
		print('            run dump_rawX.py')