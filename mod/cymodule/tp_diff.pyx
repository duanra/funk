#cython: language_level=3
#distutils: language=c++

from libcpp.vector cimport vector
from numpy import asarray
from numpy.random import random


def get_mask(list tp_list, dict ratio):

	cdef vector[long] idxs_vec
	cdef double[:] tp_view
	cdef double bwi
	cdef int i, ixoff, n

	ixoff = 0
	bwi 	= 0.8 # sampling period

	for tp_arr in tp_list:

		tp_view = tp_arr
		n 	= len(tp_view)

		idxs_vec.push_back( 0+ixoff )

		for i in range(1, n):

			diff = round(tp_view[i] - tp_view[i-1], 1)
			bnum = int(round( (diff + .5*bwi) / bwi, 1))

			p = ratio.get(bnum, None)
			if p is None: # not a reflection
				idxs_vec.push_back( i+ixoff )
			else:
				rv = random()
				if rv < p: idxs_vec.push_back( i+ixoff )

		ixoff += n

	return asarray(idxs_vec)