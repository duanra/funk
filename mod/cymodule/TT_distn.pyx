#cython: language_level=3
#distutils: language=c++

from libcpp.vector cimport vector
from numpy import asarray


def get_mask(list ttag_list, double deadtime):

	cdef vector[long] idxs_vec
	cdef double[:] ttag_view
	cdef double ttag_ref
	cdef int i, ixoff, n

	ixoff = 0
	for ttag in ttag_list:
		ttag_view = ttag
		ttag_ref 	= ttag_view[0]
		n 				= len(ttag_view)

		idxs_vec.push_back( 0+ixoff )
	
		for i in range(1, n):

			if ttag_view[i] >= ttag_ref + deadtime:
				idxs_vec.push_back( i+ixoff )
				ttag_ref = ttag_view[i]

			else:
				continue

		ixoff += n

	return asarray(idxs_vec)