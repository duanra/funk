#!usr/bin/env python3

import os
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
# import matplotlib.lines as mlines
# import matplotlib.patches as mpatches

from functools import wraps
from funk_utils import order_unzip

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def show():
	plt.show()
	plt.close() # mutiple plt instance aren't somehow garbage collected


# for ROOT gui
def pause():
	input('press enter to continue::')


def order_legend(*order, lgd=None, ax=None):
	""" pass labels in the prefered order, use for e.g. as
			plt.legend(*order_legend('label1', 'label2'), **kwargs)
	"""

	if ax is None:
		ax = plt.gca()
	if lgd is None:
		lgd  = ax.get_legend_handles_labels()

	order_ = []
	for label in order:

		if label in lgd[1]:
			order_.append(label)

		else:
			# attempt to find pattern
			i = next((i for i, text in enumerate(lgd[1]) if label in text), None)
			if i is not None:
				order_.append(lgd[1][i])
			else:
				print('[Info] funk_plt.order_legend(): '
							"missing label '{}' in axes object".format(label))

	# order_ = [ label for label in order if label in lgd[1] ]

	return order_unzip(order_, *lgd)


def group_legend(*axes):
	""" gather handles/labels from multiple axes
			note: do not return iterator
	"""
	lgds = ( ax.get_legend_handles_labels() for ax in axes )

	return list(
						map(lambda ls: tuple(
									subitem for item in ls for subitem in item),
						 		zip(*lgds)
					))


def add_toplegend(labels=None, lgd=None, bbox_to_anchor=None, lw=None,
									ax=None, fig=None, **kwargs):
	""" labels: list of legend labels, if ordering is needed
			lgd: can take the return of group_legend for multiple axes
	"""

	if ax is None:
		ax = plt.gca()

	# default lgd-frame linewidth is 1, while axes has it 0.8
	axlw = ax.spines['top'].get_linewidth()
	
	if bbox_to_anchor is None:

		x0, y0, wi, hw = ax.get_position().bounds
		dy = (axlw+0.24) / 72 / ax.figure.get_figheight() / (y0+hw)
		bbox_to_anchor = (0, 1+dy, 1, 0.1)

	kwargs.setdefault('bbox_to_anchor', bbox_to_anchor)
	kwargs.setdefault('loc', 'lower left')
	kwargs.setdefault('mode', 'expand')
	kwargs.setdefault('borderaxespad', 0)
	kwargs.setdefault('borderpad', 0.3)
	kwargs.setdefault('handlelength', 1.5)
	kwargs.setdefault('handletextpad', 0.6)

	if labels is None:
		lgd = ax.legend(**kwargs)
	else:
		lgd = ax.legend(
						*order_legend(*labels, lgd=lgd, ax=ax),
						**kwargs
					)

	if lw is not None:
		for line in lgd.get_lines():
			line.set_linewidth(lw)

	lgd.get_frame().set_linewidth(axlw)

	return lgd


def autofmt_xdate(datefmt=None, monthly=None, daily=None, hourly=None,
									minutely=None, secondly=None, microsecondly=None,
					 				rotation=30, ha='right', ax=None):

	if ax is None:
		ax = plt.gca()

	if monthly is not None:
		locator = mdates.MonthLocator(interval=monthly)

	elif daily is not None:
		locator = mdates.DayLocator(interval=daily)

	elif hourly is not None:
		locator = mdates.HourLocator(interval=hourly)

	elif minutely is not None:
		locator = mdates.MinuteLocator(interval=minutely)

	elif secondly is not None:
		locator = mdates.SecondLocator(interval=secondly)

	elif microsecondly is not None:
		locator = mdates.MicrosecondLocator(interval=microsecondly)

	else:
		locator = mdates.AutoDateLocator(interval_multiples=False)


	if datefmt is None:
		formatter = mdates.AutoDateFormatter(locator)
	else:
		formatter = mdates.DateFormatter(datefmt)

	try:
		ax.xaxis.set_major_locator(locator)
		ax.xaxis.set_major_formatter(formatter)
		plt.setp(ax.get_xticklabels(), ha=ha, rotation=rotation)
		# fig.autofmt_xdate(rotation=rotation)

		ax.fmt_xdata = mdates.DateFormatter('%Y-%b-%d: %H:%M:%S') # gui toolbox

	except:
		# Tkinter exception traceback are not catched properly ???
		# and fig.autofmt_xdate() needs to be in try-block for this to work
		# print('[Exception] funk_plt.autofmt_xdate(): '
		# 			'check if xdata are proper pydatetime object')
		# sys.exit(0)
		print('[Exception] funk_plt.autofmt_xdate(): check args / xdata dtype')
		sys.exit(0)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


def step(xintvls, ys, ax=None, **kwargs):
	""" matplotlib step always assumes uniformly increasing xs intervals
			xintivls: [ (x0, x1), (x1, x2), ...]
			ys: (array) y-values for each x interval 

			TODO: change function call signature (see cuts_efficiency.py)
	"""

	if ax is None:
		ax = plt.gca()

	xintvls 		= np.asarray(xintvls)
	xlows, xups = xintvls[:,0], xintvls[:,1]

	ax.hlines(ys, xlows, xups, **kwargs)
	kwargs.update(label=None)
	ax.vlines(xups[:-1], ys[:-1], ys[1:], **kwargs)



def heatmap(mat, cmap='RdYlBu_r', vmin=None, vmax=None,
						annot=True, annotmat=None, mask=None, fmt=None, annot_kwargs={},
						cax=None, cbarlabel=None, cbar_kwargs={},
						xticklabels=None, yticklabels=None,
						ls='-', lw=0, lc='w', spines=True,
						ax=None, **kwargs):

	""" TODO: autocolor label based on a color threshold
			see mpl documentations
	""" 
	
	annot_kwargs.setdefault('ha', 'center')
	annot_kwargs.setdefault('va', 'center')

	cbar_kwargs.setdefault('pad', 0.01)
	cbar_kwargs.setdefault('aspect', 30)
	cbar_kwargs.setdefault('shrink', 1)
	cbar_kwargs.setdefault('fraction', 0.1)
	cbar_kwargs.setdefault('orientation', 'vertical')
	cbar_kwargs.setdefault('ticklocation', 'left')


	def strfmt(val, f=None):
		if f is not None:
			return f%(val) if '%' in f else f.format(val)
		else:
			return val

	if ax is None:
		ax = plt.gca()


	if isinstance(mat, pd.DataFrame):
		if xticklabels is None:
			xticklabels = mat.columns
		if yticklabels is None:
			yticklabels = mat.index

		mat = np.asarray(mat, dtype=float)

	else:
		mat = np.asarray(mat, dtype=float)

	if mask is None:
		mask = np.ones_like(mat, dtype=bool)


	# do not create cax manually
	# x0, y0, wi, hi = ax.get_position().bounds
	# cax = fig.add_axes([x0+1.01*wi, y0, 0.04*wi, hi], zorder=2)
	# cax.tick_params(
	# 	axis='both', direction='in', length=2,
	# 	left=False, labelleft=False,
	# 	bottom=False, labelbottom=False
	# )

	mat[~mask] = np.nan # need mat dtype to be float

	im 	 = ax.imshow(mat, cmap=cmap, vmin=vmin, vmax=vmax, **kwargs)
	cbar = ax.figure.colorbar(im, ax=ax, cax=cax, **cbar_kwargs)


	if annot:
		if annotmat is None:
			annotmat = mat

		for (i,j), val in np.ndenumerate(annotmat):
			if isinstance(val, str):
				ax.text(j, i, val, **annot_kwargs)

			elif not np.isnan(val): #mask[(i,j)]:
				ax.text(j, i, strfmt(val, fmt), **annot_kwargs)

	if cbarlabel is not None:
		cbar.set_label(cbarlabel, rotation=-90)

	if xticklabels is not None:
		ax.set_xticks(range(len(xticklabels)))
		ax.set_xticklabels(xticklabels)
	else:
		ax.set_xticks([])

	if yticklabels is not None:
		ax.set_yticks(range(len(yticklabels)))
		ax.set_yticklabels(yticklabels)
	else:
		ax.set_yticks([])


	# grid
	if lw != 0:
		ax.set_xticks(np.arange(mat.shape[1]+1)-.5, minor=True)
		ax.set_yticks(np.arange(mat.shape[0]+1)-.5, minor=True)
		ax.grid(which='minor', ls=ls, lw=lw, color=lc)
		ax.tick_params(which='minor', bottom=False, left=False)

	# spines
	if spines is False:
		for edge, spine in ax.spines.items():
			spine.set_visible(False)

	# ticks
	ax.tick_params(axis='both', labeltop=True, left=False, bottom=False)
	cbar.ax.tick_params(direction='in')

	# use im.axes, cbar.ax to access to underlying axes object
	return im, cbar



def setup_scatterplot(gs=(4,4), figsize=(6,6)):
	""" return instance of figure, ax and cax dicts """

	fig 	= plt.figure(figsize=figsize)
	axes  = {} # main subplots
	caxes = {} # colorbar subplots (attached to ax, but set to not visible)

	plt.subplots_adjust(
		left=0.06, right=0.93, bottom=0.06, top=0.97,
		wspace=0.05, hspace=0.05
	)

	for i in range(gs[0]): # rows
		for j in range(gs[1]): # cols

			axes[(i,j)] = plt.subplot2grid(
											gs, (i,j), zorder=0 if j>=i+1 else 1,
											sharex=axes[(0,j)] if (i>0 and j<i) else None,
											sharey=axes[(i,0)] if (j>0 and j<i) else None
										)

			if j>i: # upper triangular subplots
				for k in ['left', 'top', 'right', 'bottom']:
					axes[(i,j)].spines[k].set_visible(False)

				axes[(i,j)].tick_params(
					axis='both',
					left=False, labelleft=False,
					bottom=False, labelbottom=False,
					right=False, labelright=False,
					top=False, labeltop=False
				)

			elif j==i:
				axes[(i,j)].tick_params(
					axis='both', left=True, labelleft=False,
					right=True, labelright=True
				)

			else: # j<i:
				x0, y0, wx, hy = axes[(i,j)].get_position().bounds

				# caxes[(i,j)] = fig.add_axes(
				# 								 [ x0+0.995*wx, y0+0.1*hy, 0.04*wx, 0.8*hy ],
				# 								 zorder=2
				# 							 )
			
				caxes[(i,j)] = fig.add_axes(
												 [ x0+0.96*wx, y0+0.1*hy, 0.04*wx, 0.8*hy ],
												 zorder=2
											 )
			
				caxes[(i,j)].tick_params(
					axis='both', direction='in', length=2,
					left=False, labelleft=False,
					bottom=False, labelbottom=False
				)
				caxes[(i,j)].tick_params(
					which='minor', direction='in', length=1.5
				)
				
				for k in ['left', 'top', 'right', 'bottom']:
					caxes[(i,j)].spines[k].set_visible(False)

			if i != gs[0]-1:
				axes[(i,j)].tick_params(
					axis='both', bottom=False, labelbottom=False
				)

			if j != 0:
				axes[(i,j)].tick_params(
					axis='both', left=False, labelleft=False
			)

	return fig, axes, caxes


def cornertext(text, loc='upper right', frameon=False,
							 textprops=None, boxstyle=None, ax=None):
	"""
	::cleaned::
	conveniently places text in a corner of a plot.

	text 			: str or tuple of str
	textprops	: dict, for TextArea object
	boxstyle 	: str, for AnchoredOffsetbox.patch (FancyBboxPatch object)
							for further tweaks, use the returned object

	return AnchoredOffsetbox object
	-------
	Hans Dembinski <hans.dembinski@kit.edu>
	"""

	from matplotlib.offsetbox import AnchoredOffsetbox, VPacker, TextArea
	rcParams = plt.rcParams

	if ax is None:
		ax = plt.gca()

	if isinstance(text, str):
		text = (text, )

	if boxstyle is None:
		boxstyle = "round, pad=0., rounding_size=0.1"

	tas = []
	for txt in text:
		ta = TextArea(
						txt, textprops=textprops,
						multilinebaseline=True,
						minimumdescent=True )
		tas.append(ta)

	vpack  = VPacker(children=tas, pad=0, sep=rcParams['legend.handletextpad'])
	anchor = AnchoredOffsetbox(
								loc, child=vpack, frameon=frameon,
								pad=rcParams['legend.borderpad'],
								borderpad=rcParams['legend.borderaxespad'] )
	if frameon:
		anchor.patch.set_boxstyle(boxstyle)
		anchor.patch.set_edgecolor('lightgrey')
	ax.add_artist(anchor)

	return anchor


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

class OnLeftClick:
	""" print Mouse(x,y) coords upon left click MouseEvent """	

	def __init__(self):
		self.event_coords = None

	def __str__(self):
		return 'Mouse(x, y): ({:.3e}, {:.3e})'.format(*self.event_coords)

	def _error_handler(fn):
		@wraps(fn)
		def wrapper(self, *args):
			try:
				if args[0].button==1:
					fn(self, *args)
				else:
					pass

			except:
				print('[Exception] mouse outside of axes object area')

		return wrapper

	@_error_handler
	def data(self, event):
		self.event_coords = (event.xdata, event.ydata)
		print(self)

	@_error_handler
	def axes(self, event):
		self._ax  = plt.gca() # do not put in class constructor()
		self._fig = plt.gcf()
		self.event_coords = self._ax.transAxes.inverted().transform(
													self._ax.transData.transform(
														(event.xdata, event.ydata)))
		print(self)

	@_error_handler
	def fig(self, event):
		self._ax  = plt.gca()
		self._fig = plt.gcf()
		self.event_coords = self._fig.transFigure.inverted().transform(
													self._ax.transData.transform(
														(event.xdata, event.ydata)))
		print(self)

mousecoords = OnLeftClick()


class PlotIndexer:
	""" indexer for interactive plot based on mpl_connect events """

	def __init__(self, X, i=0, loop=False, direc=0):

		""" X : sequence (of data) to be indexed
				might be more practical to initialize with i=-1
		"""
		# from matplotlib.backend_bases import KeyEvent
		
		self.fX 		 = X
		self.fi 		 = i
		self.fi_save = i%len(X)
		self.fi_lim  = len(X)
		self.floop   = loop
		self.fdirec  = direc
		
		self.fkey 	 = None
		self.fpause  = 0.2

		# # this class has been reimplemented not to use recursion 
		# # for continious plotting (recursive key press event)
		# self.k_left  = KeyEvent('k_left', fig.canvas, key='left')
		# self.k_right = KeyEvent('k_right', fig.canvas, key='right')
		# self.k_space = KeyEvent('k_space', fig.canvas, key=' ')


	def action(self): 
		""" action triggered by self.on_key()
				to be implemented within an inherited class
		"""
		pass


	def on_key(self, event):
		""" event is instance of matplotlib.backend_bases.KeyEvent 
				on event --> trigger self.action()
		"""

		self.fkey = event.key # allowing to overload these behavior in action()

		if event.key=='q':
			plt.close()
			# this terminates the program but is needed if plot is still in loop
			assert os._exit(0) if self.floop else sys.exit(0)

		elif event.key == 'left':
			self.fi = (self.fi - 1) % self.fi_lim
		
		elif event.key == 'right':
			self.fi = (self.fi + 1) % self.fi_lim

		elif event.key == 'ctrl+left':
			self.fi_save = self.fi
			self.floop 	 = True
			self.fdirec  = -1

		elif event.key == 'ctrl+right':
			self.fi_save = self.fi
			self.floop 	 = True
			self.fdirec  = 1

		elif event.key == 'escape':
			self.fi 		 = self.fi_save
			self.floop 	 = False
			self.fdirec  = 0
			return

		elif event.key == ' ': # spacebar
			self.floop 	 = False
			self.fdirec  = 0
			return

		else:
			return

		# if self.fi==self.fi_lim or self.fi==-self.fi_lim:
		# 	self.fi = 0 # reset counter

		# # # # # # # # # # # # # # # # # # # # # # # # #

		if self.floop:
			
			while True:
				if self.fdirec == -1:
					self.fi = (self.fi - 1) % self.fi_lim
				elif self.fdirec == 1:
					self.fi = (self.fi + 1) % self.fi_lim

				self.action()
				plt.pause(self.fpause) # duration of each plot

				if (self.fi == self.fi_save) and (self.fkey != 'escape'):
					print('loop completed')
					self.floop 	 = False
					self.fdirec  = 0
					return

				if (self.fkey == 'escape') or (self.fkey == ' '):
					break

		else:

			self.action()