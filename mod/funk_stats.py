#!usr/bin/env python3
import sys
import numpy as np
import pandas as pd

from array import array
from scipy.interpolate import interp1d
from scipy.fftpack import fft, fftfreq
from scipy.signal import convolve, detrend
from funk_stat_distns import * # use current namespace by default


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def bin_centers(bin_edges):
	bin_edges = np.asarray(bin_edges)
	return 0.5 * (bin_edges[1:] + bin_edges[:-1])


def bin_widths(bin_edges):
	bin_edges = np.asarray(bin_edges)
	return bin_edges[1:] - bin_edges[:-1]


def bin_intervals(bin_edges):
	bin_edges = np.asarray(bin_edges)
	return tuple(zip(bin_edges[:-1], bin_edges[1:]))


def integer_binning(xlow, xup):
	""" binning on integer values between xlow and xup
			note: in numpy, xup is included in histogram bins. in ROOT it's not

			return nbins and histogram range=(low, high)
	"""
	return int(xup-xlow+1), xlow-0.5, xup+0.5


def unique_binning(xs):
	""" binning on unique values of x, quantization is assumed to be uniform """

	bins = np.unique(xs).tolist()
	bwi  = bins[1] - bins[0]
	bins.append(bins[-1] + bwi)

	return np.array([ b-0.5*bwi for b in bins ])


def normalization(rvs, bin_edges):
	""" only apply for equal-size bins """
	_rvs = rvs[(rvs>=bin_edges[0]) & (rvs<=bin_edges[-1])]
	bwi = bin_edges[1] - bin_edges[0]
	return len(_rvs) * bwi


def hist_integral(hist, bin_edges):
	return np.sum(hist * bin_widths(bin_edges))


def mean_stderr(xs):

	xs 		 = np.asarray(xs)
	mean 	 = xs.mean()
	stderr = xs.std(ddof=1) / np.sqrt(len(xs))

	return mean, stderr


def mean_varn(xs):

	xs 	 = np.asarray(xs)
	mean = xs.mean()
	varn = xs.var(ddof=1)

	return mean, varn


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def neg_log_likelihood(pars, fn, x):
	return -np.sum(np.log(fn(x, *pars)))


def squared_resids(pars, fn, x, y, weight=None):
	if weight is None:
		ret = np.sum((y-fn(x, *pars))**2)
	else:
		ret = np.sum((y-fn(x, *pars))**2 / weight)

	return ret


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def xcorr(x, y, dt=1, maxlags=None, normed=True):
	""" normalized cross-correlation (or to be precise, cross-covariance):
				correls are normalized such that the 0-lag component
				gives  to the standard pearson correlation
			note:
				- detrending is very important (result will be totally different)
				- in numpy, it is 'x' which is being shifted by the lag
				- if there is not enough datapoints, the estimate of optimal lag
					may be a little bit off (just because of bias in the summation) 
	"""

	x = detrend(x, type='constant')
	y = detrend(y, type='constant')
	n = len(x)	

	if maxlags is None:
		maxlags = n - 1

	correls = np.correlate(x, y, mode='full')[n-1-maxlags:n+maxlags]
	lags  	= dt * np.arange(-maxlags, maxlags + 1)
	norm 		= (n-1) * np.std(x, ddof=1) * np.std(y, ddof=1) if normed else 1
	# norm 		= np.sqrt(np.dot(x, x) * np.dot(y, y)) # equiv. in matplotlib

	return lags, correls / norm


def fourier(arr, Fs, window=None, amplitude=True, normed=True, onesided=True):
	""" args amplitude, normed, onesided are applied in this order

			notes on fft:

				Fs			  : sampling rate
				freq_step : Fs/len(arr)  
										min frequency = max periodicity (duration of the sample)
				Nyfreq		: Fs/2 (Nyquist frequency, Fs > 2*max{freq in samples})
										max freq (min period) that can be recorded given Fs

				so the number of positive freqs are s.t k*Fs/N = Fs/2 --> k = N/2
				(well not quite, depends if n even or odd --> see scipy.fftfreq)

				for real signal: the amplitude of the fourier coeffs are equally 
					distributed between neg. and pos. harmonics, except Nyfreq and DC
						--> affect normalization of one sided for negative parts
								otherwise just mulptiply by 2 for positive freq (see fftfreq)
					if n=len(arr) is even Nyfreq only appear once (at n//2), else none

				can check Parseval theorem:
					power in freq domain equals power in time domain
					use the two sided, unormalized fft

					fy = fourier(y, Fs, amplitude=True, normed=False, onesided=False)
					assert np.sum(fy['fval']**2)/len(fy) == np.sum(y**2)

					if normed --> multiply len(fy) instead	of dividing
					careful if to check on one

				also ifft should be corrected by len(fy) if fy normed

				alternatively look at scipy.fftpack.rfft()
	"""

	arr  	= np.asarray(arr)
	n 	  = len(arr)
	
	fvals = fft(window*arr) if window is not None else fft(arr)
	freqs = fftfreq(n, 1./Fs)

	if amplitude:
		fvals = np.abs(fvals)

	if normed:
		fvals /= n

	if onesided:
		k 	 	= n//2 if n%2==0 else (n+1)//2 # positive frequencies
		freqs = freqs[:k]
		fvals = fvals[:k]
		fvals[1:] *= 2

	return freqs, fvals


def power_spectrum(arr, Fs, window=None, onesided=True):
	"""	similar to periodogram from scipy.signal, but include DC (no-detrending)
			and exclude NyFreq, to be consistent with fftfreq indexing
				
				periodogram(arr, Fs, scaling='spectrum', return_onesided=True)

			further notes: http://www.ni.com/white-paper/4278/en/
			for averaged spectrum: see scipy.signal.welch() and Bartlett's method
			note that fft over some array and splitted(array) have different freq res
 	"""

	freqs, fvals = fourier(arr, Fs, window, True, True, False) # twosided
	fvals **= 2

	if onesided:
		n  = len(arr)
		k  = n//2 if n%2==0 else (n+1)//2
		
		freqs = freqs[:k]
		fvals = fvals[:k]
		fvals[1:] *= 2

	return freqs, fvals


def peakfreqs(freqs, fvals, threshold=None):

	if threshold is None:
		ix = np.argmax(fvals)
	else:
		ix = np.abs(fvals) >= threshold

	return freqs[ix], fvals[ix]


def convolve_pmf(x, p1, p2, check_normed=True, norm_tol=1e-3):
	""" compute pmf of discrete rdv x = x1+x2 (univariates) such that
				p(x) = conv(p1,p2) where
				p1 and p2 must be defined on the same support x
	"""

	def _is_sorted(arr): # also check for uniqueness
		return all([ arr[i] < arr[i+1] for i in range(len(arr)-1) ])

	try:

		x  = np.asarray(x)
		p1 = np.asarray(p1)
		p2 = np.asarray(p2)

		if not(len(x) == len(p1) == len(p2)):
			raise Exception('Exception',
											'funk_stats.convolve_pmf(): '
											'input arrays must have the same dimemsion')

		if not _is_sorted(x):
			raise Exception('Exception',
											'funk_stats.convolve_pmf(): '
											'the support array x must be sorted')

		if check_normed:
			if not np.isclose(np.sum(p1), 1., atol=norm_tol):
				raise Exception('Exception',
												'funk_stats.convolve_pmf(): pmf p1 is not normed')

			if not np.isclose(np.sum(p2), 1., atol=norm_tol):
				raise Exception('Exception',
												'funk_stats.convolve_pmf(): pmf p2 is not normed')

	except Exception as ex:
		print('[{}] {}'.format(*ex.args))
		sys.exit(0)

	else:
		sum_x   = np.append( (x[::-1]+x[0])[::-1], x[-1]+x[1:] )
		sum_pmf = convolve(p1, p2, mode='full')
		# ensure proper normalisation, but should not be needed
		# sum_pmf /= np.sum(sum_pmf) 

		return sum_x, interp1d(sum_x, sum_pmf, bounds_error=False, fill_value=0.)

# x = np.array([1,2,3])
# p1 = np.array([0.1,0.3,0.6])
# p2 = np.array([0.3,0.5,0.2])
# sx, p = convolve_pmf(x, p1, p2)
# print(sx)
# print(p(sx))
# print(convolve(p1, p2, mode='same'))

# import matplotlib.pyplot as plt
# P1 	 = st.triang(c=.5, loc=20, scale=20) 
# P2 	 = st.triang(c=.5, loc=70, scale=20) 
# x  	 = np.arange(0,100,.01)
# dx 	 = x[1] - x[0]
# pmf1 = P1.pdf(x)*dx
# pmf2 = P2.pdf(x)*dx
# sum_x, sum_pmf = convolve_pmf(x, pmf1, pmf2, check_normed=False)
# # print(np.sum(pmf1), np.sum(pmf2), np.sum(sum_pmf(sum_x)))
# nsize = 1000
# rv1  = P1.rvs(size=nsize)
# rv2  = P2.rvs(size=nsize)
# sum_rv = np.array([P1.rvs() + P2.rvs() for i in range(nsize)])
# # plot pdf
# plt.plot(x, pmf1/dx, color='b') 
# plt.plot(x, pmf2/dx, color='g')
# plt.plot(sum_x, sum_pmf(sum_x)/dx, color='r')
# # plot rdv hist
# bins = 50 
# plt.hist(rv1, bins=bins, range=None, density=True, color='b', alpha=.5)
# plt.hist(rv2, bins=bins, range=None, density=True, color='g', alpha=.5)
# plt.hist(sum_rv, bins=bins, range=None, density=True, color='r', alpha=.5)
# plt.show()
# plt.close()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def gen_from_cdfinv(cdfinv, *args, size=10):
	""" use inverse transform sampling
			args are the paramters of the function
	"""
	return cdfinv(np.random.random_sample(size), *args)


def gen_dummy_count(*args):
	""" args (tuple): (mode, len(count), *means(count))"""

	def _dummy_count(n, mean):
		c		= np.random.poisson(lam=mean, size=n)

		return pd.DataFrame({
							'counts' : c,
							'err' 	 : np.sqrt(c),
							})

	def _dummy_diff(n, mean1, mean2):
		c1 	 = np.random.poisson(lam=mean1, size=n) # generate separately
		c2 	 = np.random.poisson(lam=mean2, size=n)

		return pd.DataFrame({
							'counts' : c1 - c2,
							'err'		 : np.sqrt(c1 + c2),
							})

	if args[0]==0: # mode
		dummy = _dummy_count(*args[1:])

	elif args[0]==1: 
		dummy  = _dummy_diff(*args[1:])

	else:
		print('[Info] check mode')
		sys.exit(0)

	return dummy