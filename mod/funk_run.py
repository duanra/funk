#!usr/bin/env python3
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import funk_manip as fma
import funk_utils as fus
import funk_consts as fcn
import funk_io as fio

from IPython import embed
from functools import wraps
from datetime import datetime, timedelta
from sys import exit
Namespace = fus.Namespace
# from pandas.core.groupby.groupby import DataFrameGroupBy

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class Run:

	prefix_pmt = fcn.prefix_pmt
	pulse_par  = fcn.pulse_par
	positions  = fcn.positions 

	def __init__(self, run_v, **kwargs):

		infile 	 = fma.parse_runs(run_v, Run.prefix_pmt, Run.pulse_par,
															on_error=True)[0]
		t0 			 = fio.load_npz(infile, cols='time', npy=0, quiet=True)[0]
		t1 			 = fio.load_npz(infile, cols='time', npy=-1, quiet=True)[-1]
		period	 = fus.posix2utc(t0,t1)
		duration = period[1] - period[0]

		self.run_v 		= run_v
		self.infile 	= infile
		self.period 	= period
		self.duration = duration

		print(str(self))

		if kwargs.get('raw', False):

			self.mode 	 = 'raw'
			self.cuts 	 = kwargs['cuts']
			self.configs = fma.load_raw(self.infile, kwargs['cols'], kwargs['npy'],
																	self.cuts)

		elif kwargs.get('counts', False):

			self.mode 	 = 'counts'
			self.trig 	 = kwargs['trig']
			self.spu 		 = kwargs['spu']
			self.cuts 	 = kwargs['cuts']
			self.configs = fma.load_configs(self.infile, self.trig, self.spu,
																			self.cuts, self.pulse_par)

			self._add_stats()

		else:
			pass


	def __str__(self):

		return 'run_v   : {}\n'\
					 'period  : {} to {} (UTC)\n'\
					 'duration: {}\n'\
					 'infile  : {}'\
					 .format(self.run_v, *self.period, self.duration, self.infile)


	@classmethod
	def raw_configs(cls, run_v, cols=[], npy='auto', cuts=Namespace()):
		"""	raw means looking at each trigger """

		return cls(run_v, raw=True, cols=cols, npy=npy, cuts=cuts)


	@classmethod
	def configs(cls, run_v, trig=False, spu=False, cuts=Namespace()):
		""" count means average over 60 sec """

		return cls(run_v, counts=True, trig=trig, spu=spu, cuts=cuts)


	def _mode_counts(fn):

		@wraps(fn)
		def wrapper(self, *args, **kwargs):
			if self.mode=='counts':
				return fn(self, *args, **kwargs)
			else:
				print('[MethodError] {}() not available in mode={}'
							.format(fn.__name__, self.mode))

		return wrapper


	@_mode_counts
	def _get_diff(self, key):
		key = fma.parse_diff(key, on_error=True, quiet=True)[0]
		key = ((int(key[0]),int(key[1])),
					 (int(key[2]),int(key[3])))
		
		_df0 = self.configs.get_group(key[0])
		_df1 = self.configs.get_group(key[1])

		if len(_df0)!=len(_df1):
			print('[Warning] funk_manip.diff_rates() got different len(dfs)\n'\
						'          run apply_cuts() on dt or check data')
			sys.exit(0)

		_df  = pd.DataFrame({
							'time'	: (_df0['time'].values + _df1['time'].values)/2,
							'dtdiff': (_df0['dt'].values - _df1['dt'].values),
							'counts': (_df0['counts'].values - _df1['counts'].values),
							'rates'	: _df0['rates'].values - _df1['rates'].values,
							'err_rates'	: np.sqrt(_df0['err_rates'].values**2 +\
																		_df1['err_rates'].values**2),
							'T1'		:	(_df0['T1'].values + _df1['T1'].values)/2,
							'T2'		:	(_df0['T2'].values + _df1['T2'].values)/2, 
							'p'			:	(_df0['p'].values + _df1['p'].values)/2,
							})

		return	_df[['time', 'dtdiff', 'counts', 'rates', 'err_rates',\
								 'T1', 'T2', 'p']]


	def _get_rate(self, key):

		key = fma.parse_which(key, on_error=True, quiet=True)[0]
		# print('returning df at:', pos)
		_df = self.configs.get_group(Run.positions[key])

		if self.mode=='raw':
			return _df

		else:
			cols = ['time', 'dt', 'counts', 'rates', 'err_rates', 'T1', 'T2', 'p']
			return _df[cols]


	def get_df(self, key):

		try:
			return self._get_rate(key)

		except:

			try:
				return self._get_diff(key) 

			except:
				print('[ArgError] check get_df(key=arg)')
				exit(0)


	def print_stats(self, key):

		df = self.get_df(key)

		if df is not None:

			pd.set_option('display.width', 150)
			pd.set_option('display.max_columns', None)		

			print('stats_summary: key={}, no.data={}'\
						.format(key, len(df)))
			print(fma.stats_summary(df), '\n')


	def _add_stats(self):

		mpos = {key:val for val, key in Run.positions.items()}
		mseq = map(tuple, self.configs.head(1)\
													.sort_values('time')\
													[['in', 'open']].values)

		self.mseq = list(map(lambda k: mpos[k], mseq))

		ndata, meanrates, stdrates, varmeans = {}, {}, {}, {}

		for pos in self.mseq:

			df = self.get_df(pos)[['time', 'rates']]
			ndata[pos] 		 = len(df)
			meanrates[pos] = df['rates'].mean()
			stdrates[pos]  = df['rates'].std()
			varmeans[pos]	 = stdrates[pos]**2 / ndata[pos]

		self.ndata 		 = ndata
		self.meanrates = meanrates
		self.stdrates  = stdrates
		self.varmeans  = varmeans

		
# cuts = fcn.cuts.select('dtlim')
# run  = Run.raw_configs(run_v='v17', npy='auto', cols=['q'], cuts=cuts)
# run  = Run.configs(run_v='v32', cuts=cuts)
# # df = run.get_df('in_open')
# # print(df)
# run.print_stats('out_open')

# print(run.get_df('out_open'))
# print(fma.reset_configs(run.configs))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


class DailyStats(Run):
	""" add daily stats """

	def __init__(self, run_v, trig=False, spu=False, cuts=Namespace()):
	
		super(DailyStats, self).__init__(
					run_v, counts=True, trig=trig, spu=spu, cuts=cuts )

		self.dailys 	 			 = self._daily_stats()		
		self.daily_dates 		 = self._daily_dates()
		self.daily_ndata 		 = self._daily_ndata()
		self.daily_meanrates = self._daily_meanrates()
		self.daily_stdrates  = self._daily_stdrates()
		self.daily_varmeans  = self._daily_varmeans() # stderror^2
		# self.daily_weights 	 = self._daily_weights()


	@staticmethod
	def _get_timebins(timestamps):
		""" get day bins, a day is defined from-6-to-6 am """ 
		
		tbegin, tend = fus.posix2utc(timestamps.iloc[[0,-1]])
		dtdays = (tend - tbegin).days
		t0 		 = datetime(tbegin.year, tbegin.month, tbegin.day, 6, 0, 0)

		return fus.utc2posix([t0 + timedelta(i) for i in range(dtdays)])


	def _splitting_indices(self):
		""" get (integer) indices to split data in daily parts	"""
		
		init_pos 	 = self.mseq[0]
		timestamps = self.get_df(init_pos)['time']
		timebins 	 = self._get_timebins(timestamps)

		ndata = [] # no. daily data points
		for i in range(len(timebins)-1):
			 ndata.append(
			 		len(timestamps[ (timestamps>=timebins[i]) &
			 									  (timestamps<timebins[i+1]) ])
			 		)
		ndata.append(len(timestamps[ (timestamps>=timebins[i+1]) ]))

		return np.cumsum(ndata)[:-1]


	def _daily_stats(self):
		""" return (no. data, mean rate, std rate) daily """
		
		dailys = {}
		sp_idx = self._splitting_indices()

		for pos in self.mseq:
			df 					= self.get_df(pos)[['time', 'rates']]
			dailys[pos] = np.split(df, sp_idx)

		return dailys


	def _daily_dates(self):

		ret = np.array([
			fus.posix2utc(d['time'].iloc[0]) for d in self.dailys[self.mseq[0]]
			])

		return ret


	def _daily_ndata(self):
		ret = {
				pos: np.array([len(d) for d in self.dailys[pos]]) \
				for pos in self.dailys.keys() }

		return ret


	def _daily_meanrates(self):
		ret = {
				pos: np.array([d['rates'].mean() for d in self.dailys[pos]]) \
				for pos in self.dailys.keys() }

		return ret


	def _daily_stdrates(self):
		ret = {
				pos: np.array([d['rates'].std() for d in self.dailys[pos]]) \
				for pos in self.dailys.keys() }

		return ret


	def _daily_varmeans(self):
		ret = {
				pos: self.daily_stdrates[pos]**2 / self.daily_ndata[pos] \
				for pos in self.dailys.keys() }

		return ret 


	# def _daily_weights(self):
	# 	n 	= {
	# 			pos: sum(map(len, self.dailys[pos])) for pos in self.dailys.keys()}

	# 	ret = {
	# 			pos: np.array([len(d) / n[pos] for d in self.dailys[pos]]) \
	# 			for pos in self.dailys.keys() }

	# 	return ret



# ds = DailyStats('v25')
# print(ds.mseq)
# print(ds.daily_ndata)


class CombinedRun:

	mpos = ['out_open', 'out_closed', 'in_open', 'in_closed']
	
	def __init__(self, *runs):
		if not(all(isinstance(run, (Run, DailyStats)) for run in runs)):
			print('[ArgError] CombinedRun:'
						'constructor args must be Run or DailyStats class instance')
			exit(0)	

		self.runs 		 = runs
		self.runs_v 	 = [run.run_v for run in runs]
		self.durations = [run.duration for run in runs]
		self.periods 	 = [run.period for run in runs]
		self.ndata 		 = self._ndata()
		self.meanrates = self._meanrates()
		self.stdrates  = self._stdrates()
		self.varmeans  = self._varmeans()


	def _ndata(self):
		ret =	{
				pos: np.array([ run.ndata[pos] for run in self.runs ]) \
				for pos in CombinedRun.mpos
				}
		return ret


	def _meanrates(self):
		ret =	{
				pos: np.array([ run.meanrates[pos] for run in self.runs ]) \
				for pos in CombinedRun.mpos
				}
		return ret


	def _stdrates(self):
		ret =	{
				pos: np.array([ run.stdrates[pos] for run in self.runs ]) \
				for pos in CombinedRun.mpos
				}
		return ret


	def _varmeans(self):
		ret =	{
				pos: np.array([ run.varmeans[pos] for run in self.runs ]) \
				for pos in CombinedRun.mpos
				}
		return ret

# cuts = fcn.cuts.select('dtlim')
# run0 = Run.configs(run_v='v24', trig=True, cuts=cuts)
# run1 = Run.configs(run_v='v25', trig=True, cuts=cuts)

# crun = CombinedRun(run0, run1)
# print(crun.dates)


class ConcatenatedRun(Run):

	positions = fcn.positions 
	mpos = ['out_open', 'out_closed', 'in_open', 'in_closed']

	def __init__(self, *runs):
		if not(all(isinstance(run, (Run, DailyStats)) for run in runs)):
			print('[ArgError] CombinedRun:'
						'constructor args must be Run or DailyStats class instance')
			exit(0)	

		self.runs 		= runs
		self.run_v 		= '_'.join([run.run_v for run in runs])
		self.duration = np.sum([run.duration for run in runs])
		self.configs 	= self._configs()
		
		self._add_stats()


	def get_df(self, key):

		return fma.get_df(self.configs, key)

		# key = fma.parse_which(key, on_error=True, quiet=True)[0]
		# _df = self.configs.get_group(ConcatenatedRun.positions[key])

		# return _df[['time', 'dt', 'counts', 'rates', 'err_rates', 'T1', 'T2', 'p']]


	def _configs(self):

		ret = pd.concat(
				[fma.reset_configs(run.configs) for run in self.runs],
				axis=0, ignore_index=True 
				)

		return ret.groupby(['in', 'open'])


	def _add_stats(self):
		ndata, meanrates, stdrates, varmeans = {}, {}, {}, {}
		for pos in ConcatenatedRun.mpos:
			df = self.get_df(pos)[['time', 'rates']]
			ndata[pos] 		 = len(df)
			meanrates[pos] = df['rates'].mean()
			stdrates[pos]  = df['rates'].std()
			varmeans[pos]	 = stdrates[pos]**2 / ndata[pos]

		self.ndata 		 = ndata
		self.meanrates = meanrates
		self.stdrates  = stdrates
		self.varmeans  = varmeans


# cuts = fcn.cuts.select('dtlim')
# run0 = Run.configs(run_v='v24', trig=True, cuts=cuts)
# run1 = Run.configs(run_v='v25', trig=True, cuts=cuts)
# crun = ConcatenatedRun(run0, run1)

# print(run0.meanrates)
# print(run0.ndata)
# print(run1.meanrates)
# print(run1.ndata)
# print(crun.meanrates)
# print(crun.ndata)



# class SplittedRun(Run):
# 	""" split data in some dt regular intervals """

# 	def __init__(self, run_v, trig=False, spu=False, cuts=Namespace(), delta=None):
# 		""" delta (timedelta object) time interval to split """

# 		super(DailyStats, self).__init__(
# 					run_v, counts=True, trig=trig, spu=spu, cuts=cuts )

# 		self.delta 			= delta
# 		self.partitions = self._partitions()


# 	@staticmethod
# 	def _get_timebins(timestamps):
# 		""" get day bins, a day is defined from-6-to-6 am """ 
		
# 		tbegin, tend = fus.posix2utc(timestamps.iloc[[0,-1]])
# 		dtdays = (tend - tbegin).days
# 		t0 		 = datetime(tbegin.year, tbegin.month, tbegin.day, 6, 0, 0)

# 		return fus.utc2posix([t0 + timedelta(i) for i in range(dtdays)])



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


class Flasher:

	prefix_flasher = fcn.prefix_flasher
	pulse_par 		 = fcn.pulse_par
# 
	def __init__(self, run_v, split=False):
		infile = fma.parse_runs(run_v, Flasher.prefix_flasher, Flasher.pulse_par,
														on_error=True)[0]
		print('loading', infile)
	
		if split:
			npz  = np.load(infile)
			data = {k: pd.DataFrame(np.array(npz[k], ndmin=1)) for k in npz.files}
	
		else:
			data = pd.DataFrame(fio.load_npz(infile, cols='all', npy='all'))
			data.loc[:,'tvar'] = np.sqrt(data['tvar'])
			data.rename(columns={'tvar' : 'tsig'}, inplace=True)

		self.infile = infile
		self.data 	= data

	def __str__(self):
		return 'Flasher: {}'.format(self.infile)

	def apply_cuts(self, cuts=Namespace()):
		# do not update self.data
		return fma.apply_cuts(self.data, cuts)

# flasher = Flasher('v09f', split=False).data
# print (flasher)



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


class Capture:
	""" simple attribute holder, event structure are from either
			.../dump_funk_multi_run, or .../dump_flasher_run
	"""

	ncapt 		= -1 # first trace has ncapt=0
	cuts 			= fcn.cuts.select('riselim', 'Slim', 'tsiglim')
	pulse_par = np.fromstring(fcn.pulse_par[1:-1], dtype=np.float, sep=',')

	bstruct 	= (	'fLength', 'fTruncDelta', 'fMin', 'fMax',
								'fMinPos', 'fMaxPos', 'fMode',
								'fMean', 'fStd', 'fTruncMean', 'fTruncStd' ) # in Baseline.cc

	pstruct 	= ( 'fBegin', 'fLength', 'fNNeg', 'fSaturated', # in Pulse.cc
								'fMin', 'fMax', 'fSum', 'fSumNeg',
								'fTimeTrigger', 'fTimeMode', 'fTimeMean',
								'fTimeVar', 'fTimeEntropy',
								'fHeight', 'fDepth', 'fAbsTimeTrigger', # in CalibPulse.cc
								'fCharge', 'fLgCharge', 'fChargeNeg',
								'fAoP', 'fRiseTime' )

	dt = 0.8 # ns
	dU = 7.87402 # mV
	aOffset = 0.7*127 # analog offset

	def __init__(self):

		Capture.ncapt += 1
		self.icapt 		 = Capture.ncapt
		self.baseline  = None
		self.pulses		 = [] # list of array
		self.traces 	 = [] # flasher run yields 2 traces


	def __str__(self):
		ret = 'capture no.{}\nbaseline\n{}\npulses\n{}'.format(
					 self.icapt, self._fmt_baseline(), self._fmt_pulses())

		return ret


	@staticmethod
	def _fmt_dict(d, *keys):

		return ', '.join('{}={}'.format(key, d[key]) for key in keys)


	def _fmt_baseline(self):
		baseline = self.baseline

		if baseline is not None:
			b = dict(zip(Capture.bstruct, baseline))
			s = self._fmt_dict(b, 'fTruncMean', 'fTruncStd', 'fTruncDelta')

			return '  ' + s

		else:
			return None


	def _fmt_pulses(self):
		pulses = self.pulses 
		ret = '  '

		if len(pulses):

			for pulse in pulses:

				p  = dict(zip(Capture.pstruct, pulse))
				s1 = 'fTimeSig={}, fTimeEntropy={}'.format(
								Capture.dt * np.sqrt(p['fTimeVar']),
								p['fTimeEntropy']
							)
				s2 = self._fmt_dict(p, 'fHeight', 'fDepth', 'fAbsTimeTrigger', 'fCharge')
				s3 = self._fmt_dict(p, 'fLgCharge', 'fChargeNeg', 'fAoP', 'fRiseTime')

				ret += '\n  '.join([s1, s2, s3]) + '\n\n  '

			return ret.rstrip() + '\n'

		else:
			return None


	def print_(self): # sugar
		print(self)


	def plot(self, check_cuts=True, fig=None, ax=None):

		if fig is None:
			fig = plt.gcf()
		if ax is None:
			ax = plt.gca()

		# traces
		for trace, color, label in zip(
				self.traces, ['k', 'y'], ['trace', 'TTL trigger']):

			tbins = Capture.dt * np.arange(len(trace))
			ax.plot(
				tbins, trace, color=color, lw=1.5, label=label
			)

		# baseline and thresholds
		base_mu, base_sig = self.baseline[[9, 10]]
		sigup, siglow 		= Capture.pulse_par[[1,2]]
		threshup	= int(sigup) if sigup<0 else sigup*base_sig
		threshlow = int(siglow) if siglow<0 else siglow*base_sig

		ax.axhline(base_mu, color='r', lw=1, label='baseline')

		ax.axhline(
			base_mu - threshlow, color='limegreen', lw=1, ls='-',
			label = (
				'$%d\,\mathrm{ADC}$'%threshlow if threshlow<0 else
				'$%s\,\sigma_\mathrm{trunc}$'%siglow)
		)
		ax.axhline(base_mu + threshlow, color='limegreen', lw=1, ls='-')

		ax.axhline(
			base_mu - threshup, color='olive', lw=1, ls='-',
			label = (
				'$%d\,\mathrm{ADC}$'%threshup if threshup<0 else
				'$%s\,\sigma_\mathrm{trunc}$'%sigup)
		)
		ax.axhline(base_mu + threshup, color='olive', lw=1, ls='-')

		# pulses
		for pulse in self.pulses:
	
			begin = Capture.dt * (pulse[0] - 0.25)
			end 	= Capture.dt * (pulse[0] + pulse[1] - 0.75)
			tp 		= Capture.dt * (pulse[0] + pulse[8])

			ax.axvspan(begin, end, color='b', alpha=.2)
			ax.axvline(tp, color='b', lw=.8, ls='--')

			if check_cuts:

				riselim = Capture.cuts['riselim']
				Slim		= Capture.cuts['Slim']
				tsiglim = Capture.cuts['tsiglim']

				fTimeSig 		 = Capture.dt * np.sqrt(pulse[11])
				fTimeEntropy = pulse[12] 
				fRiseTime 	 = pulse[20]
				fy = base_mu + 0.5 * (abs(threshup) + abs(threshlow))

				if (riselim[0] < fRiseTime ) and\
					 (tsiglim[0] <= fTimeSig <= tsiglim[1]) and\
					 (Slim[0] <= fTimeEntropy <= Slim[1]):

					ax.scatter(
						tp+8, fy, s=200, lw=0,
						marker=r'$\checkmark$', color='deeppink'
					)

				else:
					ax.scatter(
						tp+8, fy, s=70, lw=0,
						marker='X', color='deeppink'
					)


		lgd = ax.legend(loc='lower left', handlelength=1)
		for line in lgd.get_lines():
			line.set_linewidth(1.5)
		fig.tight_layout(pad=0)
		# fig.canvas.draw()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# bad practice:
# 	initiating a class with another classes INSTANCE
# 	self is a local var so it's pointless to assign something to it
# 	would need to store the instance in an attribute, that's pointless
# 	--> just write a function which takes the instance as arg
# better:
# 	try to derive from base class instead and override constructor

# class Configs(DataFrameGroupBy):
# 	""" override pandas constructor and add get_df method """

# 	def __init__(self, configs):
# 		try:
# 			if not(isinstance(configs, DataFrameGroupBy)):
# 				raise TypeError('TypeError', 'funk_run.Configs: arg must be a '\
# 																		 'pandas DataFrameGroupBy object')
# 			self._configs = configs

# 		except TypeError as te:
# 			print('[{}] {}'.format(*te.args))
# 			exit(0)

# 	def f(self):
# 		print(self._configs.groups.keys())