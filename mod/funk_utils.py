#!usr/bin/env python3
import os
import sys
import numpy as np
import pandas as pd
import pickle

from collections import OrderedDict
from datetime import datetime, timezone
from inspect import signature
from itertools import chain
from functools import partial, wraps
from time import perf_counter
from warnings import catch_warnings, filterwarnings, warn

from IPython import embed


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def handle_warnings(func):
	""" decorator to output func args/kwargs values upon warnings """

	@wraps(func)
	def wrapper(*args, **kwargs):	
		
		with catch_warnings(record=True) as ws:

			filterwarnings('always')
			ret = func(*args, **kwargs)
	
			if len(ws):

				func_sig = signature(func)
				bound 	 = func_sig.bind(*args, **kwargs)
				bound.apply_defaults() # kwargs become absorbed into args?

				# # Parameter object is immutable
				# fpars = OrderedDict(func_sig.parameters)
				# for arg, name in zip(args, fpars.keys()):
				# 	fpars[name] = arg
				# for kw in kwargs.keys():
				# 	fpars[kw] = kwargs[kw]

				params_str = '  ' + (
						'\n  '.join('{} = {}'
						.format(*s) for s in bound.arguments.items()
					))

				# w = ws[-1] # last warning
				for w in ws:
					print('[{}] {}'.format(w.category.__name__, w.message))

				print(
					'function {} has been called with args: \n{}\n'
					.format(func.__name__, params_str))

		return ret

	return wrapper



def asarray(func=None, *, varnames=[]):
	""" decorator to convert function arguments to numpy array
			caution: the additional overhead might not worth it
							 if len(argument) is relatively small
	"""

	if func is None:
		return partial(asarray, varnames=varnames)

	@wraps(func)
	def wrapper(*args, **kwargs):

		func_sig  = signature(func)
		bound 	  = func_sig.bind(*args, **kwargs)
		bound.apply_defaults()

		arguments = bound.arguments

		for vname in varnames:
			try:
				arguments[vname] = np.asarray(arguments[vname])
			except KeyError:
				warn(UserWarning(
					"funk_utils.asarray() "
					"argument '%s' is not in function signature"%vname),
					stacklevel=2
				)

		return func(**arguments)

	return wrapper



def timing(func):
	""" decorator for simple timing of a function (include sleep time) """

	@wraps(func)
	def wrapper(*args, **kwargs):
		start = perf_counter()
		ret 	= func(*args, **kwargs)
		dt 		= perf_counter() - start

		if dt>120:
			factor, unit = 1/60, 'mins'
		elif 0.1<=dt<=120:
			factor, unit = 1, 's'
		else:
			factor, unit = 1e3, 'ms'

		print( 'Execution time of <{}> ~ {:.3f} {}'\
						.format(func.__name__, dt*factor, unit) )

		return ret

	return wrapper


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def islistlike(arg):
	""" note: hasattr(arg, '__getitem__') is also true for dict, str... """
	return isinstance(arg, (list, tuple, np.ndarray, pd.Series))


def isnumber(arg):
	""" check if finite/real numbers """
	from numbers import Number

	ret = (
		isinstance(arg, Number) and # discard None, str
		not(isinstance(arg, bool)) and # subclass of Number
		np.isreal(arg) and # note: np.isreal(None), np.isscalar(str)
		np.isfinite(arg) # check finiteness
		)

	return ret


def isstr(arg):
	""" note: use basestring in py2 """
	return isinstance(arg, (str, np.unicode_))


def iterover(keys):
	""" guaranty iterator even on 0d object """
	if not(islistlike(keys)): keys = (keys, )
	return iter(keys)


def subdic(dic, keys):
	return { k:dic.get(k, None) for k in iterover(keys) }


def make_pairs(ls):
	return [(ls[i],ls[j]) for i in range(len(ls)) for j in range(i+1,len(ls))]


def unique_list(ls):
	# return [ x for i, x in in enumerate(ls) if ls.index(x)==i ]
	return list(OrderedDict.fromkeys(ls))


def flatten_list(x, nesting_level=1):
	"""" caution: if nesting_level is set to a higher value than the actual
			 nesting, the recursion will always go to the except-block till stop
			 condition. the issue is that you cannot break out of the inner-calls
	"""	
	if nesting_level==1:
		try:
			return [ xxx for xx in x for xxx in xx ]
		except:
			print('except')
			return x
	else:
		return flatten_list( flatten_list(x, nesting_level-1) )


def order_unzip(order, ls1, ls2):
	""" take two listlike objects (one will be used as key for ordering) 
			then zip, apply order/select and unzip 
	"""
	# ignore unecessary overhead for the moment
	if order[0] in ls1:
		_keys = ls1
		_vals = ls2
	else: 
		_keys = ls2
		_vals = ls1

	d = dict(zip(_keys, _vals))
	_ls1, _ls2 = zip(*((k, d[k]) for k in order))

	# keep the order of the input lists
	return [_ls1, _ls2] if _keys is ls1 else [_ls2, _ls1]


def positive_index(i, n):
	""" return positive index, for eg. when looping over a list
			note that % is a modulus, not just remainder, operator
			difference is for negative value
	"""
	return i % n
	# j = i%n
	# if j<0: j = range(n-1,0,-1)[np.abs(j)-1]

	# return j


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def pickle_dump(obj, fname, fpath='./data/'):
	""" obj: object to dump, see pickle doc """

	outfile = os.path.join(fpath, fname)
	with open(outfile, 'wb') as f:
		print('saving', outfile, type(obj))
		pickle.dump(obj, f)


def pickle_load(fname, fpath='./data/'):
	""" obj_name: str """

	infile = os.path.join(fpath, fname)
	with open(infile, 'rb') as f:
		print('loading', infile)
		obj = pickle.load(f)

	return obj

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# py3tocheck: throw an error for pandas datetime / pydatetime / numpy datatime
def fn_over(arr, m=1, fn=np.nanmean, decimals=8, on_error=False):
	""" m (int or seq): split arr into bins of m sizes (discarding tail)
				and apply fn over each bins. if seq, follow np.split() rules
	"""

	if not(len(arr)):
		print('[Warning] funk_utils.fn_over(): got an empty array')
		return arr

	else:

		if m==1:
			return arr

		elif islistlike(m):
			ls = np.split(arr, m)
			if len(ls[-1])==0: ls.pop()
			
			ret = np.array([ fn(x) for x in ls ])
		
		elif m==0 or m>=len(arr):
			# return 0-d array for practical purpose
			ret = np.array( fn(arr) ) # ndmin=1)
			
		else:
			ret = np.array([ 
							fn(arr[0+i:m+i]) for i in range(0,len(arr)-m+1,m)
							])

		return ret if decimals is None else ret.round(decimals) 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def posix2utc(*args, **kwargs):
	""" return utcdatetime from unix timestamps """

	fmt = kwargs.get('fmt', None)
	if fmt=='default': fmt='%Y-%m-%d %H:%M:%S'

	def _get(x):
		if np.isnan(x):
			ret = np.nan
		else:
			ret = datetime.utcfromtimestamp(x)
			if fmt is not None:
				ret = ret.strftime(fmt)

		return ret

	if not(all([isnumber(x) or islistlike(x) for x in args])):
		print('[ArgError] funk_utils.posix2utc(): check args')
		ret = None

	else:
		
		if all([isnumber(x) for x in args]):
			if len(args)==1:
				ret = _get(args[0])
			else:
				ret = np.array([_get(x) for x in args], dtype=object)

		elif len(args)==1 and islistlike(args[0]):
			ret = np.array([_get(x) for x in args[0]], dtype=object)

		else: # mixed input types
			ret = []
			for arg in args:
				if isnumber(arg):
					ret.append(_get(arg))
				else:
					ret.append(np.array([_get(x) for x in arg], dtype=object))

	return ret


def utc2posix(*args, **kwargs):
	""" return unix timestamp from utcdatetime """ 

	fmt = kwargs.get('fmt', None)
	if fmt=='default': fmt='%Y-%m-%d %H:%M:%S'

	def _get(x):
		if fmt is not None:
			x = datetime.strptime(x, fmt)
		return int((x - datetime(1970, 1, 1, 0)).total_seconds())


	if not(all([isinstance(x, datetime) or isstr(x) or islistlike(x)\
							for x in args])):
		print('[ArgError] funk_utils.posix2utc(): check args')
		ret = None

	else:

		if all([isinstance(x, datetime) or isstr(x) for x in args]):
			if len(args)==1:
				ret = _get(args[0])
			else:
				ret = np.array([_get(x) for x in args], dtype=int)

		elif len(args)==1 and islistlike(args[0]):
			ret = np.array([_get(x) for x in args[0]], dtype=int)

		else: # mixed input types
			ret = []
			for arg in args:
				if isinstance(arg, datetime) or isstr(arg):
					ret.append(_get(arg))
				else:
					ret.append(np.array([_get(x) for x in arg], dtype=int))

	return ret


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def num(x):
	""" just convenient formatting """
	print('%.10e'%x)


def fmt(val, f=None):
	""" aimed to be used as default formatter for print output
			alternatively, provide a syntactic sugar for string formatting
	"""
	if f is not None:
		ret = f%(val) if '%' in f else f.format(val)

	else:
		if isinstance(val, str):
			ret = val
		
		elif isnumber(val):
			if val==int(val): # isinstance(val, int):
				if val<=1e+8:
					ret = '{:,}'.format(val)
				else:
					ret = '{:.4e}'.format(val)

			else: # isinstance(val, float):
				if 1e-1<val<1e+3:
					ret = '{:.4f}'.format(val)
				elif 1e-2<=val<=1e-1 or -1e-2>=val>=-1e-1:
					ret = '{:.5f}'.format(val)
				else:
					ret = '{:.4e}'.format(val)

		else: # bool, complex, repr(other obj)...
			# print('in else', type(val))
			ret = '{}'.format(val)

	return ret

# print(fmt(-0.00123))

def fmt_seq(*args):
	""" formatter for sequence of values """
	s = str()

	for arg in args:
		s += fmt(arg) + '  '

	return s


def fmt_lr(left_str, right_str, length=None):
	""" left/right string formatter """
	left_str 	= fmt(left_str)
	right_str = fmt(right_str)
	if length is None:
		length = len(left_str) + len(right_str) + 5

	s = '{}{}{}'.format(left_str,
											' '*(length-len(left_str)-len(right_str)),
											right_str)
	return s


def table_lr(left_col, right_col, length=None, indent=False, hlines=False):
	""" tabulate using fmt_lr """
	s = str()

	if length is None:
		length = max(map(len,
										 map(lambda l: ''.join((fmt(l[0]), fmt(l[1]))),
								 		 		 zip(iterover(left_col), iterover(right_col)))
										)
								 ) + 5

	for l, r in zip(iterover(left_col), iterover(right_col)):
		s += ('\t' if indent else '') + fmt_lr(l, r, length) + '\n'

	if hlines:
		# n = max(map(len, s.split('\n')))
		s = ('\t' if indent else '') + '-'*length + '\n' + s +\
				('\t' if indent else '') + '-'*length + '\n'

	return s

# print(lrt(['a', 'b'], [1e3, 3.14]))

def fmt_dic(dic, length=None, indent=False,  hlines=False):
	return table_lr(*zip(*dic.items()), length, indent, hlines)


def tabulate_latex(*args, **kwargs):
	""" args are list-like object """
	hline = kwargs.get('hline', False)
	outf 	= kwargs.get('outf', None)

	def _tex(seq): 
		s  = str()
		s += ' & '.join((str(e) for e in seq))
		s += ' \\\\'
		if hline: s += ' \\hline'
		s += '\n'
		return s 

	tab = str()	
	for arg in args:
		if isinstance(arg, (list, tuple)):
			tab += _tex(arg)

		elif isinstance(arg, np.ndarray):
			if len(arg.shape)==1:
				tab += _tex(arg) 
			else:
				for seq in arg:
					tab += _tex(seq) 

		else:
			print('[ArgError] funk_utils.tabulate_latex() :'\
						'args must be a list, tuple or array')
			return None
					
	if outf is not None:
		print(tab, file=open(outf, 'w+'))
	return tab

# a = np.array([[1,2], [3,4]])
# print(tabulate_latex(('a', 'b'), a))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# TODO: improve, add more options (see df.info(memory_usage='deep'))
def mem_usage(x):
	if isinstance(x, np.ndarray):
		mem = x.nbytes 
	if isinstance(x, pd.DataFrame): # this is wrong
		mem = x.memory_usage(deep=True)[0] 
		
	print('mem_usage: {:.2} MB'.format(mem/float(1024**2))) 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class Namespace(OrderedDict):
	""" simple attribute holder, similar to argparse.Namespace object
			but with a more flexible constructor (with **kw)
			in particular, __getattr__ return None if no attribute
	"""

	def __init__(self, *args, **kwargs):

		if args:
			super(Namespace, self).__init__(*args)
		elif kwargs:
			super(Namespace, self).__init__(kwargs)
		else:
			super(Namespace, self).__init__()

		for key in self.keys():
			setattr(self, key, self[key])


	def __setattr__(self, key, value):
		self[key] = value

	def __getattr__(self, key):
		return self.get(key, None)


	def select(self, *args):
		return self.__class__(
				[ (key, getattr(self, key)) for key in iterover(args) ])

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# def add_color(c1, c2):
# 	return '#'+hex(int(c1[1:], 16) + int(c2[1:], 16))[-6:]