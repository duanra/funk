from os.path import dirname, realpath
from sys import path

path.append( dirname(realpath(__file__)) )
# from inspect import getsourcefile
# print(getsourcefile(lambda _: None)) # lambda:0

# implicit relative import doesn't work anymore in python 3
# alternative: use explicit relative import inside module
# but then can't run that as a script --> use python -m parent.module
# other alterative: use setup.py --> bundle as package

