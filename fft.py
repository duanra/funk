#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from IPython import embed
from matplotlib.patches import Rectangle
from mod.funk_manip import get_muons
from mod.funk_run import Run
from mod.funk_plt import show
from mod.funk_stats import power_spectrum

plt.rcParams.update({
	# tex document \columnwidth = 246pt and \textwidth = 510pt
	'figure.figsize' 			: ( 246/72, 0.6*246/72 ),
	'savefig.format'			: 'pdf',
	'text.latex.preamble'	: [ r'\usepackage{mathptmx}' ],
	'text.usetex'					: True,
	'font.family'					: 'serif',
	'font.size'						:	10,
	'axes.labelsize'			: 10,
	'legend.fontsize'			: 10,
	'xtick.labelsize'			: 9,
	'xtick.labelsize'			: 9,
	# rather use plt.tight_layout(pad=0)
	# 'savefig.bbox'				:'tight'
	# 'savefig.pad_inches'	: 0.01
})

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def dB(arr, ref_value):
	return 10 * np.log10(arr / ref_value)


def hertz2cpd(arr):
	""" cpd: cycles per day """
	return np.asarray(arr) * 60 * 60 * 24


def cpd2hertz(arr):
	return np.asarray(arr) / (60 * 60 * 24)


def get_transform(xs, ts):
	""" xs: sampled values, ts: sampling timestamps """

	dt = np.mean(np.diff(ts))
	Fs = 1 / dt # sampling frequency

	freqs, powers = power_spectrum(xs, Fs)
	
	freqs 	 = freqs[1:]
	powers 	 = dB(powers[1:], powers[0]) # relative to DC component

	return freqs, powers, Fs


def plot_transform(xs, ts, funit=None, **kwargs):
	""" funit: callable """

	kwargs.setdefault('lw', 1)

	freqs, powers, Fs = get_transform(xs, ts)
	
	if funit is not None:
		freqs = funit(freqs)

	plt.step(freqs, powers, where='mid', **kwargs)

	return freqs, powers, Fs



cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
run_v = 'v35'
run 	= Run.configs(run_v, trig=False, cuts=cuts)


plt.figure()

funit  = hertz2cpd
df_run = run.get_df('out_open')
plot_transform(
	df_run['rates'].values, df_run['time'].values,
	funit=funit, color='dimgrey', label='data')

# df_muons = get_muons(label='vcoinc', tperiod=run.period)
# plot_transform(
# 	df_muons['rates'].values, df_muons['time'].values,
# 	funit=hertz2cpd, color='g', label='vert. muons' )



ymin, ymax = plt.ylim(-100, 5) # (-80, 5)
plt.gca().add_patch(
	Rectangle(xy=(0,ymin), zorder=10,
	width= 1/(6*60*60) if funit is None else funit(1/(6*60*60)), # T=6h=0.25d
	height=ymax-ymin,
	facecolor='lightsalmon', alpha=0.4)
	)

plt.xlabel('frequencies [cycles per day]') # Hz
plt.ylabel('power spectrum [dB]')
# '$10\ \log_{10}\ \dfrac{|fft[k]|^2}{|fft[0]|^2}$'
# plt.grid(which='major', ls=':')


# # # # # # zoom # # # # #

plt.tight_layout(pad=0)
bbox = plt.gca().get_position().extents.tolist() # (x0, y0, x1, y1)
rect = (0.4, 0.7, bbox[2]-0.4, bbox[3]-0.7 ) # (x0, y0, width height)
plt.gcf().add_axes(rect)


freqs, powers, Fs = plot_transform(
		df_run['rates'].values, df_run['time'].values,
		funit=funit, color='dimgrey', label='data')

plt.xticks(fontsize=9)
plt.yticks([-30, -60], fontsize=9)

xmin, xmax = plt.xlim(-1e-6, 9)
ymin, ymax = plt.ylim(-80, -20)
plt.gca().add_patch(
	Rectangle(xy=(0,ymin), zorder=10,
	width= 1/(6*60*60) if funit is None else funit(1/(6*60*60)), # T=6h=0.25d
	height=ymax-ymin,
	facecolor='lightsalmon', alpha=0.3)
	)

# powers_trunc = powers[freqs>10]
# mu, std = np.mean(powers_trunc), np.std(powers_trunc, ddof=1)
# plt.axhline(mu+2*std, color='k', lw=1, ls='--')

# lgd = plt.legend()
# for line in lgd.get_lines():
# 	line.set_linewidth(3)

# show()
# plt.savefig('./plt/8/power_spectrum')
# plt.close()
embed()