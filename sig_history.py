""" fit the open(30)/closed(200) data to get the parameters of the PMT """
#!usr/bin/env python3

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from contextlib import suppress
from IPython import embed
from iminuit import Minuit
from itertools import accumulate, cycle
from warnings import catch_warnings, filterwarnings

from mod.funk_plt import show, add_toplegend, order_legend
from mod.funk_utils import timing, pickle_dump, pickle_load, Namespace

plt.rcParams.update(fcn.rcDefense)
# plt.rcParams.update(fcn.rcThesis)
# plt.rcParams.update(fcn.rcPaper)
# plt.rcParams.update(fcn.rcPoS)

# plt.rcParams.update(fcn.rcPoster)
# plt.rcParams['figure.figsize'] = (550/72, 350/72)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


class Model:

	def __init__(self, nopen, nclosed, tbegin, tend):
	
		length 	= tend - tbegin
		nseq 		= int(np.ceil( tend / (nopen + nclosed) ))
		tbounds = list(accumulate( [0] + [nclosed, nopen]*nseq ))
		
		# fix for when the shutter didn't work
		with suppress(ValueError):
			tbounds.remove(3220)
			tbounds.remove(3420)
			tbounds.remove(7590)
			tbounds.remove(7790)

		tbounds_ref = tbounds.copy()

		if tbegin:
			tbegin_ix = np.searchsorted(tbounds, tbegin)
			tbounds = ([] if tbegin in tbounds else [tbegin]) + tbounds[tbegin_ix:]

		if tbounds[-1] != tend:
			tend_ix = np.searchsorted(tbounds, tend)
			tbounds = tbounds[:tend_ix] + [tend]

		tintvls = [ (tbounds[i], tbounds[i+1]) for i in range(len(tbounds)-1) ]

		for i in range(len(tbounds_ref)-1):
			if (tbounds_ref[i] <= tbounds[0] < tbounds_ref[i+1]):
				status0 = True if i%2 else False
				break
		status1 = not(status0)

		shutter_seq 	 = cycle([status0, status1])
		shutter_status = [ next(shutter_seq) for i in range(len(tintvls)) ]
		
		self.tintvls				= tintvls
		self.tintvls_open 	= [
			tintvl for tintvl, cond in zip(tintvls, shutter_status) if cond ]
		self.shutter_status = shutter_status
		self.nclock 				= shutter_status.count(True)
		self.itime 					= np.arange(tbegin, tend)

		self._init_parameters()


	def _init_parameters(self):
		
		params = Namespace([ ('up%d'%(i+1), 200) for i in range(self.nclock) ])
		params.low, params.base, = 0, 50
		params.alpha, params.tau = 1, 60

		self.params = params
		self._integrals()


	def _set_parameters(self, *args):
		""" all parameters have to be given at once """

		params = Namespace( zip(self.params.keys(), args) )

		if self.params != params:
			self.params.update(params)
			self._integrals()


	def signal(self, t):
		# on top of self.base
		if (t < self.tintvls[0][0]) or (t >= self.tintvls[-1][-1]):
			return 0

		else:
			for i, (t1, t2) in enumerate(self.tintvls_open):
				if t1 <= t < t2:
					return self.params['up%d'%(i+1)]

			else:
				return self.params['low']

	
	def _integrals(self):
		
		self._piecewise_sum = {}
		
		for i, (t1, t2) in enumerate(self.tintvls):

			tm = (t1+t2) / 2
			q  = self.signal(tm) * (1 - np.exp((t1-t2) / self.params.tau))	

			self._piecewise_sum[i] = q


	def _tindex(self, t):
	
		if t < self.tintvls[0][0]:
			ret = -1

		elif t >= self.tintvls[-1][-1]:
			ret = len(self.tintvls)

		else:
			ret = np.flatnonzero([ s[0] <= t < s[1] for s in self.tintvls ])[0]

		return ret


	def history(self, t, *args):

		if len(args): self._set_parameters(*args)

		tix = self._tindex(t)

		if tix == -1:
			ret = 0

		else:
			s  = 0
			for i in range(0, tix):
				s += np.exp((self.tintvls[i][1]-t) / self.params.tau)\
						 	* self._piecewise_sum[i]
			
			if tix < len(self.tintvls): # else signal(t) = 0
				s	+= self.signal(t) * (
							1 - np.exp((self.tintvls[tix][0]-t) / self.params.tau))
		
			ret =  self.params.alpha * s

		return ret


	def response(self, t, *args):
		""" params checks are only done in history """

		h = self.history(t, *args)
		i = self.signal(t)
		b = self.params.base

		return b + i + h


	def fit(self, data, print_level=0):
		
		time   = self.itime
		counts = data['counts'].values
		nclock = self.nclock

		init_fitargs = Namespace(
				[ ('up%d'%i, 200) for i in range(1, nclock+1) ] +
				[ ('low', 0), ('base', 50), ('alpha', 0.5), ('tau', 60) ] +
				[ ('limit_up%d'%i, (0, None)) for i in range(1, nclock+1) ] +
				[ ('limit_low', (0, None)), ('limit_base', (0, None)) ] +
				[	('limit_alpha', (0, None)), ('limit_tau', (0, None)) ] +
				[ ('error_up%d'%i, 0.1) for i in range(1, nclock+1) ] +
				[ ('error_low', 0.1), ('error_base', 0.1) ] +
				[	('error_alpha', 0.1), ('error_tau', 0.1) ] +
				[ ('fix_low', True), ('errordef', 1) ]
			)

		parnames = list(self.params.keys())

		def lsq(*args):
			# print('called lsq')
			self._set_parameters(*args)
			response = np.array([ self.response(t) for t in time ])
			
			# error can be assumed to be constant or proportional to poissonian
			return np.sum( (counts - response)**2 / counts)


		m = Minuit(
					lsq, forced_parameters=parnames,
					print_level=print_level, **init_fitargs
				)

		timing(m.migrad)()
		ndof = len(counts) - len(self.params) - 1
		print('reduced chi2: {:.3f} / {} = {:.3f}'
					.format(m.fval, ndof, m.fval / ndof))

		self.fitted = m  # has cython object that are not picklable
		self.fitarg = {'fval': m.fval, 'dof' : ndof, **m.fitarg}


	def plot_signal(self, label='fitted signal', ax=None, **kwargs):

		if ax is None:
			ax = plt.gca()
		
		time 	 = self.itime
		signal = np.array([ self.signal(t) for t in time ])

		ax.plot(
			time/60, (signal + self.params.base)/60,
			label=label, **kwargs
		)


	def plot_response(self, label='PMT response', ax=None, **kwargs):

		if ax is None:
			ax = plt.gca()
		
		time  	 = self.itime
		response = np.array([ self.response(t) for t in time ])

		ax.plot(time/60, response/60, label=label, **kwargs)



def read_data(infile):

	dtype = [
		('time', np.uint),
		('counts', np.uint), # triggers
		('dt_long', np.float),
		('dt_short', np.float),
		('closed', np.uint8)
		]

	with catch_warnings():
		filterwarnings('ignore', category=UserWarning)
		data = pd.DataFrame(
							np.genfromtxt(
								infile, dtype=dtype, usecols=(0, 3, 6, 7, 10),
								invalid_raise=False, encoding=None, max_rows=None	
							))

	data = data.assign( dt = (data['dt_long'] + data['dt_short']) / 2 )
	data = data.assign( rates = data['counts'] / data['dt'] )
	data = data[['time', 'dt', 'counts', 'rates', 'closed']]

	return data

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # !!! the fit is performed at the end of the interaval since we are really
# # fitting the number of events, as it accumulates every minutes !!!

plt.rcParams['figure.figsize'] = (300/72, 165/72)
# plt.rcParams['figure.figsize'] = (260/72, 165/72)
# plt.rcParams['figure.figsize'] = (220/72, 150/72)

infile 	= './data/pmt_memory/logLonOpen_newSh_30-200_2019-02-08.log'
data 		= read_data(infile)
# data 		= pd.read_pickle('./data/pmt_memory/dummy_open30_closed200.sdf')

nopen 	= 30
nclosed = 200

tbegin 	= 0
tend 		= 200+2*230 # None

data 		= data[tbegin:tend]
model 	= Model(nopen, nclosed, tbegin, tend=len(data))
time 		= model.itime


fig, ax = plt.subplots() # (figsize=(10,3))

counts = data['counts'].values
ax.errorbar(
	time/60, counts/60, yerr=np.sqrt(counts)/60,
	fmt='s', ms=1.5, mew=0, elinewidth=0.5,
	color='k', alpha=0.7, label='data', zorder=0
)

model.fit(data, print_level=1)
model.fitted.print_param()
# fitarg =	pickle_load('fitarg.sdic', './log/sig_history/0')
# model._set_parameters(*[fitarg[k] for k in model.params.keys()])
# print(model.params)

model.plot_response( #mediumblue
	label='response $r(t)$', color='blue', lw=2, zorder=5, 
)
model.plot_signal( #crimson
	label='input $s(t)$',	color='red', lw=1, zorder=10
)

# shades regions corresponding to open shutter
ys 	 = np.linspace(0, 10)
ylim = ax.get_ylim()
for (a,b) in model.tintvls_open:
	ax.fill_betweenx(
		ys, a/60, b/60, lw=0, color='lightgrey',
		alpha=0.5, zorder=0
	)


ax.set_xlabel('time/h')
ax.set_ylabel('rate/Hz')
ax.set_ylim(ylim)
ax.set_yticks(range(0,8,2))
add_toplegend(
	labels=['data', 'response', 'input'], ncol=3,
	handlelength=1.5, handletextpad=0.5
)
# add_toplegend(labels=['data', 'illumination', 'response'], ncol=3)
# ax.legend(
# 	*order_legend('input', 'response', 'data'),
# 	loc='upper left'
# )

fig.tight_layout(pad=0.05)
# fig.savefig('./plt/12/history_samplefit')
fig.savefig('../talks/src/phd_defense/figs/history_samplefit')
plt.close()

# show()
# embed()

# # save fitlog
# fpath = './log/sig_history/2/'
# fig.savefig(fpath+'fitplot.pdf')
# pickle_dump(model.fitarg, fname='fitarg.sdic', fpath=fpath)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# plt.rcParams['figure.figsize'] = (340/72, 145/72)

# infile 	= './data/pmt_memory/logLonOpen_newSh_30-200_2019-02-08.log'
# data 		= read_data(infile)

# nopen 	= 30
# nclosed = 200

# tbegin 	= 0
# tend 		= 200 + 7*230 


# data 	= data[tbegin:tend]
# time 	= np.arange(tbegin, tend)
# model = Model(nopen, nclosed, tbegin, tend=len(data))
# # time 	= model.itime


# fig, ax = plt.subplots()

# counts = data['counts'].values
# ax.scatter(
# 	time/60, counts/60, s=4, marker='+', lw=0.1,
# 	color='k', alpha=1, label='data'
# )

# ax.axhline(y=1.55, lw=1.5, ls='--', color='darkslategrey', zorder=0)

# ys 	 = np.linspace(0, 10)
# ylim = ax.get_ylim()
# for (a,b) in model.tintvls_open:
# 	ax.fill_betweenx(
# 		ys, a/60, b/60, lw=0, color='lightgrey',
# 		alpha=0.5, zorder=0
# 	)

# ax.set_xlabel('time/h')
# ax.set_ylabel('rate/Hz')
# ax.set_ylim(ylim)
# ax.set_yticks(range(0,10,2))


# fig.tight_layout(pad=0.05)
# # fig.savefig('./plt/12/history_measurement')
# fig.savefig('../talks/src/phd_defense/figs/history_measurement')
# plt.close()

# show()