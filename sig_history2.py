""" simulation of the model with history """
#!usr/bin/env python3

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from itertools import accumulate
from scipy.stats import poisson
from subprocess import check_call

from mod.funk_plt import show, step
from mod.funk_stats import negbinom, bin_centers, bin_widths
from mod.funk_utils import unique_list, fn_over, Namespace
from mod.funk_run import Run

plt.rcParams['font.size'] = 8

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


class MCevents:

	def __init__(self, lam0, lam1, dt0=1, dt1=1, Dt=60,
							 mode='poi'):

		""" lam0, dt0 : event rate, duration for closed shutter
				lam1, dt1 : . . . . . . . . . . . . .open shutter
				Dt: total durations, unit of time [minute]
				mode: 'const', 'poi', 'nbinom'
		"""

		n0, n1 	= int(2*dt0), int(2*dt1)
		nevents = int(Dt - Dt%(n0+n1)) # full cycle
		
		mseq 		= ['01']*(n1//2) + ['00']*(n0//2) +\
							['11']*(n1//2) + ['10']*(n0//2)
		# mseq 		= ['00']*(n0//2) + ['01']*(n1//2) +\
		# 					['10']*(n0//2) + ['11']*(n1//2)

		ncycle 	= nevents // len(mseq)
		
		mseq_full = np.array(mseq * ncycle)
		
		if mode=='const':
			events = np.zeros(nevents)
			events[ mseq_full=='00' ] = lam0
			events[ mseq_full=='10' ] = lam0 #+ 5
			events[ mseq_full=='01' ] = lam1
			events[ mseq_full=='11' ] = lam1 #+ 0.2*lam1

		else:
			def getp(status):
				return p0 if status[1]=='0' else p1

			if mode=='nbinom':
				p0 = negbinom(lam0, (10*lam0)**0.5)
				p1 = negbinom(lam1, (10*lam1)**0.5)
			else: # default: poi
				p0 	= poisson(lam0)
				p1 	= poisson(lam1)

			events = np.zeros(nevents, dtype=int)
			for i, status in enumerate(mseq_full):
				events[i] = getp(status).rvs()

		# note: use 'out/closed' instead of 'in/open' in labels
		mock_data = pd.DataFrame({
				'counts'	: events,
				'rates'		: events / 60,
				'out'			: [ 1^int(s[0]) for s in mseq_full ],
				'closed'	: [ 1^int(s[1]) for s in mseq_full ],
				})[['counts', 'rates', 'out', 'closed']]


		self.lam0 	 = lam0
		self.lam1 	 = lam1
		self.dt0 		 = dt0
		self.dt1 		 = dt1
		self.Dt 		 = Dt

		self.n0 		 = n0
		self.n1 		 = n1
		self.nevents = nevents
		self.mseq 	 = mseq
		self.ncycle	 = ncycle 
		self.mseq_full = mseq_full
		self.data 	 = mock_data


	def __getitem__(self, label):

		return self.data[label]



class Model:

	def __init__(self, events, alpha, tau, base=0, xneg=0):

		mseq 		= events.mseq
		ncycle 	= events.ncycle
		nevents = events.nevents
		tbounds = list(accumulate(
										[ 0 ] +
										[ mseq.count(s) for s in unique_list(mseq) ]*ncycle
										# [ events.dt1, events.dt0 ]*2*ncycle
									))
		tintvls = [ (tbounds[i], tbounds[i+1]) for i in range(len(tbounds)-1) ]

		self.alpha 	 = alpha
		self.tau 	 	 = tau
		self.base  	 = base
		self.xneg  	 = xneg

		self.events  = events['counts'].values
		self.itime 	 = np.arange(nevents)
		self.midtime = self.itime + 0.5
		self.tintvls = tintvls

		self._integrals()


	def signal(self, t): # on top of self.base
		
		if self.tintvls[0][0] <= t < self.tintvls[-1][-1]:
			# events is always sampled every mins
			return self.events[int(t)]

		else:
			return 0

	
	def _integrals(self):
		
		self._piecewise_sum = {}
		
		for i, (t1, t2) in enumerate(self.tintvls):

			tm = (t1+t2) / 2
			q  = self.signal(tm) * (1 - np.exp((t1-t2) / self.tau))

			self._piecewise_sum[i] = q


	def _tindex(self, t):
	
		if t < self.tintvls[0][0]:
			return -1

		elif t >= self.tintvls[-1][-1]:
			return len(self.tintvls)

		else:
			for i, (t1, t2) in enumerate(self.tintvls):
				if t1 <= t < t2: return i


	def history(self, t):
		
		tix = self._tindex(t)

		if tix == -1:
			ret = 0

		else:
			s  = 0
			for i in range(0, tix):
				t2 = self.tintvls[i][1]
				s += self._piecewise_sum[i] * np.exp((t2-t) / self.tau)
		
			if tix < len(self.tintvls):
				t1 = self.tintvls[tix][0]
				s	+= self.signal(t) * (1 - np.exp((t1-t) / self.tau))

			ret =  self.alpha * s

		return ret


	def response(self, t):

		hneg = self.xneg * self.alpha * np.exp(-t/self.tau) 

		return hneg + self.base + self.signal(t) + self.history(t)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


def plot_signal(tintvls, signal, ax=None, **kwargs):

	if ax is None: ax = plt.gca()
	kwargs.setdefault('color', 'grey')
	kwargs.setdefault('lw', 0.5)

	tintvls_ = tintvls / 60 # in hours
	signal_  = signal[ tintvls[:,0] ] # for when dt!=1

	step(tintvls_, signal_, label='clock', zorder=0, **kwargs)


def plot_response(time, response, mseq_full=None, ax=None, **kwargs):
	
	if ax is None: ax = plt.gca()
		
	time_ = time / 60

	if mseq_full is not None: # visually useful only when dt=1
		
		lstyle 	= {
			'00': dict(color='k', lw=0.8, label='R [out_closed]', zorder=5),
			'01': dict(color='b', lw=0.8, label='R [out_open]', zorder=10),
			'10': dict(color='k', lw=0.8, ls='--', label='R [in_closed]', zorder=5),
			'11': dict(color='r', lw=0.8, label='R [in_open]', zorder=10),
			}

		for s in ['01', '00', '11', '10']:
			ixs = (mseq_full==s)
			ax.plot(time_[ixs], response[ixs], **lstyle[s])
	
	else:
		kwargs.setdefault('color', 'r')
		kwargs.setdefault('lw', 1)

		ax.plot(time_, response, label='response', **kwargs)


def plot_diff(time, response, mseq_full, nseq=4, bins=None, ax=None):
	""" only makes sense when tintvls have same dt (assumed =1 for now)
			TODO: average over adjacent measurement if nseq!=4
	"""

	if ax is None: ax = plt.gca()

	# mseq_ix = [ mseq.index(s) for s in unique_list(mseq) ]

	response_11 = response[ mseq_full=='11' ]
	response_10 = response[ mseq_full=='10' ]
	response_01 = response[ mseq_full=='01' ]

	rdiff_1110 	= response_11 - response_10
	rdiff_1101 	= response_11 - response_01

	if bins is None:

		time_ = fn_over(time, fn=np.mean, m=nseq) / 60

		ax.plot(
			time_, rdiff_1110, lw=1, color='k',
			label='R [in_open] - R [in_closed]' )
		ax.plot(
			time_, rdiff_1101, lw=1, color='g',
			label='R [in_open] - R [out_open]' )

	else:
		# TODO 
		pass

		# rderr_1110  = (response_11 + response_10) / np.sqrt(60)
		# rderr_1101  = (response_11 + response_01) / np.sqrt(60)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# lam0, lam1 = 0, 250 # fully-closed, open
# dt0, dt1, Dt = 200, 30, 24*60
# alpha, tau = 1, 60
# base, xneg = 20, 0

lam0, lam1 = 150, 150
dt0, dt1, Dt = 1, 1, 24*60
alpha, tau = 1, 60
base, xneg = 0, 40

events 		= MCevents(lam0, lam1, dt0, dt1, Dt, mode='const')
model  		= Model(events, alpha, tau, base, xneg)

time 	 		= model.midtime # model.itime
tintvls  	= np.array(model.tintvls)
signal 		= (base + np.array([ model.signal(t) for t in time ])) #/ 60
response 	= np.array([ model.response(t) for t in time ]) #/ 60
mseq 			= events.mseq
mseq_full	= events.mseq_full


print(response)
fig, ax 	= plt.subplots(figsize=(6,4))

plot_signal(tintvls, signal)
# plot_response(time, response, mseq_full)
plot_response(time, response, mseq_full=None)
# plot_diff(time, response, mseq_full)

# ax.axvline(3, color='grey', lw=3, zorder=0) # truncation line

lgd = ax.legend(loc='lower right')
for line in lgd.get_lines():
	line.set_linewidth(2)

ax.grid(which='major', lw=0.5, ls=':')
show()


# # save response into a dataframe
# revents = events.data.copy()
# revents.rename(
# 	columns={'counts': 'counts_true', 'rates': 'rates_true'},
# 	inplace=True)
# revents = revents.assign(counts=response, rates=response/60)
# revents.to_pickle(
# 	'./data/pmt_memory/dummy_open%d_closed%d.sdf'%(dt1, dt0))


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
# trig 	= False
# run 	= Run.configs(run_v='v34', trig=trig, cuts=cuts)
# df11 	= run.get_df('in_open')
# df01 	= run.get_df('out_open')
# dfall = pd.concat([df01, df11], axis=0).sort_values('time')

# ndebut	= 1000
# dfdebut = dfall.iloc[:ndebut]

# time_data = (dfdebut['time'].values - dfdebut['time'].iloc[0])
# rate_data = dfdebut['rates'].values


# base, xneg = 0, 50
# alpha, tau = 0.9223, 54.1247

# lam 	 = 220
# events = MCevents(lam, lam, 1, 1, ndebut, mode='const')
# model  = Model(events, alpha, tau, base, xneg)

# time_model = model.midtime # model.itime
# response_model 	= np.array([ model.response(t) for t in time_model ])

# plt.scatter(
# 	time_data / (60*60), rate_data,
# 	s=4, marker='+', lw=1,
# 	color='k', alpha=0.9,
# 	label='data'
# )

# plt.plot(
# 	time_model / 60, response_model/60
# )

# show()
# # embed()





# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # comparison between different shutter efficiency 20%, 100%, 0%


# def set_axes(fig=None):

# 	if fig is None: fig= plt.gcf()

# 	axmain 	= fig.add_axes([.1, .425, .65, .5])
# 	axbot 	= fig.add_axes([.1, .125, .65, .3], sharex=axmain)
# 	axright = fig.add_axes([.75, .425, .2, .5], sharey=axmain)

# 	axmain.set_ylabel('rates')
# 	axmain.tick_params(bottom=False, labelbottom=False)
# 	axbot.set_xlabel('time')
# 	axbot.set_ylabel('diffs')
# 	axright.set_xlabel('pdf')
# 	axright.tick_params(labelleft=False)

# 	return axmain, axbot, axright


# def plot_model(model, mseqs, fig=None):

# 	if fig is None: fig = plt.gcf()
# 	axmain, axbot, axright = set_axes(fig)

# 	time 	 		= model.midtime
# 	tintvls  	= np.array(model.tintvls)

# 	time_ 		= time / 60
# 	tintvls_ 	= tintvls / 60

# 	signal 		= (base + np.array([ model.signal(t) for t in time ])) / 60
# 	response 	= np.array([ model.response(t) for t in time ]) / 60

# 	# rates
# 	signal_11 	= signal[ mseqs=='11' ]
# 	signal_01 	= signal[ mseqs=='01' ]
# 	response_11 = response[ mseqs=='11' ]
# 	response_01 = response[ mseqs=='01' ]

# 	axmain.plot(
# 		time_[ mseqs=='11' ], response_11,
# 		color='r', lw=1, label='R [in_open]' )
# 	axmain.plot(
# 		time_[ mseqs=='11' ], signal_11,
# 		color='b', lw=1, label='S [in_open]' )

# 	# difference
# 	m = 15

# 	sdiff_1101 	= signal_11 - signal_01
# 	rdiff_1101 	= response_11 - response_01

# 	sderr_1101  = np.sqrt( (signal_11 + signal_01) / 60 )
# 	rderr_1101  = np.sqrt( (response_11 + response_01) / 60 )

# 	axbot.axhline(0, lw=0.8, ls='--', color='k')
# 	axbot.errorbar(
# 		fn_over(time_, fn=np.mean, m=4*m),
# 		fn_over(rdiff_1101, fn=np.mean, m=m),
# 		fn_over(rdiff_1101, fn=lambda z: np.std(z, ddof=1)/np.sqrt(m), m=m),
# 		# fn_over(rderr_1101, fn=lambda z: np.sqrt(np.sum(z**2))/m, m=m),
# 		fmt='s', ms=6, capsize=5, elinewidth=2, color='r', label='R - R [open]' )
# 	axbot.errorbar(
# 		fn_over(time_, fn=np.mean, m=4*m),
# 		fn_over(sdiff_1101, fn=np.mean, m=m),
# 		fn_over(sdiff_1101, fn=lambda z: np.std(z, ddof=1)/np.sqrt(m), m=m),
# 		# fn_over(sderr_1101, fn=lambda z: np.sqrt(np.sum(z**2))/m, m=m),
# 		fmt='s', ms=3, capsize=3, elinewidth=2, color='b', label='S - S [open]'  )


# 	# distns
# 	bins 	 	 = 20
# 	itrunc 	 = 45 # trunctation of ~3hours
	
# 	shist_11 = np.histogram(signal_11[itrunc:], bins, density=True)
# 	smu_11 	 = np.mean(signal_11[itrunc:])
# 	svar_11  = np.var(signal_11[itrunc:], ddof=1)

# 	rhist_11 = np.histogram(response_11[itrunc:], bins, density=True)
# 	rmu_11 	 = np.mean(response_11[itrunc:])
# 	rvar_11  = np.var(response_11[itrunc:], ddof=1)

# 	axright.barh(
# 		bin_centers(rhist_11[1]), rhist_11[0], bin_widths(rhist_11[1]),
# 		color='r', label='mu={:.2f}, var={:.3f}'.format(rmu_11, rvar_11) )
# 	axright.barh(
# 		bin_centers(shist_11[1]), shist_11[0], bin_widths(shist_11[1]),
# 		color='b', label='mu={:.2f}, var={:.3f}'.format(smu_11, svar_11) )


# 	axmain.legend(loc='upper left')
# 	axbot.legend(loc='upper left')
# 	axright.legend(loc='center right')


# alpha, tau, base = 1, 60, 100
# Dt 	= 24*60

# fig = plt.figure(figsize=(8,4))

# events0 = MCevents(60, 300, Dt=Dt)
# model0 	= Model(events0, alpha, tau, base)
# mseqs0 	= events0.mseq_full

# plot_model(model0, mseqs0)
# plt.figtext(0.8, 0.2,
# 		'inputs: \n'
# 		'  - open 5 Hz\n'
# 		'  - closed 1 Hz')
# # fig.savefig('5/model0.png', format='png', dpi=600)
# show()

# # (0, 300), (300, 300), (60, 300)

# # cmd = 'montage 5/model* -tile 1x3 -geometry +0+0 -page a4 5/out.pdf'
# # check_call(cmd.split())