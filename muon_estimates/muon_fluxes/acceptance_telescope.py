""" THIS MAY TAKE SOME TIME """
from scipy import *
from scipy import integrate
import numpy as np
from scipy import interpolate
import time
from tabulate import tabulate

a =80.; b=25.;  # dimensions of the paddles (cm)
z=1             # distance between the two paddles

def jv(E):      # diff intensity of vertical muons
    return 2*10**-3 * E**(-0.595 - 0.382*log(E) + 0.016*(log(E))**2)

def Iv(E_min):  # intensity of vertical muons with energy >= E_min
    return integrate.quad(jv, E_min, infty)[0]

# approximation: independent of x,y
# def Omega(z):
#     def f(x,y): return z/(x**2 + y**2 + z**2)**(3./2.)
#     return integrate.nquad(f, [[-a/2., a/2.], [-b/2., b/2.]])[0]
# print "acceptance (approx) = %.2f cm^2 sr"%(a*b*Omega(z))

def dOmega(x,y,z):
    def f(xp, yp): return z/(z**2 + xp**2 + yp**2)**(3./2.)
    return integrate.nquad(f, [[-(a/2.+x), a/2.-x], [-(b/2.+y), b/2.-y]])[0]

# this is slow, better to go with an interpolation function
# print "acceptance = %.2f cm^2 sr"%(integrate.nquad(lambda x, y: dOmega(x, y, z), [[-a/2., a/2.], [-b/2., b/2.]])[0])
# integrate.nquad(lambda xp, yp, x, y: z/(z**2 + xp**2 + yp**2)**(3./2.), [[-a/2., a/2.], [-b/2., b/2.], [-a/2., a/2.], [-b/2., b/2.]])

# if you put the following inside the function, the dOmega() will be called all the time, not good with integrate
X = np.linspace(-a/2, a/2)
Y = np.linspace(-b/2, b/2)
f = np.array([dOmega(x, y, z) for y in Y for x in X])
f = f.reshape(len(Y), len(X))
def dOmegaInterp(x, y): return interpolate.interp2d(X, Y, f, kind='cubic')(x, y)
dAdOmega = integrate.nquad(lambda x, y: dOmegaInterp(x, y), [[-a/2., a/2.], [-b/2., b/2.]])[0]  # acceptance
print "acceptance (accurate) = %.2f cm^2 sr"%(dAdOmega)

# integrand in the muon rate integral
def func(x,y,z):
    def f(xp, yp): return z**4/(z**2 + xp**2 + yp**2)**3
    return integrate.nquad(f, [[-(a/2.+x), a/2.-x], [-(b/2.+y), b/2.-y]])[0]
g = np.array([func(x,y,z) for y in Y for x in X])
g = g.reshape(len(Y), len(X))
def funcInterp(x,y): return interpolate.interp2d(X, Y, g, kind='cubic')(x,y)

Iv_est = Iv(1)
Iv_ras = 7.29*10**-3
A_eff = integrate.nquad(lambda x, y: funcInterp(x,y), [[-a/2., a/2.], [-b/2., b/2.]])[0]  # effective acceptance
print "effective acceptance (accurate) = %.2f cm^2 sr"%(A_eff)

phi_est = Iv_est * A_eff
phi_ras = Iv_ras * A_eff
phi_exp = 26.359 # from muon_rate.py
Iv_exp = phi_exp / A_eff

res = [['', 'vertical intensity', 'muon rate'], ['estimate', '%.2e'%Iv_est, '%.2f'%phi_est],
       ['rastin', '%.2e'%Iv_ras, '%.2f'%phi_ras], ['exp', '%.2e'%Iv_exp, '%.2f'%phi_exp]]
print(tabulate(res))

# print "muon rate (approx) = %.2f s^-1"%(Iv(1)*integrate.nquad(lambda x, y: funcInterp(0,0), [[-a/2., a/2.], [-b/2., b/2.]])[0])
