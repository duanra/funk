# Muon flux calculations
# Lipari, Astroparticle physics 1 (1993) 195-227
# Due to some numerical stability issue, the integration at low energy is very slow,
# the whole script would take about 1 hour to run

# %reset
from scipy import *
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from scipy import integrate
import scipy.constants as const
import scipy.interpolate as interp
import time
from tabulate import tabulate

start = time.time()

# initial conditions, primary cosmic ray spectrum profile: 1.7 E^(-2.7)
# index in the power law
a = 2.7
# so the normalisation of the nucleons' spectrum is at 1.7 nucleons/(cm^2/sr/GeV^(-1.7)/s)
# neutron to proton ratio
d0 = 0.14
# proton
p0 = 1.7/(1.+d0)
# neutron
n0 = p0*d0

# rest masses - mean lifetime [PDG]
mPion = 139.57018*10**-3 #GeV
tauPion = 2.6033*10**-8 #s
mKaon = 493.677*10**-3
tauKaon = 1.2384*10**-8
mKaonL = 497.611*10**-3
tauKaonL = 5.116*10**-8
mMuon = 105.6584*10**-3
tauMuon = 2.19703*10**-6

# interaction lengths in g/cm^2 [Gaisser]
lProton = 88.; lNeutron = 88.; lPion = 116.; lKaon = 134.

# hadronic Z factors for a =2.7 (table 5)
Z = {'ProtonProton':0.263, 'ProtonNeutron':0.035, 'ProtonPionPlus':0.046, 'ProtonPionMinus':0.033, 'ProtonKaonPlus':0.009, 'ProtonKaonMinus':0.0028, 'ProtonKaonL':0.0059,
     'NeutronProton':0.035, 'NeutronNeutron':0.263, 'NeutronPionPlus':0.033, 'NeutronPionMinus':0.046, 'NeutronKaonPlus':0.0065, 'NeutronKaonMinus':0.0028, 'NeutronKaonL':0.0014,
     'PionPlusPionPlus':0.243, 'PionPlusPionMinus':0.028, 'PionPlusKaonPlus':0.0067, 'PionPlusKaonMinus':0.0067, 'PionPlusKaonL':0.0067,
     'PionMinusPionPlus':0.028, 'PionMinusPionMinus':0.243, 'PionMinusKaonPlus':0.0067, 'PionMinusKaonMinus':0.0067, 'PionMinusKaonL':0.0067,
     'KaonPlusKaonPlus':0.211, 'KaonPlusKaonMinus':0., 'KaonPlusKaonL':0.007,
     'KaonMinusKaonPlus':0., 'KaonMinusKaonMinus':0.211, 'KaonMinusKaonL':0.007,
     'KaonLKaonPlus':0.007, 'KaonLKaonMinus':0.007, 'KaonLKaonL':0.211}

# primary flux
# (equation 12)
L1 = lProton / (1.-Z['ProtonProton']-Z['ProtonNeutron'])
L2 = lProton / (1.-Z['ProtonProton']+Z['ProtonNeutron'])
# (equation 11)
def proton(x): return 0.5 * ((p0+n0)*exp(-x/L1) + (p0-n0)*exp(-x/L2))
def neutron(x): return 0.5 * ((p0+n0)*exp(-x/L1) - (p0-n0)*exp(-x/L2))

# atmosphere model (equation 110)
# units in g/cm^3, x: slant depth in g/cm^2 , theta: zenith angle
def rho(x, theta):
    # troposphere's vertical depth
    xt = 2.054*10**-3 * 6.344*10**5 * exp(-11/6.344)
    # atmosphere density model, 0 <= x <= 1030 g/cm^2
    if x<=0.01:
        return 0.01*cos(theta)/(6.344*10**5)
    elif x>0.01 and x<=xt:
        return x*cos(theta)/(6.344*10**5)
    else:
        return 4.439*10**-6 * (x*cos(theta))**0.8096

# decay lengths in g/cm^2 (equation 2)
def lDecPion(E, x, theta): return const.c*10**2 *tauPion * E/mPion * rho(x, theta)
def lDecKaon(E, x, theta): return const.c*10**2 * tauKaon * E/mKaon * rho(x, theta)

# step1: pion and kaon fluxes
# low energy meson sources
# (equation 14)
def sPionPlusLowx(x): return (proton(x)*Z['ProtonPionPlus']/lProton + neutron(x)*Z['NeutronPionPlus']/lNeutron)
def sPionMinusLowx(x): return (proton(x)*Z['ProtonPionMinus']/lProton + neutron(x)*Z['NeutronPionMinus']/lNeutron)
# (equation 18)
def sKaonPlusLowx(x): return (proton(x)*Z['ProtonKaonPlus']/lProton + neutron(x)*Z['NeutronKaonPlus']/lNeutron)
def sKaonMinusLowx(x): return (proton(x)*Z['ProtonKaonMinus']/lProton + neutron(x)*Z['NeutronKaonMinus']/lNeutron)

# high energy meson sources
# (equation 17)
LP1 = lPion / (1.-Z['PionPlusPionPlus']-Z['PionPlusPionMinus'])
LP2 = lPion / (1.-Z['PionPlusPionPlus']+Z['PionPlusPionMinus'])
# (equation 16)
def sPionPlusHighx(x):
    return 0.5 * ((p0+n0)/lProton * (Z['ProtonPionPlus']+Z['ProtonPionMinus']) * (LP1*L1/(LP1-L1)) * (exp(-x/LP1) - exp(-x/L1))
                + (p0-n0)/lProton * (Z['ProtonPionPlus']-Z['ProtonPionMinus']) * (LP2*L2/(LP2-L2)) * (exp(-x/LP2) - exp(-x/L2)))
def sPionMinusHighx(x):
    return 0.5 * ((p0+n0)/lProton * (Z['ProtonPionPlus']+Z['ProtonPionMinus']) * (LP1*L1/(LP1-L1)) * (exp(-x/LP1) - exp(-x/L1))
                - (p0-n0)/lProton * (Z['ProtonPionPlus']-Z['ProtonPionMinus']) * (LP2*L2/(LP2-L2)) * (exp(-x/LP2) - exp(-x/L2)))

# (equation 27)
APion = lProton**-1 * (p0+n0) * (Z['ProtonPionPlus']+Z['ProtonPionMinus']) * LP1*L1/(LP1-L1)
BPion = lProton**-1 * (p0-n0) * (Z['ProtonPionPlus']-Z['ProtonPionMinus']) * LP2*L2/(LP2-L2)
# (equation 28)
LK = np.array([lKaon/(1.-Z['KaonPlusKaonPlus']),
               lKaon/(1.-(Z['KaonPlusKaonPlus']+sqrt(2)*Z['KaonLKaonPlus'])),
               lKaon/(1.-(Z['KaonPlusKaonPlus']-sqrt(2)*Z['KaonLKaonPlus']))])
Lsigma = np.array([L1,L2, LP1, LP2])
# (equation 31)
U = 0.5 * np.array([[sqrt(2), -sqrt(2), 0.], [1,1, sqrt(2)], [1,1,-sqrt(2)]])
# (equations 23 -26)
C = np.array([[lProton**-1 * 0.5*(p0+n0) * (Z['ProtonKaonPlus']+Z['NeutronKaonPlus']) - lPion**-1 * 0.5*APion * (Z['PionPlusKaonPlus']+Z['PionMinusKaonPlus']),
                    lProton**-1 * 0.5*(p0-n0) * (Z['ProtonKaonPlus']-Z['NeutronKaonPlus']) - lPion**-1 * 0.5*BPion * (Z['PionPlusKaonPlus']-Z['PionMinusKaonPlus']),
                    lPion**-1 * 0.5*APion * (Z['PionPlusKaonPlus'] + Z['PionMinusKaonPlus']),
                    lPion**-1 * 0.5*BPion * (Z['PionPlusKaonPlus'] - Z['PionMinusKaonPlus'])],
              [lProton**-1 * 0.5*(p0+n0) * (Z['ProtonKaonMinus']+Z['NeutronKaonMinus']) - lPion**-1 * 0.5*APion * (Z['PionPlusKaonMinus']+Z['PionMinusKaonMinus']),
                    lProton**-1 * 0.5*(p0-n0) * (Z['ProtonKaonMinus']-Z['NeutronKaonMinus']) - lPion**-1 * 0.5*BPion * (Z['PionPlusKaonMinus']-Z['PionMinusKaonMinus']),
                    lPion**-1 * 0.5*APion * (Z['PionPlusKaonMinus'] + Z['PionMinusKaonMinus']),
                    lPion**-1 * 0.5*BPion * (Z['PionPlusKaonMinus'] - Z['PionMinusKaonMinus'])],
              [lProton**-1 * 0.5*(p0+n0) * (Z['ProtonKaonL']+Z['NeutronKaonL']) - lPion**-1 * 0.5*APion * (Z['PionPlusKaonL']+Z['PionMinusKaonL']),
                    lProton**-1 * 0.5*(p0-n0) * (Z['ProtonKaonL']-Z['NeutronKaonL']) - lPion**-1 * 0.5*BPion * (Z['PionPlusKaonL']-Z['PionMinusKaonL']),
                    lPion**-1 * 0.5*APion * (Z['PionPlusKaonL'] + Z['PionMinusKaonL']),
                    lPion**-1 * 0.5*BPion * (Z['PionPlusKaonL'] - Z['PionMinusKaonL'])]
             ])

# (equation 29)
def sKaonHighx(j, x):
    s=np.array([])
    for a in range(3):
        for b in range(4):
            for k in range(3):
                s=np.append(s, U[a,j] * LK[a]*Lsigma[b]/(LK[a]- Lsigma[b]) * U[a,k]*C[k,b] * (exp(-x/LK[a]) - exp(-x/Lsigma[b])))
    return sum(s)
def sKaonPlusHighx(x): return sKaonHighx(0, x)
def sKaonMinusHighx(x): return sKaonHighx(1, x)

# meson fluxes from these sources
# (equation 34)
def pionPlus(E, x, theta): return sPionPlusLowx(x) * lDecPion(E,x,theta) / (1.+ sPionPlusLowx(x)*lDecPion(E,x,theta)/sPionPlusHighx(x)) * E**-a
def pionMinus(E, x, theta): return sPionMinusLowx(x) * lDecPion(E,x,theta) / (1. + sPionMinusLowx(x)*lDecPion(E,x,theta)/sPionMinusHighx(x)) * E**-a
def kaonPlus(E, x, theta): return sKaonPlusLowx(x) * lDecKaon(E,x,theta) / (1. + sKaonPlusLowx(x)*lDecKaon(E,x,theta)/sKaonPlusHighx(x)) * E**-a
def kaonMinus(E, x, theta): return sKaonMinusLowx(x) * lDecKaon(E,x,theta) / (1. + sKaonMinusLowx(x)*lDecKaon(E,x,theta)/sKaonMinusHighx(x)) * E**-a

# step 2: muon fluxes
# muon sources from pion and kaon decays
# F factors
def heaviside(t, t0): return 1. if t>=t0 else 0.
rPion = (mMuon/mPion)**2
rKaon = (mMuon/mKaon)**2
# (equation 65 - 66)
def probLeftP(t): return 1./(1.-rPion) * (rPion/t - rPion)
def probRightP(t): return 1./(1.-rPion) * (1.- rPion/t)
def probLeftK(t): return 1./(1.-rKaon) * (rKaon/t - rKaon)
def probRightK(t): return 1./(1.-rKaon) * (1.- rKaon/t)
# (equation 67)
def fPionMuonLeft(t): return 1./(1.-rPion) * probLeftP(t) * heaviside(t, rPion)
def fPionMuonRight(t): return 1./(1.-rPion) * probRightP(t) * heaviside(t, rPion)
def fKaonMuonLeft(t): return 1./(1.-rKaon) * probLeftK(t) * heaviside(t, rKaon)
def fKaonMuonRight(t): return 1./(1.-rKaon) * probRightK(t) * heaviside(t, rKaon)

# sources (equation 36)
def sMuon(y, fMesonMuon, mesonFlux, lDecMeson): return 1./y**2 * fMesonMuon * mesonFlux / lDecMeson

# energy loss (equation 57)
# only from ionization, neglecting radiative loss
c1 = 2.*10**-3 # in GeV/(g/cm^2)
def E(E0, x, x0): return E0 - c1 * (x-x0)
def E0(Ef, xf, x0): return Ef + c1 * (xf - x0)

# survival proabability (equation 56)
def probMuonSurv(Ef, xf, x0, theta):
    def func(x):
        return mMuon/(const.c*10**2 * tauMuon * E(E0(Ef, xf, x0), x, x0) * rho(x, theta))
    return exp(-integrate.quad(func, x0, xf)[0])

# (equation 58)
def muonMinusLeft(Ef, xf, theta, prob):
    def source(y, x0):
        return 0.5 * (0.999*sMuon(y, fPionMuonLeft(y), pionMinus(E0(Ef, xf, x0)/y, x0, theta), lDecPion(E0(Ef, xf, x0), x0, theta))
                + 0.669*sMuon(y, fKaonMuonLeft(y), kaonMinus(E0(Ef, xf, x0)/y, x0, theta), lDecKaon(E0(Ef, xf, x0), x0, theta)))
    def func(y, x0): return source(y, x0) * prob(x0)
    return integrate.nquad(func, [[0., 1.], [0.001, xf]], opts={'limit':100})

def muonMinusRight(Ef, xf, theta, prob):
    def source(y, x0):
        return 0.5 * (0.999*sMuon(y, fPionMuonRight(y), pionMinus(E0(Ef, xf, x0)/y, x0, theta), lDecPion(E0(Ef, xf, x0), x0, theta))
                + 0.669*sMuon(y, fKaonMuonRight(y), kaonMinus(E0(Ef, xf, x0)/y, x0, theta), lDecKaon(E0(Ef, xf, x0), x0, theta)))
    def func(y, x0): return source(y, x0) * prob(x0)
    return integrate.nquad(func, [[0., 1.], [0.001, xf]], opts={'limit':100})

def muonPlusLeft(Ef, xf, theta, prob):
    def source(y, x0):
        return 0.5 * (0.999*sMuon(y, fPionMuonLeft(y), pionPlus(E0(Ef, xf, x0)/y, x0, theta), lDecPion(E0(Ef, xf, x0), x0, theta))
                + 0.669*sMuon(y, fKaonMuonLeft(y), kaonPlus(E0(Ef, xf, x0)/y, x0, theta), lDecKaon(E0(Ef, xf, x0), x0, theta)))
    def func(y, x0): return source(y, x0) * prob(x0)
    return integrate.nquad(func, [[0., 1.], [0.001, xf]], opts={'limit':100})

def muonPlusRight(Ef, xf, theta, prob):
    def source(y, x0):
        return 0.5 * (0.999*sMuon(y, fPionMuonRight(y), pionPlus(E0(Ef, xf, x0)/y, x0, theta), lDecPion(E0(Ef, xf, x0), x0, theta))
                + 0.669*sMuon(y, fKaonMuonRight(y), kaonPlus(E0(Ef, xf, x0)/y, x0, theta), lDecKaon(E0(Ef, xf, x0), x0, theta)))
    def func(y, x0): return source(y, x0) * prob(x0)
    return integrate.nquad(func, [[0., 1.], [0.001, xf]], opts={'limit':100})

def muonDiffSpectra(xf, theta):
    res = np.array([])
    err = np.array([])
    xx = np.linspace(0.001, xf, endpoint=True, num=1000)
    for Ef in muonEnergy:
        pp = [probMuonSurv(Ef, xf, x0, theta) for x0 in xx]
        prob = interp.interp1d(xx, pp, kind='linear')
        m = np.array([muonMinusLeft(Ef, xf, theta, prob), muonMinusRight(Ef, xf, theta, prob),
             muonPlusLeft(Ef, xf, theta, prob), muonPlusRight(Ef, xf, theta, prob)])
        res = np.append(res, m[:,0])
        err = np.append(err, m[:,1])
    res = np.reshape(res, (len(muonEnergy), 4))
    err = np.reshape(err, (len(muonEnergy), 4))
    return res,err

# vertical muons
theta=0.
# vertical depth at sea level ~ 1030 g/cm^2
# -------------- in the lab ~ 1500 g/cm^2 (assumed under 2 meters of concrete ~ 480 g/cm^2 in depth)
xfSea = 1030. #g/cm^2

muonEnergy = np.logspace(log10(0.35), log10(10**4), endpoint=True, num=30)
# muonEnergy = np.array([100., 300., 700., 1000., 3000.])

muonSea, err = muonDiffSpectra(xfSea, theta)
muonSeaTotal = np.array([sum(muonSea[i,:]) for i in range(muonSea.shape[0])])
errProp = np.array([ sqrt(sum(err[i,:]**2)) for i in range(err.shape[0])])

# diff spectrum fit:  c*E**(b+a*log(E))
coeff = np.polyfit(log(muonEnergy), log(muonSeaTotal), 3)
def muonSeaFit(E): return exp(coeff[3]) * E**(coeff[2] + coeff[1]*log(E) + coeff[0]*(log(E))**2)

# data (Kiel, sea level): Alkofer et al. 1971b
momentum_Alk = np.array([1., 1.5, 2., 3., 5., 7., 10., 15., 20., 30., 50., 70., 100., 150., 200., 300., 500., 700., 1000])
energy_Alk = sqrt(momentum_Alk**2 + mMuon**2)
muonSea_Alk = np.array([2.79*10**-3, 2.14*10**-3, 1.67*10**-3, 1.06*10**-3,
                        4.97*10**-4, 2.73*10**-4, 1.33*10**-4, 5.40*10**-5, 2.70*10**-5, 9.59*10**-6, 2.36*10**-6,
                        8.92*10**-7, 3.04*10**-7, 8.51*10**-8, 3.35*10**-8, 8.70*10**-9, 1.52*10**-9,
                        4.71*10**-10, 1.34*10**-10])

# best fit: Rastin 1984a
momentum_Ras = np.concatenate(([0.35], [0.40+.1*i for i in range(6)], [1.,1.5], [2.+1*i for i in range(9)], [15., 20., 25.],
                               [30+10*i for i in range(8)], [150. , 200., 250.], [300+100*i for i in range(8)],
                               [1500., 2000., 2500., 3000.]), axis=0)

energy_Ras = sqrt(momentum_Ras**2 + mMuon**2)
muonSea_Ras = np.array([2.85*10**-3, 2.90*10**-3, 2.94*10**-3, 2.92*10**-3, 2.87*10**-3, 2.80*10**-3, 2.71*10**-3,
                        2.62*10**-3, 2.12*10**-3, 1.69*10**-3, 1.10*10**-3, 7.40*10**-4, 5.17*10**-4, 3.75*10**-4,
                        2.80*10**-4, 2.16*10**-4, 1.69*10**-4, 1.35*10**-4, 5.28*10**-5, 2.58*10**-5,  1.45*10**-5,
                        8.69*10**-6, 3.90*10**-6, 2.11*10**-6, 1.26*10**-6, 8.03*10**-7, 5.42*10**-7, 3.81*10**-7,
                        2.77*10**-7, 7.85*10**-8, 3.12*10**-8, 1.50*10**-8, 8.20*10**-9, 3.11*10**-9,
                        1.45*10**-9, 7.75*10**-10, 4.55*10**-10, 2.86*10**-10, 1.89*10**-10, 1.31*10**-10,
                        3.14*10**-11, 1.13*10**-11, 5.11*10**-12, 2.67*10**-12])
# fit Rastin data
coeff_Ras = np.polyfit(log(E), log(diff), 3)
def muonSeaFit_Ras(E): return exp(coeff_Ras[3]) * E**(coeff_Ras[2] + coeff_Ras[1]*log(E) + coeff_Ras[0]*(log(E))**2)


# plots
plt.rc('text',  usetex=True)
muonEnergyNew = np.logspace(log10(muonEnergy[0]), log10(muonEnergy[-1]), endpoint=True)
fig0, ax0 = plt.subplots(figsize=(10,6))
# Alkofer
ax0.plot(energy_Alk, muonSea_Alk, 'o', fillstyle='none', color=(0,.7,0,1), ms=4, label='Alkofer et al. (1971b)', zorder=1)
# Rastin
ax0.plot(energy_Ras, muonSea_Ras, '^', fillstyle='none', color=(0,0,.9,1), ms=4, label='Rastin (1984a)', zorder=2)
ax0.plot(np.logspace(log10(energy_Ras[0]),log10(energy_Ras[-1])), muonSeaFit_Ras(np.logspace(log10(energy_Ras[0]),log10(energy_Ras[-1]))),
        '--', color='blue', label='polynomial fit (Rastin)')
ax0.text(energy_Ras[30], muonSea_Ras[30], r"$%.3f\ E_\mu^{\ %.3f\  %.3f \log E_\mu\ +%.3f (\log E_\mu)^2}$"%(exp(coeff_Ras[3]), coeff_Ras[2], coeff_Ras[1], coeff_Ras[0]),
        fontdict=dict(color='blue', fontsize=13))

# our calculation
ax0.plot(muonEnergy, muonSeaTotal, '+', ms=5, color='red', label='estimation', zorder=3)
ax0.plot(muonEnergyNew, muonSeaFit(muonEnergyNew), '-', color='black', label='polynomial fit', zorder=0)
ax0.text(muonEnergyNew[15], mean(muonSeaTotal), r"$%.3f\ E_\mu^{\ %.3f\  %.3f \log E_\mu\ +%.3f (\log E_\mu)^2}$"%(exp(coeff[3]), coeff[2], coeff[1], coeff[0]),
        fontdict=dict(color='black', fontsize=13))
ax0.set_xlabel(r"muons' energy $[GeV]$")
ax0.set_ylabel(r"$j(E, \theta=0) = dN_\mu / (dA\ d\Omega\ dE_\mu\ dt) \ \ [ cm^{-2} sr^{-1} GeV^{-1} s^{-1}]$")
ax0.set_xscale('log')
ax0.set_yscale('log')
ax0.set_title("diff. intensity of vertical muons at sea level")
ax0.legend()
# fig0.savefig('./0.eps', format='eps', dpi=1200)

fig1, ax1 = plt.subplots()
# Alkofer
ax1.plot(energy_Alk, energy_Alk**3 * muonSea_Alk, 'o', fillstyle='none', color=(0,.7,0,1), ms=4, label='Alkofer et al. (1971b)')
# Rastin
ax1.plot(energy_Ras, energy_Ras**3 * muonSea_Ras, '^', fillstyle='none', color=(0,0,.9,1), ms=4, label='Rastin (1984a)')
ax1.plot(muonEnergy, muonEnergy**3 * muonSeaTotal, '+', ms=5, color='red', label='estimation')
ax1.set_xlabel(r"muons' energy $[GeV]$")
ax1.set_ylabel(r"$E^{3} j(E, \theta) \ \ [ cm^{-2} sr^{-1} GeV^{2} s^{-1}]$")
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.legend()
ax1.set_title("diff. spectrum of vertical muons at sea level")
# fig1.savefig('./1.eps', format='eps', dpi=1200)

end = time.time()
print "\nduration = %.3f min"%((end - start)/60)
# print tabulate([["%.1f"%muonEnergy[i], "%.2E"%muonSeaTotal[i], "%.2E"%errProp[i]] for i in range(len(muonEnergy))],
#                headers=['energy', 'diff. intensity at sea level', 'error (from num. integrations)'])
# with open('a', 'w') as f:
#     f.write(tabulate([["%.1f"%muonEnergy[i], "%.2E"%muonSeaTotal[i], "%.2E"%errProp[i]] for i in range(len(muonEnergy))],
#                    headers=['energy', 'diff. intensity at sea level', 'error (from num. integrations)']))
