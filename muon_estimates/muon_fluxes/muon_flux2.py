import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font', size=8)

mass_muon   = 105.6584 * 10**-3 #GeV
estimation  = pd.DataFrame(np.genfromtxt('./muon_results.txt',
                          skip_header=2, usecols=(0,1),
                          dtype=[('E', np.float), ('jv', np.float)]))
alkofer     = pd.DataFrame({
                'E':  np.sqrt(np.array([1., 1.5, 2., 3., 5., 7., 10., 15.,
                                        20., 30., 50.,70., 100., 150., 200.,
                                        300., 500., 700., 1000])**2
                              + mass_muon**2),
                'jv': np.array([2.79*10**-3, 2.14*10**-3, 1.67*10**-3, 1.06*10**-3,
                                4.97*10**-4, 2.73*10**-4, 1.33*10**-4, 5.40*10**-5,
                                2.70*10**-5, 9.59*10**-6, 2.36*10**-6,8.92*10**-7,
                                3.04*10**-7, 8.51*10**-8, 3.35*10**-8, 8.70*10**-9,
                                1.52*10**-9, 4.71*10**-10, 1.34*10**-10])
              })

rastin      = pd.DataFrame({
                'E': np.sqrt(np.concatenate(([0.35], [0.40+.1*i for i in range(6)],
                                            [1.,1.5], [2.+1*i for i in range(9)],
                                            [15., 20., 25.],
                                            [30+10*i for i in range(8)],
                                            [150. , 200., 250.],
                                            [300+100*i for i in range(8)],
                                            [1500., 2000., 2500., 3000.]),
                                            axis=0)**2
                              + mass_muon**2),
                'jv': np.array([2.85*10**-3, 2.90*10**-3, 2.94*10**-3, 2.92*10**-3,
                                2.87*10**-3, 2.80*10**-3, 2.71*10**-3, 2.62*10**-3,
                                2.12*10**-3, 1.69*10**-3, 1.10*10**-3, 7.40*10**-4,
                                5.17*10**-4, 3.75*10**-4, 2.80*10**-4, 2.16*10**-4,
                                1.69*10**-4, 1.35*10**-4, 5.28*10**-5, 2.58*10**-5,
                                1.45*10**-5, 8.69*10**-6, 3.90*10**-6, 2.11*10**-6,
                                1.26*10**-6, 8.03*10**-7, 5.42*10**-7, 3.81*10**-7,
                                2.77*10**-7, 7.85*10**-8, 3.12*10**-8, 1.50*10**-8,
                                8.20*10**-9, 3.11*10**-9, 1.45*10**-9, 7.75*10**-10,
                                4.55*10**-10, 2.86*10**-10, 1.89*10**-10, 1.31*10**-10,
                                3.14*10**-11, 1.13*10**-11, 5.11*10**-12, 2.67*10**-12])
              })
# diff spectrum fit:  c*E**(b+a*log(E))
def fit_coeff(df):
  return np.polyfit(np.log(df['E'].values),
          np.log(df['jv'].values),
          deg=3)

def fit(df, E):
  coeff = fit_coeff(df)
  return np.exp(coeff[3]) * E**(coeff[2] + coeff[1]*np.log(E) + coeff[0]*(np.log(E))**2)

fig, ax = plt.subplots(figsize=(6,4))
# use plot not scatter when using log scale
ax.plot(alkofer['E'].values, alkofer['E'].values**3 * alkofer['jv'].values, 'o', fillstyle='none', ms=5,
        color=(0,0,.9,1), label='Alkofer et al. (1971b)', zorder=0)
ax.plot(rastin['E'].values, rastin['E'].values**3 * rastin['jv'].values, '^', fillstyle='none', ms=5,
        color=(0,.7,0,1), label='Rastin (1984a)', zorder=1)
ax.plot(estimation['E'].values, estimation['E'].values**3 * estimation['jv'].values, '+', ms=7, mew=2,
        color='r', label='estimation', zorder=2)
ax.plot(estimation['E'].values, estimation['E'].values**3 * fit(estimation, estimation['E'].values), ls='--',
        color='k', label='polyfit', zorder=2)

coeff = fit_coeff(estimation)
ax.text(2, .2*10**-2,
        r"$j_v=%.3f\ E_\mu^{\ %.3f\  %.3f \log E_\mu\ +%.3f (\log E_\mu)^2}$"\
        %(np.exp(coeff[3]), coeff[2], coeff[1], coeff[0]),
        fontdict=dict(color='black', fontsize=13))
ax.axvline(1, lw=.8, ls=':', color='k')
ax.set_xlabel(r"muon energy $E_\mu\ [GeV]$")
ax.set_ylabel(r"$E^{3}_\mu\ j(E_\mu, \theta=0) \ \ [ cm^{-2} sr^{-1} GeV^{2} s^{-1}]$")
ax.set_title("differential spectrum of vertical muons at sea level",
             fontweight='semibold')
ax.set_xscale('log')
ax.set_yscale('log')
ax.legend()

plt.show()

import os
fig.savefig(os.environ['HOME']+'/Dropbox/funk/talks/group_seminar_thomas/figs/muon_spectrum.svg', format='svg')
