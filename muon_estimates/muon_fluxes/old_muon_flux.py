# Muon flux calculations
# Lipari, Astroparticle physics 1 (1993) 195-227
# Due to some stability issue, the numerical integration at low energy is very slow,
# the whole script would take about 1 hour to run

from scipy import *
import numpy as np
import matplotlib.pyplot as py
from scipy import integrate
import scipy.constants as const
import scipy.interpolate as interp
import time

start = time.time()

# inition conditions
p0 = 1.7/(1.+0.14)
n0 = p0*0.14
a = 2.7

# rest masses - mean lifetime
mPion = 139.57018*10**-3 #GeV
tauPion = 2.6033*10**-8 #s
mKaon = 493.677*10**-3
tauKaon = 1.2384*10**-8
mKaonL = 497.611*10**-3
tauKaonL = 5.116*10**-8
mMuon = 105.6584*10**-3
tauMuon = 2.19703*10**-6

# interaction lengths in g/cm²
lProton = 86.; lNeutron = 86.; lPion = 116.; lKaon = 138.

# hadronic Z factors for a =2.7 (table 5)
Z = {'ProtonProton':0.263, 'ProtonNeutron':0.035, 'ProtonPionPlus':0.046, 'ProtonPionMinus':0.033, 'ProtonKaonPlus':0.009, 'ProtonKaonMinus':0.0028, 'ProtonKaonL':0.0059,
     'NeutronProton':0.035, 'NeutronNeutron':0.263, 'NeutronPionPlus':0.033, 'NeutronPionMinus':0.046, 'NeutronKaonPlus':0.0065, 'NeutronKaonMinus':0.0028, 'NeutronKaonL':0.0014,
     'PionPlusPionPlus':0.243, 'PionPlusPionMinus':0.028, 'PionPlusKaonPlus':0.0067, 'PionPlusKaonMinus':0.0067, 'PionPlusKaonL':0.0067,
     'PionMinusPionPlus':0.028, 'PionMinusPionMinus':0.243, 'PionMinusKaonPlus':0.0067, 'PionMinusKaonMinus':0.0067, 'PionMinusKaonL':0.0067,
     'KaonPlusKaonPlus':0.211, 'KaonPlusKaonMinus':0., 'KaonPlusKaonL':0.007,
     'KaonMinusKaonPlus':0., 'KaonMinusKaonMinus':0.211, 'KaonMinusKaonL':0.007,
     'KaonLKaonPlus':0.007, 'KaonLKaonMinus':0.007, 'KaonLKaonL':0.211}

# primary flux
# (equation 12)
L1 = lProton / (1.-Z['ProtonProton']-Z['ProtonNeutron'])
L2 = lProton / (1.-Z['ProtonProton']+Z['ProtonNeutron'])
# (equation 11)
proton = lambda x: 0.5 * ((p0+n0)*exp(-x/L1) + (p0-n0)*exp(-x/L2))
neutron = lambda x: 0.5 * ((p0+n0)*exp(-x/L1) - (p0-n0)*exp(-x/L2))

# atmosphere model (equation 110) + concrete
def rho(x, theta): #in g/cm³, x = slant depth, theta = zenith angle
    xt = 2.054*10**-3 * 6.344*10**5 * exp(-11/6.344) #troposphere's vertical depth
    # atmosphere
    if x<=0.01:
        return 0.01*cos(theta)/(6.344*10**5)
    elif x>0.01 and x<=xt:
        return x*cos(theta)/(6.344*10**5)
    elif x>xt and x<=xfSea:
        return 4.439*10**-6 * (x*cos(theta))**0.8096
    # concrete
    else:
        return 2.4

# decay lengths in g/cm² (equation 2)
lDecPion = lambda E, x, theta: const.c*10**2 *tauPion * E/mPion * rho(x, theta)
lDecKaon = lambda E, x, theta: const.c*10**2 * tauKaon * E/mKaon * rho(x, theta)

# step1: pion and kaon fluxes
# low energy meson sources
# (equation 14)
sPionPlusLowx = lambda x: (proton(x)*Z['ProtonPionPlus']/lProton + neutron(x)*Z['NeutronPionPlus']/lNeutron)
sPionMinusLowx = lambda x: (proton(x)*Z['ProtonPionMinus']/lProton + neutron(x)*Z['NeutronPionMinus']/lNeutron)
# (equation 18)
sKaonPlusLowx = lambda x: (proton(x)*Z['ProtonKaonPlus']/lProton + neutron(x)*Z['NeutronKaonPlus']/lNeutron)
sKaonMinusLowx = lambda x: (proton(x)*Z['ProtonKaonMinus']/lProton + neutron(x)*Z['NeutronKaonMinus']/lNeutron)

# high energy meson sources
# (equation 17)
LP1 = lPion / (1.-Z['PionPlusPionPlus']-Z['PionPlusPionMinus'])
LP2 = lPion / (1.-Z['PionPlusPionPlus']+Z['PionPlusPionMinus'])
# (equation 16)
sPionPlusHighx = lambda x: 0.5 * ((p0+n0)/lProton * (Z['ProtonPionPlus']+Z['ProtonPionMinus']) * (LP1*L1/(LP1-L1)) * (exp(-x/LP1) - exp(-x/L1))
                                 +(p0-n0)/lProton * (Z['ProtonPionPlus']-Z['ProtonPionMinus']) * (LP2*L2/(LP2-L2)) * (exp(-x/LP2) - exp(-x/L2)))
sPionMinusHighx = lambda x: 0.5 * ((p0+n0)/lProton * (Z['ProtonPionPlus']+Z['ProtonPionMinus']) * (LP1*L1/(LP1-L1)) * (exp(-x/LP1) - exp(-x/L1))
                                 -(p0-n0)/lProton * (Z['ProtonPionPlus']-Z['ProtonPionMinus']) * (LP2*L2/(LP2-L2)) * (exp(-x/LP2) - exp(-x/L2)))

# (equation 27)
APion = lProton**-1 * (p0+n0) * (Z['ProtonPionPlus']+Z['ProtonPionMinus']) * LP1*L1/(LP1-L1)
BPion = lProton**-1 * (p0-n0) * (Z['ProtonPionPlus']-Z['ProtonPionMinus']) * LP2*L2/(LP2-L2)
# (equation 28)
LK = np.array([lKaon/(1.-Z['KaonPlusKaonPlus']),
               lKaon/(1.-(Z['KaonPlusKaonPlus']+sqrt(2)*Z['KaonLKaonPlus'])),
               lKaon/(1.-(Z['KaonPlusKaonPlus']-sqrt(2)*Z['KaonLKaonPlus']))])
Lsigma = np.array([L1,L2, LP1, LP2])
# (equation 31)
U = 0.5 * np.array([[sqrt(2), -sqrt(2), 0.], [1,1, sqrt(2)], [1,1,-sqrt(2)]])
# (equations 23 -26)
C = np.array([[lProton**-1 * 0.5*(p0+n0) * (Z['ProtonKaonPlus']+Z['NeutronKaonPlus']) - lPion**-1 * 0.5*APion * (Z['PionPlusKaonPlus']+Z['PionMinusKaonPlus']),
                    lProton**-1 * 0.5*(p0-n0) * (Z['ProtonKaonPlus']-Z['NeutronKaonPlus']) - lPion**-1 * 0.5*BPion * (Z['PionPlusKaonPlus']-Z['PionMinusKaonPlus']),
                    lPion**-1 * 0.5*APion * (Z['PionPlusKaonPlus'] + Z['PionMinusKaonPlus']),
                    lPion**-1 * 0.5*BPion * (Z['PionPlusKaonPlus'] - Z['PionMinusKaonPlus'])],
              [lProton**-1 * 0.5*(p0+n0) * (Z['ProtonKaonMinus']+Z['NeutronKaonMinus']) - lPion**-1 * 0.5*APion * (Z['PionPlusKaonMinus']+Z['PionMinusKaonMinus']),
                    lProton**-1 * 0.5*(p0-n0) * (Z['ProtonKaonMinus']-Z['NeutronKaonMinus']) - lPion**-1 * 0.5*BPion * (Z['PionPlusKaonMinus']-Z['PionMinusKaonMinus']),
                    lPion**-1 * 0.5*APion * (Z['PionPlusKaonMinus'] + Z['PionMinusKaonMinus']),
                    lPion**-1 * 0.5*BPion * (Z['PionPlusKaonMinus'] - Z['PionMinusKaonMinus'])],
              [lProton**-1 * 0.5*(p0+n0) * (Z['ProtonKaonL']+Z['NeutronKaonL']) - lPion**-1 * 0.5*APion * (Z['PionPlusKaonL']+Z['PionMinusKaonL']),
                    lProton**-1 * 0.5*(p0-n0) * (Z['ProtonKaonL']-Z['NeutronKaonL']) - lPion**-1 * 0.5*BPion * (Z['PionPlusKaonL']-Z['PionMinusKaonL']),
                    lPion**-1 * 0.5*APion * (Z['PionPlusKaonL'] + Z['PionMinusKaonL']),
                    lPion**-1 * 0.5*BPion * (Z['PionPlusKaonL'] - Z['PionMinusKaonL'])]
             ])

# (equation 29)
def sKaonHighx(j, x):
    s=np.array([])
    for a in range(3):
        for b in range(4):
            for k in range(3):
                s=np.append(s, U[a,j] * LK[a]*Lsigma[b]/(LK[a]- Lsigma[b]) * U[a,k]*C[k,b] * (exp(-x/LK[a]) - exp(-x/Lsigma[b])))
    return sum(s)
sKaonPlusHighx = lambda x: sKaonHighx(0, x)
sKaonMinusHighx = lambda x: sKaonHighx(1, x)

# meson fluxes from these sources
# (equation 34)
pionPlus = lambda E, x, theta: sPionPlusLowx(x) * lDecPion(E,x,theta) / (1.+ sPionPlusLowx(x)*lDecPion(E,x,theta)/sPionPlusHighx(x)) * E**-a
pionMinus = lambda E, x, theta: sPionMinusLowx(x) * lDecPion(E,x,theta) / (1. + sPionMinusLowx(x)*lDecPion(E,x,theta)/sPionMinusHighx(x)) * E**-a
kaonPlus = lambda E, x, theta: sKaonPlusLowx(x) * lDecKaon(E,x,theta) / (1. + sKaonPlusLowx(x)*lDecKaon(E,x,theta)/sKaonPlusHighx(x)) * E**-a
kaonMinus = lambda E, x, theta: sKaonMinusLowx(x) * lDecKaon(E,x,theta) / (1. + sKaonMinusLowx(x)*lDecKaon(E,x,theta)/sKaonMinusHighx(x)) * E**-a

# step 2: muon fluxes
# muon sources from pion and kaon decays
# F factors
def heaviside(t, t0):
    return 1. if t>=t0 else 0.
rPion = (mMuon/mPion)**2
rKaon = (mMuon/mKaon)**2
# (equation 65 - 66)
probLeftP = lambda t: 1./(1.-rPion) * (rPion/t - rPion)
probRightP = lambda t: 1./(1.-rPion) * (1.- rPion/t)
probLeftK = lambda t: 1./(1.-rKaon) * (rKaon/t - rKaon)
probRightK = lambda t: 1./(1.-rKaon) * (1.- rKaon/t)
# (equation 67)
fPionMuonLeft = lambda t: 1./(1.-rPion) * probLeftP(t) * heaviside(t, rPion)
fPionMuonRight = lambda t: 1./(1.-rPion) * probRightP(t) * heaviside(t, rPion)
fKaonMuonLeft = lambda t: 1./(1.-rKaon) * probLeftK(t) * heaviside(t, rKaon)
fKaonMuonRight = lambda t: 1./(1.-rKaon) * probRightK(t) * heaviside(t, rKaon)

# sources (equation 36)
def sMuon(y, fMesonMuon, mesonFlux, lDecMeson):
    return 1./y**2 * fMesonMuon * mesonFlux / lDecMeson

# energy loss (equation 57)
# only from ionization, neglecting radiative loss
c1 = 2.*10**-3 #GeV/(g/cm²)
E = lambda E0, x, x0: E0 - c1 * (x-x0)
E0 = lambda Ef, xf, x0: Ef + c1 * (xf - x0)

# survival proabability (equation 56)
def probMuonSurv(E0, xf, x0, theta):
    ii = integrate.quad(lambda x: mMuon/(const.c*10**2 * tauMuon * E(E0, x, x0) *rho(x, theta) ),  x0, xf)
    return exp(-ii[0])


# (equation 58)
def muonMinusLeft(Ef, xf, theta):
    s = lambda y, x0: 0.5 * (0.999*sMuon(y, fPionMuonLeft(y), pionMinus(E0(Ef, xf, x0)/y, x0, theta), lDecPion(E0(Ef, xf, x0), x0, theta))
                            +0.669*sMuon(y, fKaonMuonLeft(y), kaonMinus(E0(Ef, xf, x0)/y, x0, theta), lDecKaon(E0(Ef, xf, x0), x0, theta)))
    p = lambda x0: probMuonSurvInterp(x0)
    return integrate.dblquad(lambda y, x0: s(y, x0) * p(x0), 0.001, xf, lambda x0: 0., lambda x0:1.)
#    return integrate.nquad(lambda y, x0: s(y, x0) * p(x0) * exp(c2*(xf-1.)), [[0, 1.], [1., xf]])
def muonMinusRight(Ef, xf, theta):
    s = lambda y, x0: 0.5 * (0.999*sMuon(y, fPionMuonRight(y), pionMinus(E0(Ef, xf, x0)/y, x0, theta), lDecPion(E0(Ef, xf, x0), x0, theta))
                            +0.669*sMuon(y, fKaonMuonRight(y), kaonMinus(E0(Ef, xf, x0)/y, x0, theta), lDecKaon(E0(Ef, xf, x0), x0, theta)))
    p = lambda x0: probMuonSurvInterp(x0)
    return integrate.dblquad(lambda y, x0: s(y, x0) * p(x0), 0.001, xf, lambda x0: 0., lambda x0:1.)
def muonPlusLeft(Ef, xf, theta):
    s = lambda y, x0: 0.5 * (0.999*sMuon(y, fPionMuonLeft(y), pionPlus(E0(Ef, xf, x0)/y, x0, theta), lDecPion(E0(Ef, xf, x0), x0, theta))
                            +0.669*sMuon(y, fKaonMuonLeft(y), kaonPlus(E0(Ef, xf, x0)/y, x0, theta), lDecKaon(E0(Ef, xf, x0), x0, theta)))
    p = lambda x0: probMuonSurvInterp(x0)
    return integrate.dblquad(lambda y, x0: s(y, x0) * p(x0), 0.001, xf, lambda x0: 0., lambda x0:1.)
def muonPlusRight(Ef, xf, theta):
    s = lambda y, x0: 0.5 * (0.999*sMuon(y, fPionMuonRight(y), pionPlus(E0(Ef, xf, x0)/y, x0, theta), lDecPion(E0(Ef, xf, x0), x0, theta))
                            +0.669*sMuon(y, fKaonMuonRight(y), kaonPlus(E0(Ef, xf, x0)/y, x0, theta), lDecKaon(E0(Ef, xf, x0), x0, theta)))
    p = lambda x0: probMuonSurvInterp(x0)
    return integrate.dblquad(lambda y, x0: s(y, x0) * p(x0), 0.001, xf, lambda x0: 0., lambda x0:1.)

# vertical muons
theta=0.
# vertical depth at sea level ~ 1030 g/cm²
# -------------- in the lab ~ 1500 g/cm² (assumed under 2 meters of concrete ~ 480 g/cm² in depth)
xfSea = 1030. #g/cm²
xfLab = 1500. #g/cm²

EMuon = [100.]
# EMuon = [1., 2.5, 5., 7.5, 10., 25., 50., 75., 100.,  300., 700., 1000., 1500., 2000., 2500., 3000.]

def muonDiffSpectra(xf, theta):
    global probMuonSurvInterp
    xx = np.linspace(0.001, xf, endpoint=True, num = 1000)
    m = np.array([])
    for Ef in EMuon:
        pp = [probMuonSurv(E0(Ef, xf, x0), xf, x0, theta) for x0 in xx]
        probMuonSurvInterp = interp.interp1d(xx, pp, kind='linear')
        mEf = [muonMinusLeft(Ef, xf, theta)[0], muonMinusRight(Ef, xf, theta)[0], muonPlusLeft(Ef, xf, theta)[0], muonPlusRight(Ef, xf, theta)[0]]
        m = np.append(m, mEf)
    m = np.reshape(m, (len(EMuon), 4))
    return m

muonSea = muonDiffSpectra(xfSea, theta)
muonLab = muonDiffSpectra(xfLab, theta)

muonSeaTotal = np.array([sum(muonSea[i,:]) for i in range(muonSea.shape[0])])
muonLabTotal = np.array([sum(muonLab[i,:]) for i in range(muonLab.shape[0])])

from tabulate import tabulate
print tabulate([[EMuon[i], "%.2E"%muonSeaTotal[i], "%.2E"%muonLabTotal[i]] for i in range(len(EMuon))], headers=['Energy', 'Sea level', 'Lab'])

end = time.time()
print "duration = %.3f"%(end - start)

# diff spectrum fit:  c*E**(b+a*log(E))
# fit parameters
# cL = np.polyfit(log(EMuon), log(muonLabTotal), 2)
# cS = np.polyfit(log(EMuon), log(muonSeaTotal), 2)
# muonLabFit =  lambda E: exp(cL[2]) * E**(cL[1]+cL[0]*log(E))
# muonSeaFit = lambda E: exp(cS[2]) * E**(cS[1]+cS[0]*log(E))
# #plot
# EMuonNew = np.logspace(log10(EMuon[0]), log10(EMuon[-1]))
# fig = py.figure(figsize=(10,6))
# ax = fig.add_axes([0.1,0.1,0.8,0.8])
# ax.plot(EMuon, muonSeaTotal, '*', color='red', label='at sea evel')
# ax.plot(EMuonNew, muonSeaFit(EMuonNew), '--', color='red', label='fit (sea)')
# ax.plot(EMuon, muonLabTotal, '+', color='blue', label='in the lab')
# ax.plot(EMuonNew, muonLabFit(EMuonNew), color='blue', label='fit (lab)')
# ax.text(3*10**1, 10**-9, r'$2.12\times10^{-3}\ E_{\mu}^{-1.062-0.195\ \log(E_{\mu})}$', color='blue')
# ax.text(5, 10**-3, r'$3.52\times10^{-3}\ E_{\mu}^{-1.221-0.183\ \log(E_{\mu})}$', color='red')
# ax.set_xlabel("muon's energy "+r'$[GeV]$')
# ax.set_ylabel("differential spectrum " r'$[cm^{-2} s^{-1} sr^{-1} GeV^{-1}]$')
# ax.set_xscale('log')
# ax.set_yscale('log')
# ax.legend()
# fig
#
# fig.savefig('./06/00.eps', format='eps', dpi=1200)
