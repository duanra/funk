# Rastin, 1984a

from scipy import *
import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from matplotlib import rc

mMuon =105.6584*10**-3
# momentum range

p = np.concatenate(([0.35], [0.40+.1*i for i in range(6)], [1.,1.5], [2.+1*i for i in range(9)], [15., 20., 25.],
                               [30+10*i for i in range(8)], [150. , 200., 250.], [300+100*i for i in range(8)],
                               [1500., 2000., 2500., 3000.]), axis=0)
E = sqrt(p**2 + mMuon**2)
diff = np.array([2.85*10**-3, 2.90*10**-3, 2.94*10**-3, 2.92*10**-3, 2.87*10**-3, 2.80*10**-3, 2.71*10**-3,
                        2.62*10**-3, 2.12*10**-3, 1.69*10**-3, 1.10*10**-3, 7.40*10**-4, 5.17*10**-4, 3.75*10**-4,
                        2.80*10**-4, 2.16*10**-4, 1.69*10**-4, 1.35*10**-4, 5.28*10**-5, 2.58*10**-5,  1.45*10**-5,
                        8.69*10**-6, 3.90*10**-6, 2.11*10**-6, 1.26*10**-6, 8.03*10**-7, 5.42*10**-7, 3.81*10**-7,
                        2.77*10**-7, 7.85*10**-8, 3.12*10**-8, 1.50*10**-8, 8.20*10**-9, 3.11*10**-9,
                        1.45*10**-9, 7.75*10**-10, 4.55*10**-10, 2.86*10**-10, 1.89*10**-10, 1.31*10**-10,
                        3.14*10**-11, 1.13*10**-11, 5.11*10**-12, 2.67*10**-12])
C = np.polyfit(log(E), log(diff), 3)
def diffFit(E): return exp(C[3]) * E**(C[2] + C[1]*log(E) + C[0]*(log(E))**2)

Enew=np.logspace(log10(E[0]),log10(E[-1]))
plt.rc('text',  usetex=True)

fig, ax = plt.subplots(figsize=(10,6))
ax.plot(E, diff, '+', ms=4, color='red')
ax.plot(Enew, diffFit(Enew), color='blue')
ax.text(E[30], diff[30], r"$%.3f\ E_\mu^{\ %.3f\  %.3f \log E_\mu\ +%.3f (\log E_\mu)^2}$"%(exp(C[3]), C[2], C[1], C[0]),
        fontdict=dict(color='black', fontsize=13))
ax.set_xscale('log')
ax.set_yscale('log')
fig
fig.savefig('./rastin.eps', format='eps', dpi=1200)

integrate.quad(diffFit, 1., infty)
# energy = np.array([1., 3.16, 10., 31.6, 100., 3.16*10**2, 10**3, 3.16*10**3, 10**4, 3.16*10**4, 10**5, 3.16*10**5, 10**6])
# flux = np.array([2.24*10**-3, 8.16*10**-4, 1.10*10**-4, 6.80*10**-6, 2.48*10**-7, 6.06*10**-9, 1.14*10**-10, 1.87*10**-12,
                    # 2.81*10**-14, 4.06*10**-16, 5.78*10**-18, 8.18*10**-20, 1.16*10**-21])
# y = np.array([math.log(flux[i+1]/flux[0], energy[i+1]) for i in range(len(energy)-1)])
# x = log(np.array([energy[i+1] for i in range(len(energy)-1)]))
# np.polyfit(x, y, 1)
# math.log(flux[1]/flux[0], energy[1])
