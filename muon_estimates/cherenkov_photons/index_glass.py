import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font', size=8)


def delta_glass(l):
  """ delta = n-1, n: refractive index
      https://refractiveindex.info/?shelf=main&book=SiO2&page=Malitson (fused silica)
      wavelength l in m, fit range (0.21 - 6.7)um
  """
  return np.sqrt(1. + 0.6961663*(l/10**-6)**2/((l/10**-6)**2-0.0684043**2)
              + 0.4079426*(l/10**-6)**2/((l/10**-6)**2-0.1162414**2)
              + 0.8974794*(l/10**-6)**2/((l/10**-6)**2-9.896161**2)) - 1.

fig, ax = plt.subplots()
l_range = np.linspace(0.2*10**-6, 0.7*10**-6)
ax.plot(l_range, delta_glass(l_range), lw=3, color='gray')
ax.set_xticklabels([int(10**9*x) for x in ax.get_xticks()])
ax.set_xlabel('wavelength [nm]', fontweight='semibold')
ax.set_ylabel('n $-$ 1', fontweight='semibold')
ax.set_title('spectral index (fused silica)', fontweight='semibold')
plt.show()
import os
fig.savefig(os.environ['HOME']+'/Dropbox/funk/talks/group_seminar_thomas/figs/index_glass.svg', format='svg')
