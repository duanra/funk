# muon fluxes and cherenkov photons
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np
import scipy.integrate as integrate
from scipy import *
import scipy.constants as const
from tabulate import tabulate
import matplotlib.ticker as mtick


# muon diff spectrum in the lab, fit from muon_flux.py
# E_sea --> E_lab +1
# per cm^2 per sr per GeV per sec
def jv(E):
    return 2*10**-3 * (E+1)**(-0.595 - 0.382*log(E+1) + 0.016*(log(E+1))**2)

# best fit from Rastin's data, rastin.py
def jv_ras(E):
    return 3*10**-3 * (E+1)**(-0.433 - 0.434*log(E+1) + 0.021*(log(E+1))**2)

# delta = n-1, n: refractive index
# https://refractiveindex.info/?shelf=main&book=SiO2&page=Malitson (fused silica)
# wavelength l in m, fit range (0.21 - 6.7)um
def deltaGlass(l):
    return sqrt(1. + 0.6961663*(l/10**-6)**2/((l/10**-6)**2-0.0684043**2)
                + 0.4079426*(l/10**-6)**2/((l/10**-6)**2-0.1162414**2)
                + 0.8974794*(l/10**-6)**2/((l/10**-6)**2-9.896161**2)) - 1.

# http://nvlpubs.nist.gov/nistpubs/jres/086/jresv86n1p27_A1b.pdf (equation 8)
# wavelength in m, pressure in Pa, temperature in degC
def deltaAir(l,P,T):
    # for standard air at P0 = 101,325 Pa and T0 = 15 degC
    # https://refractiveindex.info/?shelf=other&book=air&page=Ciddor
    def deltaS(l): return 0.05792105/(238.0185-(10**-6/l)**2) + 0.00167917/(57.362 -(10**-6/l)**2)
    return deltaS(l) * 1.055*(P/P0) / (1 + 0.055*T/T0)

# no. cherenkov photons per meter (photon's wavelength) per meter (muon's path length)
# from a single muon with energy E > E_threshold
# dN in cm, wavelength in m, energy in GeV
def dN(l,E,delta):
    return  10**-2 * 2.*pi*const.alpha/l**2 * (1. - E**2/(E**2 - mMuon**2) * (delta + 1)**-2)

# energy threshold
# if n(l) increasing within dl: E_threshold(delta(l1))
# if n(l) decreasing within dl: E_threshold(delta(l2))
def E_threshold(delta):
    return mMuon * (delta+1)/sqrt(delta**2 + 2*delta)

# input parameters
mMuon = 105.6584*10**-3  # in GeV
# range of wavelength of interest, in m
l1 = 0.16*10**-6; l2 = 0.63*10**-6
# pressure in hPa
P0 = 1013.25;
P = np.arange(900.,1100.,5);
# temperature in degC
T0 = 15. ;

# medium 1: glass
# energy threshold in glass, in GeV
E_thresholdG = E_threshold(deltaGlass(l2))
# this corresponds to a velocity threshold: const.c*sqrt(1-(mMuon/E_thresholdG)**2)
# muon intensity with energy above the threshold, in  [cm^2 s sr]^(-1)
muonG = integrate.quad(lambda E: jv(E), E_thresholdG, Infinity)[0]
# cherenkov photons intensity in glass in [cm^2 sr s cm]^(-1)
photonG = integrate.dblquad(lambda l,E: jv(E) * dN(l,E,deltaGlass(l)), E_thresholdG, Infinity, lambda E: l1, lambda E: l2)[0]
photonG_ras = integrate.dblquad(lambda l,E: jv_ras(E) * dN(l,E,deltaGlass(l)), E_thresholdG, Infinity, lambda E: l1, lambda E: l2)[0]

# medium 2: air at P = 1013.25 hPa and T = 15 degC
# energy threshold in air, in GeV
E_thresholdA = E_threshold(deltaAir(l2, P0, T0))
# this corresponds to a velocity threshold: const.c*sqrt(1-(mMuon/E_thresholdA)**2)
# muon intensity with energy above the threshold, in  [m² s sr]^(-1)
muonA = integrate.quad(lambda E: jv(E), E_thresholdA, Infinity)[0]
# cherenkov photons intensity in air in [cm^2 sr s cm]^(-1)
photonA = integrate.dblquad(lambda l,E: jv(E) * dN(l,E,deltaAir(l, P0, T0)), E_thresholdA, Infinity, lambda E: l1, lambda E: l2)[0]
photonA_ras = integrate.dblquad(lambda l,E: jv_ras(E) * dN(l,E,deltaAir(l, P0, T0)), E_thresholdA, Infinity, lambda E: l1, lambda E: l2)[0]

print tabulate([['glass (fused silica)',  "%.3f"%E_thresholdG, "%.4f"%muonG, "%.4f"%photonG, "%.4f"%photonG_ras] ,
                ['air (T=15 deg, P=1013.25 hPa)', "%.3f"%E_thresholdA, "%.4f"%muonA, "%.4f"%photonA, "%.4f"%photonA_ras]],
            headers=['medium', 'energy threshold [GeV]', 'muon flux (E>E_t) [cm^2 sr s]^(-1)', 'cherenkov photons [cm^2 sr sr cm]^(-1)', '(Rastin)'])

###########################################################################
# # photon rate in a box
# # box dimensions
a=500.; b=500.; c=500.
# # mean free path of muons within the box
l_mean = 4./3. * (a*b*c) / (a*b+c/2*(a+b))
# # effective surface: 1 horizontal + 4 vertical
eff_surface = pi/2*a*b + pi/4*c*(a+b)
print "rate of photon (P0, T0)= %.3E"%(l_mean * eff_surface * photonA)
print "rate of photon (P0, T0) (rastin) = %.3E"%(l_mean * eff_surface * photonA_ras)
###########################################################################

# MISTAKE: THE ENERGY THRESHOLD ALSO VARIES WITH THE PRESSURE - THIS IS FIXED IN THE NEW VERSION
# pressure dependence in air, at T = 15 degC
photonA_press = np.array([integrate.dblquad(lambda l,E: jv(E) * dN(l,E,deltaAir(l, Press, T0)), E_thresholdA, Infinity,
                            lambda E: l1, lambda E: l2)[0] for Press in P])
photonA_press_ras = np.array([integrate.dblquad(lambda l,E: jv_ras(E) * dN(l,E,deltaAir(l, Press, T0)), E_thresholdA, Infinity,
                            lambda E: l1, lambda E: l2)[0] for Press in P])
plt.rc('text',  usetex=True)
fig, ax = plt.subplots()
ax.plot(P, l_mean * eff_surface* photonA_press /10**6, '-', label='estimation', color='black')
ax.plot(P, l_mean * eff_surface * photonA_press_ras /10**6, '--', label="using Rastin's data", color='blue')
ax.set_xlabel('pressure of air [hPa]')
ax.set_ylabel('rate of Cerenkov photons in air [MHz]')
ax.set_title('for standard air at temperature T = 15 degC')
ax.yaxis.set_major_formatter(mtick.ScalarFormatter(useMathText=True))
ax.legend()
plt.show()
# fig.savefig('/home/naud/Dropbox/python/cherenkov_photons/in_air_vs_press.eps', format='eps', dpi=1200)


###########################################################################
# plot deltaGlass(l)
# plt.rc('text',  usetex=True)
# fig1, ax1 = plt.subplots()
# wl = np.linspace(0.2*10**-6, 0.7*10**-6, num=100)
# ax1.plot(wl, deltaGlass(wl), color='green')
# xticks = [.2*10**-6, .4*10**-6, .6*10**-6]
# xtickslabel = ['200', '400', '600']
# ax1.set_xticks(xticks)
# ax1.set_xticklabels(xtickslabel)
# ax1.set_xlabel('wavelength $\lambda$ [nm]')
# ax1.set_ylabel('$\delta(\lambda) = n(\lambda) - 1$')
# ax1.set_title('spectral index (fused silica)')
# fig1
# fig1.savefig('/home/naud/Dropbox/python/cherenkov_photons//index_glass.eps', format='eps', dpi=1200)
