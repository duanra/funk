muon flux and cherenkov photons

medium                           energy threshold [GeV]    muon flux (E>E_t) [m^2 s sr]^(-1)    cherenkov photons [m^3 s sr]^(-1)
-----------------------------  ------------------------  -----------------------------------  -----------------------------------
glass (fused silica)                              0.145                               74.01                          8.44e+06
air (T=15 deg, P=1013.25 hPa)                     4.493                               12.80                          1159.72
