# muon fluxes and cherenkov photons
import numpy as np
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.constants as const
import matplotlib.pyplot as plt
from matplotlib import rc
from tabulate import tabulate
from IPython import embed
rc('font', size=8)

m       = 105.6584*10**-3  # muon mass, GeV
l1, l2  = 0.16*10**-6, 0.63*10**-6 # range of wavelength of interest, m
P0      = 1013.25 # pressure of std air, hPa
P       = np.arange(900.,1105.,5)
T0      = 15. # temperature of std air, degC

#pmt quantum efficiency
# with open('../common/pmt/pmt_9107B', 'r') as pmt:
#     lines = [line.strip().split(',') for line in pmt]
#     for line in lines:
#         for i in range(len(line)):
#             line[i] = float(line[i])
#     lines = np.array(lines)
#     qe = interpolate.interp1d(lines[:,0]*10**-9, lines[:,1]*10**-2, kind='cubic')
pmt_data = np.loadtxt('../common/pmt/pmt_9107B')
pmt_data[:,0]  *= 10**-9 # unit m
pmt_data[:,1]  *= 10**-2 # percent
pmt_efficiency 	= interp.interp1d(pmt_data[:,0], pmt_data[:,1], kind="cubic")

# muon diff spectrum in the lab, fit from muon_flux.py
# E_sea --> E_lab +1
def jv_fit(E):
  """ per cm^2 per sr per GeV per sec """
  return 2*10**-3 * (E+1)**(-0.595 - 0.382*np.log(E+1) + 0.016*(np.log(E+1))**2)

def jv_rastin(E):
  """ best fit from Rastin's data, rastin.py """
  return 3*10**-3 * (E+1)**(-0.433 - 0.434*np.log(E+1) + 0.021*(np.log(E+1))**2)

def delta_glass(l):
  """ delta = n-1, n: refractive index
      https://refractiveindex.info/?shelf=main&book=SiO2&page=Malitson (fused silica)
      wavelength l in m, fit range (0.21 - 6.7)um
  """
  return np.sqrt(1. + 0.6961663*(l/10**-6)**2/((l/10**-6)**2-0.0684043**2)
              + 0.4079426*(l/10**-6)**2/((l/10**-6)**2-0.1162414**2)
              + 0.8974794*(l/10**-6)**2/((l/10**-6)**2-9.896161**2)) - 1.

def delta_air(l,P,T):
  """ http://nvlpubs.nist.gov/nistpubs/jres/086/jresv86n1p27_A1b.pdf (equation 8)
      wavelength in m, pressure in Pa, temperature in degC
      for standard air at P0 = 101,325 Pa and T0 = 15 degC
      https://refractiveindex.info/?shelf=other&book=air&page=Ciddor
  """
  def _delta_std(l):
    return 0.05792105/(238.0185-(10**-6/l)**2)\
            + 0.00167917/(57.362 -(10**-6/l)**2)

  return _delta_std(l) * 1.055*(P/P0) / (1 + 0.055*T/T0)

def E_threshold(delta):
  """ energy threshold
      if n(l) increasing within dl: E_threshold(delta(l1))
      if n(l) decreasing within dl: E_threshold(delta(l2))
  """
  return m * (delta+1)/np.sqrt(delta**2 + 2*delta)

def muon_intensity(jv, E_th):
  """ intensity of muons with energy above the cherenkov threshold
      in [cm^2 s sr]^(-1)
  """
  return integrate.quad(lambda E: jv(E), E_th, np.infty)[0]

def average_sin2_l(E, delta, P=P0, T=T0):
  """ average cherenkov angle over the muon energy """
  def _sin2(E, delta):
    return 1. - E**2/(E**2 - m**2) * (delta + 1)**-2

  if delta==delta_glass:
    return integrate.quad(lambda l: 10**-2 * 2*np.pi*const.alpha/l**2\
                                    * _sin2(E, delta(l)), l1,l2)[0]\
            / integrate.quad(lambda l: 10**-2 * 2*np.pi*const.alpha / l**2, l1, l2)[0]

  elif delta==delta_air:
    return integrate.quad(lambda l: 10**-2 * 2*np.pi*const.alpha/l**2\
                                    * _sin2(E, delta(l, P, T)), l1,l2)[0]\
            / integrate.quad(lambda l: 10**-2 * 2*np.pi*const.alpha/l**2, l1, l2)[0]

  else:
    return None

def average_sin2_lE(jv, E_th, delta):
  return integrate.quad(lambda E: jv(E) * average_sin2_l(E, delta), E_th, np.infty)[0]\
          / muon_intensity(jv, E_th)

def dl(l1, l2, qeff=False): # quantum efficency enters here * pmt_efficiency(l)
  if qeff:
    return integrate.quad(lambda l: 10**-2 * l**-2 * pmt_efficiency(l), l1,l2)[0]

  else:
    return integrate.quad(lambda l: 10**-2 * l**-2, l1,l2)[0]

def photon_intensity(jv, E_th, delta, l1, l2, qeff=False):
  """ cherenkov photons intensity per unit path length of muons
      in [cm^2 sr s cm]^(-1)
  """
  return 2*np.pi*const.alpha * dl(l1,l2, qeff=qeff)\
                             * average_sin2_lE(jv, E_th, delta)\
                             * muon_intensity(jv, E_th)

qeff = False
# medium 1: glass
# energy threshold in glass, in GeV
E_th_glass = E_threshold(delta_glass(l2))
# velocity threshold: const.c*np.sqrt(1-(m/E_th_glass)**2)
photon_glass_fit    = photon_intensity(jv_fit, E_th_glass, delta_glass, l1, l2, qeff=qeff)
photon_glass_rastin = photon_intensity(jv_rastin, E_th_glass, delta_glass, l1, l2, qeff=qeff)

# medium 2: air at P = 1013.25 hPa and T = 15 degC
# energy threshold in air, in GeV
E_th_air = E_threshold(delta_air(l2, P0, T0))
# velocity threshold: const.c*np.sqrt(1-(m/E_th_air)**2)
photon_air_fit    = photon_intensity(jv_fit, E_th_air, delta_air, l1, l2, qeff=qeff)
photon_air_rastin = photon_intensity(jv_rastin, E_th_air, delta_air, l1, l2, qeff=qeff)

print tabulate([['glass (fused silica)',
                 '%.3f'%E_th_glass,
                 '%.4f'%muon_intensity(jv_fit, E_th_glass),
                 '%.4f'%muon_intensity(jv_rastin, E_th_glass),
                 '%.4f'%photon_glass_fit,
                 '%.4f'%photon_glass_rastin],
                ['air (T=15 deg, P=1013.25 hPa)',
                 '%.3f'%E_th_air,
                 '%.4f'%muon_intensity(jv_fit, E_th_air),
                 '%.4f'%muon_intensity(jv_rastin, E_th_air),
                 '%.4f'%photon_air_fit,
                 '%.4f'%photon_air_rastin]
               ],
               headers=
               ['medium',
                'energy threshold [GeV]',
                'muon flux (E>E_t) [cm^2 sr s]^(-1)',
                '(Rastin)',
                'cherenkov photons [cm^2 sr sr cm]^(-1)',
                '(Rastin)'])

##########################################################
# jv = jv_fit
# photon_glass_intensity = photon_glass_fit
# photon_air_intensity = photon_air_fit

jv = jv_rastin
photon_glass_intensity = photon_glass_rastin
photon_air_intensity = photon_air_rastin

# dimension pmt window
print '\n##### GLASS'
a1 = 0.1
b1 = 2.5
c1 = 2.5
# a=0.2; b=5.; c=5.
l_mean1 = 4./3 * a1*b1*c1 / (a1*b1 + (c1/2)*(a1+b1))
A_eff1  = np.pi/2 * (a1*b1 + (c1/2)*(a1+b1))
coeff1  = 2*np.pi*const.alpha * average_sin2_lE(jv, E_th_glass, delta_glass) * dl(l1, l2, qeff=qeff)

print "muon's mean free path = %.2f cm"%l_mean1
print "rate of cherenkov photons produced in the PMT's window = %.3f Hz"%(l_mean1 * A_eff1 * photon_glass_intensity)
print "R_muons = %.3f Iv_muons = %.3f Hz"%(A_eff1, A_eff1*muon_intensity(jv, E_th_glass))
print "R_photon_glass = %.3f l_mean R_muons = %.3f R_muons= %.3f Iv_muons"%(coeff1, coeff1*l_mean1 , coeff1*l_mean1*A_eff1)


# in air
print '\n##### AIR'
a2 = 498.
b2 = 435.
c2 = 430.
l_mean2 = 4./3 * a2*b2*c2 / (a2*b2 + (c2/2)*(a2+b2))
A_eff2  = np.pi/2 * (a2*b2 + (c2/2)*(a2+b2))
coeff2  = 2*np.pi*const.alpha * average_sin2_lE(jv, E_th_air, delta_air) * dl(l1, l2, qeff=qeff)

print "muon's mean free path = %.2f cm"%l_mean2
print "rate of cherenkov photons produced in the air = %.3E Hz"%(l_mean2 * A_eff2 * photon_air_intensity)
print "R_muons = %.3E Iv_muons = %.3f Hz"%(A_eff2, A_eff2*muon_intensity(jv, E_th_air))
print "R_photon_air = %.3f l_mean R_muons = %.3E R_muons= %.3E Iv_muons"%(coeff2, coeff2*l_mean2 , coeff2*l_mean2*A_eff2)


# # in air
# print '\n##### AIR (reduced volume)'
# a3 = 33.
# b3 = 65.
# c3 = 92.
# l_mean3 = 4./3 * a3*b3*c3 / (a3*b3 + (c3/2)*(a3+b3))
# A_eff3  = np.pi/2 * (a3*b3 + (c3/2)*(a3+b3))
# coeff3  = 2*np.pi*const.alpha * average_sin2_lE(jv, E_th_air, delta_air) * dl(l1, l2, qeff=qeff)

# print "muon's mean free path = %.2f cm"%l_mean3
# print "rate of cherenkov photons produced in the air = %.3E Hz"%(l_mean3 * A_eff3 * photon_air_intensity)
# print "R_muons = %.3E Iv_muons = %.3f Hz"%(A_eff3, A_eff3*muon_intensity(jv, E_th_air))
# print "R_photon_air = %.3f l_mean R_muons = %.3E R_muons= %.3E Iv_muons"%(coeff3, coeff3*l_mean3 , coeff3*l_mean3*A_eff3)

# (muon_intensity(jv_rastin, E_th_glass) - muon_intensity(jv_fit, E_th_glass)) / (muon_intensity(jv_rastin, E_th_glass))

##########################################################
# note: in the previous version, didn't take into account the variation of the energy thereshold with the pressure
# pressure dependence in air, at T = 15 degC
# THIS IS WRONG ANYWAY, delta_air never takes the new pressure here (very bad implementation anyway)

photon_air_press = np.array([photon_intensity(jv_rastin, E_threshold(delta_air(l2, p, T0)), delta_air, l1, l2)
                            for p in P])
fig, ax = plt.subplots()
R = l_mean2 * A_eff2 * photon_air_press / 10**3
gradient = (R[-1] - R[0]) / (P[-1] - P[0])
ax.plot(P, R, '-', lw=3, label='gradient ~ %.1E'%gradient)
ax.set_xlabel('pressure of air [hPa]', fontweight='semibold')
ax.set_ylabel('rate of Cerenkov photons in air [KHz]', fontweight='semibold')
ax.set_title('standard air at temperature T = 15 degC', fontweight='semibold')
ax.legend()
plt.show()

# import os
# fig.savefig(os.environ['HOME']+'/Dropbox/funk/talks/group_seminar_thomas/figs/in_air_vs_press.svg', format='svg')
# embed()