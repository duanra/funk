# from scipy import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
from datetime import datetime

def find_files(pattern, path):
    res = []
    for root, dirs, files, in os.walk(path):
        for file in files:
            if pattern in file:
                res.append(os.path.join(root, file))
    return res

def read_all_files(filenames, columns, time_index=0):
    if not isinstance(filenames, list):
        filenames = [filenames]
    ls = []
    for file in filenames:
        with open(file, 'r') as f:
            lines = [line.strip().split(' ') for line in f]
            new_lines = []
            for line in lines:
                new_lines.append([float(line[i]) for i in columns])
            ls.append([float(lines[0][time_index]), new_lines])
    ls.sort(key=lambda x: x[0])
    all = []
    for day in ls:
        all = all + day[1]
    return np.array(all)

def compute_rates(data, time_index=0):
    res = []
    jj = [j for j in range(data.shape[1]) if j!= time_index]
    for i in range(data.shape[0]-1):
        dt = data[i+1,time_index] - data[i,time_index]
        # take care of the discontinuity in the data
        if dt<=62.:
            ls = [data[i+1,time_index]]
            for j in jj:
                ls.append((data[i+1,j] - data[i,j])/dt)
            res.append(ls)
        else:
            pass
    new_res = []
    # the counter may be reset at some point or the High voltage need to be adjusted
    for r in res:
        if not(r[1]<=50 or r[2]<=10 or r[3]<=0):
            new_res.append(r)
    return np.array(new_res)

def select_data(time1, time2, data, time_index=0):
    res = []
    def find_index(time):
        i=0
        for d in data:
            if round(d[time_index], 2)==round(time, 2): return i
            else: i+=1
        if i==len(data): raise Exception("index not found")
    i1 = find_index(time1)
    i2 = find_index(time2)
    return data[i1:i2+1]

#len(rr) compute mean rates every 5 minutes
def new_rates(data, time_index=0, timing=310):
    res = []
    current_timing = 60.
    time_zero = data[0, time_index]
    arr = []
    jj = [j for j in range(data.shape[1]) if j!= time_index]
    # T = []
    for i in  range(data.shape[0]):
        current_timing += data[i,time_index] - time_zero
        if current_timing<=timing:
            arr.append(data[i])
            time_zero = data[i,time_index]
        else:
            arr = np.array(arr)
            ls = [arr[-1,0]]
            for j in jj:
                ls.append(np.mean(arr[:,j]))
            res.append(ls)
            # T.append(arr[:, 0])

            current_timing = 60.
            time_zero = data[i, time_index]
            arr = []
            arr.append(data[i])
    # for the remaining elements
    if len(arr)!=0:
        arr = np.array(arr)
        ls = [arr[-1,0]]
        for j in jj:
            ls.append(np.mean(arr[:,j]))
        res.append(ls)
        # T.append(arr[:, 0])
    return np.array(res) #, np.array(T)

# deal with the discontinuity of the measurement
def discontinued_rates(data, time_index=0, timing=62.):
    res = []
    i = 0
    for j in range(len(data)-1):
        if data[j+1,time_index]-data[j,time_index]>timing:
            res.append(data[i:j+1])
            i=j+1
        elif j==len(data)-2 and data[j+1,time_index]-data[j,time_index]<=timing:
            res.append(data[i:-1])
        elif j==len(data)-2 and data[j+1,time_index]-data[j,time_index]>timing:
            res.append(data[i:j+1])
            res.append(data[j+1])
        else:
            pass
    return res

def find_time(timestamp, data, time_index=0):
    return [[datetime.fromtimestamp(d[time_index]).strftime('%Y-%m-%d %H:%M:%S'), d] for d in data if round(d[time_index], 2)==round(timestamp, 2)][0]

def to_local_date(timestamps):
    if isinstance(timestamps, (int, long, float)):
        return datetime.fromtimestamp(timestamps)
    else:
        return [datetime.fromtimestamp(t) for t in timestamps]

def find_closest_time(timestamp, data, time_index=0):
    return round(min(data[:, time_index], key=lambda x: abs(x-timestamp)), 2)

###############################################################################
counts_files = find_files('rate', '/home/naud/Dropbox/python/muon_rate/rate/')
# counts_files = find_files('rate', '/cr/users/arnaud/Dropbox/python/muon_rate/rate')
all_counts = read_all_files(counts_files, range(0,4))
all_rates = compute_rates(all_counts)

# average muon rates from 07-08-2017, every one minute
t_initial = 1502087584.; t_final = all_rates[-1,0]
selected_rates = select_data(t_initial, t_final, all_rates)
t = selected_rates[:,0]
v_rates = selected_rates[:,2]
h_rates = selected_rates[:,3]

# average muon rates, every five minutes
new_selected_rates = new_rates(selected_rates)

# all plots
fig, ax = plt.subplots(2, sharex=True, figsize=(10,6))
rates = discontinued_rates(new_selected_rates, timing=310.)

for r in rates:
    new_t = r[:,0]
    new_v_rates = r[:,2]
    new_h_rates = r[:,3]
    ax[0].plot(to_local_date(new_t), new_v_rates, color='#1f77b4')
    ax[1].plot(to_local_date(new_t), new_h_rates, color='#8c564b')
    # ax[0].plot(new_t, new_v_rates, color='C3')
    # ax[1].plot(new_t, new_h_rates, color='C2')
# change properties of x-xis
ax[0].spines['bottom'].set_visible(False)
ax[0].set_xticklabels([])
ax[0].get_xaxis().tick_top()
ax[0].tick_params(labeltop='off')
ax[0].set_ylabel('muon rate [Hz] \n (vertical coincidence)')
ax[0].set_title('average muon count rates (every 5 minutes)')

ax[1].spines['top'].set_visible(False)
# alternative to sharex, not really needed 'cause x is both axis have the same x-values
# ax[1].get_shared_x_axes().join(ax[1][0], ax[0][0])
ax[1].set_ylabel('muon rate [Hz] \n (horizontal coincidence)')

# ax[1][0].xaxis.set_minor_locator(mdates.HourLocator(interval=4))
ax[1].xaxis.set_major_locator(mdates.DayLocator(interval=2))
ax[1].xaxis.set_major_formatter(mdates.DateFormatter('%b-%d'))
# for label in ax[1].get_xticklabels():
#    label.set_rotation(20)
ax[1].set_xlabel('local time')
fig.tight_layout()

plt.show()
fig.savefig('/home/naud/Dropbox/python/muon_rate/muon_rate.pdf', format='pdf', dpi=1200)

# mean, std
# print "mean = %.2f"%np.mean(v_rates)
# print "std = %.2f"%np.std(v_rates, ddof=0)
