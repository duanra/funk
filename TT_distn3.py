#!usr/bin/env python3

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt

from array import array
from itertools import chain
from IPython import embed
from matplotlib.ticker import NullFormatter, LogLocator
# https://github.com/matplotlib/matplotlib/issues/8027

from mod.cymodule.TT_distn import get_mask
from mod.funk_consts import rcPaper, rcPoster
from mod.funk_manip import apply_cuts
from mod.funk_plt import show, order_legend, add_toplegend
from mod.funk_utils import timing, handle_warnings, Namespace
from mod.funk_ROOTy import TH1Wrapper, plot_th1, plot_tf1

plt.rcParams.update(rcPaper)

np.set_printoptions(linewidth=150)
pd.set_option('display.max_rows', 5)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# @timing
def get_deltas(df):
	""" get interarrival-times of triggers """

	_, counts = np.unique(df.index.values, return_counts=True)
	ttags 		= np.split(df['ttag'].values, np.cumsum(counts)[:-1])

	n 	 = len(df) - len(counts)
	ret  = np.fromiter(
					chain.from_iterable( np.diff(ttag) for ttag in ttags ),
					dtype=float, count=n
					)

	return ret


# @timing
def selective_count(df, deadtime=0.1):
	""" implement counting dead time [secs],
			pulse-cuts can be done externally
	"""

	if deadtime is None or deadtime==0:
		return df

	else:

		_, counts = np.unique(df.index.values, return_counts=True)
		ttags 		= np.split(df['ttag'].values, np.cumsum(counts)[:-1])
		idxs 			= get_mask(ttags, deadtime)

		return df.iloc[idxs]


@handle_warnings
def expon_cdf(rvs, lam, deadtime=None):
	""" lam is the mean of (poissonian) event rate """

	if deadtime is not None:
		rvs -= deadtime
		# np.exp(-lam*deadtime) - np.exp(-lam*rvs)

	return 1 - np.exp(-lam*rvs)


class Deltas:

	def __init__(self, df, deadtime=None):

		self.__name__ 	 = 'deltas' # -required in TH1Fitter
		self.df_original = df
		self.nbins 			 = 100
		self.density 		 = True

		self.update(deadtime)


	def __call__(self, x, deadtime, N=1):
		""" for fitting purpose """

		if (deadtime is not None) and (self.deadtime != deadtime):
			self.update(deadtime)

		i 	= self.th1.FindBin(x)
		ret = N * self.th1.GetBinContent(i)
		
		return ret


	def update(self, deadtime):

		df 		 = selective_count(self.df_original, deadtime)
		deltas = get_deltas(df)
		lam 	 = df.groupby('time').size().mean() / 60
		if deadtime is not None:
			lam /= (1 - lam * deadtime) # correction

		self.deadtime 	 = deadtime
		self.nevents 		 = len(df)
		self.lam 				 = lam
		self.transformed = expon_cdf(deltas, lam, deadtime)
		self.th1 				 = TH1Wrapper(
												self.transformed,
												self.nbins, 0, 1,
												self.density )


	def get_stats(self, deadtime, nevents_ref=1):

		print('deadtime = %e'%deadtime)
		if self.deadtime != deadtime:
				self.update(deadtime)

		nbins  = self.nbins				
		ys 		 = np.array([ self.th1.GetBinContent(i) for i in range(1, nbins+1) ])
		ys_err = np.array([ self.th1.GetBinError(i) for i in range(1, nbins+1) ])

		MSD 	 = np.sum( (1 - ys)**2 ) / nbins # mean squared deviation
		DKL_PQ = np.sum( -np.log(ys) ) / nbins # Kullback–Leibler divergence
		DKL_QP = np.sum( (ys * np.log(ys)) ) / nbins
		chi2 	 = np.sum( ((1 - ys)/ys_err)**2 ) / len(ys) # average chi2

		return MSD, DKL_PQ, DKL_QP, chi2, self.nevents / nevents_ref


def nevents_corrected(counts_mean, deadtime):

	return counts_mean / ( 1 + counts_mean*deadtime/60)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

cuts = Namespace([
	('riselim', (0,)),
	('Slim', (2.08, 3.56)),
	('tsiglim', (3.34, 10.44))
	])


def make_plots(df, lw=None, color=None, label=None):
	
	dts = np.logspace(-9, 0, 200) # deadtime

	deltas = Deltas(df, deadtime=0)
	nevents_ref = deltas.nevents

	df_wcuts = apply_cuts(df, cuts)
	deltas_wcuts = Deltas(df_wcuts, deadtime=0)
	nevents_ref_wcuts = deltas_wcuts.nevents


	plt.figure('ttag distsn')
	# plot_th1(
	# 	deltas.th1, endlines=True, fillprops=None,
	# 	lw=lw, color=color, label=label
	# )
	plot_th1(
		deltas_wcuts.th1, endlines=True, fillprops=None,
		lw=lw, color=color, label=label
	)


	print('selective counts on raw pulses')
	MSDs, DKL_PQs, DKL_QPs, chi2s, nevents\
		= np.array([ deltas.get_stats(dt, nevents_ref) for dt in dts ]).T

	print('selective counts selected pulses')
	MSDs_wcuts, DKL_PQs_wcuts, DKL_QPs_wcuts, chi2s_wcuts, nevents_wcuts\
		= np.array([ deltas_wcuts.get_stats(dt, nevents_ref) for dt in dts ]).T

	plt.figure('MSDs')
	MSD_ref = np.max(MSDs)
	plt.plot(dts, MSDs/MSD_ref, lw=lw, color=color, label=label)
	plt.plot(dts, MSDs_wcuts/MSD_ref, ls='--', lw=lw, color=color)

	plt.figure('nevents')
	plt.plot(dts, nevents, lw=lw, color=color, label=label)
	plt.plot(dts, nevents_wcuts, ls='--', lw=lw, color=color)


df = pd.read_pickle('./data/timetag/pulse_ttag_1535701375.sdf') # full
make_plots(df, lw=1, color='k', label='open PMT')

df = pd.read_pickle('./data/timetag/pulse_ttag_13-26.sdf')
counts = df.groupby('time').size().values
time 	 = df.index.unique().values
trunc  = counts[:600].sum() # truncate 10 hours out
df = df.iloc[trunc:]
make_plots(df, lw=0.5, color='red', label='sealed PMT')


plt.figure('ttag distsn')
plt.xlabel('$1-\exp{[-\lambda (t-\\tau_\mathrm{dead})]}$')
plt.ylabel('pdf')
plt.yscale('log')
add_toplegend(labels=['open PMT', 'sealed PMT'], ncol=2)
plt.grid(which='major', ls=':', lw=0.5)
plt.tight_layout(pad=0)
plt.subplots_adjust(left=0.15)
plt.savefig('./plt/ttag_distn')


plt.figure('MSDs')
plt.xlabel(r'$\tau_\mathrm{dead}$/s')
plt.xscale('log')
plt.xticks([ 10**x for x in range(-9,1,3) ])
plt.ylabel('MSD')
add_toplegend(labels=['open PMT', 'sealed PMT'], ncol=2)
plt.grid(which='major', ls=':', lw=0.5)
plt.tight_layout(pad=0)
plt.subplots_adjust(left=0.15)
plt.savefig('./plt/ttag_MSD_scan.pdf')


plt.figure('nevents')
plt.xlabel(r'$\tau_\mathrm{dead}$/s')
plt.xscale('log')
plt.xticks([ 10**x for x in range(-9,1,3) ])
plt.ylabel('fraction of data')
plt.yticks([ 0.2, 0.4, 0.6, 0.8, 1 ])
add_toplegend(labels=['open PMT', 'sealed PMT'], ncol=2)
plt.grid(which='major', ls=':', lw=0.5)
plt.tight_layout(pad=0)
plt.subplots_adjust(left=0.15)
plt.savefig('./plt/ttag_frac_scan.pdf')

plt.close()

embed()