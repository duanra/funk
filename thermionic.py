#!usr/bin/env python3

import numpy as np 
import matplotlib.pyplot as plt 
import mod.funk_consts as fcn

from scipy.interpolate import interp1d
from IPython import embed

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (165/72, 145/72)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

data 	= np.loadtxt(
					'./data/thermionic/richardson.txt', delimiter=','
				) # 1/T, log(N/T**2)
A 	 	= 1
Ts 	 	= 1 / data[:,0] # in K
Ns 	 	= A * Ts**2 * np.exp(data[:,1]) # in unit of A
Nfit 	= interp1d(Ts, Ns, kind='cubic')

Tvals = np.linspace(10, 20) + 273.15
Nvals = Nfit(Tvals)
Nref  = 1.48 # spe rates for v31 (totally closed)
# Nref 	= 2.438 # Hz--> in find.signal.py rates_true[ixs['out_closed']].mean()
Anew  = Nref / np.mean(Nvals)

Tgrad = Anew * np.mean(np.gradient(Nvals, Tvals))
# Tgrad = Anew * (Nvals[-1]-Nvals[0]) / (Tvals[-1]-Tvals[0])


plt.figure('richardson plot')
plt.plot(1/Ts, Ns, lw=2, color='darkblue')
plt.axvline(1/Tvals[0], ls='--', color='k')
plt.axvline(1/Tvals[-1], ls='--', color='k')
plt.xlabel(r'$T^{-1}/\mathrm{K}^{-1}$')
plt.ylabel(r'$\lg(\phi_\mathrm{th}/AT^2)$')

plt.tight_layout(pad=0.05)
# plt.savefig('./plt/12/thermionic_coates')


plt.figure('FUNK PMT')
plt.plot(
	Tvals - 273.15, Anew * Nvals, lw=2, color='darkgreen',
	label= 'gradient: {:.3f} Hz/$^\circ$C'.format(Tgrad)
)
plt.xlabel(r'$T/^\circ\mathrm{C}$')
plt.ylabel(r'$\phi_\mathrm{th}/\mathrm{Hz}$')
# plt.legend(loc='upper left')
plt.tight_layout(pad=0.05)
# plt.savefig('./plt/12/thermionic_rescaled')

plt.show()
plt.close()
embed()