#!usr/bin/env python3
""" solve the system for increasing values of dt --> check convergence """

import numpy as np
import numpy.linalg as nalg
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from datetime import timedelta
from matplotlib import rc
from scipy.interpolate import interp1d
from mod.funk_plt import show
from mod.funk_run import Run
from mod.funk_utils import table_lr, posix2utc, pickle_load

rc('font', size=8)
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def get_data(run, pos, trend=None):

	df = run.get_df(pos)[['time', 'rates']].reset_index(drop=True)

	if trend is not None:
		df['rates'] -= trend[pos]['rates']

	df = df.assign(
						timedelta = posix2utc(df['time']) - \
												posix2utc(df['time'].iloc[0])
						# note: this is relative to the starting of the measurement at pos
						)

	return df


def get_Bint(**kwargs):
	""" kw options are spu, trig and cuts """

	run = Run.configs(run_v='v31', **kwargs)
	df  = run.get_df('in_closed')[['time', 'rates']]

	return len(df), df['rates'].mean(), df['rates'].std()


def get_trend(run, df_trends, fcut):

	idxcut  = np.argmin(np.abs( df_trends['fcut'].values - fcut ))
	# print(df_trends['fcut'].iloc[idxcut])
	ifft_lf = interp1d(
			df_trends['time'].iloc[idxcut],
			df_trends['ifft_lf'].iloc[idxcut],
			kind='quadratic',
			fill_value='extrapolate')

	ret = {}
	for pos in ['in_open', 'in_closed', 'out_open', 'out_closed']:
		time = run.get_df(pos)['time'].values
		ret[pos] = pd.DataFrame({'time': time, 'rates' : ifft_lf(time)})

	return ret



def get_matrices(rates, varns):

	base_a = np.array([
							[1, 1, 0,  0],
							[0, 1, 0, -1],
							[0, 0, 1,  0],
							[0, 0, 1, -1]
							], dtype=np.float)

	base_b = np.array([1, 0, 1, 0], dtype=np.float)

	a, b 		 = base_a.copy(), base_b.copy()
	a[:,-1] *= rates
	b[:] 		*= rates

	var_a, var_b  = np.abs(base_a.copy()), base_b.copy()
	var_a[:,:-1] *= 0 # constants
	var_a[:,-1]	 *= varns
	var_b[:]		 *= varns

	return a, b, var_a, var_b


def solve_rates(a, b, var_a, var_b):

	def _var_inv(mInv, var_m):
		return nalg.multi_dot([mInv**2, var_m, mInv**2])

	aInv 		 	= nalg.inv(a)
	var_aInv 	= _var_inv(aInv, var_a)

	solns 		= np.dot(aInv, b)
	var_solns = np.dot(aInv**2, var_b) + np.dot(var_aInv, b**2)

	return solns, var_solns



seq   		= ['in_open', 'in_closed', 'out_open', 'out_closed']
cuts  		= fcn.cuts.select('dtlim')
trig 			= True
run_v 		= 'v25'
run   		= Run.configs(run_v, trig=trig, cuts=cuts)
Bint  		= get_Bint(trig=True, cuts=cuts)
df_trends = pickle_load('v25_trends.dfpkl')
trend 		= get_trend(run, df_trends, fcut=2)


hduration = int(np.ceil(run.duration.total_seconds() / (60 * 60)))
dtseries 	= [timedelta(hours=6+h) for h in range(0, hduration, 6)] 
# print(list(range(0, hduration, 6)))
# if (dtseries[-1].total_seconds() / (60 * 60)) < hduration:
# 	dtseries.append(timedelta(hours=hduration))

data 			= {pos : get_data(run, pos, trend=trend) for pos in seq}
n 				= len(dtseries)
meanrates = {pos: np.zeros(n) for pos in seq}
varmeans 	= {pos: np.zeros(n) for pos in seq}
signal 		= np.zeros((n,2))
Bair_in		= np.zeros((n,2))
Bair_out	= np.zeros((n,2))
shutter		= np.zeros((n,2))


for i, dt in enumerate(dtseries):
	for pos in seq:
		df 		= data[pos]
		subdf = df[df['timedelta'] <= dt]
		meanrates[pos][i] = subdf['rates'].mean() - Bint[1]
		varmeans[pos][i]  = subdf['rates'].var() / len(subdf) +\
												Bint[2]**2 / Bint[0]

	rates = np.array([
 						meanrates[seq[0]][i], meanrates[seq[1]][i],
 						meanrates[seq[2]][i], meanrates[seq[3]][i],
 						])
	varns = np.array([
 						varmeans[seq[0]][i], varmeans[seq[1]][i],
 						varmeans[seq[2]][i], varmeans[seq[3]][i],
 						])

	a, b, var_a, var_b = get_matrices(rates, varns)
	solns, var_solns 	 = solve_rates(a, b, var_a, var_b)
	
	signal[i,0], signal[i,1] 		 = solns[0], np.sqrt(var_solns[0])
	Bair_in[i,0], Bair_in[i,1] 	 = solns[1], np.sqrt(var_solns[1])
	Bair_out[i,0], Bair_out[i,1] = solns[2], np.sqrt(var_solns[2])
	shutter[i,0], shutter[i,1] 	 = 1 - 1 / solns[3], \
																 np.sqrt(var_solns[3]) / solns[3]**2


fig, ax 	= plt.subplots(2, 2, figsize=(8,5))
mdtseries = [dt.total_seconds() / (60*60*24) for dt in dtseries]

def add_errorband(soln, ax=None, **kwargs):
	kwargs.setdefault('alpha', 0.3)
	kwargs.setdefault('lw', 0)
	if ax is None: ax = plt.gca()
	ax.fill_between(mdtseries,
									soln[:,0] - soln[:,1],
									soln[:,0] + soln[:,1], **kwargs)


ax[0,0].plot(mdtseries, signal[:,0], color='r', lw=0.8, label='signal')
add_errorband(signal, ax[0,0], color='r')
ax[0,0].set_xlabel('days')
ax[0,0].set_ylabel('rates [Hz]')
ax[0,0].legend(loc='upper right')


ax[0,1].plot(mdtseries,  shutter[:,0], color='green', lw=0.8, label='shutter')
add_errorband(shutter, ax[0,1], color='green')
ax[0,1].set_xlabel('days')
ax[0,1].set_ylabel('shutter efficiency')
ax[0,1].legend(loc='upper right')


ax[1,0].plot(mdtseries,  Bair_in[:,0], color='k', lw=0.8, label='Bair_in')
add_errorband(Bair_in, ax[1,0], color='k')
ax[1,0].set_xlabel('days')
ax[1,0].set_ylabel('rates [Hz]')
ax[1,0].legend(loc='upper right')


ax[1,1].plot(mdtseries,  Bair_out[:,0], color='dimgrey', lw=0.8, label='Bair_out')
add_errorband(Bair_out, ax[1,1], color='dimgrey')
ax[1,1].set_xlabel('days')
ax[1,1].set_ylabel('rates [Hz]')
ax[1,1].legend(loc='upper right')


fig.tight_layout()
# fig.savefig('./3/print/%s_rates_dtseries.pdf'%run_v, format='pdf')
show()

print(signal[-1,:])
print(Bair_in[-1,:])
print(Bair_out[-1,:])
print(shutter[-1,:])