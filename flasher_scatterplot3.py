""" use for flasher optimal l-setting """
#!usr/bin/env python3

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from matplotlib.colors import LogNorm
from matplotlib.patches import Rectangle
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from mod.funk_run import Flasher
from mod.funk_plt import setup_scatterplot, show
from mod.funk_manip import apply_cuts
from mod.funk_utils import Namespace

plt.rcParams.update(fcn.rcThesis)
pd.set_option('display.max_rows', 10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

run = Flasher('v08f', split=True)
df = run.data['v08_bunker_flasher_l170_h0']

df['h'] 	 = 10**df['h'].values
df['q'] 	 = 10**df['q'] / 1e8 
df['tvar'] = np.sqrt(df['tvar'].values)
df.rename(
	columns={'h': 'H', 'q': 'Q', 'tvar': 'tsig'},
	inplace=True
)


labels = ['S', 'H', 'Q', 'tsig']
grid   = 2*(len(labels), )
labels_grid = np.array([
								'%s-%s'%(a,b) for a in labels for b in labels
							]).reshape(*grid).T

plot_props = {
	
	'S' : {
		'axislim'		: (1, 5),
		'histrange'	: (1, 5),
		'label'			: '$S$',
		'axisticks'	: (1.5, 2.5, 3.5, 4.5)
	},

	'H' : {
		'axislim' 	: (-0.2, 1.8),
		'histrange'	: (0, 1.8),
		'label'			: '$H$/V',
		'axisticks'	: (0, 0.5, 1, 1.5)
	},

	'Q' : {
		'axislim' 	: (-5, 35),
		'histrange'	: (0, 35),
		'label'			: '$Q/10^8e$',
		'axisticks'	: (0, 10, 20, 30)
	},

	'tsig' : {
		'axislim' 	: (-5, 55),
		'histrange'	: (0, 55),
		'label'			: '$\sigma_t$/ns',
		'axisticks'	: (0, 20, 40)
	},

}


fig, axes, caxes = setup_scatterplot(grid, figsize=(360/72, 360/72))
fig.canvas.set_window_title('flasher run')


for (i,j), label in np.ndenumerate(labels_grid):

	if j > i: # upper triangle
		pass

	elif j == i:
		ax = axes[(i,i)]
		
		xvar, _ = label.split('-')
		xpp = plot_props[xvar]

		ax.hist(
			df[xvar].values, 100, xpp['histrange'], False,
			histtype='step', color='k', lw=1.2
		)
		
		ax.set_xlim(*xpp['axislim'])
		ax.set_xticks(xpp['axisticks'])
		
		# ax.set_ylabel('histogram', labelpad=12, rotation=-90)
		ax.yaxis.set_label_position('right')
		ax.set_yscale('log')
		ax.set_yticks([1e1, 1e2, 1e3])
		ax.set_ylim(top=2e3)
		ax.tick_params(left=False, top=False)
		ax.tick_params(which='minor', left=False)

		if i == grid[0]-1: # last row
			ax.set_xlabel(xpp['label'])			

	else: # j<i
		ax 	= axes[(i,j)]
		cax = caxes[(i,j)]
		cax.remove() # do not create axes manually --> use inset_axes

		xvar, yvar = label.split('-')
		xs, ys 		 = df[xvar].values, df[yvar].values
		xpp, ypp	 = plot_props[xvar], plot_props[yvar]

		h2d = ax.hist2d(
						xs, ys, bins=100, range=(xpp['histrange'], ypp['histrange']),
						cmap='viridis', norm=LogNorm()
					)
		ax.tick_params(right=False)

		cax  = inset_axes(
						ax, width='5%', height='75%',
						loc='center right',
						axes_kwargs={'zorder':100},
						borderpad=0
					 )
		cax.tick_params(
			axis='both', direction='in', length=2,
			left=False, labelleft=False,
			bottom=False, labelbottom=False,
			labelsize='smaller'
		)
		cax.tick_params(
			which='minor', direction='in', length=1.5
		)

		cbar = ax.figure.colorbar(
						mappable=h2d[3], cax=cax, orientation='vertical',
						ticklocation='left', ticks=[1, 1e1, 1e2, 1e3]
					 )
		cax.yaxis.set_minor_locator(plt.NullLocator())


		ax.set_xlim(*xpp['axislim'])
		ax.set_xticks(xpp['axisticks'])

		ax.set_ylim(*ypp['axislim'])
		ax.set_yticks(ypp['axisticks'])

		if i == grid[0]-1: # last row
			ax.set_xlabel(xpp['label'])

		if j==0: # first column
			ax.set_ylabel(ypp['label'])



fig.subplots_adjust(
	top=0.99, bottom=0.078,
	left=0.085, right=0.95,
	wspace=0, hspace=0
)
fig.savefig('./plt/12/flasher_scatterplotMPE.pdf')

# show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# selection efficiency

# def get_df(dic, key):
	
# 	df = dic[key].copy()

# 	df['h'] 	 = 10**df['h'].values
# 	df['q'] 	 = 10**df['q'] / 1e8 
# 	df['tvar'] = np.sqrt(df['tvar'].values)
# 	df.rename(
# 		columns={'h': 'H', 'q': 'Q', 'tvar': 'tsig'},
# 		inplace=True
# 	)

# 	return df

# cuts = Namespace([
# 	('Slim', (2.400, 3.266)),
# 	('tsiglim', (2.066, 6.061)),
# 	('riselim', (0,))
# ])


# run = Flasher('v08f', split=True)
# dic = run.data
# for key in tuple(dic.keys()):
# 	dic[ key[-7:-3] ] = dic.pop(key)

# nspe = []
# fintensity = np.arange(120, 231)

# for i in fintensity:

# 	df 	= get_df(dic, 'l%d'%i)
# 	try:
# 		df2 = apply_cuts(df, cuts)
# 		nspe.append(len(df2) / len(df))
# 	except: # empty df
# 		nspe.append(0)


# plt.figure(figsize=(184/72, 170/72))
# plt.plot(fintensity/100, nspe, color='crimson')
# plt.xlabel('flasher intensity/a.u.')
# plt.ylabel('SPE selection efficiency (\%)')
# plt.tight_layout(pad=0.05)
# # plt.savefig('./plt/12/flasher_selectionEfficiency')

# embed()