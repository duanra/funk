#!usr/bin/env python3

import numpy as np 
import scipy.stats as st
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from mod.funk_manip import load_configsX, get_df
from mod.funk_plt import show, add_toplegend, order_legend
from mod.funk_stats import bin_centers, bin_widths, integer_binning
from mod.funk_ROOTy import TH1Wrapper, plot_tf1, get_histogram, get_rchi2

from iminuit import Minuit
from ROOT import TF1

plt.rcParams.update(fcn.rcDefense)
plt.rcParams['figure.figsize'] = (220/72, 165/72)
# plt.rcParams.update(fcn.rcThesis)
# plt.rcParams['figure.figsize'] = (200/72, 150/72)

# plt.rcParams.update(fcn.rcPaper)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# !!!! ROOT TH1D.FiT('L') does a BINNED likelihood fit 
# which does not really make sense in our case.
# but the parameter estimates would be more liess the same
# https://indico.desy.de/indico/event/13610/contribution/9/material/slides/0.pdf


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def nbfit(data):

	def negloglikelihood(n, p):
		"""" log likelihood """
		p = st.nbinom(n, p)
		logL = np.sum(np.log(p.pmf(data)))
		return - logL

	def reparametrize(n, p):
		""" mean, variance """
		mu 	= n * (1-p) / p
		var = n * (1-p) / p**2   
		return mu, var

	m = Minuit(
				negloglikelihood, print_level=1,
				n=50, error_n=1, limit_n=(0, None),
				p=0.1, error_p=0.01, limit_p=(0, 1),
				errordef=0.5
			)

	print('*'*80)
	m.migrad()
	# m.print_param()

	print(m.values)
	print('mu  = {:.5f}\nvar = {:.5f}'.format(*reparametrize(*m.args)))
	print('*'*80)

	return list(m.args)


cuts   = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
trig 	 = False
key 	 = 'out_open'
run_v  = 'v35'

counts = get_df(load_configsX(
					run_v, var='counts', trig=trig, cuts=cuts), key
				 )['counts'].values.astype(np.int)

mu, std = np.mean(counts), np.std(counts, ddof=1)
print('counts [{}]: mu={:.2f}, std={:.2f}'.format(key, mu, std))


nbins, xlow, xup = integer_binning(min(counts), max(counts))
th1 = TH1Wrapper(counts, nbins, xlow, xup, density=False)


frange = (200, 800)
fnroot = '[0]*ROOT::Math::negative_binomial_pdf(x,[1],[2])'
N = th1.Integral('width')
tf1_nbinom = TF1('tf1_nbinom', fnroot, *frange)
tf1_nbinom.SetParNames('N', 'p', 'n')
tf1_nbinom.SetParameters(N, 0.1, 50)
tf1_nbinom.SetParLimits(1, 0, 1)
# tf1_nbinom.FixParameter(0, N)

th1.Fit(tf1_nbinom, 'RLE0')
print('rchi2 = %.3f'%get_rchi2(tf1_nbinom))


plt.figure()
hist = get_histogram(th1, return_yerr=True)

plt.errorbar(
	bin_centers(hist[2]), hist[0], hist[1],
	fmt='s', ms=1.5, mew=0, elinewidth=.5,
	color='k', alpha=0.7, label='data',
	zorder=0
)


# redo the fit properly
xs = np.arange(*frange)
counts_trunc = counts[(frange[0]<=counts) & (counts<=frange[1])]
N_trunc  = len(counts_trunc)
mu_trunc = np.mean(counts_trunc)
n, p = nbfit(counts_trunc)


tf1_nbinom.SetParameters(N_trunc, p, n)
plot_tf1(
	tf1_nbinom, xs, lw=2, color='limegreen',
	label='fitted NB'
)

plt.plot(
	xs, N_trunc * st.poisson.pmf(xs, mu_trunc),
	color='silver', lw=1.5, ls='--',
	label='Poi'
)

plt.xlabel('count/min')
plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
plt.gca().yaxis.get_offset_text().set_position((-0.12,1))	
plt.ylim(-5, 65)
add_toplegend(labels=['data', 'NB', 'Poi'], ncol=3, lw=2)


# show(save='./plt/12/v35_countDistn', pad=0.05, left=0.11)
# show(save='../talks/src/phd_defense/figs/v35_countDistn', pad=0.05, left=0.11)
show(pad=0.05)

embed()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# cuts   = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
# trig 	 = False
# key 	 = 'in_closed'
# run_v  = 'v31'

# counts = get_df(load_configsX(
# 					run_v, var='counts', trig=trig, cuts=cuts), key
# 				 )['counts'].values.astype(np.int)

# mu, std = np.mean(counts), np.std(counts, ddof=1)
# print('counts [{}]: mu={:.2f}, std={:.2f}'.format(key, mu, std))

# nbins, xlow, xup = integer_binning(min(counts), 200)
# th1 = TH1Wrapper(counts, nbins, xlow, xup, density=False)

# frange = (50, 150)
# fnroot = '[0]*ROOT::Math::negative_binomial_pdf(x,[1],[2])'
# N = th1.Integral('width')
# tf1_nbinom = TF1('tf1_nbinom', fnroot, *frange)
# tf1_nbinom.SetParNames('N', 'p', 'n')
# tf1_nbinom.SetParameters(N, 0.1, 50)
# tf1_nbinom.SetParLimits(1, 0, 1)
# # tf1_nbinom.FixParameter(0, N)

# th1.Fit(tf1_nbinom, 'RLE0')
# print('rchi2 = %.3f'%get_rchi2(tf1_nbinom))


# plt.figure()
# hist = get_histogram(th1, return_yerr=True)

# plt.errorbar(
# 	bin_centers(hist[2]), hist[0], hist[1],
# 	fmt='s', ms=1.5, mew=0, elinewidth=.5,
# 	color='k', alpha=0.7, label='data',
# )

# # redo the fit properly
# xs = np.arange(*frange)
# counts_trunc = counts[(frange[0]<=counts) & (counts<=frange[1])]
# N_trunc  = len(counts_trunc)
# mu_trunc = np.mean(counts_trunc)
# n, p = nbfit(counts_trunc)


# tf1_nbinom.SetParameters(N_trunc, p, n)
# plot_tf1(
# 	tf1_nbinom, xs, lw=2, color='limegreen',
# 	label='fitted $\mathit{NB}$'
# )

# plt.plot(
# 	xs, N * st.poisson.pmf(xs, mu_trunc),
# 	color='silver', lw=1.5, ls='--',
# 	label='$\mathit{Poi}$'
# )

# plt.xlabel('count/min')
# plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
# plt.gca().yaxis.get_offset_text().set_position((-0.12,1))	
# add_toplegend(labels=['data', 'NB', 'Poi'], ncol=3, lw=2)

# plt.tight_layout(pad=0.05)
# plt.subplots_adjust(left=0.11)
# plt.savefig('./plt/12/v31_countDistn')
# plt.close()

# # show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# # configs_v24 = load_configsX('v24', var='counts', trig=False, cuts=cuts)
# # configs_v25 = load_configsX('v25', var='counts', trig=False, cuts=cuts)
# # counts = np.concatenate((
# # 						 get_df(configs_v24, key).counts.values,
# # 						 get_df(configs_v25, key).counts.values
# # 					))