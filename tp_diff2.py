#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from matplotlib import rc
from functools import wraps

from mod.cymodule.tp_diff import get_mask
from mod.funk_manip import load_rawX
from mod.funk_plt import show, order_legend, add_toplegend
from mod.funk_stats import bin_centers, bin_widths
from mod.funk_utils import timing


plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (165/72, 157/72)

# plt.rcParams.update(fcn.rcPaper)
# plt.rcParams.update(fcn.rcPoS)

pd.set_option('display.max_rows', 20)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def get_single_captures(df):

	captures 	= df.groupby(['time', 'capture'])\
								.size()\
								.reset_index(name='counts')

	multi_captures = captures[ captures['counts']==1 ]\
										 			 [ ['time', 'capture'] ].values

	search_in = df[['time', 'capture']].values

	dims = search_in.max(axis=0) + 1
	idx  = np.isin(
						np.ravel_multi_index(search_in.T, dims),
						np.ravel_multi_index(multi_captures.T, dims)
					).nonzero()[0]
	
	df_multi = df.iloc[idx]

	return df_multi


def get_multi_captures(df):

	captures 	= df.groupby(['time', 'capture'])\
								.size()\
								.reset_index(name='counts')

	multi_captures = captures[ captures['counts']>1 ]\
										 			 [ ['time', 'capture'] ].values

	search_in = df[['time', 'capture']].values

	dims = search_in.max(axis=0) + 1
	idx  = np.isin(
						np.ravel_multi_index(search_in.T, dims),
						np.ravel_multi_index(multi_captures.T, dims)
					).nonzero()[0]
	
	df_multi = df.iloc[idx]

	return df_multi


def get_multi_splitting(df_multi):
	""" return splitting indexes for different captures ids """

	cid  = df_multi['capture'].values
	cidx = np.flatnonzero(cid[1:] - cid[:-1]) + 1

	return cidx


def get_tp_diff(df_multi):
	""" run on df_multi to make it faster """

	cidx 		= get_multi_splitting(df_multi)
	tp_list = np.split(df_multi['tp'].values, cidx)

	tp_diff = np.concatenate([ np.diff(x) for x in tp_list ])

	# this doesn't work because if 2 'delayed' pulses come very close,
	# they will be seen as two reflections w.r.t the 0th pulse.
	# yet it's more likely an afterpulse or coincidental pulse
	# tp_diff = np.concatenate([ x[1:] - x[0] for x tp_list ])

	return tp_diff.round(1)


@timing
def correct_multi_captures(df_multi, hist_ratio):
	""" hist_ratio: probability of NOT being a reflection event """

	cidx 		= get_multi_splitting(df_multi)
	tp_list = np.split(df_multi['tp'].values, cidx)

	# convert to dict for cython perf
	ratio 	= dict(hist_ratio['ratio'])
	idxs 		= get_mask(tp_list, ratio)

	return df_multi.iloc[idxs]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

run_v = 'v35'
cuts_dt  = fcn.cuts.select('dtlim')
cuts_spe = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')

single_captures = {}
multi_captures 	= {}
tp_diffs = {}

pos  = fcn.positions['out_open']
bwi  = 0.8
bins = np.arange(0.5*bwi, 800+0.5*bwi, bwi)
density = False


def make_tp_diff(df, key, **kwargs):

	df_single = get_single_captures(df)
	df_multi 	= get_multi_captures(df)
	tp_diff  	= get_tp_diff(df_multi)

	single_captures[key] = df_single
	multi_captures[key]  = df_multi
	tp_diffs[key] = tp_diff

raw_configsX = load_rawX(run_v, var='tp', cuts=cuts_dt)
df_allpulses = raw_configsX.get_group(pos)
make_tp_diff(df_allpulses, key='allpulses')

raw_configsX = load_rawX(run_v, var='tp', cuts=cuts_spe)
df_spepulses = raw_configsX.get_group(pos)
make_tp_diff(df_spepulses, key='spepulses')

del raw_configsX


plt.hist(
	tp_diffs['allpulses'], bins=bins, density=density,
	histtype='step', color='k', lw=0.6, label='all pulses'
)

plt.hist(
	tp_diffs['spepulses'], bins=bins, density=density,
	histtype='step', color='crimson', lw=0.6, label='events'
)

plt.xlabel('$\Delta t_\mathrm{ev}$/ns')
plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
offset = plt.gca().yaxis.get_offset_text()
# offval = offset.get_text() # return empty string! WTF???
# print(offval)
offset.set_visible(False)
plt.figtext(0, 0.915, r'$\times10^{3}$')
add_toplegend(labels=['all', 'events'], ncol=2)

plt.tight_layout(pad=0.05)
plt.subplots_adjust(left=0.138)
# plt.savefig('./plt/12/v35_tpDiff')
plt.close()

# show()
embed()