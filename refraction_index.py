#!usr/bin/env python3
import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (170/72, 145/72)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def delta_air(l, P=1013.25, T=15):
	""" https://refractiveindex.info/?shelf=other&book=air&page=Ciddor
			http://nvlpubs.nist.gov/nistpubs/jres/086/jresv86n1p27_A1b.pdf (equation 8)
			wavelength l in nm, fit range (230 - 1690) nm
			pressure in hPa, temperature in degC
			standard air: P0 = 1013.25 Pa and T0 = 15 degC			
	"""
	l  = l*1e-3 # convert to um for the formula
	P0 = 1013.25
	T0 = 15
	c1 = 0.05792105 / (238.0185 - l**(-2))
	c2 = 0.00167917 / (57.362 - l**(-2))
	delta_standard = c1 + c2
						
	return delta_standard * 1.055*(P/P0) / (1 + 0.055*(T/T0))


def delta_glass(l):
	""" https://refractiveindex.info/?shelf=main&book=SiO2&page=Malitson 
			wavelength l given in nm, fit range (210 - 6700) nm
	"""

	l  = l*1e-3 # convert to um for the formula
	c1 = 0.6961663*l**2 / (l**2 - 0.0684043**2)
	c2 = 0.4079426*l**2 / (l**2 - 0.1162414**2)
	c3 = 0.8974794*l**2 / (l**2 - 9.896161**2)

	return np.sqrt(1 + c1 + c2 + c3) - 1


def fillx(x0, x1, ax=None):

	if ax is None:
		ax = plt.gca()

	ylow, yup = plt.ylim()
	plt.fill_betweenx(
		np.linspace(ylow, yup), l0, l1,
		color='greenyellow', lw=0, alpha=0.6
	)
	plt.ylim(ylow, yup)



l0, l1 = 150, 630

plt.figure('air')
lA = np.linspace(230, 1690)
dA = delta_air(lA)
plt.plot(lA, dA/1e-3)
fillx(l0, l1)
plt.xlabel(r'$\lambda / \mathrm{nm}$')
plt.ylabel(r'$\delta_\mathrm{air} / 10^{-3}$')
plt.yticks(np.arange(0.275,0.310,0.01))
plt.tight_layout(pad=0)
plt.subplots_adjust(left=0.26)
plt.savefig('plt/12/refractionindex_air')


plt.figure('glass')
lG = np.linspace(210, 3000)
dG = delta_glass(lG)
plt.plot(lG, dG)
fillx(l0, l1)
plt.xlabel(r'$\lambda / \mathrm{nm}$')
plt.ylabel(r'$\delta_\mathrm{glass}$')
plt.yticks(np.arange(0.425,0.530,0.025))
plt.tight_layout(pad=0)
plt.subplots_adjust(left=0.26)
plt.savefig('plt/12/refractionindex_glass')

plt.show()