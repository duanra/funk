#!usr/bin/env python3

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import matplotlib.tight_layout as tl
import mod.funk_consts as fcn

from array import array
from itertools import chain
from IPython import embed
# https://github.com/matplotlib/matplotlib/issues/8027
# from matplotlib.ticker import NullFormatter, LogLocator

from mod.cymodule.TT_distn import get_mask
from mod.funk_manip import apply_cuts
from mod.funk_plt import show, order_legend, add_toplegend
from mod.funk_utils import timing, handle_warnings, Namespace
from mod.funk_ROOTy import TH1Wrapper, plot_th1, plot_tf1

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (220/72, 150/72)

# plt.rcParams.update(fcn.rcPaper)

np.set_printoptions(linewidth=150)
pd.set_option('display.max_rows', 5)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# @timing
def get_deltas(df):
	""" get interarrival-times of triggers """

	_, counts = np.unique(df.index.values, return_counts=True)
	ttags 		= np.split(df['ttag'].values, np.cumsum(counts)[:-1])

	n 	 = len(df) - len(counts)
	ret  = np.fromiter(
					chain.from_iterable( np.diff(ttag) for ttag in ttags ),
					dtype=float, count=n
					)

	return ret


# @timing
def selective_count(df, deadtime=0.1):
	""" implement counting dead time [secs],
			pulse-cuts can be done externally
	"""

	if deadtime is None or deadtime==0:
		return df

	else:

		_, counts = np.unique(df.index.values, return_counts=True)
		ttags 		= np.split(df['ttag'].values, np.cumsum(counts)[:-1])
		idxs 			= get_mask(ttags, deadtime)

		return df.iloc[idxs]


@handle_warnings
def expon_cdf(rvs, lam, deadtime=None):
	""" lam is the mean of (poissonian) event rate """

	if deadtime is not None:
		rvs -= deadtime
		# np.exp(-lam*deadtime) - np.exp(-lam*rvs)

	return 1 - np.exp(-lam*rvs)


class Deltas:

	def __init__(self, df, deadtime=None):

		self.__name__ 	 = 'deltas' # -required in TH1Fitter
		self.df_original = df
		self.nbins 			 = 100
		self.density 		 = True

		self.update(deadtime)


	def __call__(self, x, deadtime, N=1):
		""" for fitting purpose """

		if (deadtime is not None) and (self.deadtime != deadtime):
			self.update(deadtime)

		i 	= self.th1.FindBin(x)
		ret = N * self.th1.GetBinContent(i)
		
		return ret


	def update(self, deadtime):

		df 		 = selective_count(self.df_original, deadtime)
		deltas = get_deltas(df)
		lam 	 = df.groupby('time').size().mean() / 60
		if deadtime is not None:
			lam /= (1 - lam * deadtime) # correction

		self.deadtime 	 = deadtime
		self.nevents 		 = len(df)
		self.lam 				 = lam
		self.transformed = expon_cdf(deltas, lam, deadtime)
		self.th1 				 = TH1Wrapper(
													self.transformed,
													self.nbins,
													0, 1,
													self.density)


	def get_stats(self, deadtime, nevents_ref=1):

		print('deadtime = %e'%deadtime)
		if self.deadtime != deadtime:
				self.update(deadtime)

		ys 		 = np.array([ self.th1.GetBinContent(i)
												for i in range(1, self.nbins+1) ])
		ys_err = np.array([ self.th1.GetBinError(i)
												for i in range(1, self.nbins+1) ])

		MSD 	 = np.sum( (1 - ys)**2 ) / nbins # mean squared deviation
		DKL_PQ = np.sum( -np.log(ys) ) / nbins # Kullback–Leibler divergence
		DKL_QP = np.sum( (ys * np.log(ys)) ) / nbins
		chi2 	 = np.sum( ((1 - ys)/ys_err)**2 ) / len(ys) # average chi2

		return MSD, DKL_PQ, DKL_QP, chi2, self.nevents / nevents_ref


def poisson_like(df):
	""" MC check """

	from scipy.stats import poisson

	time = df.index.unique().values
	counts_mean = len(df) / len(time) # /60

	poi  = poisson(counts_mean)

	counts = array('I', [])
	ttags  = array('d', [])

	for tixd in time:
	
		N 	 = poi.rvs()
		ttag = np.sort( np.random.uniform(0, 60, size=N) )

		counts.append( N )
		ttags.extend( ttag )

	counts = np.asarray(counts, dtype=int)
	ttags  = np.asarray(ttags, dtype=float)

	df = pd.DataFrame(ttags, columns=['ttag'], index=np.repeat(time, counts))
	df.index.name = 'time'

	return df, counts_mean



def nevents_corrected(counts_mean, deadtime):

	return counts_mean / ( 1 + counts_mean*deadtime/60)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# cuts obtained from flasher run is somewhat not applicable here (check distsn)
cuts = Namespace([
	('riselim', (0,)),
	('Slim', (1.719, 3.951)), #(2.08, 3.56)),
	('tsiglim', (1.624, 12.481)) #(3.34, 10.44))
])


# df  = pd.read_pickle('./data/timetag/pulse_ttag_1536585179.sdf')
df 	= pd.read_pickle('./data/timetag/pulse_ttag_1535701375.sdf') # full
dts = np.logspace(-9, 0, 200)


print('raw pulses')
deltas 			= Deltas(df, deadtime=0)
nbins  			= deltas.th1.GetNbinsX()
nevents_ref = deltas.nevents

MSDs, DKL_PQs, DKL_QPs, chi2s, nevents\
	= np.array([deltas.get_stats(dt, nevents_ref) for dt in dts]).T


print('selected pulses')
df_wcuts 					= apply_cuts(df, cuts)
deltas_wcuts 	 		= Deltas(df_wcuts, deadtime=0)
nevents_ref_wcuts = deltas_wcuts.nevents

MSDs_wcuts, DKL_PQs_wcuts, DKL_QPs_wcuts, chi2s_wcuts, nevents_wcuts\
	= np.array([deltas_wcuts.get_stats(dt, nevents_ref) for dt in dts]).T


# print('poisson MC')
# df_poi, counts_mean = poisson_like(df_wcuts)
# deltas_poi					= Deltas(df_poi, deadtime=0)
# nevents_ref_poi 		= deltas_poi.nevents

# MSDs_poi, DKL_PQs_poi, DKL_QPs_poi, chi2s_poi, nevents_poi\
# 	= np.array([ deltas_poi.get_stats(dt, nevents_ref) for dt in dts ]).T


print('nevents analytical')
T = len(df.index.unique().values)
nevents_anal1 = T * np.array(
									[nevents_corrected(274, dt) for dt in dts]
								) / nevents_ref

nevents_anal2 = T * np.array(
									[nevents_corrected(256, dt) for dt in dts]
								) / nevents_ref

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

savedir = './plt/12/' #None

plt.figure('MSDs')

MSD_ref = np.max(MSDs)
plt.plot(dts, MSDs/MSD_ref, color='k', lw=1, label='all pulses')
plt.plot(dts, MSDs_wcuts/MSD_ref, color='crimson', lw=1, label='events')
# plt.plot(dts, MSDs_poi/MSD_ref, color='b', lw=1, label='MC')
plt.axhline(
	y=0, color='silver', lw=1.5, ls='--',
	label='Poi', zorder=0)
plt.axvline(x=1e-6, color='b', lw=0.2, ls='--', zorder=0)
plt.axvline(x=1e-3, color='g', lw=0.2, ls='--', zorder=0)

plt.xlabel('$t_\mathrm{dead}$/s')
plt.xscale('log')
plt.xticks([10**x for x in range(-9,1,3)])
plt.ylabel('MSD')
plt.yticks(np.arange(0, 1.2, 0.2))
plt.ylim(-0.1, 1.1)
add_toplegend(labels=['all', 'events', 'Poi'], ncol=3, lw=2)

plt.tight_layout(pad=0.05)
if savedir is not None:
	plt.savefig(savedir + 'caen_ttagMSDscan.pdf')
	plt.close()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

plt.figure('nevents')

plt.plot(dts, nevents, color='k', lw=1, label='all pulses')
plt.plot(dts, nevents_wcuts, lw=1, color='crimson', label='events')
# plt.plot(dts, nevents_poi, lw=1, color='b', label='MC')
plt.plot(
	dts, nevents_anal1, color='silver', lw=1.5, ls='--',
	label='Poi', zorder=0)
plt.plot(
	dts, nevents_anal2, color='silver', lw=1.5, ls='--',
	zorder=0)
plt.axvline(x=1e-6, color='b', lw=0.2, ls='--', zorder=0)
plt.axvline(x=1e-3, color='g', lw=0.2, ls='--', zorder=0)

plt.xlabel('$t_\mathrm{dead}$/s')
plt.xscale('log')
plt.xticks([10**x for x in range(-9,1,3)])
plt.ylabel('fraction of data')
plt.yticks(np.arange(0, 1.2, 0.2))
plt.ylim(-0.1, 1.1)
add_toplegend(labels=['all', 'events', 'Poi'], ncol=3, lw=2)

plt.tight_layout(pad=0.05)
rect = tl.get_tight_layout_figure(
					plt.gcf(), [plt.gca()], tl.get_subplotspec_list([plt.gca()]),
					tl.get_renderer(plt.gcf()), pad=0
				)
print(rect)
if savedir is not None:
	plt.savefig(savedir + 'caen_ttagFracscan.pdf')
	plt.close()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# plt.figure('DKL_PQs')

# plt.plot(dts, DKL_PQs, color='k', lw=1, label='all pulses')
# plt.plot(dts, DKL_PQs_wcuts, color='crimson', lw=1, label='events')
# # plt.plot(dts, DKL_PQs_poi, color='b', lw=1, label='MC')

# plt.xlabel('$t_\mathrm{dead}$/s')
# plt.xscale('log')
# plt.xticks([10**x for x in range(-9,1,3)])
# plt.ylabel('$\mathrm{DKL_{PQ}}$')
# plt.yscale('log')
# plt.yticks([10**x for x in range(-4,1)])
# plt.tick_params(axis='y', which='minor', left=True)
# plt.gca().yaxis.set_major_locator(plt.LogLocator(numticks=5))
# plt.gca().yaxis.set_minor_locator(
# 	plt.LogLocator(subs=np.arange(0.1,1,0.1), numticks=5)
# )
# plt.gca().yaxis.set_minor_formatter(plt.NullFormatter())
# add_toplegend(labels=['all', 'events'], ncol=2, lw=2)

# plt.tight_layout(pad=0.05)
# plt.subplots_adjust(left=0.165)
# if savedir is not None:
# 	plt.savefig(savedir + 'caen_ttagDKLPQscan.pdf')
# 	plt.close()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# plt.figure('DKL_QPs')

# plt.plot(dts, DKL_QPs, color='k', lw=1, label='all pulses')
# plt.plot(dts, DKL_QPs_wcuts, color='crimson', lw=1, label='events')
# # plt.plot(dts, DKL_QPs_poi, color='b', lw=1, label='MC')

# plt.xlabel('$t_\mathrm{dead}$/s')
# plt.xscale('log')
# plt.xticks([10**x for x in range(-9,1,3)])
# plt.ylabel('$\mathrm{DKL_{QP}}$')
# plt.yscale('log')
# plt.yticks([10**x for x in range(-4,1)])
# plt.tick_params(axis='y', which='minor', left=True)
# plt.gca().yaxis.set_major_locator(plt.LogLocator(numticks=5))
# plt.gca().yaxis.set_minor_locator(
# 	plt.LogLocator(subs=np.arange(0.1,1,0.1), numticks=5)
# )
# plt.gca().yaxis.set_minor_formatter(plt.NullFormatter())
# add_toplegend(labels=['all', 'events'], ncol=2, lw=2)

# plt.tight_layout(pad=0.05)
# plt.subplots_adjust(left=0.165)
# if savedir is not None:
# 	plt.savefig(savedir + 'caen_ttagDKLQPscan.pdf')
# 	plt.close()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# plt.figure('chi2s')

# plt.plot(dts, chi2s, color='k', lw=1, label='all pulses')
# plt.plot(dts, chi2s_wcuts, color='crimson', lw=1, label='events')
# # plt.plot(dts, chi2s_poi, color='b', lw=1, label='MC')
# plt.axhline(1, color='dimgrey', ls='--', lw=1.5,
# 	label='Poisson', zorder=10)

# plt.xlabel('$t_\mathrm{dead}$/s')
# plt.xscale('log')
# plt.xticks([10**x for x in range(-9,1,3)])
# plt.ylabel('average $\chi^2$')
# plt.yscale('log')
# plt.yticks([10**x for x in range(1,4)])
# plt.tick_params(axis='y', which='minor', left=True)
# plt.gca().yaxis.set_major_locator(plt.LogLocator(numticks=5))
# plt.gca().yaxis.set_minor_locator(
# 	plt.LogLocator(subs=np.arange(0.1,1,0.1), numticks=5))
# plt.gca().yaxis.set_minor_formatter(plt.NullFormatter())
# add_toplegend(labels=['all', 'events', 'Poisson'], ncol=3, lw=2)

# # # add fraction of data on x-axis top
# # xlim 	 = plt.xlim()
# # xticks = np.array([ x for x in  plt.gca().get_xticks()
# # 											if (x>=xlim[0]) & (x<=xlim[1]) ])
# # # do not round off
# # xticklabels = np.array([ str(get_frac(dt)*100)[:4] for dt in xticks ])

# # plt.twiny()
# # plt.xlabel('fraction of data/\%')
# # plt.xscale('log')
# # plt.xlim(xlim)
# # plt.xticks(xticks, xticklabels)
# # plt.tick_params('x', which='minor', top=False)

# plt.tight_layout(pad=0.05)
# if savedir is not None:
# 	plt.savefig(savedir + 'caen_ttagChi2scan.pdf')
# 	plt.close()


embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# th1_unif 		= TH1Wrapper(np.random.uniform(0, 1, len(df)), nbins, 0, 1, True)
# ys_unif  		= np.array([ th1_unif.GetBinContent(i) for i in range(1, nbins+1) ])
# ys_err_unif = np.array([ th1_unif.GetBinError(i) for i in range(1, nbins+1) ])
# chi2_unif 	= np.sum(((1 - ys_unif) / ys_err_unif)**2 ) / (len(ys_unif) - 1)
# plt.axhline(chi2_unif, color='darkred', ls='--')