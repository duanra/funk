#!usr/bin/env python3

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

# from copy import deepcopy
from mod.funk_manip import load_rawX
from mod.funk_plt import pause, show
from mod.funk_run import Run, Flasher
from mod.funk_ROOTy import TH1Wrapper, TH1Fitter, plot_tf1, plot_th1

from IPython import embed


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


cuts = fcn.cuts #.select('dtlim', 'riselim', 'Slim', 'tsiglim', 'tp')
pos  = fcn.positions['in_open']

q25  = load_rawX('v25', var='q').get_group(pos)['q'].values

frun = Flasher(run_v='v09f', split=False)
q09f = frun.data['q'].values
# q09f = frun.apply_cuts(
# 					cuts.select('riselim', 'tplim',	 'Slim', 'tsiglim')
# 			 )['q'].values


bins 	 	= 100
qrange 	= (6.5, 9.5)
density = False


q25_hist 	= TH1Wrapper(q25, bins, *qrange, density=density)
q09f_hist = TH1Wrapper(q09f, bins, *qrange, density=density)


def spe(x, N0):
	ibin = q09f_hist.GetXaxis().FindBin(x)
	return N0 * q09f_hist.GetBinContent(ibin)

def gauss(x, N1, mu, sig):
	return N1 * np.exp( -0.5 * ((x-mu)/sig)**2 )

def model(x, N0, N1, mu, sig):
	return spe(x, N0) + gauss(x, N1, mu, sig)


N0 = q25_hist.GetMaximum() / q09f_hist.GetMaximum()
N1 = q25_hist.Integral('width')
q09f_mean = np.mean(q09f)
q09f_std  = np.std(q09f, ddof=1)
qfitlim 	= (q09f_mean - q09f_std, q09f_mean + 1.5*q09f_std)


# fitter = TH1Fitter(q25_hist, spe, *qfitlim)
# fitter.set_parameters(N0=N0)

fitter = TH1Fitter(q25_hist, model, *qfitlim)
fitter.set_parameters(N0, N1, q09f_mean, q09f_std)

fitter.fit(option='RP')
print('rchi2 = %.3f'%fitter.get_rchi2())


plot_th1(q25_hist, color='k', label='q25')
plot_th1(q09f_hist, color='b', label='q09f')
fitter.plot(hprops=False, fprops=dict(label='spe fit'))
plt.ticklabel_format(
	axis='both', style='sci', scilimits=(0,0),
	useMathText=True)
plt.legend()


show()
embed()


# def fbins(qfitlim):

# 	fbinnums = np.unique([
# 									q09f_hist.GetXaxis().FindBin(q)
# 									for q in np.linspace(*qfitlim, 1000)] )
# 	fbce 		 = np.array([q09f_hist.GetBinCenter(int(i)) for i in fbinnums])

# 	return fbce, fbinnums

# fbce, fbinnums = fbins(qfitlim)
# plt.step(fbce, [fspe.Eval(q) for q in fbce], where='mid', color='r')