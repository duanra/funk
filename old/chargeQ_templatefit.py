#!usr/bin/env python3
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

# from copy import deepcopy
from mod.funk_manip import load_rawX
from mod.funk_plt import pause, show, order_legend, autofmt_xdate
from mod.funk_run import Run, Flasher
# from mod.funk_stats import polya, poi_branch
from mod.funk_utils import posix2utc, utc2posix, fmt, table_lr
from mod.funk_ROOTy import get_fitresults, get_parameters,\
													 get_histogram, TF1_signature,\
													 get_rchi2, fplot, hplot

from ROOT import TH1D, TF1
from root_numpy import fill_hist

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

cuts = fcn.cuts #.select('dtlim', 'riselim', 'Slim', 'tsiglim', 'tp')

q25_raw = load_rawX(run_v='v25', var='q')\
						.get_group(fcn.positions['in_open'])['q'].values
Q25_raw = 10**q25_raw / 1e8

# q25_clean = load_rawX(run_v='v25', var='q',
# 						cuts=cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim'))\
# 						.get_group(fcn.positions['in_open'])['q'].values
# Q25_clean = 10**q25_clean / 1e8


frun = Flasher(run_v='v09f', split=False)
q09f = frun.apply_cuts(
						cuts.select('riselim', 'tplim',	 'Slim', 'tsiglim'))['q'].values
# q09f = frun.data['q'].values
Q09f = 10**q09f / 1e8
mean_Q09f = np.mean(Q09f)
std_Q09f  = np.std(Q09f, ddof=1)


bins 	 	= 200
Qrange 	= (0, 6)
density = False

def fbins(Qfitlim):
	fbinnums = np.unique([
									Qhist09f.GetXaxis().FindBin(Q)
									for Q in np.linspace(*Qfitlim, 1000)] )
	fbce 		 = np.array([Qhist09f.GetBinCenter(int(i)) for i in fbinnums])

	return fbce, fbinnums


Qhist25  = TH1D('v25', 'Q-charge histogram', bins, *Qrange)
fill_hist(Qhist25, Q25_raw)
# Qhist25.Scale(1 / Qhist25.Integral('width'))

Qhist09f = TH1D('v09f', 'Q-charge histogram', bins, *Qrange)
fill_hist(Qhist09f, Q09f)
# Qhist09f.Scale(1 / Qhist09f.Integral('width'))


def spe(x, N0):
	ibin = Qhist09f.GetXaxis().FindBin(x)
	return N0 * Qhist09f.GetBinContent(ibin)

def poi_branch(x, N0, lam):
	from mod.funk_stats import poi_branch as  mpoi_branch
	return N0 * mpoi_branch(x, lam, k=11, scale_x=1e8)

# def polya(x, N0, mu, a):
# 	from mod.funk_stats import polya as mpolya
# 	return N0 * mpolya(x, mu, a)

# def expo(x, N1, lam):
# 	return N1 * np.exp(-lam*x)

def gauss(x, N1, mu, sig):
	return N1 * np.exp( -0.5 * ((x-mu)/sig)**2 )

def model(x, N0, N1, mu, sig):
	return spe(x, N0) + gauss(x, N1, mu, sig)

# def model2(x, N0, lam0, N1, lam1):
# 	return poi_branch(x, N0, lam0) + expo(x, N1, lam1)

# def model3(x, N0, lam0, N1, mu1, sig1):
# 	return poi_branch(x, N0, lam0) +\
# 				 gauss(x, N1, mu1, sig1)

N0 = Qhist25.GetMaximum() / Qhist09f.GetMaximum()
N1 = Qhist25.Integral('width')

# print('nevents', sum([fspe.Eval(Q) for Q in fbce]))
# print('error', np.sqrt( 
# 			 sum([fspe.GetParError(0)**2 * Qhist09f.GetBinContent(int(ibin))**2 +\
# 			 			fspe.GetParameter(0)**2 * Qhist09f.GetBinError(int(ibin))**2
# 			 for Q, ibin in zip(fbce, fbinnums)]) ))


hplot(Qhist25, color='k')
hplot(Qhist09f, color='b')


Qfitlim = (0.4, 3) #(mean_Q09f - 1*std_Q09f, mean_Q09f + 1.5*std_Q09f)
fspe 		= TF1('fspe', TF1_signature(spe), *Qfitlim, 1)
fspe.SetParameter(0, N0)
Qhist25.Fit(fspe, 'R0')
print(get_rchi2(fspe))
fbce, fbinnums = fbins(Qfitlim)
plt.step(fbce, [fspe.Eval(Q) for Q in fbce], where='mid', color='r')


Qfitlim = (0.4, 3) #(mean_Q09f - 1*std_Q09f, mean_Q09f + 1.5*std_Q09f)
Qfitlim = (0.4, 3)
fpoi_branch = TF1('fpoi_branch', TF1_signature(poi_branch), *Qfitlim, 2)
fpoi_branch.SetParameters(N1, mean_Q09f)
Qhist25.Fit(fpoi_branch, 'R0')
print(get_rchi2(fpoi_branch))
fbce, fbinnums = fbins(Qfitlim)
plt.step(fbce, [fpoi_branch.Eval(Q) for Q in fbce], where='mid', color='limegreen')


# Qfitlim = (0.4, 3)
# fmodel = TF1('fmodel', TF1_signature(model), *Qfitlim, 4)
# fmodel.SetParameters(N0, N1, mean_Q09f, std_Q09f)
# Qhist25.Fit(fmodel, 'R0')
# print(get_rchi2(fmodel))
# N0, N1, mu, sig = get_parameters(fmodel)
# fbce, fbinnums = fbins(Qfitlim)
# plt.step(fbce, [fspe.Eval(Q) for Q in fbce], where='mid', color='orange', ls='--')
# plt.plot(fbce, gauss(fbce, N1, mu, sig), color='orange', ls=':')
# plt.step(fbce, [fmodel.Eval(Q) for Q in fbce], where='mid', color='orange')



# Qfitlim = (0, 4)
# fmodel2 = TF1('fmodel2', TF1_signature(model2), *Qfitlim, 4)
# fmodel2.SetParameters(0.9*N1, mean_Q09f, 0.1*N1, mean_Q09f)
# Qhist25.Fit(fmodel2, 'R0')
# print(get_rchi2(fmodel2))
# N0, lam0, N1, lam1 = get_parameters(fmodel2)
# fbce, fbinnums = fbins(Qfitlim)
# plt.plot(fbce, poi_branch(fbce, N0, lam0), color='magenta', ls='--')
# plt.plot(fbce, expo(fbce, N1, lam1), color='magenta', ls=':')
# plt.step(fbce, [fmodel2.Eval(Q) for Q in fbce], where='mid', color='g')


# Qfitlim = (0, 4)
# fmodel3 = TF1('fmodel3', TF1_signature(model3), *Qfitlim, 5)
# fmodel3.SetParameters(
# 			0.9*N1, mean_Q09f,
# 			0.1*N1, mean_Q09f, std_Q09f)
# Qhist25.Fit(fmodel3, 'R0')
# print(get_rchi2(fmodel3))
# N0, lam0, N1, mu1, sig1 = get_parameters(fmodel3)
# fbce, fbinnums = fbins(Qfitlim)
# plt.plot(fbce, poi_branch(fbce, N0, lam0), color='magenta', ls='--')
# plt.plot(fbce, gauss(fbce, N1, mu1, sig1), color='magenta', ls=':')
# plt.step(fbce, [fmodel3.Eval(Q) for Q in fbce], where='mid', color='g')



# fpolya = TF1('fpolya', TF1_signature(polya), *Qfitlim, 3)
# fpolya.SetParameters(N1, mean_Q09f, (mean_Q09f/std_Q09f)**2)
# Qhist25.Fit(fpolya, 'R0')
# print(get_rchi2(fpolya))
# plt.step(fbce, [fpolya.Eval(Q) for Q in fbce], where='mid', color='orange')


plt.ticklabel_format(axis='both', style='sci',
										 scilimits=(0,0), useMathText=True)
show()