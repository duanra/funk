#!usr/bin/env python3

import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt

from array import array
from matplotlib import rc
from scipy.stats import expon, gamma, beta
from warnings import catch_warnings, filterwarnings
from mod.funk_plt import show, pause, autofmt_xdate
from mod.funk_utils import fmt, table_lr, posix2utc
from mod.funk_stats import bin_centers, bin_widths,\
													 pareto2
from mod.funk_ROOTy import TF1_signature, add_fitresults, get_rchi2,\
													 get_parameters, get_fitresults, get_histogram
from ROOT import TH1D, TF1
from root_numpy import fill_hist

rc('font', size=10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def umap_expon(rvs, lam=None):
	""" probabilility integral transform of the exponential (CDF)	"""
	if lam is None:
		lam = np.mean(rvs)
	return 1 - np.exp(-lam*rvs)


def transformed_gamma(y, n, theta, lam):
	from scipy.special import gamma as gf
	return 1 / (lam * (1-y) * theta**n * gf(n))\
				 	 * np.power(1-y, 1/(theta*lam))\
				 	 * np.power(-np.log(1-y)/lam, n-1)


def transformed_pareto2(y, n, theta, lam):
	""" probability of Y=F(X) where F is the CDF of expon(lam) and X~pareto2
			with y in [0,1), i.e apply umap_expon transform of pareto2 rvs
	"""
	with catch_warnings(record=True) as w:
		filterwarnings('always')
		ret = n*theta / (lam*(1-y)) * np.power(1 - theta/lam * np.log(1-y), -n-1)
		if len(w):
			print('[Warning] transformed_pareto2:', w[-1].message, 
						', input params', y, n, theta, lam)
	return ret 


def transformed_sum(y, N1, n1, theta1, N2, n2, theta2, lam):
	return N1 * transformed_gamma(y, n1, theta1, lam) +\
				 N2 * transformed_pareto2(y, n2, theta2, lam)


def fgamma(x, a, b):
	return gamma.pdf(x, a, 0, b)


def sum2(x, N1, a, b, N2, n, theta, lam):
	return N1*fgamma(x, a, b) + N2*transformed_pareto2(x, n, theta, lam)


def cut(arr, bounds):
	if bounds is None:
		return arr
	else:
		low, up = bounds
		return arr[(arr>=low) & (arr<=up)]


def rate_parameters(df):
	trig_rates = df.groupby('time').size().values / 60 # roughly
	mu  	 = np.mean(trig_rates)
	sig 	 = np.std(trig_rates, ddof=1)
	n 		 = (mu / sig)**2 # for the pareto2 (not the gamma part)
	theta  = mu / n

	return mu, sig, n, theta


def hist_step(th1, ax=None, color='midnightblue', **kwargs):
	""" kwargs are common kw arguments to step, hlines, vlines """
	kwargs.setdefault('color', 'midnightblue')
	kwargs.setdefault('lw', 0.8)
	if ax is None:
		ax = plt.gca()

	hist, edg = get_histogram(th1)
	bce = bin_centers(edg)
	ax.step(bce, hist, where='mid', **kwargs)
	ax.vlines(edg[0], 0, hist[0], **kwargs) # purely aestetic
	ax.hlines(hist[0], edg[0], bce[0], **kwargs)
	ax.vlines(edg[-1], 0, hist[-1], **kwargs)
	ax.hlines(hist[-1], bce[-1], edg[-1], **kwargs)


def fplot(tf1, xrange=None, scale=1, ax=None, **kwargs):
	kwargs.setdefault('color', 'red')
	kwargs.setdefault('lw', 1)
	if ax is None:
		ax = plt.gca()
	if xrange is None:
		xrange = np.linspace(tf1.GetXmin(), tf1.GetXmax(), tf1.GetNpx())
	elif isinstance(xrange, tuple):
		xrange = np.linspace(*xrange, 100)
	else:
		pass #assume have passed iterator

	ax.plot(xrange,
					scale*np.array([tf1.Eval(x) for x in xrange]),
					**kwargs)


# df = pd.read_pickle('./data/trace_dtdistn_1536585179.sdf') # light
# df = pd.read_pickle('./data//trace_dtdistn_1535449844.sdf') 
df = pd.read_pickle('./data/trace_ttag_1535701375.sdf') # full
mu, sig, n, theta = rate_parameters(df)
time_idx = df.index.unique().values

dts = array('d', [])
for t in time_idx:
	eventTT = df.loc[t, 'ttag'].values
	dts.extend(eventTT[1:] - eventTT[:-1])
dts = np.array(dts)

dtmin  = min(dts)
dtmax  = max(dts)
dtmean = np.mean(dts)
bins 	 = int(1e2)
hrange = (dtmin, 3)

dts = cut(dts, hrange)
transformed_dts = umap_expon(dts, mu)

h = TH1D('h', 'transformed histo', bins, 0, 1)
fill_hist(h, transformed_dts)
h.Scale(1/h.Integral('width'))
frange = (h.GetBinCenter(2), h.GetBinCenter(bins))
hist_step(h)


# print('* '*40)
# f1 = TF1('f1', TF1_signature(transformed_gamma),
# 				 h.GetBinCenter(5), 0.2, 3)
# f1.SetParNames('n1', 'theta1', 'mu')
# f1.SetParameters(0.1, 1, mu)
# f1.FixParameter(2, mu)
# h.Fit(f1, 'R0')
# print(get_rchi2(f1))
# fplot(f1, np.linspace(*frange, bins))

# print('* '*40)
# f2 = TF1('f2', TF1_signature(transformed_pareto2),
# 				 0.2, frange[1], 3)
# f2.SetParNames('n2', 'theta2', 'mu')
# f2.SetParameters(n, theta, mu)
# f2.SetParLimits(0, 0, 1)
# f2.SetParLimits(1, 0, 500)
# f2.FixParameter(2, mu)
# h.Fit(f2, 'R0')
# print(get_rchi2(f2))
# # fplot(f2, color='maroon', lw=2)

# # print('* '*40)
# fsum = TF1('fsum', TF1_signature(transformed_sum),
# 					 *frange, 7)
# n1, theta1, _= get_parameters(f1)
# n2, theta2, _= get_parameters(f2)
# N1, N2 = 0.5, 0.5
# fsum.SetParNames('N1', 'n1', 'theta1', 'N2', 'n2', 'theta2', 'mu')
# fsum.SetParameters(N1, n1, theta1, N2, n2, theta2, mu)
# fsum.FixParameter(6, mu)
# h.Fit(fsum, 'R0')
# print(get_rchi2(fsum))


# print('* '*40)
# f1 = TF1('f1', TF1_signature(fgamma),
# 				 h.GetBinCenter(2), 0.1, 2)
# f1.SetParNames('a', 'b')
# f1.SetParameters(0.1, 1)
# f1.SetParLimits(0, 0, 1)
# h.Fit(f1, 'R0')
# print(get_rchi2(f1))
# fplot(f1, frange, color='green', ls='--', lw=1.5)


print('* '*40)
f2 = TF1('f2', TF1_signature(transformed_pareto2),
				 frange[0], frange[1], 3)
f2.SetParNames('n2', 'theta2', 'mu')
f2.SetParameters(n, theta, mu)
f2.SetParLimits(0, 0, 500)
f2.SetParLimits(1, 0, 1)
f2.FixParameter(2, mu)
h.Fit(f2, 'R0')
print(get_rchi2(f2))
fplot(f2, color='maroon', ls='--', lw=1.5)


# print('* '*40)
# fsum2 = TF1('fsum2', TF1_signature(sum2), *frange, 7)
# a, b = get_parameters(f1)
# n, theta, _= get_parameters(f2)
# N1, N2 = 0.2, 0.8
# fsum2.SetParNames('N1', 'a', 'b', 'N2', 'n', 'theta', 'lam')
# fsum2.SetParameters(N1, a, b, N2, n, theta, mu)
# fsum2.SetParLimits(0, 0, 1)
# fsum2.SetParLimits(1, 0, 1)
# fsum2.SetParLimits(3, 0, 1)
# fsum2.SetParLimits(5, 0, 1)
# fsum2.FixParameter(6, mu)
# h.Fit(fsum2, 'R0')
# print(get_rchi2(fsum2))


# fplot(f1, frange, scale=N1, color='green', ls='--', lw=1.5)
# fplot(f2, scale=N2, color='maroon', ls='--', lw=1.5)
# fplot(fsum2, color='k', lw=1.5)

# # # print(fsum.GetParameter(0), fsum.GetParameter(3))
plt.yscale('log')
# # # plt.savefig('./1/TT_distn2.png', dpi=600)
show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # THRESHOLD FOR CORRELATED EVENTS
# dtmin  = min(dts)
# dtmax  = max(dts)
# dtmean = np.mean(dts)
# p25  	 = np.percentile(dts, 25)
# p50  	 = np.percentile(dts, 50)
# p75  	 = np.percentile(dts, 75)
# bins 	 = int(1e7)
# hrange = (dtmin, 1)

# dts  			= cut(dts, hrange)
# hist, edg = np.histogram(dts, bins)
# bce 			= bin_centers(edg)
# bwi 			= bin_widths(edg)[0]
# nentries 	= len(dts)
# corrbins 	= hist[hist>0.2*max(hist)]
# dtlow 		= dtmin + bwi*len(corrbins) 

# print(table_lr(
# 	[
# 		'dt_min',
# 		'dt_max',
# 		'dt_mean',
# 		'bin_widths',
# 		'dt_threshold',
# 		'#histo entries',
# 		'#correlated bins',
# 		'#correlated triggers'
# 	],
# 	[
# 		dtmin,
# 		dtmax,
# 		dtmean,
# 		bwi,
# 		dtlow,
# 		nentries,
# 		len(corrbins),
# 		fmt(sum(corrbins) / nentries, '{:.2%}')
# 	]
# 	))

# plt.xlim(0, 5e-6)
# plt.step(bce, hist, where='mid', color='midnightblue')
# plt.vlines(edg[0], 0, hist[0], color='midnightblue') # purely aestetic
# plt.hlines(hist[0], edg[0], bce[0], color='midnightblue')

# ymax = plt.ylim()[1]
# plt.stem([p25, p50, p75], [0.9*ymax, 0.7*ymax, 0.5*ymax],
# 				  linefmt='k-.', basefmt='k-', markerfmt='ko')
# plt.text(p25, 0.93*ymax, 'p25')
# plt.text(p50, 0.73*ymax, 'p50')
# plt.text(p75, 0.53*ymax, 'p75')
# plt.stem([dtlow], [ymax], linefmt='k-.', basefmt='k-', markerfmt='ko')

# plt.ticklabel_format(axis='both', style='sci',
# 										 scilimits=(0,0), useMathText=True)

# # plt.yscale('log')
# # plt.savefig('./0/dt_distn2.png',dpi=600)
# show()