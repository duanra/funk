
#!usr/bin/env python3

import os
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt

from IPython import embed
from matplotlib.backends.backend_pdf import PdfPages
from mod.funk_plt import show, cornertext
from mod.funk_utils import islistlike, timing, table_lr,
													 Namespace, pickle_dump
from warnings import catch_warnings, filterwarnings

from iminuit import Minuit
from probfit import Chi2Regression

# import ROOT
# from ROOT import TF1
# from ROOT.Math import GSLIntegrator, WrappedTF1
# from mod.funk_ROOTy import TF1_signature

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# ig = GSLIntegrator()

class Model:

	def __init__(self, nopen, nclosed, length):

		so = [ [i, i+nopen] for i in range(nclosed, length, nopen+nclosed) ]
		del so[14]; so[13][1] = 3450 # correct for when the shutter didn't work

		sc = [ [i,i+nclosed] for i in range(0, length, nopen+nclosed) ]
		del sc[14]

		sa = [ x for y in zip(sc, so) for x in y ]
		sa.append(sc[-1])

		self.shutter_open 	= so
		self.shutter_closed = sc
		self.shutter_all 		= sa

		self.ipoints = np.arange(0, length)

		self.up, self.low, self.tau, self.alpha = [None]*4
		self._piecewise_sum = {}

		# self.TF1_integrand 	= TF1( self.integrand.__name__,
		# 													 TF1_signature(self.integrand),
		# 													 0, 1, 4 )



	def _save_parameters(self, up, low, tau, alpha):

		print('model params: up={}, low={}, tau={}, alpha={}'\
					.format(up, low, tau, alpha))

		self.up, self.low, self.tau, self.alpha = up, low, tau, alpha
		
		self._piecewise_sum = {}

		# self.saved_history = {} # for old_response2()


	def _tindex(self, t):
	
		if t < self.shutter_all[0][1]: # to include negative time
			ret = 0

		elif t >= self.shutter_all[-1][-1]:
			ret = len(self.shutter_all) - 1

		else:
			ret = np.flatnonzero([ s[0] <= t < s[1] for s in self.shutter_all ])[0]

		return ret

	
	def signal(self, t, up, low):
		
		return up if any([ s[0] <= t < s[1] for s in self.shutter_open ]) else low

	
	def _integrals(self, up, low, tau):

		for i, shutter in enumerate(self.shutter_all):

			if i==0:
				self._piecewise_sum[i] = low * tau

			else:
				t1, t2 = shutter
				tm = (t1+t2) / 2
				q  = self.signal(tm, up, low) * tau * (1 - np.exp( (t1-t2)/tau ))

				self._piecewise_sum[i] = q


	def history(self, t, up, low, tau, alpha):

		if any([ self.up!=up, self.low!=low, self.tau!=tau, self.alpha!=alpha ]):
			self._save_parameters(up, low, tau, alpha)
			self._integrals(up, low, tau)

		tix = self._tindex(t)

		if tix == 0:
			ret = alpha * self._piecewise_sum[0]

		else:
			s  = 0
			for i in range(0, tix):
				s += np.exp( (self.shutter_all[i][1]-t)/tau ) * self._piecewise_sum[i]
		
			s	+= self.signal(t, up, low) * tau *\
					 (1 - np.exp( (self.shutter_all[tix][0]-t)/tau) )
		
			ret =  alpha * s

		return ret


	def response(self, t, up, low, tau, alpha):

		return self.signal(t, up, low) + self.history(t, up, low, tau, alpha)


	# def integrand(self, tt, t, up, low, tau):
	
	# 	return self.signal(tt, up, low) * np.exp(-(t-tt)/tau)


	# def old_response(self, t, up, low, tau, alpha):
	
	# 	self.TF1_integrand.SetParameters(t, up, low, tau)

	# 	return self.signal(t, up, low) +\
	# 				 alpha * ig.IntegralLow(WrappedTF1(self.TF1_integrand), t)


	# def old_response2(self, t, up, low, tau, alpha):

	# 	if any([ self.up!=up, self.low!=low, self.tau!=tau, self.alpha!=alpha ]):
	# 		self._save_parameters(up, low, tau, alpha)
	# 	self.TF1_integrand.SetParameters(t, up, low, tau)

	# 	def _history():

	# 		if t in self.saved_history.keys():
	# 			return self.saved_history[t]
			
	# 		elif t-1 in self.saved_history.keys():
	# 			ret =  self.saved_history[t-1] * np.exp(-1/tau) +\
	# 						 ig.Integral(WrappedTF1(self.TF1_integrand), t-1, t)

	# 		else:
	# 			ret = ig.IntegralLow(WrappedTF1(self.TF1_integrand), t)

	# 		self.saved_history[t] = ret

	# 		return ret

	# 	return self.signal(t, up, low) + alpha*_history()


	def fit(self, data):
		
		triggers = data['triggers'].values
		errors 	 = np.sqrt(triggers)
		time  	 = self.ipoints

		m = Minuit(
					Chi2Regression(self.pmt_response, time, triggers, errors),
					up=200, low=50, tau=50, alpha=0.05,
					limit_up=(0, None), limit_low=(0, None),
					limit_tau=(0, None), limit_alpha=(0,1),
					error_up=0.1, error_low=0.1,
					error_tau=0.1, error_alpha=0.01,
					errordef=1 )

		timing(m.migrad)()
		print('reduced chi2: {:.3f} / {}'.format(m.fval, len(triggers)-4))
		self.fitted = m


	def plot(self, ax=None):

		if ax is None:
			ax = plt.gca()

		xdata  = self.ipoints
		params = self.fitted.values
		errors = self.fitted.errors
		
		response = np.array([
				self.pmt_response(x, **params) for x in xdata ])

		signal 	 = np.array([
				self.signal(x, params['up'], params['low']) for x in xdata ])

		ax.plot(xdata, response, color='darkred', lw=1.5, zorder=10)

		ax.plot(xdata, signal, color='orange', lw=1, zorder=5)

		fitresult = table_lr(
				[ 'up', 'low', 'tau', 'alpha', ' ', 'rchi2'],
				[ '{:.3f} +- {:.3f}'.format( params[x], errors[x] )\
							for x in ['up', 'low', 'tau', 'alpha']
				] + [' ', '{:.3f}'.format(self.fitted.fval/(len(xdata)-4)) ]
				)

		cornertext(fitresult, axes=ax)




def read_data(infile):

	dtype = [
		('time', np.uint),
		('triggers', np.uint),
		('dt_long', np.float),
		('dt_short', np.float),
		('closed', np.uint8)
		]

	with catch_warnings():
		filterwarnings('ignore', category=UserWarning)
		data = pd.DataFrame(
							np.genfromtxt(
								infile, dtype=dtype, usecols=(0, 3, 6, 7, 10),
								invalid_raise=False, encoding=None, max_rows=None	))

	data = data.assign( dt=(data['dt_long']+data['dt_short'])/2 )
	data = data.assign( rates=data['triggers']/data['dt'] )
	data = data[['time', 'dt', 'triggers', 'rates', 'closed']]

	return data


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


infile 	= './data/logLonOpen_newSh_30-200_2019-02-08.log'
df 			= read_data(infile)
# df 			= df.iloc[:200+12*230]
nopen 	= 30
nclosed = 200
length 	= len(df)

model = Model(nopen, nclosed, length)
up, low, tau, alpha = 200, 50, 50, 0.05
time 	= model.ipoints

# plt.plot(time, df['triggers'])
# plt.plot(time, [model.signal(t, up, low) for t in time])
# show()
# embed()

time = model.ipoints
@timing
def f1():
	return np.array([ model.response(t, up, low, tau, alpha) for t in time ])

r1 = f1()

plt.plot(df['triggers'])
plt.plot(model.ipoints, [model.signal(t, up, low) for t in model.ipoints])

show()
embed()



# fig, ax = plt.subplots(figsize=(10,6))

# model = Model(nopen, nclosed, length)
# time 	= model.ipoints


# triggers = df['triggers'].values
# ax.errorbar(time, triggers, yerr=np.sqrt(triggers),
# 						fmt='s', ms=2, elinewidth=.5)

# model.fit(df)
# model.plot()

# ax.set_xlabel('time [mins]')
# ax.set_ylabel('ntriggers [per min]')

# pickle_dump({
# 		**model.fitted.fitarg,
# 		'fval': model.fitted.fval,
# 		'dof' : len(df)-4
# 		},'fit_asone', './data/sig_history/' )


# fig.tight_layout()
# fig.savefig('./data/sig_history/out2.pdf', format='pdf')
# show()
# embed()