import __pardir__
import numpy as np
import numpy.linalg as nalg
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

# from collections import OrderedDict
from datetime import datetime, timedelta
from scipy.stats import binned_statistic
from sys import exit
from mod.funk_plt import pause, show
from mod.funk_run import Run
from mod.funk_utils import posix2utc, utc2posix, fmt, table_lr

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def daily_stats(run):
	""" return (no. data, mean rate, std rate) daily """

	def _get_timebins(df):
		""" get day bins, a day is defined from-6-to-6 am """ 
		tbegin, tend = posix2utc(df['time'].iloc[[0,-1]])
		dtdays = (tend - tbegin).days
		t0 = datetime(tbegin.year, tbegin.month, tbegin.day, 6, 0, 0)

		return utc2posix([t0 + timedelta(i) for i in range(dtdays)])

	def _splitting_indices(run, init_pos):
		""" get (integer) indices to split data in daily parts	"""
		df 	 = run.get_df(init_pos)
		timestamps = df['time']
		timebins = _get_timebins(df)

		ndata = [] # number of data points (daily)
		for i in range(len(timebins)-1):
			 ndata.append(
			 		len(timestamps[(timestamps>=timebins[i]) &\
			 									 (timestamps<timebins[i+1])]))
		ndata.append(len(timestamps[timestamps>=timebins[i+1]]))

		return np.cumsum(ndata)[:-1]

	mpos = {key:val for val, key in fcn.positions.items()}
	mseq = list(map(tuple, run.configs.head(1)[['in', 'open']].values))

	init_pos = mpos[mseq[0]]
	sidx		 = _splitting_indices(run, init_pos)

	dailys = {}

	for key in mseq:
		pos  = mpos[key]
		df 	 = run.get_df(pos)[['time', 'rates']]

		dailys[pos] = np.split(df, sidx)

	return dailys


def daily_ndata(dailys):
	ret = {
			pos: np.array([len(d) for d in dailys[pos]]) \
			for pos in dailys.keys() }

	return ret


def daily_meanrates(dailys):
	ret = {
			pos: np.array([d['rates'].mean() for d in dailys[pos]]) \
			for pos in dailys.keys() }

	return ret


def daily_stdrates(dailys):
	ret = {
			pos: np.array([d['rates'].std() for d in dailys[pos]]) \
			for pos in dailys.keys() }

	return ret


def daily_weights(dailys):
	n 	= {
			pos: sum(map(len, dailys[pos])) for pos in dailys.keys()}
	ret = {
			pos: np.array([len(d) / n[pos] for d in dailys[pos]]) \
			for pos in dailys.keys() }

	return ret


def daily_dates(dailys):
	ret = {
			pos: np.array([posix2utc(d['time'].iloc[0]) for d in dailys[pos]]) \
			for pos in dailys.keys() }

	return ret


def get_Bint(**kwargs):
	""" kw options are spu, trig and cuts """
	run = Run.configs(run_v='v31', **kwargs)
	df = run.get_df('in_closed')[['time', 'rates']]

	return len(df), df['rates'].mean(), df['rates'].std()


def get_matrices(Bint=None, run=None, dailys=None, option=1):
	""" return a, b such that a X = b where
				X = (S, Bair_in, Bair_out, ...,  eps_inv)
			
			option: 0 	split the data then take the weighted average
						 	1 	split to daily measurements
			(if dailys is given)
	"""
	if Bint is None:
		print('[Exception] get_matrices(): need Bint')
		exit(0)

	seq = ['in_open', 'in_closed', 'out_open', 'out_closed']

	base_a = np.array([
							[1, 1, 0,  0],
							[0, 1, 0, -1],
							[0, 0, 1,  0],
							[0, 0, 1, -1]
							], dtype=np.float)

	base_b = np.array([1, 0, 1, 0], dtype=np.float)

	if run is not None:

		rates, varns = np.zeros(4), np.zeros(4)
		for i, pos in enumerate(seq):
			df = run.get_df(pos)['rates']
			rates[i] = df.mean() - Bint[1]
			varns[i] = df.var() / len(df) + Bint[2]**2 / Bint[0]

		a, b 		 = base_a.copy(), base_b.copy()
		a[:,-1] *= rates
		b[:] 		*= rates

		var_a, var_b  = np.abs(base_a.copy()), base_b.copy()
		var_a[:,:-1] *= 0
		var_a[:,-1]	 *= varns
		var_b[:]		 *= varns

	elif dailys is not None:

		ndata 		=	daily_ndata(dailys)
		meanrates = daily_meanrates(dailys)
		stdrates  = daily_stdrates(dailys)
		weights 	= daily_weights(dailys)
		stderrors = {}

		# substract Bint and get standard errors
		for pos in seq:
			meanrates[pos] -= Bint[1]
			stderrors[pos] 	= np.sqrt(stdrates[pos]**2 / ndata[pos] +\
																Bint[2]**2 / Bint[0])

		if option==0:

			rates, varns = np.zeros(4), np.zeros(4)
			for i, pos in enumerate(seq):
				rates[i] = np.average(meanrates[pos], weights=weights[pos])
				varns[i] = np.sum((weights[pos]**2 * stderrors[pos]**2)) \
											/ np.sum(weights[pos])**2

			a, b 		 = base_a.copy(), base_b.copy()
			a[:,-1] *= rates
			b[:] 		*= rates

			var_a, var_b  = np.abs(base_a.copy()), base_b.copy()
			var_a[:,:-1] *= 0
			var_a[:,-1]	 *= varns
			var_b[:]		 *= varns

		else:

			ndays = len(ndata[seq[0]])
			m, n 	= 4*ndays, 3*ndays+1

			def _base():
				""" build base maTix """
				a = np.zeros((m, n))
				for i, j in zip(range(0, m, 4), range(0, n, 3)):
					a[i:i+4, j:j+3] = base_a[:4, :3]
					# a[1::2,-1] = -1
				
				b = np.zeros(m)
				# b[0::2] = 1
				return a, b

			a, b = _base()
			var_a, var_b = np.zeros_like(a), np.zeros_like(b)

			for i,k in zip(range(0,m,4), range(ndays)):
				a[i+1,-1] 		= -meanrates['in_closed'][k]
				a[i+3,-1] 		= -meanrates['out_closed'][k]

				b[i+0] 				= meanrates['in_open'][k]
				b[i+2] 				= meanrates['out_open'][k]

				var_a[i+1,-1] = stderrors['in_closed'][k]**2
				var_a[i+3,-1] = stderrors['out_closed'][k]**2
				
				var_b[i+0] 		= stderrors['in_open'][k]**2
				var_b[i+2] 		= stderrors['out_open'][k]**2

	else:
		print('[Exception] get_matrices(): need arg run or dailys')
		exit(0)

	return a, b, var_a, var_b


def solve_rates(a, b, var_a, var_b, method='auto'):
	""" method: auto, inverse or lsq """

	if method=='auto': method = ('inverse' if a.shape==(4,4) else 'lsq')

	def _var_inv(mInv, var_m):
		""" calculate the variances of the elements 
				of the matrix inverse
		"""
		return nalg.multi_dot([mInv**2, var_m, mInv**2])

	def _var_aTa(a, var_a):
		""" calculate the variances of the elements
				of the matrix product transpose(a).a
		""" 
		shape = a.shape
		ret 	= np.zeros((shape[1], shape[1]))

		for i in range(shape[1]):
			for j in range(shape[1]):
				for k in range(shape[0]):
				# ret[i,j] += a[k,i] * a[k,j]
					if i==j:
						ret[i,j] += (2*a[k,i])**2 * var_a[k,i]
					else:
						ret[i,j] += var_a[k,i] * a[k,j]**2 +\
												a[k,i]**2  * var_a[k,j]

		return ret

	if method=='inverse':
		print("[Info] solve_rates(method='inverse'):")

		aInv 		 		= nalg.inv(a)
		var_aInv 		= _var_inv(aInv, var_a)

		solns 			= np.dot(aInv, b)
		error_solns = np.sqrt( np.dot(aInv**2, var_b) +\
													 np.dot(var_aInv, b**2) )

		residuals 	= None

	else: # by least-squares
		print("[Info] solve_rates(method='lsq'):")

		aT   	 		 	= np.transpose(a)
		aTa 	 		 	= np.dot(aT, a)
		aTaInv 		 	= nalg.inv(aTa)

		var_aT 	 	 	= np.transpose(var_a)
		var_aTa 	 	= _var_aTa(a, var_a)
									# np.dot(var_aT, a**2) + np.dot(aT**2, var_a)
		var_aTaInv	= _var_inv(aTaInv, var_aTa)

		# print(m_inv.shape, aT.shape, b.shape)
		solns				= nalg.multi_dot([aTaInv, aT, b])
		error_solns	= np.sqrt( nalg.multi_dot([var_aTaInv, aT**2, b**2]) +\
													 nalg.multi_dot([aTaInv**2, var_aT, b**2]) +\
													 nalg.multi_dot([aTaInv**2, aT**2, var_b]) )
		residuals 	= b - np.dot(a, solns)


	return solns, error_solns, residuals


# def get_daily_solns(solns, error_solns, residuals):
# 	Xs = np.array([
# 					solns[0:-1:3],
# 					solns[1:-1:3],
# 					solns[2:-1:3],
# 					solns[-1]
# 					])

def average_daily_solns(solns, error_solns, residuals):

	Xs 				= np.array([
								np.mean(solns[0:-1:3]), # unweighted ???
								np.mean(solns[1:-1:3]),
								np.mean(solns[2:-1:3]),
								solns[-1],
								])

	n 				= len(solns[:-1]) / 3
	error_Xs 	= np.array([
								np.sqrt(np.sum(error_solns[0:-1:3]**2)) / n,
								np.sqrt(np.sum(error_solns[1:-1:3]**2)) / n,
								np.sqrt(np.sum(error_solns[2:-1:3]**2)) / n,
								error_solns[-1]
								])

	if residuals is not None:
		residual2 = nalg.norm(residuals)**2 
	else:
		residual2 = None

	return Xs, error_Xs, residual2


def print_results(solns, error_solns, residuals=None):
	def _fmt(x, error_x):
		return '{:.5f} +- {:.5f}'.format(x, error_x)

	eps_bar 			= 1 - 1 / solns[-1]
	error_eps_bar	= error_solns[-1] / solns[-1]**2

	left_col 	= [
			'signal',
			'Bair_in',
			'Bair_out',
			'shutter',
			'residual2'
			]	

	right_col = [
			_fmt(solns[0], error_solns[0]),
			_fmt(solns[1], error_solns[1]),
			_fmt(solns[2], error_solns[2]),
			_fmt(eps_bar, error_eps_bar),
			fmt(residuals)
			]

	print(table_lr(left_col, right_col, hlines=True))


cuts = fcn.cuts.select('dtlim')
run  = Run.configs(run_v='v25', trig=True, cuts=cuts)
Bint = get_Bint(trig=True, cuts=cuts)


# a, b, var_a, var_b = get_matrices(Bint, run=run)
# solns, error_solns, _= solve_rates(a, b, var_a, var_b,
# 																	 method='inverse')
# print_results(solns, error_solns)

dailys = daily_stats(run)
a, b, var_a, var_b = get_matrices(Bint, dailys=dailys, option=1)
solns, error_solns, residuals = solve_rates(a, b, var_a, var_b,
																						method='auto')
print_results(*average_daily_solns(solns, error_solns, residuals))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# dailys = daily_stats(run)
# ndata 		=	daily_ndata(dailys)
# meanrates = daily_meanrates(dailys)
# stdrates  = daily_stdrates(dailys)
# # dates 		= daily_dates(dailys)

# for pos in ['in_open', 'out_open', 'in_closed', 'out_closed']:
# 	y 	 = meanrates[pos]
# 	yerr = stdrates[pos] / np.sqrt(ndata[pos])
# 	x 	 = np.arange(len(y))
# 	plt.errorbar(x, y, yerr, fmt='s-', lw=.3,
# 							 ms=2, elinewidth=1.2, capsize=2,
# 							 color=fcn.color[pos], label=pos)

# plt.legend()
# show()