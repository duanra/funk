#!usr/bin/env python3

import os
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt

from array import array
from datetime import timedelta
from IPython import embed
from iminuit import Minuit
from matplotlib import rc

import mod.funk_consts as fcn
from mod.funk_manip import reset_configs
from mod.funk_plt import show, order_legend
from mod.funk_run import Run
from mod.funk_utils import  posix2utc, flatten, timing,\
														dump_obj, load_obj, Namespace

rc('font', size=10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

class Model:

	def __init__(self, data, base, alpha, tau):

		seq 	 	= list(map(tuple, data[['in', 'open']].iloc[:4].values))
		# [ i for i,x in enumerate(seq) if x==fcn.positions['in_open'] ][0]
		ix_in  	= seq.index(fcn.positions['in_open'])
		ix_out 	= seq.index(fcn.positions['out_open'])

		m 			= 4*30
		n 			= len(data)
		tintvls = [ (i, i+1) for i in range(n) ]
		tbins 	= [ (0+i, i+m if i+m<n else n) for i in range(0, n, m) ]

		self.ix_in 	 = ix_in
		self.ix_out	 = ix_out
		self.tintvls = tintvls
		self.tbins 	 = tbins
		self.tintvl_len = 1
		self.tbin_len 	= m
		self.events  = data['counts']

		self._init_parameters(base, alpha, tau)


	def _init_parameters(self, base, alpha, tau):

		params = Namespace(
							flatten([
									[('in%d'%i, 200), ('out%d'%i, 180)]\
									for i in range(len(self.tbins))
							]))

		params.low 	 = 0
		params.base  = base
		params.alpha = alpha
		params.tau 	 = tau

		self.params  = params
		self.saved_history = {}
		self._integrals()


	def _set_parameters(self, *args, **kwargs):
		""" only use this to update params, do not manually assign
				params to avoid more overhead check upon update
		"""

		if len(args): # all params must be given at once and keep the order
			new_params = Namespace( zip(self.params.keys(), args) )
			if self.params != new_params:
				self.params.update(new_params)
				self.saved_history = {}
				self._integrals()

		elif len(kwargs): # only for checking purpose, not prefered method
			if all([ kw in self.params.keys() for kw in kwargs.keys() ]):
				self.params.update(kwargs)
				self.saved_history = {}
				self._integrals()

		else:
			print('_set_parameters(): no parameters given')
			return


	def signal(self, t):

		if t < self.tintvls[0][0] or t >= self.tintvls[-1][-1]:
			ret = 0

		else:
			i = int(t / self.tbin_len) # integer division returns a fucking float
			k = int(t / self.tintvl_len) % 4
			if k==self.ix_in:
				ret = self.params['in%d'%i]
			elif k==self.ix_out:
				ret = self.params['out%d'%i]
			else:
				ret = self.params['low']

		return ret

		# for i, (t1, t2) in enumerate(self.tbins):
		# 	if t1 <= t < t2:
		# 		for j in range(t2): # check in which tintvl (faster than list lookup)
		# 			if 0 <= t-t1-j < 1: 
		# 				k = j%4
		# 				if k==self.ix_in:
		# 					return self.params['in%d'%i]
		# 				elif k==self.ix_out:
		# 					return self.params['out%d'%i]
		# 				else:
		# 					return self.params['low']

		# else:
		# 	return 0 # signal is on top of base


	def _integrals(self):

		self._piecewise_sum = np.array([
				self.signal(t1) * ( 1 - np.exp((t1-t2)/self.params.tau) )\
				for (t1, t2) in self.tintvls
				])
		

	def _tindex(self, t):
	
		if t < self.tintvls[0][0]:
			ret = -1

		elif t >= self.tintvls[-1][-1]:
			ret = len(self.tintvls)

		else:
			ret = int(t / self.tintvl_len)
			# ret = np.flatnonzero([t1 <= t < t2 for (t1, t2) in self.tintvls])[0]

		return ret


	def history(self, t, *args):
		
		if len(args): self._set_parameters(*args)

		tix = self._tindex(t)

		if tix == -1:
			ret = 0

		else:
			s = 0 # self.tau * self.base * np.exp( -t/self.tau )
			alpha, tau = self.params.alpha, self.params.tau

			for i in range(0, tix):
				t2 = self.tintvls[i][1]
				s += self._piecewise_sum[i] * np.exp((t2-t)/tau)
		
			if tix < len(self.tintvls):
				t1 = self.tintvls[tix][0]
				s	+= self.signal(t) * ( 1 - np.exp((t1-t)/tau) )

			ret =  alpha * tau * s

		return ret


	def history2(self, t, *args):
		""" save history to speed up evaluation for t+1 and all t MUST be
				within tintvls bounds to avoid additional overheads
		"""

		if len(args): self._set_parameters(*args)

		alpha, tau = self.params.alpha, self.params.tau

		if t in self.saved_history.keys():
			# print('in saved')
			return self.saved_history[t]

		elif t-1 in self.saved_history.keys():
			# print('using history')
			tt 	 = t-1
			ttix = int(tt) # self._tindex(tt)
			
			tt1, tt2 = self.tintvls[ttix]

			h 	 = self.saved_history[tt] + alpha * tau * self.signal(tt1) \
							* ( np.exp((tt2-tt)/tau) * (1 - np.exp((tt1-tt2)/tau))
									- (1 - np.exp((tt1-tt)/tau)) )
	
			ret  = alpha * tau * self.signal(tt2) * (1 - np.exp((tt2-t)/tau)) \
							+ np.exp(-1/tau) * h

		else:
			ret  = self.history(t)

		self.saved_history[t] = ret

		return ret


	def response(self, t, *args):
		""" params checks are only done in history """

		h = self.history2(t, *args)
		i = self.signal(t)
		b = self.params.base

		return b + i + h


	# def response2(self, t, *args):
	# 	""" params checks are only done in history """

	# 	h = self.history2(t, *args)
	# 	i = self.signal(t)
	# 	b = self.params.base

	# 	return b + i + h


	def fit(self, **kwargs):
		""" response is calculated at the middle of each tintvl, this is a 
				good approximation for the mean event rate if the response is
				linear within that tintvl (and tintvl is assumed to be 1)
		"""

		data = self.events.values
		time = np.arange(len(data)) + 0.5

		nclock 			 = len(self.tbins)
		init_fitargs = self.params.copy()
		init_fitargs.update(
			Namespace(
				[ ('limit_in%d'%i, (0, None)) for i in range(nclock) ] +
				[ ('limit_out%d'%i, (0, None)) for i in range(nclock) ] +
				[ ('limit_low', (0, None)), ('limit_base', (0, None)),
					('limit_tau', (0, None)), ('limit_alpha', (0,1)) ] +
				[ ('error_in%d'%i, 0.1) for i in range(nclock)] +
				[ ('error_out%d'%i, 0.1) for i in range(nclock) ] +
				[ ('error_low', 0.1), ('error_base', 0.1),
					('error_tau', 0.1), ('error_alpha', 0.01) ] +
				[ ('fix_low', True), ('fix_base', True),
					('fix_tau', True), ('fix_alpha', True) ] +
				[ ('errordef', 1) ]
			))

		def lsq(*args):
			self._set_parameters(*args)
			response = np.array([ self.response(t) for t in time ])

			return np.sum( (data - response)**2 / data )

		parnames = list(self.params.keys())

		mfit = Minuit(lsq, forced_parameters=parnames, **init_fitargs)

		print('running migrad...')
		timing(mfit.migrad)(**kwargs)
		print('reduced chi2: {:.3f} / {}'\
					.format(mfit.fval, len(data)-len(self.params)-4) )

		self.fitted = mfit


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


def get_data(run, begin=None, Dt=None):
	""" begin/Dt: timedelta object (duration to retrieve) """

	df = reset_configs(run.configs)\
				[['time', 'in', 'open', 'dt', 'counts']]
	df = df.assign(
					timedelta = posix2utc(df['time']) - \
											posix2utc(df['time'].iloc[0])
					)

	if (begin is not None) and (Dt is not None):
		ret = df[ (df['timedelta']>= begin) & (df['timedelta'] <= begin+Dt) ]

	elif begin is not None:
		ret = df[ (df['timedelta']>= begin) ]

	elif Dt is not None:
		ret = df[ (df['timedelta'] <= Dt) ]

	else:
		ret = df

	return ret.iloc[:len(ret)-len(ret)%4]\
						.reset_index(drop=True)



# cuts  = fcn.cuts.select('dtlim')
cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
trig 	= False
run_v = 'v35'
run 	= Run.configs(run_v, trig=trig, cuts=cuts)

begin = None # timedelta(hours=5)
Dt 		= None

base 	= 96.3677
alpha = 0.0275
tau 	= 52.879

data 	= get_data(run, begin, Dt)
model = Model(data, base, alpha, tau)

fitarg = load_obj(
					'fitarg.sdic', fpath='./log/sig_history_v35/save_4x30/' )
params = [ fitarg[kw] for kw in model.params.keys() ]
model._set_parameters(*params)

time 	 	 = np.arange(len(model.events)) + 0.5
signal 	 = model.params.base + np.array([ model.signal(t) for t in time ])
response = np.array([ model.response(t) for t in time ])



gdata = data.groupby(['in', 'open'])
din 	= gdata.get_group((1,1))['counts'].values
dout  = gdata.get_group((0,1))['counts'].values

sin 	= signal[model.ix_in::4]
sout 	= signal[model.ix_out::4]

rin 	= response[model.ix_in::4]
rout 	= response[model.ix_out::4]

# truncate data and read error from that (using standard deviation)
# TODO: reconstructed signal std seems low compared to truncated data ???
# 			maybe some binning effect in the fitting procedure

din_, dout_ = din[50:], dout[50:]
sin_, sout_	= sin[50:], sout[50:]

ndata 		= len(din_)
dt 				= data['dt'].mean()
rate_in 	= np.mean(sin_) / dt
rate_out 	= np.mean(sout_) / dt
rate_diff = rate_in - rate_out
err_in 		= np.std(din_, ddof=1) / (np.sqrt(ndata) * dt)
err_out 	= np.std(dout_, ddof=1) / (np.sqrt(ndata) * dt)
err_diff 	= np.sqrt(err_in**2 + err_out**2)
print(rate_diff, err_diff)

# projected sensitivity
ndata 		= len(data) # equivalent to 8 weeks worth of data taking
err_in 		= np.std(din_, ddof=1) / (np.sqrt(ndata) * dt)
err_out 	= np.std(dout_, ddof=1) / (np.sqrt(ndata) * dt)
err_diff 	= np.sqrt(err_in**2 + err_out**2)
print(rate_diff, err_diff)

embed()

# tin 	= time[model.ix_in::4]
# tout 	= time[model.ix_out::4]

# plt.scatter(tin, din, s=1, color='r', label='data (in)')
# plt.plot(tin, sin, color='r', label='signal (in)')
# plt.plot(tin, rin, color='r', label='response (in)')
# plt.plot(tout, sout, color='b', label='signal (out)')
# plt.plot(tout, rout, color='b', label='response (out)')

# show()
# embed()