#!usr/bin/env python3
import numpy as np 
import matplotlib.pyplot as plt

from scipy.stats import gamma, norm
from mod.funk_utils import fn_over
from mod.funk_manip import load_configsX, get_df
from mod.funk_plt import show, pause
from mod.funk_stats import bin_centers, bin_widths, integer_binning
from mod.funk_ROOTy import ( TF1_signature, get_histogram,
														 get_fitresults, add_fitlegend )
from ROOT import TF1, TH1F
from root_numpy import fill_hist, hist2array

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

configs_trigs = load_configsX('v25', var='counts', trig=True)
configs_rates = load_configsX('v25', var='rates', trig=True)
key 	= 'in_open'
trigs = get_df(configs_trigs, key)['counts'].values
rates = get_df(configs_rates, key)['rates'].values


fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(10, 6))
bins, low, up = integer_binning(min(trigs), max(trigs))
hist = TH1F('hist', 'trigger count frequency', bins, low, up)
fill_hist(hist, trigs)
# hist.Scale(1/hist.Integral('width'))


fnbinom = TF1('fnbinom',
						 	'[0]*ROOT::Math::negative_binomial_pdf(x,[1],[2])',
							200, 800)
fnbinom.SetParNames('N', 'p', 'n')
fnbinom.SetParameters(1, 0.1, 50)
# fnbinom.FixParameter(0, 1)
fnbinom.SetParLimits(1, 0, 1)
hist.Fit(fnbinom, 'R0')


pyhist = get_histogram(hist, return_yerr=True)
fitted = get_fitresults(fnbinom)
x = bin_centers(pyhist[2])
ax0.errorbar(
	x, pyhist[0], pyhist[1], color='midnightblue',
	fmt='s', ms=2, elinewidth=.5)
x = np.arange(200, 800)
ax0.plot(
	x, [fnbinom.Eval(xval) for xval in x],
	color='red', zorder=10)
ax0.set_title('trigger count frequency')
add_fitlegend(fnbinom, ax=ax0)


binned_evrates = fn_over(rates, m=15)
hist = TH1F('hist', 'event mean rate distns',
						50, min(binned_evrates), max(binned_evrates))
fill_hist(hist, binned_evrates)


fgamma = TF1('fgamma', 
						 '[0]*ROOT::Math::gamma_pdf(x, [1], [2])',
						 4, 10)
fgamma.SetParNames('N', 'a', 'theta')
fgamma.SetParameters(1, 1, 1)
hist.Fit(fgamma, 'R0')


pyhist = get_histogram(hist, return_yerr=True)
fitted = get_fitresults(fgamma)
x = bin_centers(pyhist[2])
ax1.errorbar(
	x, pyhist[0], pyhist[1], color='midnightblue',
	fmt='s', ms=2, elinewidth=.5)
x = np.linspace(4, 10)
ax1.plot(
	x, [fgamma.Eval(xval) for xval in x],
	color='red', zorder=10)
ax1.set_title('average event rate (binned over 1h)')
add_fitlegend(fnbinom, ax=ax1)


# fig.savefig('./0/count_distns.png', dpi=600)
show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# def two_poi(x, par):
# 	return par[0]*two_poisson.pmf(x[0], par[1], par[2])
# fitfn2 = TF1('fitfn2', two_poi, low, up, 3)
# fitfn2.SetNpx(bins)
# fitfn2.SetParNames('N2', 'mu1', 'mu2')
# fitfn2.SetParameters(1, 20, 20)
# fitfn2.FixParameter(0, 1)
# fitfn2.SetParLimits(2, 0, 200)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# from iminuit import Minuit
# from probfit import UnbinnedLH
# from scipy.stats import nbinom

# data = data[(data>200) & (data<800)]
# xval = np.sort(np.unique(data))
# bins = np.append(xval, [xval[-1]+1]) # build pmf
# hval, hedg, = np.histogram(data, bins=bins)
# pmf  = hval/np.sum(hval)

# def nb(x, n, p):
# 	return nbinom.pmf(x, n, p)
# nb_negll = UnbinnedLH(nb, data)
# nb_fit   = Minuit(nb_negll, n=50,  error_n=1,
# 								p=0.1, error_p=0.01, limit_p=(0, 1), 
# 								errordef=0.5)
# nb_fit.migrad()

# fig, ax = plt.subplots()
# xnew = np.arange(xval[0], xval[-1]+1) # in case of missing value
# ax.vlines(xval, 0, pmf)
# ax.plot(xnew, nb(xnew, **nb_fit.values), label='negative binomial')
# plt.legend()
# plt.show()
# plt.close()