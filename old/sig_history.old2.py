#!usr/bin/env python3

import os
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt

from IPython import embed
from iminuit import Minuit
from itertools import accumulate, cycle
from matplotlib import rc
from contextlib import suppress
from warnings import catch_warnings, filterwarnings
# from inspect import signature
# from functools import wraps

from mod.funk_plt import show
from mod.funk_utils import islistlike, timing, pickle_dump, Namespace

rc('font', size=10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

class Model:

	def __init__(self, nopen, nclosed, length, toffset=0):

		so = [ [i, i+nopen] for i in range(nclosed, length, nopen+nclosed) ]
		with suppress(IndexError):
			del so[14]; so[13][1] = 3450
			del so[32]; so[31][1] = 7820

		sc = [ [i, i+nclosed] for i in range(0, length, nopen+nclosed) ]
		with suppress(IndexError):
			del sc[14]
			del sc[32]

		sa = [ x for y in zip(sc, so) for x in y ]
		if len(sc)>len(so):
			sa.append(sc[-1])
		elif len(sc)<len(so):
			sa.append(so[-1])
	
		self.shutter_open 	= so
		self.shutter_closed = sc
		self.shutter_all 		= sa

		self.ipoints = np.arange(toffset, toffset+length)

		self._init_parameters()

		nseq 		= int(np.ceil( length/(nopen+nclosed) ))
		tbounds = list(accumulate( [0] + [nclosed, nopen]*nseq ))
		with suppress(ValueError):
			# correct for when the shutter didn't work
			tbounds.remove(3220)
			tbounds.remove(3420)
			tbounds.remove(7590)
			tbounds.remove(7790)

		tbounds_ref = tbounds.copy()

		if toffset:
			toffset_ix = np.searchsorted(tbounds, toffset)
			tbounds = ([] if toffset in tbounds else [toffset]) + tbounds[toffset_ix:]

		if tbounds[-1] != length:
			tend_ix = np.searchsorted(tbounds, length)
			tbounds = tbounds[:tend_ix] + [length]

		tintvls = [ (tbounds[i], tbounds[i+1]) for i in range(len(tbounds)-1) ]

		for i in range(len(tbounds_ref)-1):
			if (tbounds_ref[i] <= tbounds[0] < tbounds_ref[i+1]):
				status0 = True if i%2 else False
				break
		status1 = not(status0)

		shutter_seq 	 = cycle([status0, status1])
		shutter_status = [ next(shutter_seq) for i in range(len(tintvls)) ]
		
		for tintv in  tintvls:
			if list(tintv) not in sa:
				print(tintvls.index(tintv), tintv)

		self.tintvls				= tintvls
		self.shutter_status = shutter_status


	def _init_parameters(self):
		
		nclock = len(self.shutter_open)
		params = Namespace([ ('up%d'%i, 200) for i in range(1, nclock+1) ])
		params.low, params.base, = 0, 50
		params.tau, params.alpha = 50, 0.02

		self.params = params
		self._integrals()


	def _set_parameters(self, *args):
		""" all parameters have to be given at once """
		params = Namespace( zip(self.params.keys(), args) )

		if self.params != params:
			self.params.update(params)
			self._integrals()
			# print('called _set_parameters', args)


	def signal(self, t):
		# on top of self.base
		if (t < self.shutter_all[0][1]) or (t >= self.shutter_all[-1][-1]):
			return 0 

		else:
			for i, (t1, t2) in enumerate(self.shutter_open):
				if t1 <= t < t2:
					return self.params['up%d'%(i+1)]

			else:
				return self.params['low']

	
	def _integrals(self):
		
		self._piecewise_sum = {}
		
		for i, (t1, t2) in enumerate(self.shutter_all):

			tm 	= (t1+t2) / 2
			q  	= self.signal(tm) * self.params.tau \
						* ( 1 - np.exp((t1-t2)/self.params.tau) )	

			self._piecewise_sum[i] = q


	def _tindex(self, t):
	
		if t < self.shutter_all[0][1]:
			ret = -1

		elif t >= self.shutter_all[-1][-1]:
			ret = len(self.shutter_all)

		else:
			ret = np.flatnonzero([ s[0] <= t < s[1] for s in self.shutter_all ])[0]

		return ret


	def history(self, t, *args):

		if len(args): self._set_parameters(*args)

		tix = self._tindex(t)

		if tix == -1:
			ret = 0

		else:
			s  = 0
			for i in range(0, tix):
				s += np.exp( (self.shutter_all[i][1]-t)/self.params.tau ) \
						 * self._piecewise_sum[i]
			
			if tix < len(self.shutter_all): # else signal(t) = 0
				s	+= self.signal(t) * self.params.tau \
						 * ( 1 - np.exp((self.shutter_all[tix][0]-t)/self.params.tau) )
		
			ret =  self.params.alpha * s

		return ret


	def response(self, t, *args):
		""" params checks are only done in history """

		h = self.history(t, *args)
		i = self.signal(t)
		b = self.params.base

		return b + i + h

	# easier to write own lsq function, then force parnames in iminuit
	# def _func_signature(self, fn):
	# 	""" needed to decorate response() for probfit parser 
	# 			note: no 'self' in wrapper since fn will be called as self.fn
	# 	"""
	# 	# @wraps(fn) # need the signature of wrapper, not that of fn
	# 	def wrapper(t, up1, up2, up3, up4, up5, up6, up7, up8, up9, up10,
	# 							up11, up12, up13, up14, up15, up16, up17, up18, up19, up20,
	# 							up21, up22, up23, up24, up25, up26, up27, up28, up29,
	# 							low, tau, alpha):

	# 		dic  = locals() # do not call this inside the list comprehension
	# 		args = [ dic[k] for k in tuple(signature(wrapper).parameters.keys())[1:] ]

	# 		return fn(t, *args)

	# 	return wrapper


	def fit(self, data):
		
		time  	 = self.ipoints
		triggers = data['triggers'].values

		nclock 	 = len(self.shutter_open)
		init_fitargs = Namespace(
				[ ('up%d'%i, 200) for i in range(1, nclock+1) ] +\
				[ ('low', 0), ('base', 50), ('tau', 50), ('alpha', 0.05) ] +\
				[ ('limit_up%d'%i, (0, None)) for i in range(1, nclock+1) ] +\
				[ ('limit_low', (0, None)), ('limit_base', (0, None)),
					('limit_tau', (0, None)), ('limit_alpha', (0, 1)) ] +\
				[ ('error_up%d'%i, 0.1) for i in range(1, nclock+1) ] +\
				[ ('error_low', 0.1), ('error_base', 0.1), 
					('error_tau', 0.1), ('error_alpha', 0.01) ]
				)

		parnames = list(self.params.keys())

		def lsq(*args):
			# print('called lsq')
			self._set_parameters(*args)
			response = np.array([ self.response(t) for t in time ])
			
			return np.sum( (triggers - response)**2 / triggers )

		m = Minuit(lsq, forced_parameters=parnames,
							 errordef=1, fix_low=True, **init_fitargs)

		timing(m.migrad)()
		# timing(m.minos)()
		print('reduced chi2: {:.3f} / {}'\
					.format(m.fval, len(triggers)-len(self.params)-1) )
		self.fitted = m


	def plot(self, ax=None):

		if ax is None: ax = plt.gca()

		xdata  = self.ipoints
		params = self.fitted.values
		errors = self.fitted.errors

		self._set_parameters(*params.values())
		signal 	 = np.array([ self.signal(t) for t in xdata ])
		response = np.array([ self.response(t) for t in xdata ])


		ax.plot(xdata, signal + self.params.base, color='k',
						lw=2, ls='--', label='signal + base', zorder=10)
		ax.plot(xdata, response, color='r',
						lw=2, label='PMT response', zorder=5)



def read_data(infile):

	dtype = [
		('time', np.uint),
		('triggers', np.uint),
		('dt_long', np.float),
		('dt_short', np.float),
		('closed', np.uint8)
		]

	with catch_warnings():
		filterwarnings('ignore', category=UserWarning)
		data = pd.DataFrame(
							np.genfromtxt(
								infile, dtype=dtype, usecols=(0, 3, 6, 7, 10),
								invalid_raise=False, encoding=None, max_rows=None	))

	data = data.assign( dt=(data['dt_long']+data['dt_short'])/2 )
	data = data.assign( rates=data['triggers']/data['dt'] )
	data = data[['time', 'dt', 'triggers', 'rates', 'closed']]

	return data


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


infile 	= './data/logLonOpen_newSh_30-200_2019-02-08_b.log'
data 		= read_data(infile)#[0:200+2*230]
nopen 	= 30
nclosed = 200
length 	= len(data)
toffset = 0


model 	= Model(nopen, nclosed, length, toffset)
time 		= model.ipoints


# fig, ax = plt.subplots(figsize=(6,4)) # (10,3)


# triggers = data['triggers'].values
# ax.errorbar(
# 	time, triggers, yerr=np.sqrt(triggers),
# 	fmt='s', ms=1, elinewidth=.5,
# 	color='darkblue', label='data')

# # model.fit(data)
# # model.plot()

# signal = np.array([ model.signal(t) for t in time ])
# ax.plot(time, signal + model.params.base, color='k',
# 				lw=2, ls='--', label='signal + base', zorder=10)


# ax.set_xlabel('dummy time [mins]')
# ax.set_ylabel('number of triggers [per min]')
# ax.legend()
# fig.tight_layout()

# # pickle_dump({
# # 		**model.fitted.fitarg,
# # 		'fval': model.fitted.fval,
# # 		'dof' : len(data) - len(model.params)
# # 		},'fit_one', './data/sig_history/' )


# # fig.savefig('./plt/6/history_model.svg', format='svg')

# show()
# # embed()
