""" Muon flux calculations, Lipari, Astroparticle physics 1 (1993) 195-227
		note:
			- this code has not been extensively tested non-vertical muons
			- the numerical integration is quite slow at low energy
"""
#!usr/bin/env python3

import os
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn
import scipy.constants as const
import scipy.interpolate as interp

from IPython import embed
from scipy import integrate

from mod.funk_plt import show, add_toplegend
from mod.funk_utils import timing

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (0.8*438/72, 0.8*0.8*438/72)

pd.set_option('display.max_rows', 10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# primary cosmic ray spectrum profile: 1.7 E^(-2.7)
alpha = 2.7 			# index of the power law
d0 = 0.14 				# neutron to proton ratio
p0 = 1.7/(1.+d0)	# proton fraction
n0 = p0*d0				# neutron fraction

# interaction lengths in g/cm^2
lProton  = 86
lNeutron = 86
lPion = 116
lKaon = 138

# rest masses and mean lifetime [PDG]
mPion 	= 139.57e-3 # GeV
tauPion = 2.60e-8 	# s
mKaon 	= 493.68e-3
tauKaon = 1.24e-8
mKaonL 	= 497.61e-3
tauKaonL = 5.12e-8
mMuon 	= 105.66e-3
tauMuon = 2.20e-6

# hadronic Z factors for alpha=2.7 (table 5)
Z = {
	'ProtonProton'				: 0.263,
	'ProtonNeutron'				: 0.035,
	'ProtonPionPlus'			: 0.046,
	'ProtonPionMinus'			: 0.033,
	'ProtonKaonPlus'			: 0.009,
	'ProtonKaonMinus'			: 0.0028,
	'ProtonKaonL'					: 0.0059,
	#
	'NeutronProton'				: 0.035,
	'NeutronNeutron'			: 0.263,
	'NeutronPionPlus'			: 0.033,
	'NeutronPionMinus'		: 0.046,
	'NeutronKaonPlus'			: 0.0065,
	'NeutronKaonMinus'		: 0.0028,
	'NeutronKaonL'				: 0.0014,
	#
	'PionPlusPionPlus'		: 0.243,
	'PionPlusPionMinus'		: 0.028,
	'PionPlusKaonPlus'		: 0.0067,
	'PionPlusKaonMinus'		: 0.0067,
	'PionPlusKaonL'				: 0.0067,
	#
	'PionMinusPionPlus'		: 0.028,
	'PionMinusPionMinus'	: 0.243,
	'PionMinusKaonPlus'		: 0.0067,
	'PionMinusKaonMinus'	: 0.0067,
	'PionMinusKaonL'			: 0.0067,
	#
	'KaonPlusKaonPlus'		: 0.211,
	'KaonPlusKaonMinus'		: 0.,
	'KaonPlusKaonL'				: 0.007,
	#
	'KaonMinusKaonPlus'		: 0.,
	'KaonMinusKaonMinus'	: 0.211,
	'KaonMinusKaonL'			: 0.007,
	#
	'KaonLKaonPlus'				: 0.007,
	'KaonLKaonMinus'			: 0.007,
	'KaonLKaonL'				  : 0.211
}

# primary flux, eqs 11 and 12
L1 = lProton / (1 - Z['ProtonProton'] - Z['ProtonNeutron'])
L2 = lProton / (1 - Z['ProtonProton'] + Z['ProtonNeutron'])

def proton(x):
	return 0.5 * ((p0+n0)*np.exp(-x/L1) + (p0-n0)*np.exp(-x/L2))

def neutron(x):
	return 0.5 * ((p0+n0)*np.exp(-x/L1) - (p0-n0)*np.exp(-x/L2))

# atmosphere model, eq 110
def rho(x, theta=0):
	""" x: slant depth in g/cm2, theta: zenith angle """

	xv = x * np.cos(theta)
	xv_troposphere = 230.2
	h0 = 6.344e5
	b  = 0.8096
	c  = 4.439e-6

	if xv < 0.1:
		ret = 1e-7

	elif 0.1 <= xv <= xv_troposphere:
		ret = xv / h0

	else:
		ret = c * np.power(xv, b)

	return ret

# decay lengths in g/cm^2, eq 2
def lDecPion(E, x, theta):
	return const.c*1e2 * tauPion * E/mPion * rho(x, theta)

def lDecKaon(E, x, theta):
	return const.c*1e2 * tauKaon * E/mKaon * rho(x, theta)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# STEP1: meson (pion and kaon) fluxes

# low energy pion sources, eqs 14 and 32
def sPionPlusLowx(x):
	coef1 = proton(x) * Z['ProtonPionPlus'] / lProton
	coef2 = neutron(x) * Z['NeutronPionPlus'] / lNeutron
	return coef1 + coef2

def sPionMinusLowx(x):
	coef1 = proton(x) * Z['ProtonPionMinus'] / lProton
	coef2 = neutron(x) * Z['NeutronPionMinus'] / lNeutron
	return coef1 + coef2

# low energy kaon sources, eqs 18 and 32
def sKaonPlusLowx(x):
	coef1 = proton(x) * Z['ProtonKaonPlus'] / lProton
	coef2 = neutron(x) * Z['NeutronKaonPlus'] / lNeutron
	return coef1 + coef2

def sKaonMinusLowx(x):
	coef1 = proton(x) * Z['ProtonKaonMinus'] / lProton
	coef2 = neutron(x) * Z['NeutronKaonMinus'] / lNeutron
	return coef1 + coef2

# high energy meson sources, eqs 16 and 17
LP1 = lPion / (1 - Z['PionPlusPionPlus'] - Z['PionPlusPionMinus'])
LP2 = lPion / (1 - Z['PionPlusPionPlus'] + Z['PionPlusPionMinus'])

def sPionPlusHighx(x):
	coef1 = (Z['ProtonPionPlus'] + Z['ProtonPionMinus'])\
						* (LP1*L1 / (LP1-L1)) * (np.exp(-x/LP1) - np.exp(-x/L1))
	coef2 = (Z['ProtonPionPlus'] - Z['ProtonPionMinus'])\
						* (LP2*L2 / (LP2-L2)) * (np.exp(-x/LP2) - np.exp(-x/L2))

	return 0.5 * (coef1*(p0+n0)/lProton + coef2*(p0-n0)/lProton)

def sPionMinusHighx(x):
	coef1 = (Z['ProtonPionPlus'] + Z['ProtonPionMinus'])\
						* (LP1*L1 / (LP1-L1)) * (np.exp(-x/LP1) - np.exp(-x/L1))
	coef2 = (Z['ProtonPionPlus'] - Z['ProtonPionMinus'])\
						* (LP2*L2 / (LP2-L2)) * (np.exp(-x/LP2) - np.exp(-x/L2))

	return 0.5 * (coef1*(p0+n0)/lProton - coef2*(p0-n0)/lProton)

# high energy kaon sources

# eq 27
AP = (p0+n0)/lProton * (Z['ProtonPionPlus'] + Z['ProtonPionMinus']) * LP1*L1/(LP1-L1)
BP = (p0-n0)/lProton * (Z['ProtonPionPlus'] - Z['ProtonPionMinus']) * LP2*L2/(LP2-L2)

# eqs 28, 30 (cf. eq 20)
LK = np.array([
				lKaon / (1 - Z['KaonPlusKaonPlus']),
				lKaon / (1 - (Z['KaonPlusKaonPlus'] + np.sqrt(2)*Z['KaonLKaonPlus'])),
				lKaon / (1 - (Z['KaonPlusKaonPlus'] - np.sqrt(2)*Z['KaonLKaonPlus']))
			])

# paragraph below eq 22
Lsigma = np.array([ L1, L2, LP1, LP2 ])

# eq 31
U = 0.5 * np.array([
			[np.sqrt(2), -np.sqrt(2), 0.],
			[1, 1, np.sqrt(2)],
			[1, 1, -np.sqrt(2)]
			])

# eqs 23 to 26
C = np.array([
			[ (p0+n0)/(2*lProton) * (Z['ProtonKaonPlus'] + Z['NeutronKaonPlus'])
					- AP/(2*lPion) * (Z['PionPlusKaonPlus'] + Z['PionMinusKaonPlus']),
				(p0-n0)/(2*lProton) * (Z['ProtonKaonPlus'] - Z['NeutronKaonPlus'])
					- BP/(2*lPion) * (Z['PionPlusKaonPlus'] - Z['PionMinusKaonPlus']),
				AP/(2*lPion) * (Z['PionPlusKaonPlus'] + Z['PionMinusKaonPlus']),
				BP/(2*lPion) * (Z['PionPlusKaonPlus'] - Z['PionMinusKaonPlus']) ],
			#
			[ (p0+n0)/(2*lProton) * (Z['ProtonKaonMinus'] + Z['NeutronKaonMinus'])
					- AP/(2*lPion) * (Z['PionPlusKaonMinus'] + Z['PionMinusKaonMinus']),
				(p0-n0)/(2*lProton) * (Z['ProtonKaonMinus'] - Z['NeutronKaonMinus'])
					- BP/(2*lPion) * (Z['PionPlusKaonMinus'] - Z['PionMinusKaonMinus']),
				AP/(2*lPion) * (Z['PionPlusKaonMinus'] + Z['PionMinusKaonMinus']),
				BP/(2*lPion) * (Z['PionPlusKaonMinus'] - Z['PionMinusKaonMinus']) ],
			#
			[ (p0+n0)/(2*lProton) * (Z['ProtonKaonL'] + Z['NeutronKaonL'])
					- AP/(2*lPion) * (Z['PionPlusKaonL'] + Z['PionMinusKaonL']),
				(p0-n0)/(2*lProton) * (Z['ProtonKaonL'] - Z['NeutronKaonL'])
					- BP/(2*lPion) * (Z['PionPlusKaonL'] - Z['PionMinusKaonL']),
				AP/(2*lPion) * (Z['PionPlusKaonL'] + Z['PionMinusKaonL']),
				BP/(2*lPion) * (Z['PionPlusKaonL'] - Z['PionMinusKaonL']) ]
		])

# eq 29
def sKaonHighx(j, x):
	""" j: 0 (KaonPlus), 1 (KaonMinus), 2 (KaonL) """
	s = []

	for a in range(3):
		for b in range(4):
			for k in range(3):
				s.append(
					U[a,j] * LK[a]*Lsigma[b] / (LK[a]- Lsigma[b])
						* U[a,k]*C[k,b] * (np.exp(-x/LK[a]) - np.exp(-x/Lsigma[b])))

	return np.sum(s)

# we are only interested in muon soures
def sKaonPlusHighx(x):
	return sKaonHighx(0, x)

def sKaonMinusHighx(x):
	return sKaonHighx(1, x)

# meson fluxes from these sources, eq 34
def pionPlus(E, x, theta):
	num = sPionPlusLowx(x) * lDecPion(E,x,theta)
	den = 1 + sPionPlusLowx(x) * lDecPion(E,x,theta) / sPionPlusHighx(x)

	return num/den * np.power(E, -alpha)

def pionMinus(E, x, theta):
	num = sPionMinusLowx(x) * lDecPion(E,x,theta)
	den = 1 + sPionMinusLowx(x) * lDecPion(E,x,theta) / sPionMinusHighx(x)

	return num/den * np.power(E, -alpha)

def kaonPlus(E, x, theta):
	num = sKaonPlusLowx(x) * lDecKaon(E,x,theta)
	den = 1 + sKaonPlusLowx(x) * lDecKaon(E,x,theta) / sKaonPlusHighx(x)

	return num/den * np.power(E, -alpha)

def kaonMinus(E, x, theta):
	num = sKaonMinusLowx(x) * lDecKaon(E,x,theta)
	den = 1 + sKaonMinusLowx(x) * lDecKaon(E,x,theta) / sKaonMinusHighx(x)

	return num/den * np.power(E, -alpha)


# STEP2: muon fluxes

# muon sources from pion and kaon decays
# F factors, eqs 65 to 67
rPion = np.power(mMuon/mPion, 2)
rKaon = np.power(mMuon/mKaon, 2)

def probLeftP(t):
	return (rPion/t - rPion) / (1 - rPion)

def probRightP(t):
	return (1 - rPion/t) / (1 - rPion)

def probLeftK(t):
	return (rKaon/t - rKaon) / (1-rKaon)

def probRightK(t):
	return (1 - rKaon/t) / (1 - rKaon)


def heaviside(t, t0):
	return 1 if t>=t0 else 0.

def fPionMuonLeft(t):
	return probLeftP(t) * heaviside(t, rPion) / (1 - rPion)

def fPionMuonRight(t):
	return probRightP(t) * heaviside(t, rPion) / (1 - rPion)

def fKaonMuonLeft(t):
	return probLeftK(t) * heaviside(t, rKaon) / (1 - rKaon)

def fKaonMuonRight(t):
	return probRightK(t) * heaviside(t, rKaon) / (1 - rKaon)


# energy loss from ionization only, eq 57 (and see [ref 22])
aLoss = 2e-3 # GeV per g/cm2
def Efunc(E0, x0, x):
	return E0 - aLoss*(x - x0)

# survival proabability. eq 56
def probMuonSurv(E0, x0, xf, theta):
	def func(x):
		return np.power(Efunc(E0, x0, x) * rho(x, theta), -1)

	coef = mMuon / (tauMuon * const.c*1e2)
	ww = integrate.quad(func, x0, xf)[0]

	return np.exp(-coef*ww)


# eqs 36 and 58
def sMuon(Ef, xf, theta):

	def source(y, x0):
		
		E0 = Ef + aLoss*(xf - x0)

		br1 = np.sum([
			fPionMuonLeft(y) * pionMinus(E0/y, x0, theta) / lDecPion(E0, x0, theta),
			fPionMuonRight(y) * pionMinus(E0/y, x0, theta) / lDecPion(E0, x0, theta),
			fPionMuonLeft(y) * pionPlus(E0/y, x0, theta) / lDecPion(E0, x0, theta),
			fPionMuonRight(y) * pionPlus(E0/y, x0, theta) / lDecPion(E0, x0, theta)
		])

		br2 = np.sum([
			fKaonMuonLeft(y) * kaonMinus(E0/y, x0, theta) / lDecKaon(E0, x0, theta),
			fKaonMuonRight(y) * kaonMinus(E0/y, x0, theta) / lDecKaon(E0, x0, theta),
			fKaonMuonLeft(y) * kaonPlus(E0/y, x0, theta) / lDecKaon(E0, x0, theta),
			fKaonMuonRight(y) * kaonPlus(E0/y, x0, theta) / lDecKaon(E0, x0, theta)
		])

		return (0.999*br1 + 0.635*br2)

	return source
 

@timing
def probFn(Ef, xf, theta):
	""" interpolated probMuonSurv as function of x0, for given value of E0
			and at fixed (xf, theta)
	"""

	x0_range = np.linspace(0, xf, num=1000)
	E0_range = Ef + aLoss*(xf - x0_range) 
	p_range  = np.array([
								probMuonSurv(E0, x0, xf, theta)
									for (E0, x0) in zip(E0_range, x0_range)
						 ])
	p_interp = interp.interp1d(x0_range, p_range, kind='linear')

	return p_interp


@timing
def muonDiffSpectrum(Ef, xf, theta):

	print('energy = {} GeV, xf = {}, theta = {}'.format(Ef, xf, theta))

	source = sMuon(Ef, xf, theta)
	prob 	 = probFn(Ef, xf, theta)

	ret = integrate.nquad(
					lambda y, x0: source(y, x0) * prob(x0),
					[[0, 1], [0.01, xf]], opts={'limit':100}
				)
	return ret


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

xfSea = 1030. # sea level, g/cm2
thetaVert = 0 # vertical muons

# Efs = [93, 175, 329, 642]
Efs = np.logspace(0, 4)
jvs, jv_errs = [], []

for Ef in Efs:
	jv, err = muonDiffSpectrum(Ef, xfSea, thetaVert)
	jvs.append(jv)
	jv_errs.append(err)

res = np.array([Efs, jvs, jv_errs]).T
np.savetxt(
	'data/muons/muon_estimated2.txt',
	res, delimiter=' ',
	header='E jv jv_err', comments=''
)

embed()