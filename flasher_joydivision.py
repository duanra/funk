#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from mod.funk_run import Flasher

# plt.rcParams.update(fcn.rcThesis)
plt.rcParams.update(fcn.rcDefense)
plt.rcParams['figure.figsize'] = (252/72, 198/72)


# plt.rcParams.update(fcn.rcPaper)
# plt.rcParams.update(fcn.rcPoS)

# plt.rcParams.update(fcn.rcPoster)
# plt.rcParams['figure.figsize'] = (960/72, 350/72)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

run  = Flasher('v08f', split=True)
data = run.data

for key in tuple(data.keys()):
	data[ int(key[-6:-3]) ] = data.pop(key)


spu 		= False  # drop traces with more than one pulse

bins 		= 100
density = True
hrange 	= (6.5, 10)


props 	= {
	'facecolor' : 'w',
	'edgecolor' : 'k',
	'zorder'		: 10
}


for i, l in tuple(enumerate(range(120, 231)))[::-2]:
	print(l, end=' ', flush=True)
	df 	= data[l]
	if spu:
		df = df.drop_duplicates('event', keep=False)

	hist, bedg = np.histogram(df['q'].values, bins, hrange, density=density)
	bce = 0.5 * (bedg[1:] + bedg[:-1])

	xoffset	= 8
	xscale 	= 1 - i / 250
	lw 			= 0.8 - ( 0.5 * i / len(data))

	if l==126:
		lspe_xy = [
			xscale * (bce - xoffset) + xoffset,
			l/100 + hist/10,
		]
		lspe_lw = lw + 0.5

	elif l==170: #l==178
		lhigh_xy = [
			xscale * (bce - xoffset) + xoffset,
			l/100 + hist/10,
		]
		lhigh_lw = lw + 0.5

	plt.fill_between(
		xscale * (bce - xoffset) + xoffset,
		l/100 + hist/10, l/100,
		lw=lw, **props
		)

# print()
plt.plot(*lspe_xy, lw=lspe_lw, color='red', zorder=100)
# plt.plot(*lhigh_xy, lw=lhigh_lw, color='mediumblue', zorder=100)


plt.xlabel(r'$q=\lg(Q/e)$')
plt.xlim(6.75, 9.45)
plt.ylabel('flasher intensity/a.u.')
# plt.ylabel('pulse strength/a.u') # r'low DAC $\ell / 100$'
plt.ylim(120/100, 285/100)


plt.tight_layout(pad=0.05)
plt.subplots_adjust(left=0.17)
# plt.savefig('./plt/12/flasher_JoyDivision')
plt.savefig('../talks/src/phd_defense/figs/flasher_JoyDivision2')

# plt.show()
# embed()