#!usr/bin/env python3
import os
import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const

from matplotlib import rc
from matplotlib.lines import Line2D
from matplotlib.patches import  Rectangle
from scipy.interpolate import interp1d

from IPython import embed
rc('font', size=12)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def find_files(path):
	fpaths, fnames = [], []

	for root, dirs, files in os.walk(path):
		for file in files:
			if file[0]!='.': # ignore hidden files
				fpaths.append(os.path.join(root, file))
				fnames.append(file)
		break # non-recursive

	return fpaths, fnames


def exclusion_line(data):
	""" data are in log-log scale """
	x = np.log10(data[:,0])
	y = np.log10(data[:,1])
	new_x = np.linspace(np.log10(data[0,0]), 
											np.log10(data[-1,0]),
											num=500)

	return new_x, interp1d(x, y, kind='linear')


def rescale(xy):
	""" loglog return of exclusion_line """
	return 10**xy[0], 10**xy[1](xy[0])


def add_line(xy, ax=None, **kwargs):
	if ax is None:
		ax = plt.gca()

	ax.plot(*rescale(xy), **kwargs)


def add_fill(xy1, xy2, ax=None, **kwargs):

	kwargs.setdefault('facecolor', (0, 0, 0, .5))
	kwargs.setdefault('edgecolor', (0, 0, 0, .5))

	if ax is None:
		ax = plt.gca()

	if isinstance(xy2, (int, float)): 
		ax.fill_between(*rescale(xy1), xy2, **kwargs)

	elif xy2 == 'ymin': 
		ax.fill_between(*rescale(xy1), ax.get_ybound()[0], **kwargs)

	elif xy2 == 'ymax':
		ax.fill_between(*rescale(xy1), ax.get_ybound()[1], **kwargs)

	elif isinstance(xy2, (tuple, list)):
		# fill between two curves
		x_common = np.linspace(max(xy1[0][0], xy2[0][0]),
													 min(xy1[0][-1], xy2[0][-1]),
													 endpoint=True, num=1000)
		ax.fill_between(10**x_common,
										10**xy1[1](x_common),
										10**xy2[1](x_common),
										interpolate=True, **kwargs)

	else:
		print('[Warning] add_fill(): check args - skipping plot')


def add_text(txt, coords, ax=None, **kwargs):
	""" coords (tuple) are in axes coordinates """
	kwargs.setdefault('color', 'snow')
	kwargs.setdefault('family', 'fantasy')
	kwargs.setdefault('weight', 'semibold')
	kwargs.setdefault('zorder', 50)

	if ax is None:
		ax = plt.gca()

	# ax.text(*coords, txt, transform=ax.transAxes, **kwargs)
	ax.text(*coords, txt, **kwargs)


def on_click_coords(event, ax=None, fig=None):
	""" print Mouse(x,y) in data coords upon left click MouseEvent """
	if ax is None:
		ax = plt.gca()

	# # in axes coords
	# def _transform(*args):
	# 	try:
	# 		return ax.transAxes.inverted().transform(
	# 							ax.transData.transform(args))
	# 	except:
	# 		print('[Exception] mouse outside of axes object area')

	# print('Mouse(x, y): ({:.3f}, {:.3f})'.format(
	# 			*_transform(event.xdata, event.ydata)))

	if event.button==1:
		try:
			print('Mouse(x, y): ({:.3e}, {:.3e})'\
						.format(event.xdata, event.ydata))
		except:
	 		print('[Exception] mouse outside of axes object area')
	else:
		pass

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

cdir  = os.path.dirname(os.path.realpath(__file__))

fpaths, fnames = find_files(
		os.path.join(cdir, 'exs'))
xy = {
	fnames[i]: exclusion_line(np.loadtxt(fpaths[i]))\
	for i in range(len(fpaths))
}

props = {
	'cdm'       	: dict(color='darksalmon', zorder=1),
	'xenon10sun'	: dict(color='lightgrey', zorder=2),
	'rg'        	: dict(color='mediumvioletred', zorder=3),
	'hb'        	: dict(color=(0.02, 0.4, 0.55), zorder=4),
	'thermal'   	: dict(color=(0.678, 0.231, 0), zorder=5),
	'solarlife' 	: dict(color='seagreen', zorder=6),
	'lsw'       	: dict(color='coral', zorder=7),
	'cmb'       	: dict(color=(0, 0, 0, 1), zorder=8),
	'heli1'     	: dict(color='seagreen', zorder=10),
	'heli2'     	: dict(alpha=0, zorder=10),
	'coulomb'   	: dict(color='slategrey', zorder=11),
	'halo1'     	: dict(color='darkslategrey', zorder=12),
	'halo2'     	: dict(color='darkslategrey', zorder=13),
	'xenon10'   	: dict(color=(0.239, 0.109, 0.039), zorder=2),
	'damic'     	: dict(color=(0.9, 0.7, 0.3), zorder=15),
	'funk'      	: dict(color=(0.752, 0.007, 0.019), zorder=15),
	'tokyo'     	: dict(color=(0, 0.1, 0.4), zorder=16),
}


fig, ax = plt.subplots(figsize=(10, 6))
ax.set_yscale('log')
ax.set_xscale('log')
# ax.axis([1e-13, 1e6, 1e-16, 1])
# ax.axis([1e-7, 8e4, 1e-16, 2e-8])

add_fill(xy['coulomb'], 'ymax', **props['coulomb'])
add_text('Coulomb', (7.943e-12, 2.512e-02))

add_fill(xy['cmb'], 'ymax', **props['cmb'])
add_text('CMB', (6.281e-13, 1.294e-04))

add_fill(xy['solarlife'], 'ymax', **props['solarlife'])
add_text(' Solar \nlifetime', (1.778e+02, 1.294e-09))

add_line(xy['heli1'], ls=':', lw=1.5, color='k', zorder=10)
add_line(xy['heli2'], ls='-', lw=1.5, color='k', zorder=10)
add_fill(xy['heli1'], xy['coulomb'], **props['heli1'])
add_text('CAST', (5.585e-04, 3.192e-05), rotation=-52)

add_fill(xy['lsw'], xy['cmb'], **props['lsw'])
add_fill(xy['lsw'], xy['heli1'], **props['lsw'])
add_text('ALPS', (3.451e-04, 7.047e-08))

add_fill(xy['hb'], 'ymax', **props['hb'])
add_text('HB', (3.243e+03, 8.166e-14))

add_fill(xy['rg'], 'ymax', **props['rg'])
add_text('RG', (5.248e+03, 6.427e-15))

add_fill(xy['thermal'], 'ymax', **props['thermal'])
add_text('Thermal Cosmology', (1.738e+05, 1.019e-05), rotation=-90)

add_fill(xy['xenon10sun'], 'ymax', **props['xenon10sun'])
add_text('Xenon10 (solar)', (1.447e-02, 2.981e-11), rotation=-40)

add_fill(xy['halo1'], xy['cdm'], **props['halo1'])
add_fill(xy['halo2'], xy['cdm'], **props['halo2'])
add_text('Haloscopes', (8.610e-07, 3.311e-13), rotation=90)

add_line(xy['cdm'], ls='--', color='r', zorder=20)
add_fill(xy['cdm'], 'ymin', **props['cdm'])
add_text('Allowed\nHP CDM', (1e-4, 5e-15), size='large')

add_fill(xy['tokyo'], xy['cdm'], **props['tokyo'])
add_text('Tokyo', (2.384e+00, 4.875e-10), rotation=-90, size='smaller')

add_fill(xy['damic'], xy['cdm'], **props['damic'])
add_text('DAMIC', (1.207e+01, 1.318e-11), rotation=-90, size='smaller')

add_fill(xy['xenon10'], xy['cdm'], **props['xenon10'])
add_text('Xenon10', (1.148e+01, 2.249e-16))


# funk status
h = const.h / const.eV
k = const.k / const.eV
c = const.c
A = 14.56
R = 0.7
eta = 2.*(np.sqrt(R)-R)/(1.-R) # ~effective reflectivity

pmt 	= np.loadtxt(os.path.join(cdir, 'pmt/pmt_9107B'))
m 		= h*c / (pmt[:,0] * 1e-9) # in eV
q_eff = interp1d(m, pmt[:,1], kind='cubic')
m_vis = np.logspace(np.log10(m[0]), np.log10(m[-1]))


def chi_vis(m, sigma):
	return  4.1e-12 * np.sqrt(sigma * m / (q_eff(m)*1e-2 * A*eta**2))
	# joerg: 5.6e-12 * sqrt(sigma * m / (q_eff(m)*1e-2 * A * R))

ax.fill_between(
	m_vis, chi_vis(m_vis, sigma=2*0.025),
	10**xy['cdm'][1](np.log10(m_vis)), **props['funk'] )

# ax.fill_between(
# 	m_vis, chi_vis(m_vis, sigma=3*0.012),
# 	10**xy['cdm'][1](np.log10(m_vis)), alpha=0.1,**props['funk'] )

add_text(
	'FUNK', (4.258e+00, 1.000e-10), rotation=-90,
	family='sans-serif', style='italic' )

print(np.min(chi_vis(m_vis, sigma=3*0.025)))
print(m_vis[0], m_vis[-1])


# # funk++
# def chi_ghz(m, T_rec=70., k_rec=1., dt=100., broadscan=False):
# 	if broadscan:
# 		B = lambda m: 20e9 - 10e9

# 	else:
# 		B = lambda m: 280. * m/1e-6

# 	return 6.7e-12 * np.sqrt(k_rec*T_rec / (100.*A*eta**2))\
# 								 * np.power(B(m)/1e9 * 100./dt, 0.25)


# def chi_thz(m, x=50, k_rec=1., dt=3., bandwidth=None, broadscan=False):
# 	T_rec = lambda m: x * m / k

# 	if broadscan:
# 		B = lambda m: 1.5e12 - 0.1e12

# 	elif bandwidth and not broadscan:
# 		B = lambda m: bandwidth

# 	else:
# 		B = lambda m: 280. * m/1e-6

# 	return 6.7e-12 * np.sqrt(k_rec*T_rec(m) / (100.*A*eta**2))\
# 								 * np.power(B(m)/1e9 * 100./dt, 0.25)


# m_ghz = h*np.logspace(np.log10(10e9), np.log10(20e9))
# ax.fill_between(m_ghz,
# 								chi_ghz(m_ghz, T_rec=300., k_rec=1., dt=30., broadscan=True),
# 								10**xy['cdm'][1](np.log10(m_ghz)),
# 								color='firebrick', zorder=20)
# add_text('GHz antenna', (4.364e-05, 3.365e-12), color='k', 
# 				 size='x-small', rotation=-90, family='sans-serif')


# m_thz_schottky = h*np.logspace(np.log10(0.1e12), np.log10(2.5e12))
# ax.fill_between(m_thz_schottky,
# 								chi_thz(m_thz_schottky, x=50., k_rec=1., dt=3.,
# 												bandwidth=88e3, broadscan=False),
#                 10**xy['cdm'][1](np.log10(m_thz_schottky)),
#                 color='firebrick', zorder=20)
# add_text('Schottky\ndiode', (4.487e-04, 3.251e-12), color='k',
# 				 size='x-small', rotation=20, family='sans-serif')


# m_thz_heb = h*np.linspace(0.1e12, 2.5e12)
# ax.fill_between(m_thz_heb,
# 								chi_thz(m_thz_heb, x=10., k_rec=1., dt=3.,
# 												bandwidth=88e3, broadscan=False),
# 								10**xy['cdm'][1](np.log10(m_thz_heb)),
# 								color='lightcoral', zorder=19)
# add_text('HEB', (4.898e-04, 6.427e-13), color='k',
# 				 size='x-small', rotation=20, family='sans-serif')


# m_thz_sis = h*np.linspace(0.1e12, 0.7e12)
# ax.fill_between(m_thz_sis,
# 								chi_thz(m_thz_sis, x=2., k_rec=1., dt=3.,
# 												bandwidth=88e3, broadscan=False),
# 								10**xy['cdm'][1](np.log10(m_thz_sis)),
# 								color='sienna', zorder=15)
# add_text('SIS', (4.898e-04, 2.466e-13), color='k',
# 				 size='x-small', rotation=20, family='sans-serif')


# ax.add_patch(Rectangle((m_ghz[0], 10**-16),
# 											 width=m_ghz[-1]-m_ghz[0], height=1,
# 											 color='k', alpha=0.1, zorder=17))
# ax.add_patch(Rectangle((m_thz_heb[0], 10**-16),
# 											 width=m_thz_heb[-1]-m_thz_heb[0], height=1,
# 											 color='k', alpha=0.1, zorder=17))
# add_text('FUNK++', (6.839e-05, 5.152e-15),
# 				 family='sans-serif', style='italic')


ax.set_xlabel('hidden-photon mass $m_\\tilde{{\\gamma}}$ [eV]')
ax.set_ylabel('mixing parameter $\\chi$')
ax.axis([1e-7, 8e4, 1e-16, 2e-8])
# ax.axis([1e-13, 1e6, 1e-16, 1])


fig.canvas.mpl_connect('button_press_event', on_click_coords)
fig.savefig(cdir+'/limit.pdf')

# plt.show()
plt.close()
# embed()