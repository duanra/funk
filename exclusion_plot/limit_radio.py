#!usr/bin/env python3

import __pardir__
import os
import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
import mod.funk_consts as fcn

from IPython import embed
from itertools import chain
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle
from scipy.interpolate import interp1d

from mod.funk_consts import rcPaper, rcPoster, rcPoS
from mod.funk_utils import handle_warnings

# mpl fontstyle/fontfamily are overriden by latex style

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (290/72, 195/72) #(320/72, 225/72)
plt.rcParams['xtick.top'] 	= False
plt.rcParams['ytick.right'] = False

# plt.rcParams.update(fcn.rcPaper)
# plt.rcParams['axes.grid'] = False
# plt.rcParams['figure.figsize'] = (0.7*510/72, 0.5*510/72)

# plt.rcParams.update(fcn.rcPoS)
# plt.rcParams['figure.figsize'] = (0.75*430.2/72, 0.62*0.75*430.2/72)

# plt.rcParams.update({
# 	# tex document \columnwidth = 246pt and \textwidth = 510pt
# 	'figure.figsize' 			: ( 960/72, 450/72 ),
# 	'savefig.format'			: 'pdf',
# 	'font.size'						: 28,
# 	'axes.labelsize'			: 30,
# 	'legend.fontsize'			: 'smaller',
# 	'text.latex.preamble'	: [
# 			r'\usepackage{times}',
# 			r'\usepackage{newpxmath}',
# 		],
# 	'text.usetex'					: True,
# })


line_props = {
	'cdm'       	: dict(color='whitesmoke', zorder=1),
	'xenon10sun'	: dict(color='slategrey', zorder=2),
	'rg'        	: dict(color='slategrey', zorder=3),
	'hb'        	: dict(color='slategrey', zorder=4),
	'thermal'   	: dict(color='slategrey', zorder=5),
	'solarlife' 	: dict(color='slategrey', zorder=6),
	'lsw'       	: dict(color='slategrey', zorder=7),
	'cmb'       	: dict(color='slategrey', zorder=8),
	'heli1'     	: dict(color='slategrey', zorder=10),
	'heli2'     	: dict(alpha=0, zorder=10),
	'coulomb'   	: dict(color='slategrey', zorder=11),
	'halo1'     	: dict(color='slategrey', zorder=12),
	'halo2'     	: dict(color='slategrey', zorder=13),
	'xenon10'   	: dict(color='slategrey', zorder=2),
	'damic'     	: dict(color='slategrey', zorder=15),
	'tokyo'     	: dict(color='slategrey', zorder=16),
	'funk'      	: dict(color='crimson', zorder=15),
	#
	'shuket'   		: dict(color='mediumspringgreen', zorder=100),
	'tokyo_old'   : dict(color='mediumspringgreen', zorder=100),
	'tokyo_mev'   : dict(color='mediumspringgreen', zorder=100),
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def find_files(path):

	fpaths, fnames = [], []

	for root, dirs, files in os.walk(path):
		for file in files:
			if file[0] != '.': # ignore hidden files
				fpaths.append(os.path.join(root, file))
				fnames.append(file)

		break # non-recursive

	return fpaths, fnames


def exclusion_line(data):
	""" data are in log-log scale """

	xs, ys = np.log10(data).T
	xs_new = np.logspace(xs[0], xs[-1], num=500)

	return xs_new, interp1d(xs, ys, kind='linear')


@handle_warnings
def rescale(line):
	""" logscale the interpolated values form exclusion_line() """
	xs, ys = line
	return xs, np.power(10, ys(np.log10(xs)))


def add_line(line, ax=None, **kwargs):

	if ax is None: ax = plt.gca()

	ax.plot(*rescale(line), **kwargs)


def add_fill(line1, line2, ax=None, **kwargs):

	if ax is None: ax = plt.gca()
	kwargs.setdefault('facecolor', (0, 0, 0, .5))
	kwargs.setdefault('edgecolor', (0, 0, 0, 0))

	if isinstance(line2, (int, float)): 
		ax.fill_between(*rescale(line1), line2, **kwargs)

	elif line2 == 'ymin':
		ax.fill_between(*rescale(line1), ax.get_ybound()[0], **kwargs)

	elif line2 == 'ymax':
		ax.fill_between(*rescale(line1), ax.get_ybound()[1], **kwargs)

	elif isinstance(line2, (tuple, list)):
		# fill between two curves
		xs1, xs2 	= line1[0], line2[0]
		ys1, ys2 	= line1[1], line2[1]
		xlow 			= np.log10(max(xs1[0], xs2[0]))
		xup  			= np.log10(min(xs1[-1], xs2[-1]))
		xs_common = np.logspace(xlow, xup, num=500)
		
		ax.fill_between(
				xs_common,
				np.power(10, ys1(np.log10(xs_common))),
				np.power(10, ys2(np.log10(xs_common))),
				**kwargs
			)

	else:
		print('[Warning] add_fill(): check args - skipping plot')


def add_text(txt, coords, ax=None, **kwargs):
	""" coords (tuple) are in data coords (x,y)
			for convenience, use on_click_coords() with mpl_connect
	"""

	if ax is None: ax = plt.gca()
	kwargs.setdefault('color', 'k')
	# kwargs.setdefault('alpha', 0.55)
	kwargs.setdefault('family', 'fantasy')
	kwargs.setdefault('weight', 'semibold')
	kwargs.setdefault('zorder', 50)

	# ax.text(*coords, txt, transform=ax.transAxes, **kwargs)
	ax.text(*coords, txt, **kwargs)


def on_click_coords(event, ax=None, fig=None):
	""" print Mouse(x,y) in data coords upon left click MouseEvent """

	if ax is None: ax = plt.gca()

	# # in axes coords
	# def _transform(*args):
	# 	try:
	# 		return ax.transAxes.inverted().transform(
	# 							ax.transData.transform(args))
	# 	except:
	# 		print('[Exception] mouse outside of axes object area')

	# print('Mouse(x, y): ({:.3f}, {:.3f})'.format(
	# 			*_transform(event.xdata, event.ydata)))

	if event.button==1:
		try:
			print('Mouse(x, y): ({:.3e}, {:.3e})'\
						.format(event.xdata, event.ydata))
		except:
	 		print('[Exception] mouse outside of axes object area')
	else:
		pass

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

cd = os.path.dirname(os.path.realpath(__file__))
os.chdir(cd)
fpaths, fnames = find_files('exs/')

lines = { fnames[i]: exclusion_line(np.loadtxt(fpaths[i]))
					for i in range(len(fpaths)) }

plt.figure()

# # # # # various exclusion lines

add_fill(lines['coulomb'], 'ymax', **line_props['coulomb'])
add_fill(lines['cmb'], 'ymax', **line_props['cmb'])
add_fill(lines['solarlife'], 'ymax', **line_props['solarlife'])
add_fill(lines['heli1'], lines['coulomb'], **line_props['heli1'])
add_fill(lines['lsw'], lines['cmb'], **line_props['lsw'])
add_fill(lines['lsw'], lines['heli1'], **line_props['lsw'])
add_fill(lines['hb'], 'ymax', **line_props['hb'])
add_fill(lines['rg'], 'ymax', **line_props['rg'])
add_fill(lines['thermal'], 'ymax', **line_props['thermal'])
add_fill(lines['xenon10sun'], 'ymax', **line_props['xenon10sun'])
add_fill(lines['halo1'], lines['cdm'], **line_props['halo1'])
add_fill(lines['halo2'], lines['cdm'], **line_props['halo2'])
add_fill(lines['tokyo'], lines['cdm'], **line_props['tokyo'])
add_fill(lines['damic19'], lines['cdm'], **line_props['damic'])
add_fill(lines['xenon10'], lines['cdm'], **line_props['xenon10'])

add_line(lines['cdm'], ls='--', lw=2, color=line_props['cdm']['color'], zorder=20)
add_fill(lines['cdm'], 'ymin', **line_props['cdm'])
add_text('Allowed\nHP CDM', (0.5e-1, 2.5e-16))

add_fill(lines['shuket'], lines['cdm'], **line_props['shuket'])
add_fill(lines['tokyo_old'], lines['cdm'], **line_props['tokyo_old'])
add_fill(lines['tokyo_mev'], 'ymax', **line_props['tokyo_mev'])

# # # # # # FUNK++

c 		= const.c
h 		= const.h / const.eV
k 		= const.k / const.eV

A 		= 14.56
R 		= 0.8
eta 	= 2*(np.sqrt(R)-R) / (1-R) # effective reflectivity

def chi_ghz(m, T_rec=70., k_rec=1., dt=100., bandwidth=None, broadscan=False):

	if broadscan:
		B = lambda m: 20e9 - 10e9

	elif bandwidth and not broadscan:
		B = lambda m: bandwidth

	else:
		B = lambda m: 280. * m/1e-6

	return 6.7e-12 * np.sqrt(k_rec*T_rec / (100.*A*eta**2))\
								 * np.power(B(m)/1e9 * 100./dt, 0.25)


def chi_thz(m, x=50, k_rec=1., dt=3., bandwidth=None, broadscan=False):

	T_rec = lambda m: x * m / k

	if broadscan:
		B = lambda m: 1.5e12 - 0.1e12

	elif bandwidth and not broadscan:
		B = lambda m: bandwidth

	else:
		B = lambda m: 280. * m/1e-6

	return 6.7e-12 * np.sqrt(k_rec*T_rec(m) / (100.*A*eta**2))\
								 * np.power(B(m)/1e9 * 100./dt, 0.25)


m_ghz = h * np.logspace(np.log10(10e9), np.log10(20e9))

plt.fill_between(
	m_ghz,
	chi_ghz(m_ghz, T_rec=300., k_rec=1., dt=30., bandwidth=1e6),
	np.power(10, lines['cdm'][1](np.log10(m_ghz))),
	color='indianred', zorder=20
)

add_text(
	'GHz antenna', (3.7e-05, 3e-13), color='k', 
	size='x-small', rotation=-90, family='sans-serif'
)


m_thz_schottky = h * np.logspace(np.log10(0.1e12), np.log10(2.5e12))

plt.fill_between(
	m_thz_schottky,
	chi_thz(m_thz_schottky, x=50., k_rec=1., dt=3., bandwidth=88e3),
	np.power(10, lines['cdm'][1](np.log10(m_thz_schottky))),
	color='indianred', zorder=20
)

add_text(
	'Schottky\ndiode', (4.487e-04, 3.251e-12), color='k',
	size='x-small', rotation=25, family='sans-serif'
)


m_thz_heb = h*np.linspace(0.1e12, 2.5e12)

# plt.fill_between(
# 	m_thz_heb, chi_thz(m_thz_heb, x=10., k_rec=1., dt=3.,
# 										 bandwidth=88e3, broadscan=False),
# 	np.power(10, lines['cdm'][1](np.log10(m_thz_heb))),
# 	color='indianred', zorder=19)

# add_text(
# 	'HEB', (4.898e-04, 6.427e-13), color='k',
# 	size='x-small', rotation=20, family='sans-serif')


m_thz_sis = h * np.linspace(0.1e12, 0.7e12)

plt.fill_between(
	m_thz_sis, chi_thz(m_thz_sis, x=2., k_rec=1., dt=3., bandwidth=88e3),
	np.power(10, lines['cdm'][1](np.log10(m_thz_sis))),
	color='lightsalmon', zorder=15)

add_text(
	'SIS', (4.898e-04, 3.866e-13), color='k',
	size='x-small', rotation=25, family='sans-serif')


plt.gca().add_patch(
	Rectangle(
		(m_ghz[0], 0.5*10**-16),
		width=m_ghz[-1]-m_ghz[0], height=1,
		color='k', alpha=0.1, zorder=17
	))
plt.gca().add_patch(
	Rectangle(
		(m_thz_heb[0], 0.5*10**-16),
		width=m_thz_schottky[-1]-m_thz_schottky[0], height=1,
		color='k', alpha=0.1, zorder=17
	))

# add_text(
# 	'FUNK++', (6.839e-05, 5.152e-15),
# 	family='sans-serif', style='italic')



plt.xlabel(r'$\lg(m_{\tilde{\gamma}}/\mathrm{eV})$')
plt.xscale('log')
plt.xlim(1e-7, 8e4) # (1e-13, 1e6)
plt.xticks(
		[ 10**x for x in range(-6, 5) ],
		[ str(x) if x%2==0 else str() for x in range(-6, 5) ]
	)


plt.ylabel(r'$\lg\chi$')
plt.yscale('log')
plt.ylim(6e-17, 2e-8) # (1e-16, 1)
plt.yticks(
		[ 10**x for x in range(-16, -7) ],
		[ str(x) if x%2==0 else str() for x in range(-16, -7) ]
	)
plt.gca().yaxis.set_minor_locator(
	plt.LogLocator(subs='all', numticks=10))
plt.gca().yaxis.set_minor_formatter(
	plt.NullFormatter())


plt.gcf().canvas.mpl_connect('button_press_event', on_click_coords)

plt.subplots_adjust(left=0.115, bottom=0.145, right=0.995, top=0.995)
# plt.subplots_adjust(left=0.1, bottom=0.12, right=0.995, top=0.995)

plt.savefig('../plt/12/sensitivity_radio')
plt.close()

# embed()