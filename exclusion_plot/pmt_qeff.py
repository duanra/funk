import __pardir__
import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
import mod.funk_consts as fcn

from IPython import embed
from scipy.interpolate import interp1d

plt.rcParams.update(fcn.rcDefense)
plt.rcParams['figure.figsize'] = (200/72, 155/72)

# plt.rcParams.update(fcn.rcThesis)
# plt.rcParams['figure.figsize'] = (184/72, 170/72)

# plt.rcParams.update(fcn.rcPaper)
# plt.gca().patch.set_facecolor('whitesmoke')

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def get_function(infile, npts=500):
	data 	= np.loadtxt(infile)
	x, y 	= data.T
	xnew 	= np.linspace(x[0], x[-1], npts)
	yfunc = interp1d(
						x, y, kind='cubic', bounds_error=False,
						fill_value=0
					)

	return xnew, yfunc

def get_extrapolation():
	xs, yfunc = get_function('./pmt/pmt_9107B')
	xnew 			= xs - 5
	ynew 			= 0.833 * yfunc(xs)
	yfunc_new = interp1d(
								xnew, ynew, kind='cubic', bounds_error=False,
								fill_value=5e-2
							)

	return xnew, yfunc_new

def dump_full_extrapolation():
	xmes, ymes 	 = get_function('./pmt/pmt_9107B_mes')
	xextr, yextr = get_extrapolation()

	xnuv = np.arange(142, 200)
	xvis = np.arange(200, 636)

	ynuv = yextr(xnuv)
	yvis = ymes(xvis)

	xall = np.concatenate([xnuv, xvis])
	yall = np.concatenate([ynuv, yvis])

	np.savetxt(
		'./pmt/pmt_9107B_extr',
		np.concatenate([xall, yall]).reshape((2,len(xall))).T,
		delimiter=' '
	)

# dump_full_extrapolation()


xnew, yfunc = get_function('./pmt/pmt_9107B_extr')
xnew = np.linspace(130, 650, 500)
plt.plot(xnew, yfunc(xnew), lw=2, color='grey') # yellowgreen

# xnew, yfunc = get_function('./pmt/pmt_9107B_mes')
# plt.plot(xnew, yfunc(xnew), lw=1, color='g')

# xnew, yfunc = get_function('./pmt/pmt_9107B')
# plt.plot(xnew, yfunc(xnew), lw=1, color='r')

# xnew, yfunc = get_extrapolation()
# plt.plot(xnew, yfunc(xnew), lw=1, color='b') 


plt.xlabel('photon wavelength/nm')
plt.xticks(range(200, 700, 100))
plt.ylabel('quantum efficiency (\%)')
plt.yticks(range(0,30,5))
plt.ylim(top=27)


# # additional axis
# def to_eV(x):
#  return const.h * const.c / const.eV / (x * 1e-9)

# xlim 	 			= plt.xlim()
# xticks 			= plt.gca().get_xticks()
# xticklabels = [ '$%.1f$'%x for x in to_eV(xticks) ]

# plt.twiny()
# plt.xlabel('HP mass [eV]', labelpad=4)
# plt.xlim(xlim)
# plt.xticks(xticks, xticklabels)


plt.tight_layout(pad=0.05)
# plt.savefig('../plt/12/pmt_qeff.pdf')
plt.savefig('../../talks/src/phd_defense/figs/pmt_qeff')
plt.close()

# plt.show()
embed()