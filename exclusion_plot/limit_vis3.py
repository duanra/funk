#!usr/bin/env python3

import __pardir__
import os
import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
import mod.funk_consts as fcn

from IPython import embed
from itertools import chain
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle
from scipy.interpolate import interp1d

from mod.funk_consts import rcPaper, rcPoster, rcPoS
from mod.funk_utils import handle_warnings


plt.rcParams.update(fcn.rcPaper)
plt.rcParams['figure.figsize'] = (246/72, 0.85*246/72)

# plt.rcParams.update(fcn.rcDefense)
# plt.rcParams.update(fcn.rcThesis)
# plt.rcParams['figure.figsize'] = (230/72, 200/72)

plt.rcParams['xtick.top'] 	= False
plt.rcParams['ytick.right'] = False

line_props = {
	'rg'        	: dict(color='plum', zorder=3),
	'hb'        	: dict(color='skyblue', zorder=4),
	'thermal'   	: dict(color='brown', zorder=5),
	'solarlife' 	: dict(color='mediumseagreen', zorder=6),
	'lsw'       	: dict(color='coral', zorder=7),
	'cmb'       	: dict(color='black', zorder=8),
	'heli1'     	: dict(color='mediumseagreen', zorder=10),
	'heli2'     	: dict(alpha=0, zorder=10),
	'coulomb'   	: dict(color='slategrey', zorder=11),
	'halo1'     	: dict(color='darmoccasin', zorder=12),
	'halo2'     	: dict(color='xenonlategrey', zorder=13),
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def find_files(path):

	fpaths, fnames = [], []

	for root, dirs, files in os.walk(path):
		for file in files:
			if file[0] != '.': # ignore hidden files
				fpaths.append(os.path.join(root, file))
				fnames.append(file)

		break # non-recursive

	return fpaths, fnames


def exclusion_line(data):
	""" data are in log-log scale """

	xs, ys = np.log10(data).T
	xs_new = np.logspace(xs[0], xs[-1], num=500)

	return xs_new, interp1d(xs, ys, kind='linear')


@handle_warnings
def rescale(line):
	""" logscale the interpolated values form exclusion_line() """
	xs, ys = line
	return xs, np.power(10, ys(np.log10(xs)))


def add_line(line, ax=None, **kwargs):

	if ax is None: ax = plt.gca()

	ax.plot(*rescale(line), **kwargs)


def add_fill(line1, line2, ax=None, **kwargs):

	if ax is None: ax = plt.gca()
	kwargs.setdefault('facecolor', (0, 0, 0, .5))
	kwargs.setdefault('edgecolor', (0, 0, 0, 0))

	if isinstance(line2, (int, float)): 
		ax.fill_between(*rescale(line1), line2, **kwargs)

	elif line2 == 'ymin':
		ax.fill_between(*rescale(line1), ax.get_ybound()[0], **kwargs)

	elif line2 == 'ymax':
		ax.fill_between(*rescale(line1), ax.get_ybound()[1], **kwargs)

	elif isinstance(line2, (tuple, list)):
		# fill between two curves
		xs1, xs2 	= line1[0], line2[0]
		ys1, ys2 	= line1[1], line2[1]
		xlow 			= np.log10(max(xs1[0], xs2[0]))
		xup  			= np.log10(min(xs1[-1], xs2[-1]))
		xs_common = np.logspace(xlow, xup, num=500)
		
		ax.fill_between(
				xs_common,
				np.power(10, ys1(np.log10(xs_common))),
				np.power(10, ys2(np.log10(xs_common))),
				**kwargs
			)

	else:
		print('[Warning] add_fill(): check args - skipping plot')


def add_text(txt, coords, ax=None, **kwargs):
	""" coords (tuple) are in data coords (x,y)
			for convenience, use on_click_coords() with mpl_connect
	"""

	if ax is None: ax = plt.gca()
	kwargs.setdefault('color', 'k')
	# kwargs.setdefault('alpha', 0.55)
	kwargs.setdefault('family', 'fantasy')
	kwargs.setdefault('weight', 'semibold')
	kwargs.setdefault('zorder', 50)

	# ax.text(*coords, txt, transform=ax.transAxes, **kwargs)
	ax.text(*coords, txt, **kwargs)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

cd = os.path.dirname(os.path.realpath(__file__))
os.chdir(cd)
fpaths, fnames = find_files('exs/')

lines = { fnames[i]: exclusion_line(np.loadtxt(fpaths[i]))
					for i in range(len(fpaths)) }
# embed()
plt.figure()

# # # # # various exclusion lines

# add_fill(lines['coulomb'], 'ymax', **line_props['coulomb'])
# add_text('Coulomb', (7.943e-12, 2.512e-02))

# add_fill(lines['cmb'], 'ymax', **line_props['cmb'])
# add_text('CMB', (6.281e-13, 1.294e-04))

# add_fill(lines['solarlife'], 'ymax', **line_props['solarlife'])
# add_text(' Solar \nlifetime', (1.778e+02, 1.294e-09))

# add_line(lines['heli1'], ls=':', lw=2, color='k', zorder=10)
# add_line(lines['heli2'], ls='-', lw=2, color='k', zorder=10)
# add_fill(lines['heli1'], lines['coulomb'], **line_props['heli1'])
# add_text('CAST', (5.585e-04, 3.192e-05), rotation=-52)

# add_fill(lines['lsw'], lines['cmb'], **line_props['lsw'])
# add_fill(lines['lsw'], lines['heli1'], **line_props['lsw'])
# add_text('ALPS', (3.451e-04, 7.047e-08))

# add_fill(lines['hb'], 'ymax', **line_props['hb'])
# add_text('HB', (3.248e+03, 9.166e-14))

# add_fill(lines['rg'], 'ymax', **line_props['rg'])
# add_text('RG', (4e+03, 8.427e-15))

# add_fill(lines['thermal'], 'ymax', **line_props['thermal'])
# add_text('Thermal Cosmology', (1.738e+05, 1.019e-05), rotation=-90)

# add_fill(lines['xenon10sun'], 'ymax', **line_props['xenon10sun'])
# add_text('Xenon10 (solar)', (2e-03, 2e-10), rotation=-45)

# add_fill(lines['halo1'], lines['cdm'], **line_props['halo1'])
# add_fill(lines['halo2'], lines['cdm'], **line_props['halo2'])
# add_text('Haloscopes', (6e-07, 5.511e-13), rotation=90)

add_line(lines['cdm'], color='gray', lw=2, ls='--', zorder=20)
add_line(lines['damic19'], color='orange', lw=1.5, ls='--', label='DAMIC')
add_line(lines['xenon10sun'], color='black', lw=1.5, ls=':', label='Xenon10 (solar)')
add_line(lines['solarlife'], color='seagreen', lw=1.5, ls='-.', label='Solar lifetime')

add_line(lines['tokyo'], color='midnightblue', lw=2)
add_text('Tokyo', (2.5, 1.3e-11), color='midnightblue')


# add_fill(lines['cdm'], 'ymin', color='whitesmoke', zorder=0)


# add_fill(
# 	lines['tokyo'], lines['cdm'], edgecolor='midnightblue',
# 	lw=2, facecolor=(0,0,0,0)
# )

# add_text('Allowed\nHP CDM', (1e-4, 2.5e-16))

# add_text('Tokyo', (2, 3.875e-10), rotation=-90, color='w')

# add_fill(lines['damic19'], lines['cdm'], **line_props['damic'])
# add_text('DAMIC', (9.5, 3.818e-11), rotation=-90)

# add_fill(lines['xenon10'], lines['cdm'], **line_props['xenon10'])
# add_text('Xenon10 (DM)', (1.148e+01, 2e-16))


# # # # # FUNK status in vis-nuv

def get_pmt_qeff(infile, npts=500):

	data 	= np.loadtxt(infile)
	x, y 	= data.T
	# xnew 	= np.linspace(x[0], x[-1], npts)
	xnew 	= np.linspace(145, 635, npts)
	yfunc = interp1d(
						x, y, kind='cubic', bounds_error=False,
						fill_value=1e-2
					)

	return xnew, yfunc


c 		= const.c
h 		= const.h / const.eV
k 		= const.k / const.eV

A 		= 14.56
R 		= 0.8
eta 	= 2*(np.sqrt(R)-R) / (1-R) # effective reflectivity

# m 		= h*c / (pmt[:,0] * 1e-9) # in eV
# m_vis = np.logspace(np.log10(m[0]), np.log10(m[-1]))

l_vis, pmt_qeff = get_pmt_qeff('pmt/pmt_9107B_extr')
m_vis = h*c / (l_vis*1e-9)

def chi_vis(m, sigma):
	# joerg: 5.6e-12 * sqrt(sigma * m / (pmt_qeff(m)*1e-2 * A * R))
	l = h*c/m/1e-9
	return  4.1e-12 * np.sqrt(sigma * m / (pmt_qeff(l)*1e-2 * A*eta**2))

plt.plot(
	m_vis, chi_vis(m_vis, sigma=1.58*0.0151), #0.59*0.0108), # 95% CL (FC limit)
	color='crimson', lw=2
)
# add_text(r'\textbf{\textsc{Funk}}', (2.5, 2.7e-13), color='crimson')
add_text(r'\textbf{\textsc{Funk}}', (5.5, 1.7e-12), color='crimson')


plt.xlabel(r'$m_{\tilde{\gamma}}/\mathrm{eV}$')
plt.xlim(1, 10)
# plt.xscale('log')
plt.ylabel(r'$\lg\chi$')
plt.yscale('log')
plt.ylim(1e-14, 1e-10)
plt.yticks(
	[ 10**x for x in range(-14, -9) ],
	[ str(x) for x in range(-14, -9) ]
)

# plt.subplots_adjust(left=0.15, bottom=0.15, right=0.975, top=0.975)
# plt.grid(ls=':')
# plt.savefig('../plt/12/exclusion_limit_zoom')

# plt.legend(loc='lower left', fontsize=9.5)
# plt.subplots_adjust(left=0.16, bottom=0.162, right=0.975, top=0.975)
# plt.savefig('../../talks/src/phd_defense/figs/exclusion_limit_zoom')

plt.legend(loc='lower left', fontsize=9)
plt.tight_layout(pad=0.05)
plt.savefig('../plt/8/exclusion_limit_zoom')

plt.show()
plt.close()