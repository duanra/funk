import os
import numpy as np 
import matplotlib.pyplot as plt

from matplotlib import rc
from scipy.interpolate import interp1d
rc('font', size=11)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def find_files(path):
	fpaths, fnames = [], []

	for root, dirs, files in os.walk(path):
		for file in files:
			if file[0]!='.': # ignore hidden files
				fpaths.append(os.path.join(root, file))
				fnames.append(file)
		break # non-recursive

	return fpaths, fnames


def exclusion_line(data):
	""" data are in log-log scale """
	x = np.log10(data[:,0])
	y = np.log10(data[:,1])
	new_x = np.linspace(np.log10(data[0,0]), 
											np.log10(data[-1,0]),
											num=1000)

	return new_x, interp1d(x, y, kind='linear')


def rescale(xy):
	""" loglog return of exclusion_line """
	return 10**xy[0], 10**xy[1](xy[0])


def add_line(xy, ax=None, **kwargs):
	if ax is None:
		ax = plt.gca()

	ax.plot(*rescale(xy), **kwargs)


def add_dlines(xy, xyup, ax=None, **kwargs):
	""" delimiter lines """
	if ax is None:
		ax = plt.gca()
	x, y = xy
	xlow = 10**min(x)
	xup  = 10**max(x)
	ylow1 = 10**y(np.log10(xlow))
	ylow2 = 10**y(np.log10(xup))

	if isinstance(xyup, (int, float)):
		yup = xyup
	elif xyup == 'ymin':
		yup = ax.get_ybound()[0]
	elif xyup == 'ymax':
		yup = ax.get_ybound()[1]
	elif isinstance(xyup, (tuple, list)):
		yup = (10**xyup[1](np.log10(xlow)),
					 10**xyup[1](np.log10(xup)))
	else:
		print('[Warning] add_dlines(): check args - skipping plot')

	ax.vlines((xlow, xup), (ylow1, ylow2), yup, **kwargs)


def add_fill(xy1, xy2, ax=None, **kwargs):

	kwargs.setdefault('facecolor', (0, 0, 0, .5))
	kwargs.setdefault('edgecolor', (0, 0, 0, .5))

	if ax is None:
		ax = plt.gca()

	if isinstance(xy2, (int, float)): 
		ax.fill_between(*rescale(xy1), xy2, **kwargs)

	elif xy2 == 'ymin': 
		ax.fill_between(*rescale(xy1), ax.get_ybound()[0], **kwargs)

	elif xy2 == 'ymax':
		ax.fill_between(*rescale(xy1), ax.get_ybound()[1], **kwargs)

	elif isinstance(xy2, (tuple, list)):
		# fill between two curves
		x_common = np.linspace(max(xy1[0][0], xy2[0][0]),
													 min(xy1[0][-1], xy2[0][-1]),
													 endpoint=True, num=1000)
		ax.fill_between(10**x_common,
										10**xy1[1](x_common),
										10**xy2[1](x_common),
										interpolate=True, **kwargs)

	else:
		print('[Warning] add_fill(): check args - skipping plot')


def add_text(txt, coords, ax=None, **kwargs):
	""" coords (tuple) are in axes coordinates """
	kwargs.setdefault('color', 'snow')
	kwargs.setdefault('family', 'sans-serif')
	kwargs.setdefault('weight', 'semibold')
	kwargs.setdefault('zorder', 50)

	if ax is None:
		ax = plt.gca()

	# ax.text(*coords, txt, transform=ax.transAxes, **kwargs)
	ax.text(*coords, txt, **kwargs)


def on_click_coords(event, ax=None, fig=None):
	""" print Mouse(x,y) in data coords upon left click MouseEvent """
	if ax is None:
		ax = plt.gca()

	# # in axes coords
	# def _transform(*args):
	# 	try:
	# 		return ax.transAxes.inverted().transform(
	# 							ax.transData.transform(args))
	# 	except:
	# 		print('[Exception] mouse outside of axes object area')

	# print('Mouse(x, y): ({:.3f}, {:.3f})'.format(
	# 			*_transform(event.xdata, event.ydata)))

	try:
		print('Mouse(x, y): ({:.3e}, {:.3e})'\
					.format(event.xdata, event.ydata))
	except:
 		print('[Exception] mouse outside of axes object area')

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

cdir  = os.path.dirname(os.path.realpath(__file__))

fpaths, fnames = find_files(
		os.path.join(cdir, 'exs'))
xy = {
	fnames[i]: exclusion_line(np.loadtxt(fpaths[i]))\
	for i in range(len(fpaths))
}

fig, ax = plt.subplots(figsize=(10,6))
ax.set_yscale('log')
ax.set_xscale('log')
ax.axis([1e-13, 1e6, 1e-16, 1])

add_fill(xy['cdm'], 'ymin', color='darkseagreen')
add_line(xy['cdm'], color='darkred', ls='--')

add_fill(xy['ex_global'], 'ymax', color='slategrey')
add_fill(xy['ex_halo'], xy['cdm'], color='slategrey')

add_text('FUNK', (2.178e+00, 5.754e-11), rotation=-90,
					family='sans-serif', style='italic')
add_line(xy['funk_vis'], lw=1.5, color=(0.752, 0.007, 0.019))

add_text('FUNK++', (2.655e-04, 1.738e-14),
					family='sans-serif', style='italic')

add_text('Schottky\ndiode', (4.295e-04, 4.055e-12), color='k',
					rotation=20, size='x-small')
add_line(xy['funkpp_schottky'], lw=1.5, ls=':', color=(0.752, 0.007, 0.019))
add_dlines(xy['funkpp_schottky'], xy['cdm'],
					 lw=1.5, linestyles='dotted', color=(0.752, 0.007, 0.019))

add_text('HEB', (4.898e-04, 6.918e-13), color='k',
				 rotation=20, size='x-small')
add_line(xy['funkpp_heb'], lw=1.5, ls='--', color=(0.752, 0.007, 0.019))
add_dlines(xy['funkpp_heb'], xy['funkpp_schottky'],
					 lw=1.5, linestyles='dashed', color=(0.752, 0.007, 0.019))

add_text('SIS', (4.898e-04, 2.754e-13), color='k',
				 rotation=20, size='x-small')
add_line(xy['funkpp_sis'], lw=1.5, ls='-', color=(0.752, 0.007, 0.019))
add_dlines(xy['funkpp_sis'], xy['funkpp_heb'],
					 lw=1.5, color=(0.752, 0.007, 0.019))

fig.canvas.mpl_connect('button_press_event', on_click_coords)
# fig.savefig(cdir+'/out.pdf', format='pdf')
plt.show()
plt.close()