#!usr/bin/env python3

import __pardir__
import os
import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
import mod.funk_consts as fcn

from IPython import embed
from itertools import chain
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle
from scipy.interpolate import interp1d

from mod.funk_consts import rcPaper, rcPoster, rcPoS
from mod.funk_utils import handle_warnings

# mpl fontstyle/fontfamily are overriden by latex style

# plt.rcParams.update(fcn.rcDefense)
# plt.rcParams['figure.figsize'] = (360/72, 245/72)

# plt.rcParams.update(fcn.rcThesis)
# plt.rcParams['figure.figsize'] = (320/72, 225/72)
# plt.rcParams['xtick.top'] 	= False
# plt.rcParams['ytick.right'] = False

plt.rcParams.update(fcn.rcPaper)
plt.rcParams['axes.grid'] = False
plt.rcParams['figure.figsize'] = (0.7*510/72, 0.5*510/72)

# plt.rcParams.update(fcn.rcPoS)
# plt.rcParams['figure.figsize'] = (0.75*430.2/72, 0.62*0.75*430.2/72)

# plt.rcParams.update({
# 	# tex document \columnwidth = 246pt and \textwidth = 510pt
# 	'figure.figsize' 			: ( 960/72, 450/72 ),
# 	'savefig.format'			: 'pdf',
# 	'font.size'						: 28,
# 	'axes.labelsize'			: 30,
# 	'legend.fontsize'			: 'smaller',
# 	'text.latex.preamble'	: [
# 			r'\usepackage{times}',
# 			r'\usepackage{newpxmath}',
# 		],
# 	'text.usetex'					: True,
# })


line_props = {
	'cdm'       	: dict(color='whitesmoke', zorder=1),
	'xenon10sun'	: dict(color='lightgrey', lw=0, zorder=2),
	'rg'        	: dict(color='plum', lw=0, zorder=3),
	'hb'        	: dict(color='skyblue', lw=0, zorder=4),
	'thermal'   	: dict(color='brown', zorder=5),
	'solarlife' 	: dict(color='mediumseagreen', zorder=6),
	'lsw'       	: dict(color='coral', zorder=7),
	'cmb'       	: dict(color='black', zorder=8),
	'heli1'     	: dict(color='mediumseagreen', zorder=10),
	'heli2'     	: dict(alpha=0, zorder=10),
	'coulomb'   	: dict(color='slategrey', zorder=11),
	'halo1'     	: dict(color='darkslategrey', zorder=12),
	'halo2'     	: dict(color='darkslategrey', zorder=13),
	'xenon10'   	: dict(color='rosybrown', lw=0, zorder=2),
	'damic'     	: dict(color='moccasin', lw=0, zorder=15),
	'tokyo'     	: dict(color='midnightblue', lw=0, zorder=16),
	'tokyo_mev'   : dict(color='blue', lw=0, zorder=16),
	'shuket'   		: dict(color='black', lw=0, zorder=17),
	'funk'      	: dict(color='crimson', lw=0, zorder=15),
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def find_files(path):

	fpaths, fnames = [], []

	for root, dirs, files in os.walk(path):
		for file in files:
			if file[0] != '.': # ignore hidden files
				fpaths.append(os.path.join(root, file))
				fnames.append(file)

		break # non-recursive

	return fpaths, fnames


def exclusion_line(data):
	""" data are in log-log scale """

	xs, ys = np.log10(data).T
	xs_new = np.logspace(xs[0], xs[-1], num=500)

	return xs_new, interp1d(xs, ys, kind='linear')


@handle_warnings
def rescale(line):
	""" logscale the interpolated values form exclusion_line() """
	xs, ys = line
	return xs, np.power(10, ys(np.log10(xs)))


def add_line(line, ax=None, **kwargs):

	if ax is None: ax = plt.gca()

	ax.plot(*rescale(line), **kwargs)


def add_fill(line1, line2, ax=None, **kwargs):

	if ax is None: ax = plt.gca()
	kwargs.setdefault('facecolor', (0, 0, 0, .5))
	kwargs.setdefault('edgecolor', (0, 0, 0, 0))

	if isinstance(line2, (int, float)): 
		ax.fill_between(*rescale(line1), line2, **kwargs)

	elif line2 == 'ymin':
		ax.fill_between(*rescale(line1), ax.get_ybound()[0], **kwargs)

	elif line2 == 'ymax':
		ax.fill_between(*rescale(line1), ax.get_ybound()[1], **kwargs)

	elif isinstance(line2, (tuple, list)):
		# fill between two curves
		xs1, xs2 	= line1[0], line2[0]
		ys1, ys2 	= line1[1], line2[1]
		xlow 			= np.log10(max(xs1[0], xs2[0]))
		xup  			= np.log10(min(xs1[-1], xs2[-1]))
		xs_common = np.logspace(xlow, xup, num=500)
		
		ax.fill_between(
				xs_common,
				np.power(10, ys1(np.log10(xs_common))),
				np.power(10, ys2(np.log10(xs_common))),
				**kwargs
			)

	else:
		print('[Warning] add_fill(): check args - skipping plot')


def add_text(txt, coords, ax=None, **kwargs):
	""" coords (tuple) are in data coords (x,y)
			for convenience, use on_click_coords() with mpl_connect
	"""

	if ax is None: ax = plt.gca()
	kwargs.setdefault('color', 'k')
	# kwargs.setdefault('alpha', 0.55)
	kwargs.setdefault('family', 'fantasy')
	kwargs.setdefault('weight', 'semibold')
	kwargs.setdefault('zorder', 50)

	# ax.text(*coords, txt, transform=ax.transAxes, **kwargs)
	ax.text(*coords, txt, **kwargs)


def on_click_coords(event, ax=None, fig=None):
	""" print Mouse(x,y) in data coords upon left click MouseEvent """

	if ax is None: ax = plt.gca()

	# # in axes coords
	# def _transform(*args):
	# 	try:
	# 		return ax.transAxes.inverted().transform(
	# 							ax.transData.transform(args))
	# 	except:
	# 		print('[Exception] mouse outside of axes object area')

	# print('Mouse(x, y): ({:.3f}, {:.3f})'.format(
	# 			*_transform(event.xdata, event.ydata)))

	if event.button==1:
		try:
			print('Mouse(x, y): ({:.3e}, {:.3e})'\
						.format(event.xdata, event.ydata))
		except:
	 		print('[Exception] mouse outside of axes object area')
	else:
		pass

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

cd = os.path.dirname(os.path.realpath(__file__))
os.chdir(cd)
fpaths, fnames = find_files('exs/')

lines = { fnames[i]: exclusion_line(np.loadtxt(fpaths[i]))
					for i in range(len(fpaths)) }

plt.figure()

# # # # # various exclusion lines

add_fill(lines['coulomb'], 'ymax', **line_props['coulomb'])
add_text('Coulomb', (7.943e-12, 2.512e-02))

add_fill(lines['cmb'], 'ymax', **line_props['cmb'])
add_text('CMB', (6.281e-13, 1.294e-04))

add_fill(lines['solarlife'], 'ymax', **line_props['solarlife'])
add_text(' Solar \nlifetime', (1.778e+02, 1.294e-09))

add_line(lines['heli1'], ls=':', lw=2, color='k', zorder=10)
add_line(lines['heli2'], ls='-', lw=2, color='k', zorder=10)
add_fill(lines['heli1'], lines['coulomb'], **line_props['heli1'])
add_text('CAST', (5.585e-04, 3.192e-05), rotation=-52)

add_fill(lines['lsw'], lines['cmb'], **line_props['lsw'])
add_fill(lines['lsw'], lines['heli1'], **line_props['lsw'])
add_text('ALPS', (3.451e-04, 7.047e-08))

add_fill(lines['hb'], 'ymax', **line_props['hb'])
add_text('HB', (3.248e+03, 9.166e-14))

add_fill(lines['rg'], 'ymax', **line_props['rg'])
add_text('RG', (4e+03, 8.427e-15))

add_fill(lines['thermal'], 'ymax', **line_props['thermal'])
add_text('Thermal Cosmology', (1.738e+05, 1.019e-05), rotation=-90)

add_fill(lines['xenon10sun'], 'ymax', **line_props['xenon10sun'])
add_text('Xenon10 (solar)', (2e-03, 2e-10), rotation=-44)

add_fill(lines['halo1'], lines['cdm'], **line_props['halo1'])
add_fill(lines['halo2'], lines['cdm'], **line_props['halo2'])
add_text('Haloscopes', (6e-07, 5.511e-13), rotation=90)

add_line(lines['cdm'], ls='--', lw=2, color=line_props['cdm']['color'], zorder=20)
add_fill(lines['cdm'], 'ymin', **line_props['cdm'])
add_text('Allowed HP CDM', (1e-4, 2.5e-16))
# add_text('Allowed\nHP CDM', (2e-2, 2.5e-16))

add_fill(lines['tokyo'], lines['cdm'], **line_props['tokyo'])
add_text('Tokyo', (2.05, 3.875e-10), rotation=-90, color='w')

add_fill(lines['damic19'], lines['cdm'], **line_props['damic'])
add_text(r'\textsc{DAMIC}', (9.5, 3.818e-11), rotation=-90)

add_fill(lines['xenon10'], lines['cdm'], **line_props['xenon10'])
add_text('Xenon10 (DM)', (1.148e+01, 2e-16))

add_fill(lines['tokyo_mev'], 'ymax', **line_props['tokyo_mev'])
add_text('Tokyo2', (5e-5, 6e-9))

add_fill(lines['shuket'], lines['cdm'], **line_props['shuket'])
add_text('Shuket', (4e-5, 2e-12))


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# FUNK status in vis-nuv

def get_pmt_qeff(infile, npts=500):

	data 	= np.loadtxt(infile)
	x, y 	= data.T
	# xnew 	= np.linspace(x[0], x[-1], npts)
	xnew 	= np.linspace(145, 635, npts)
	yfunc = interp1d(
						x, y, kind='cubic', bounds_error=False,
						fill_value=1e-2
					)

	return xnew, yfunc


c 		= const.c
h 		= const.h / const.eV
k 		= const.k / const.eV

A 		= 14.56
R 		= 0.8
eta 	= 2*(np.sqrt(R)-R) / (1-R) # effective reflectivity

# m 		= h*c / (pmt[:,0] * 1e-9) # in eV
# m_vis = np.logspace(np.log10(m[0]), np.log10(m[-1]))

l_vis, pmt_qeff = get_pmt_qeff('pmt/pmt_9107B_extr')
m_vis = h*c / (l_vis*1e-9)

def chi_vis(m, sigma):
	# joerg: 5.6e-12 * sqrt(sigma * m / (pmt_qeff(m)*1e-2 * A * R))
	l = h*c/m/1e-9
	return  4.1e-12 * np.sqrt(sigma * m / (pmt_qeff(l)*1e-2 * A*eta**2))


plt.fill_between(
	m_vis, chi_vis(m_vis, sigma=1.58*0.0151), # 95% CL (FC limit)
	np.power(10, lines['cdm'][1](np.log10(m_vis))),
	**line_props['funk']
)

add_text(
	r'\textbf{\textsc{Funk}}', (2.1, 2e-13),
	color=line_props['funk']['color'], alpha=1,
	rotation=-90, family='sans-serif', style='italic',
	size='larger'
)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# FUNK++

# def chi_ghz(m, T_rec=70., k_rec=1., dt=100., bandwidth=None, broadscan=False):

# 	if broadscan:
# 		B = lambda m: 20e9 - 10e9

# 	elif bandwidth and not broadscan:
# 		B = lambda m: bandwidth

# 	else:
# 		B = lambda m: 280. * m/1e-6

# 	return 6.7e-12 * np.sqrt(k_rec*T_rec / (100.*A*eta**2))\
# 								 * np.power(B(m)/1e9 * 100./dt, 0.25)


# def chi_thz(m, x=50, k_rec=1., dt=3., bandwidth=None, broadscan=False):

# 	T_rec = lambda m: x * m / k

# 	if broadscan:
# 		B = lambda m: 1.5e12 - 0.1e12

# 	elif bandwidth and not broadscan:
# 		B = lambda m: bandwidth

# 	else:
# 		B = lambda m: 280. * m/1e-6

# 	return 6.7e-12 * np.sqrt(k_rec*T_rec(m) / (100.*A*eta**2))\
# 								 * np.power(B(m)/1e9 * 100./dt, 0.25)


# m_ghz = h * np.logspace(np.log10(10e9), np.log10(20e9))

# plt.fill_between(
# 	m_ghz,
# 	chi_ghz(m_ghz, T_rec=300., k_rec=1., dt=30., bandwidth=1e6),
# 	np.power(10, lines['cdm'][1](np.log10(m_ghz))),
# 	color='indianred', zorder=20
# )

# add_text(
# 	'GHz antenna', (3.7e-05, 3e-13), color='k', 
# 	size='x-small', rotation=-90, family='sans-serif'
# )

# m_thz_schottky = h * np.logspace(np.log10(0.1e12), np.log10(2.5e12))

# plt.fill_between(
# 	m_thz_schottky,
# 	chi_thz(m_thz_schottky, x=50., k_rec=1., dt=3., bandwidth=88e3),
# 	np.power(10, lines['cdm'][1](np.log10(m_thz_schottky))),
# 	color='indianred', zorder=20
# )

# add_text(
# 	'Schottky\ndiode', (4.487e-04, 3.251e-12), color='k',
# 	size='x-small', rotation=25, family='sans-serif'
# )


# m_thz_heb = h*np.linspace(0.1e12, 2.5e12)

# # plt.fill_between(
# # 	m_thz_heb, chi_thz(m_thz_heb, x=10., k_rec=1., dt=3.,
# # 										 bandwidth=88e3, broadscan=False),
# # 	np.power(10, lines['cdm'][1](np.log10(m_thz_heb))),
# # 	color='lightcoral', zorder=19)

# # add_text(
# # 	'HEB', (4.898e-04, 6.427e-13), color='k',
# # 	size='x-small', rotation=20, family='sans-serif')


# m_thz_sis = h * np.linspace(0.1e12, 0.7e12)

# plt.fill_between(
# 	m_thz_sis, chi_thz(m_thz_sis, x=2., k_rec=1., dt=3., bandwidth=88e3),
# 	np.power(10, lines['cdm'][1](np.log10(m_thz_sis))),
# 	color='pink', zorder=15)

# add_text(
# 	'SIS', (4.898e-04, 3.866e-13), color='k',
# 	size='x-small', rotation=25, family='sans-serif')


# plt.gca().add_patch(
# 	Rectangle(
# 		(m_ghz[0], 0.5*10**-16),
# 		width=m_ghz[-1]-m_ghz[0], height=1,
# 		color='k', alpha=0.1, zorder=17
# 	))
# plt.gca().add_patch(
# 	Rectangle(
# 		(m_thz_heb[0], 0.5*10**-16),
# 		width=m_thz_schottky[-1]-m_thz_schottky[0], height=1,
# 		color='k', alpha=0.1, zorder=17
# 	))

# add_text(
# 	# 'FUNK++',
# 	r'\textbf{\textsc{Funk}\hspace{-0.1em}\raisebox{0.14ex}{\texttt{++}}}',
# 	(6.839e-05, 1.2e-15), color=line_props['funk']['color'],
# 	size='larger', family='sans-serif', style='italic', )


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


plt.xlabel(r'$\lg(m_{\tilde{\gamma}}/\mathrm{eV})$')
plt.xscale('log')
plt.xlim(1e-7, 8e4) # (1e-13, 1e6)
plt.xticks(
		[ 10**x for x in range(-6, 5) ],
		[ str(x) if x%2==0 else str() for x in range(-6, 5) ]
	)


plt.ylabel(r'$\lg\chi$')
plt.yscale('log')
plt.ylim(6e-17, 2e-8) # (1e-16, 1)
plt.yticks(
		[ 10**x for x in range(-16, -7) ],
		[ str(x) if x%2==0 else str() for x in range(-16, -7) ]
	)
plt.gca().yaxis.set_minor_locator(
	plt.LogLocator(subs='all', numticks=10))
plt.gca().yaxis.set_minor_formatter(
	plt.NullFormatter())


plt.gcf().canvas.mpl_connect('button_press_event', on_click_coords)

plt.subplots_adjust(left=0.1, bottom=0.14, right=0.995, top=0.995)
# plt.subplots_adjust(left=0.1, bottom=0.12, right=0.995, top=0.995)
# plt.subplots_adjust(left=0.155, bottom=0.165, right=0.995, top=0.995)
# plt.subplots_adjust(left=0.10, bottom=0.14, right=0.995, top=0.995)

plt.savefig('../plt/8/exclusion_limit')
# plt.savefig('../plt/12/exclusion_limit')
# plt.savefig('../../talks/src/phd_defense/figs/exclusion_limit')
# plt.savefig('../../talks/src/phd_defense/figs/exclusion_limitPP')
plt.close()
# plt.show()


print(np.min(chi_vis(m_vis, sigma=1.58*0.0151)))
print(m_vis[0], m_vis[-1])

m_visnew = np.logspace(np.log10(1.95), np.log10(8.55), 15)
for m in m_visnew:
	print('{:.2f} {:.2e}'.format(m, chi_vis(m, sigma=1.58*0.0151))) 

# embed()