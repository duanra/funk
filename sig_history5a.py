""" fit the difference to get the true signal in/out from funk run
		NOTE: the history has not the same normalisation as in sig_history.py
		 			and sig_history2.py here, alpha' -> alpha/tau
"""

#!usr/bin/env python3

import os
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt

from array import array
from datetime import timedelta
from IPython import embed
from iminuit import Minuit
from matplotlib import rc

import mod.funk_consts as fcn
from mod.funk_manip import reset_configs
from mod.funk_plt import show, order_legend
from mod.funk_run import Run
from mod.funk_utils import  posix2utc, flatten_list, timing,\
														pickle_dump, pickle_load, Namespace

rc('font', size=10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

class Model:

	def __init__(self, data, base, alpha, tau):

		seq 	 	= list(map(tuple, data[['in', 'open']].iloc[:4].values))
		# [ i for i,x in enumerate(seq) if x==fcn.positions['in_open'] ][0]
		ix_in  	= seq.index(fcn.positions['in_open'])
		ix_out 	= seq.index(fcn.positions['out_open'])

		m 			= 4*10
		n 			= len(data)
		tintvls = [ (i, i+1) for i in range(n) ]
		tbins 	= [ (0+i, i+m if i+m<n else n) for i in range(0, n, m) ]

		self.ix_in 	 = ix_in
		self.ix_out	 = ix_out
		self.midtime = np.mean(tintvls, axis=1)
		self.tintvls = tintvls
		self.tbins 	 = tbins
		self.tintvl_len  = 1
		self.tbin_len 	 = m

		self.events  		 = data['counts'].values
		self.events_diff = self.events[ix_in::4] - self.events[ix_out::4]

		self._init_parameters(base, alpha, tau)


	def _init_parameters(self, base, alpha, tau):

		params = Namespace(
							flatten_list([ # 200, 180
									[('in%d'%i, 200), ('out%d'%i, 180)]\
									for i in range(len(self.tbins))
							]))

		params.low 	 = 0
		params.base  = base
		params.alpha = alpha
		params.tau 	 = tau

		self.params  = params
		self.saved_history = {}
		self._integrals()


	def _set_parameters(self, *args, **kwargs):
		""" only use this to update params, do not manually assign
				params to avoid more overhead check upon update
		"""

		if len(args): # all params must be given at once and keep the order
			new_params = Namespace( zip(self.params.keys(), args) )
			if self.params != new_params:
				self.params.update(new_params)
				self.saved_history = {}
				self._integrals()

		elif len(kwargs): # only for checking purpose, not prefered method
			if all([ kw in self.params.keys() for kw in kwargs.keys() ]):
				self.params.update(kwargs)
				self.saved_history = {}
				self._integrals()

		else:
			print('_set_parameters(): no parameters given')
			return


	def signal(self, t):

		if t < self.tintvls[0][0] or t >= self.tintvls[-1][-1]:
			ret = 0

		else:
			i = int(t / self.tbin_len) # integer division preserves dtype
			k = int(t / self.tintvl_len) % 4
			if k==self.ix_in:
				ret = self.params['in%d'%i]
			elif k==self.ix_out:
				ret = self.params['out%d'%i]
			else:
				ret = self.params['low']

		return ret


	def _integrals(self):

		self._piecewise_sum = np.array([
				self.signal(t1) * ( 1 - np.exp((t1-t2)/self.params.tau) )
				for (t1, t2) in self.tintvls
				])
		

	def _tindex(self, t):
	
		if t < self.tintvls[0][0]:
			ret = -1

		elif t >= self.tintvls[-1][-1]:
			ret = len(self.tintvls)

		else:
			ret = int(t / self.tintvl_len)
			# ret = np.flatnonzero([t1 <= t < t2 for (t1, t2) in self.tintvls])[0]

		return ret


	def history(self, t, *args):
		
		if len(args): self._set_parameters(*args)

		tix = self._tindex(t)

		if tix == -1:
			ret = 0

		else:
			s = 0 # self.tau * self.base * np.exp( -t/self.tau )
			alpha, tau = self.params.alpha, self.params.tau

			for i in range(0, tix):
				t2 = self.tintvls[i][1]
				s += self._piecewise_sum[i] * np.exp((t2-t)/tau)
		
			if tix < len(self.tintvls):
				t1 = self.tintvls[tix][0]
				s	+= self.signal(t) * ( 1 - np.exp((t1-t)/tau) )

			ret =  alpha * tau * s

		return ret


	def history2(self, t, *args):
		""" save history to speed up evaluation for t+1 and all t MUST be
				within tintvls bounds to avoid additional overheads
		"""

		if len(args): self._set_parameters(*args)

		alpha, tau = self.params.alpha, self.params.tau

		if t in self.saved_history.keys():
			# print('in saved')
			return self.saved_history[t]

		elif t-1 in self.saved_history.keys():
			# print('using history')
			tt 	 = t-1
			ttix = int(tt) # self._tindex(tt)
			
			tt1, tt2 = self.tintvls[ttix]

			h 	 = self.saved_history[tt] + \
						 alpha * tau * self.signal(tt1) * (
						 		np.exp((tt2-tt)/tau)
						 		* (1 - np.exp((tt1-tt2)/tau))
								- (1 - np.exp((tt1-tt)/tau))
								)
	
			ret  = alpha * tau * self.signal(tt2) * (1 - np.exp((tt2-t)/tau)) \
						 + np.exp(-1/tau) * h

		else:
			ret  = self.history(t)

		self.saved_history[t] = ret

		return ret


	def response(self, t, *args):
		""" params checks are only done in history """

		h = self.history2(t, *args)
		i = self.signal(t)
		b = self.params.base

		return b + i + h


	def response_diff(self, *args):

		if len(args): self._set_parameters(*args)

		time 		 = self.midtime
		time_in  = time[self.ix_in::4]
		time_out = time[self.ix_out::4]

		response_in  = np.array([ self.response(t) for t in time_in ])
		response_out = np.array([ self.response(t) for t in time_out ])

		return response_in - response_out
		

	def fit(self, **kwargs):
		""" response is calculated at the middle of each tintvl, this is a 
				good approximation for the mean event rate if the response is
				linear within that tintvl (and tintvl is assumed to be 1)
		"""

		nclock 			 = len(self.tbins)
		init_fitargs = self.params.copy()
		init_fitargs.update(
			Namespace(
				[ ('limit_in%d'%i, (0, None)) for i in range(nclock) ] +
				[ ('limit_out%d'%i, (0, None)) for i in range(nclock) ] +
				[ ('limit_low', (0, None)), ('limit_base', (0, None)),
					('limit_tau', (0, None)), ('limit_alpha', (0,1)) ] +
				[ ('error_in%d'%i, 0.1) for i in range(nclock)] +
				[ ('error_out%d'%i, 0.1) for i in range(nclock) ] +
				[ ('error_low', 0.1), ('error_base', 0.1),
					('error_tau', 0.1), ('error_alpha', 0.01) ] +
				[ ('fix_low', True), ('fix_base', True),
					('fix_tau', True), ('fix_alpha', True) ] +
				[ ('errordef', 1) ]
			))

		parnames = list(self.params.keys())

		time = self.midtime
		data = self.events

		def lsq(*args):
			self._set_parameters(*args)
			response = np.array([ self.response(t) for t in time ])

			return np.sum( (data - response)**2 / data )

		mfit = Minuit(lsq, forced_parameters=parnames, **init_fitargs)

		print('running migrad...')
		timing(mfit.migrad)(**kwargs)
		print('reduced chi2: {:.3f} / {}'\
					.format(mfit.fval, len(data)-len(self.params)-4) )

		self.fitted = mfit


	def	fit_diff(self, **kwargs):
		""" kwargs are for Minuit.migrad() """ 

		nclock 			 = len(self.tbins)
		init_fitargs = self.params.copy()
		init_fitargs.update(
			Namespace(
				[ ('limit_in%d'%i, (0, None)) for i in range(nclock) ] +
				[ ('limit_out%d'%i, (0, None)) for i in range(nclock) ] +
				[ ('limit_low', (0, None)), ('limit_base', (0, None)),
					('limit_tau', (0, None)), ('limit_alpha', (0,1)) ] +
				[ ('error_in%d'%i, 0.1) for i in range(nclock)] +
				[ ('error_out%d'%i, 0.1) for i in range(nclock) ] +
				[ ('error_low', 0.1), ('error_base', 0.1),
					('error_tau', 0.1), ('error_alpha', 0.01) ] +
				[ ('fix_low', True), ('fix_base', True),
					('fix_tau', True), ('fix_alpha', True) ] +
				[ ('errordef', 1) ]
			))

		parnames = list(self.params.keys())

		data 		 = self.events_diff
		data_err = np.sqrt(self.events[self.ix_in::4] +
											 self.events[self.ix_out::4])
		
		def lsq(*args):
			response_diff = self.response_diff(*args)

			return np.sum( (data - response_diff)**2 / data_err )

		mfit = Minuit(lsq, forced_parameters=parnames, **init_fitargs)

		print('running migrad...')
		timing(mfit.migrad)(**kwargs)
		print('reduced chi2: {:.3f} / {}'\
					.format(mfit.fval, len(data)-len(self.params)-4) )

		self.fitted_diff = mfit


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


def get_data(run, begin=None, Dt=None):
	""" begin/Dt: timedelta object (duration to retrieve) """

	df = reset_configs(run.configs)\
				[['time', 'in', 'open', 'dt', 'counts']]
	df = df.assign(
					timedelta = posix2utc(df['time']) - \
											posix2utc(df['time'].iloc[0])
					)

	if (begin is not None) and (Dt is not None):
		ret = df[ (df['timedelta']>= begin) & (df['timedelta'] <= begin+Dt) ]

	elif begin is not None:
		ret = df[ (df['timedelta']>= begin) ]

	elif Dt is not None:
		ret = df[ (df['timedelta'] <= Dt) ]

	else:
		ret = df

	return ret.iloc[:len(ret)-len(ret)%4]\
						.reset_index(drop=True)


def plot_data(data, length=100, ax=None):

	if ax is None:
		ax = plt.gca()

	counts = data['counts'].values[:length]
	time 	 = np.arange(len(counts)) + 0.5 # shift to middle of intervals
	seq 	 = list(map(tuple, data[['in', 'open']].iloc[:4].values))

	xy 		 = {
		seq[i]: [ time[i::4], counts[i::4], np.sqrt(counts[i::4]) ]\
		for i in range(4)
		}

	color  = {
		(0, 0): 'k', (1, 0): 'k',
		(0, 1): 'b', (1, 1): 'grey'
		}

	label  = {
		(0, 0): '', (1, 0): 'in/out_closed',
		(0, 1): 'out_open', (1, 1): 'in_open'
		}

	for d in xy.items():
		if d[0]==(1,1):
			ax.errorbar(
				*d[1], fmt='o', ms=1, elinewidth=0.5,
				color=color[d[0]], label=label[d[0]],
				alpha=1, zorder=0 )


def plot_signal(model, length=100, ax=None):

	if ax is None:
		ax = plt.gca()

	time 	 		 = model.midtime[:length]
	# time_11 	 = time[model.ix_in::4]
	# time_01 	 = time[model.ix_out::4]

	tintvls  	 = np.array(model.tintvls)[:length]
	tintvls_11 = tintvls[model.ix_in::4]
	tintvls_01 = tintvls[model.ix_out::4]

	tbins			 = np.array(model.tbins)[:len(time)//4]
	bw 				 = model.tbin_len // 4

	signal 		 = model.params.base + np.array([ model.signal(t) for t in time ])
	signal_11  = signal[model.ix_in::4]
	signal_01  = signal[model.ix_out::4]

	# ax.hlines(
	# 	signal_11, tintvls_11[:,0], tintvls_11[:,1],
	# 	color='r', lw=2, label='input (in_open)', zorder=100 )

	# ax.hlines(
	# 	signal_01, tintvls_01[:,0], tintvls_01[:,1],
	# 	color='b', lw=2, label='input (out_open)', zorder=100 )

	ax.hlines(
		signal_11[::bw], tbins[:,0], tbins[:,1],
		color='r', lw=1.5, label='input (in_open)', zorder=200 )

	# ax.hlines(
	# 	signal_01[::bw], tbins[:,0], tbins[:,1],
	# 	color='darkblue', lw=1.5, label='input (out_open)', zorder=200 )

	# ax.plot(
	# 	time_11, signal_11, color='r', lw=1,
	# 	label='input (in_open)', zorder=100 )

	# ax.plot(
	# 	time_01, signal_01, color='b', lw=1,
	# 	label='input (out_open)', zorder=100 )

	# baseline
	ax.axhline(
		model.params.base, lw=2, ls=':',
		color='k', label='base' )


def plot_response(model, length=None, ax=None):

	if ax is None:
		ax = plt.gca()

	time 		 = model.midtime[:length]
	response = np.array([ model.response(t) for t in time ])

	ax.plot(
		time[model.ix_in::4], response[model.ix_in::4],
		color='k', label='pmt response', zorder=100
		)

	# ax.scatter(
	# 	time, response, marker='o', s=1,
	# 	edgecolor='grey', facecolor='none',
	# 	label='pmt response', zorder=100 )


def plot_legend(ax=None):
	""" need fig.tight_layout() """

	if ax is None:
		ax = plt.gca()

	lgda = ax.legend(
		*order_legend('in_open', 'out_open', 'in/out_closed'),
		title='data', fancybox=True,
		bbox_to_anchor=(1.005, 0.52, 0.15, 0.48),
		loc='lower left')
	lgda.get_title().set_fontweight('semibold')
	ax.add_artist(lgda)

	lgdb = ax.legend(
		*order_legend(
			'input (in_open)', 'input (out_open)', 'pmt response', 'base'),
		title='fitted', fancybox=True,
		bbox_to_anchor=(1.005, 0, 0.15, 0.48),
		loc='lower left')
	lgdb.get_title().set_fontweight('semibold')
	ax.add_artist(lgdb)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# base, alpha, tau = 96.3677, 0.0275, 52.879

# # cuts  = fcn.cuts.select('dtlim')
# cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
# trig 	= False
# run_v = 'v35'
# run 	= Run.configs(run_v, trig=trig, cuts=cuts)

# begin = None # timedelta(hours=5)
# Dt 		= timedelta(days=1)
# data 	= get_data(run, begin, Dt)
# embed()


base, alpha, tau = 100, 0.0275, 55

data  = pd.read_pickle('./data/dummy_open1_closed1.sdf')[:4*100]
data['out'] 	 ^= 1
data['closed'] ^= 1
data.rename(
	columns={'triggers': 'counts', 'out': 'in', 'closed': 'open'},
	inplace=True )
# todo: check mseq when fit_diff --> maybe redump dummy


model = Model(data, base, alpha, tau)
# model.fit()
# model.fit_diff()

d  = model.events_diff
rd = model.response_diff()

# plt.plot(d)
# plt.plot(rd, ls='--')
# show()

embed()

# pickle_dump({
# 	**model.fitted.fitarg,
# 	'fval': model.fitted.fval,
# 	'dof' : len(data)-len(model.params)-4 },
# 	fname='fitarg.sdic',
# 	fpath='./log/sig_history_dummy/' )


# fitarg = pickle_load(
# 					'fitarg.sdic', fpath='./log/sig_history_dummy/' )
# params = [ fitarg[kw] for kw in model.params.keys() ]
# model._set_parameters(*params)


# fig, ax = plt.subplots(figsize=(8,3))

# length = None
# plot_data(data, length)
# plot_signal(model, length)
# plot_response(model, length)
# plot_legend()

# ax.set_xlabel('dummy time [mins]')
# ax.set_ylabel('counts [per min]')
# ax.ticklabel_format(axis='x', style='sci', scilimits=(0,0), useMathText=True)
# # ax.set_xticklabels(posix2utc(data['time'].values), rotation=30)

# fig.tight_layout()
# # fig.savefig('./log/sig_history_dummy/fit.pdf', format='pdf')

# show()

# embed()