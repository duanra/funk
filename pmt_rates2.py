#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from IPython import embed
from matplotlib import rc
from matplotlib.patches import Rectangle
from datetime import timedelta
from mod.funk_manip import get_muons, reset_configs
from mod.funk_run import Run
from mod.funk_plt import autofmt_xdate, order_legend, group_legend, show
from mod.funk_utils import posix2utc, fn_over_df

rc('font', size=11)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def get_data(run, pos, begin=None, dt=None):
	""" dt is timedelta object (duration to retrieve) """

	df = run.get_df(pos)[['time', 'rates']]
	df = df.assign(
					timedelta = posix2utc(df['time']) - \
											posix2utc(df['time'].iloc[0])
					)

	if (begin is not None) and (dt is not None):
		return df[ (df['timedelta']>= begin) &\
							 (df['timedelta'] <= begin+dt) ]

	elif begin is not None:
		return df[ (df['timedelta']>= begin) ]

	elif dt is not None:
		return df[ (df['timedelta'] <= dt) ]

	else:
		return df


# cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
cuts  = fcn.cuts.select('dtlim')
trig 	= True
run_v = 'v35'
run 	= Run.configs(run_v, trig=trig, cuts=cuts)

dfT 	= reset_configs(run.configs)[['time', 'T1', 'T2']]
dfmu 	= get_muons(
					label='vcoinc', tperiod=run.period,
					timestamps=dfT['time'].values)


label = {
	'in_open'			: 'in/open',
	'in_closed'		: 'in/closed',
	'out_open'		: 'out/open',
	'out_closed'	: 'out/closed',
	'T1'					: 'T1',
	'muons'				: 'vert. muons.'
}

color = {
	'in_open'			: 'red',
	'in_closed'		: 'black',
	'out_open'		: 'blue',
	'out_closed'	: 'grey',
	'T1'					: 'darkgreen',
	'muons'				: 'saddlebrown'
}


fig 	= plt.figure(figsize=(8,5))
axtop = fig.add_axes([.12, .45, .8, .45])
axbot = fig.add_axes([.12, .15, .8, .3], sharex=axtop)
axbottw = axbot.twinx()
axtop.tick_params(labelbottom=False, right=True, labelright=True)


m = 10 # averaging over m*1*4 mins
for pos in ['in_closed', 'out_closed', 'in_open', 'out_open']:

	try:
		df = get_data(run, pos) # begin=timedelta(days=1), dt=timedelta(weeks=2))
		df = fn_over_df(df[['time', 'rates']], fn=np.mean, m=m)

		axtop.plot(
			posix2utc(df['time'].values),
			df['rates'].values,
			color=color[pos], lw=1, label=label[pos] )

	except:
		print('[Info] nan in df[{}]'.format(pos))

axtop.axhline(1.6, lw=1.5, ls='--', color='k')


dfT_ 	= fn_over_df(dfT, fn=np.mean, m=m)
muT1 	= '$\mu=${:.2f}'.format(dfT['T1'].mean())
sigT1 = '$\sigma=${:.2f}'.format(dfT['T1'].std())
axbot.plot(
	posix2utc(dfT_['time'].values),
	dfT_['T1'].values,
	color=color['T1'], lw=1 )
	# label=label['T1'] + ' (%s, %s)'%(muT1, sigT1))


dfmu_ = fn_over_df(dfmu, fn=np.mean, m=m)
mumu  ='$\mu=${:.2f}'.format(dfmu['rates'].mean())
sigmu = '$\sigma=${:.2f}'.format(dfmu['rates'].std())
axbottw.plot(
	posix2utc(dfmu_['time'].values),
	dfmu_['rates'].values,
	color=color['muons'], lw=0.5 )
	# label=label['muons'] + ' (%s, %s)'%(mumu, sigmu))

axbot.set_zorder(axbottw.get_zorder()+1)
axbot.patch.set_visible(False) 


lgdtop = axtop.legend(
	*order_legend('in/open', 'in/closed', 'out/open', 'out/closed', ax=axtop),
	bbox_to_anchor=(0., 1.012, 1., 0.102), loc='lower left',
	ncol=4, mode="expand", borderaxespad=0
	)
for line in lgdtop.get_lines():
	line.set_linewidth(2)
plt.figtext(0.14, 0.49, 'internal-background level', fontstyle='italic')


# lgdbot = axbot.legend(
# 	*group_legend(axbot, axbottw),
# 	loc='lower left')
# for line in lgdbot.get_lines():
# 	line.set_linewidth(2)



axtop.set_ylabel('raw trigger rates [Hz]')
axtop.grid(which='major', ls=':')

axbot.set_ylabel('temp. $[^\circ C]$', color=color['T1'])
axbot.tick_params(axis='y', colors=color['T1'])
axbottw.set_ylabel('muon rates [Hz]', color=color['muons'])
axbottw.tick_params(axis='y', colors=color['muons'])
axbottw.set_yticks([19,20,21])


autofmt_xdate(daily=3, ax=axbot)
for xl in axbot.get_xticklabels():
	xl.set_rotation(20)


# fig.savefig('./3/raw_trigger_rates_2weeks.svg', format='svg')
# fig.savefig('./plt/6/v35_raw_triggers_sensors.svg', format='svg')
# fig.savefig('./plt/6/v35_raw_triggers_sensors.png', format='png', dpi=600)

show()