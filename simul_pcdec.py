#!usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from datetime import timedelta
from mod.funk_plt import show
from mod.funk_manip import redo_configs
from mod.funk_stats import fourier
from mod.funk_run import Run
from mod.funk_utils import posix2utc, fn_over

from IPython import embed

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def get_data(run, pos, begin=None, dt=None):
	""" want to check the decay when the shutter is closed
			rework configs to open/closed only at 'in' position
			--> get closed-closed in one df, sampled every 2mins
	"""

	configs = redo_configs(run.configs, mode=1)
	
	df = configs.get_group(fcn.positions[pos])[['time', 'dt', 'counts', 'rates']]

	df = df.assign(
					timedelta = posix2utc(df['time']) - \
											posix2utc(df['time'].iloc[0])
					)

	if (begin is not None) and (dt is not None):
		return df[ (df['timedelta']>= begin)& (df['timedelta'] <= begin+dt) ]

	elif begin is not None:
		return df[ (df['timedelta']>= begin) ]

	elif dt is not None:
		return df[ (df['timedelta'] <= dt) ]

	else:
		return df


def get_Bint(begin=None, dt=None, **kwargs):
	""" kw options are spu, trig and cuts """

	run = Run.configs(run_v='v31', **kwargs)
	df  = run.get_df('in_closed')[['time', 'rates']]

	df = df.assign(
					timedelta = posix2utc(df['time']) - \
											posix2utc(df['time'].iloc[0])
					)

	if (begin is not None) and (dt is not None):
		return df[ (df['timedelta']>= begin)& (df['timedelta'] <= begin+dt) ]

	elif begin is not None:
		return df[ (df['timedelta']>= begin) ]

	elif dt is not None:
		return df[ (df['timedelta'] <= dt) ]

	else:
		return df


def shifted_y(x, dx=2/60, tau=10/60):
	""" sample y at later time dx """

	dx_base = 2/60
	y = np.random.random() * np.exp(-(x-dx)/tau)
	y[ :int(dx/dx_base) ] = 0

	return y


def simulate(Dx=24, tau=10/60):
	""" unit in hours, default: sample every 2mins """

	dx 	 = 2/60
	x  	 = np.arange(0, Dx, dx)
	ys 	 = np.array([ shifted_y(x, i*dx, tau) for i in range(len(x)) ])
	ytot = np.sum(ys, axis=0)

	return x, ys, ytot


def plot(x, y, ax=None, *args, **kwargs):

	if ax is None:
		ax 	= plt.gca()

	idx = (y!=0)
	ax.plot(x[idx], y[idx], *args, **kwargs)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# fig, ax = plt.subplots(2, 1, figsize=(10,6))

# x, ys, ytot = simulate()

# for y in ys:
# 	plot(x, y, lw=0.4, ax=ax[0])
# 	# plot(x, y, '.', ms=1)

# plot(x, ytot, color='k', lw=.7, ax=ax[0])
# ax[0].set_xlabel('time [hours]')


# Fs 	 = 1 / (x[1]-x[0]) # unit in cycles per hours
# fft  = fourier(ytot, Fs).iloc[1:] # remove dc

# ax[1].plot(fft['freq'], fft['fval'], lw=1)
# ax[1].set_xlabel('frequencies [cycles per hours]')

# fig.tight_layout()
# # fig.savefig('./4/1.pdf', format='pdf')

# show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# fig, ax = plt.subplots(figsize=(10,6))

# for tau, label in zip([1/60, 10/60, 30/60, 1], ['1/60', '10/60', '30/60', '1']):

# 	x, ys, ytot = simulate(tau=tau)
# 	plot(x, ytot, lw=.7, label='tau = %s h'%label)

# ax.set_xlabel('time [hours]')
# lgd = ax.legend()
# for line in lgd.get_lines():
# 	line.set_linewidth(3)

# fig.tight_layout()
# # fig.savefig('./4/2.pdf', format='pdf')
# show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# def stats(x, y, m=15):
# 	""" default: m=15, average over half-hour (sample every 2min) """ 

# 	x_ = fn_over(x, fn=np.mean, m=m) 

# 	y_mean = fn_over(y, fn=np.mean, m=m)
# 	y_var  = fn_over(y, fn=lambda z: np.var(z, ddof=1), m=m)

# 	return x_, y_var/y_mean


# fig, ax = plt.subplots(2, 1, figsize=(10,6))


# x, ys, ytot = simulate(Dx=7*24, tau=15/60)

# # cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
# cuts = fcn.cuts.select('dtlim')
# run  = Run.configs('v32', trig=True, cuts=cuts)
# df0  = get_data(run, 'in_closed',
# 								begin=timedelta(hours=1),
# 								dt=timedelta(weeks=1.5))
# df0  = df0.iloc[:len(x)]

# # run  = Run.configs('v34', trig=True, cuts=cuts)
# # df0  = get_data(run, 'in_open',
# # 								begin=timedelta(hours=1),
# # 								dt=timedelta(weeks=1.5))
# # df0  = df0.iloc[:len(x)]


# ax[0].plot(x, ytot*df0['dt'].values, color='k', lw=.5)
# ax[0].plot(x, df0['counts'].values, color='r', lw=.5)
# ax[0].set_ylim(ymax=600)

# plot(*stats(x, ytot*df0['dt'].values), color='k', lw=.8, ax=ax[1])
# plot(*stats(x, df0['counts'].values), color='r', lw=.8, ax=ax[1])
# ax[1].axhline(1, color='k', lw=1, ls='--')
# ax[1].set_ylim(ymin=-1, ymax=50)


# fig.tight_layout()
# # fig.savefig('./4/3.pdf', format='pdf')
# show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# fig, ax = plt.subplots(figsize=(10,6))

# Dx = 7*24
# dx = 2/60
# x  = np.arange(0, Dx, dx)

# # cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
# cuts = fcn.cuts.select('dtlim')
# trig = True
# run  = Run.configs('v32', trig=trig, cuts=cuts)
# df0  = get_data(run, 'in_closed',
# 								begin=timedelta(hours=1),
# 								dt=timedelta(weeks=1.5))
# df0  = df0.iloc[:len(x)]

# ax.hist(
# 	df0['counts'].values, bins=100, range=(100, 500),
# 	color='k', histtype='step', lw=2, label='data' )


# for tau, label, color in \
# 		zip([10/60, 15/60, 20/60],
# 				['10/60', '15/60', '20/60'],
# 				['b', 'r', 'g']):

# 	*_, ytot = simulate(Dx=Dx, tau=tau)
	
# 	ax.hist(
# 		ytot*df0['dt'].values, bins=100, range=(100, 500),
# 		histtype='step', color=color, label='MC [tau={}]'.format(label) )

# ax.legend()
# fig.tight_layout()
# # fig.savefig('./4/4.pdf', format='pdf')

# show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

from scipy.signal import correlate
from array import array

# *_, ytot = simulate(Dx=Dx, tau=tau)


fig, ax = plt.subplots(figsize=(10,6))

Dx = 7*24
dx = 2/60
x  = np.arange(0, Dx, dx)

# cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
cuts = fcn.cuts.select('dtlim')
trig = True
run  = Run.configs('v32', trig=trig, cuts=cuts)
df0  = get_data(run, 'in_closed',
								begin=timedelta(hours=1),
								dt=timedelta(weeks=1.5))
df0  = df0.iloc[:len(x)]

counts = df0['counts'].values

autocorr = correlate(counts, counts)

# corr = array('d', [])
# mu 	 = np.mean(counts)
# sig  = np.std(counts, ddof=1)

ax.plot(autocorr)

fig.tight_layout()
# fig.savefig('./4/5.pdf', format='pdf')
embed()
show()

