# implicit relative import doesn't work anymore in python 3
# alternative: use explicit relative import inside module
# but then can't run that as a script --> use python -m parent.module
# other alterative: use setup.py --> bundle as package

import os, sys; sys.path.append(os.path.dirname(os.path.realpath(__file__)))
# import inspect; print(inspect.getsourcefile(lambda _: None)) # lambda:0