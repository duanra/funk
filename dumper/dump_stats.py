#!usr/bin/env python3
import __pardir__
import os
import sys
import argparse
import numpy as np
import mod.funk_manip as fma
import mod.funk_utils as fus
import mod.funk_consts as fcn
from tabulate import tabulate
from tempfile import TemporaryFile
Namespace = fus.Namespace

################################################################################

def dump_ss(runs='v06', diff=False, trig=False, spu=False, cuts=Namespace(),
					  outfile='./txt/ss_out.txt', buff=True):
	
	infiles = fma.parse_runs(runs, on_error=True)
	keys 		= fma.parse_diff(4) if diff else fma.parse_which('all')

	if outfile is None or not(outfile):
		f = TemporaryFile('wt+')
	else:
		print('writing into', outfile)
		f = open(outfile, 'wt+')

	for infile  in infiles:
		print('infile:', infile)
		run_v 	 	= os.path.basename(infile)[:3]
		configs 	= fma.load_configs(infile, trig=trig, spu=spu, cuts=cuts)
		tbounds 	= fma.get_tbounds(configs)
		period 		= fus.posix2utc(tbounds)
		duration	= period[1] - period[0]

		if diff:
			dic = fma.diff_rates(configs, keys)
		else:
			dic = fma.rates(configs, keys)

		label, mean, std, stderr = ['rates'], ['mean'], ['std'], ['stderr']
		for key in keys:
			df = fma.stats_summary(dic[key])
			label.append(fcn.diffs[key] if diff else key)
			mean.append(np.round(df.loc['rates', 'mean'], decimals=5))
			std.append(np.round(df.loc['rates', 'std'], decimals=5))
			stderr.append(np.round(df.loc['rates', 'stderr'], decimals=5))

		# printing stats
		print(('='*103 if diff else '='*57), file=f)
		print('run_v   : {}\n'\
					'period  : {} to {} (UTC)\n'\
					'duration: {}'\
					.format(run_v, *period, duration), file=f)
		print('no.cycle:', max([len(dic[k]) for k in dic.keys()]), '\n', file=f)
		print(tabulate([mean, std, stderr], headers=label), file=f)

	print(('='*103 if diff else '='*57), file=f)

	if buff:
		f.seek(0)
		print(f.read())

	f.close()


parser = argparse.ArgumentParser()
parser.add_argument('--diff', action='count',
										help='do differences instead of total rates')
parser.add_argument('--trig', action='count',
										help='count trigger rate (instead of pulse rate)')
parser.add_argument('--spu', action='count',
										help='cut on traces which contain more than one pulse')
parser.add_argument('--hcut', action='count', help='cut on h')
parser.add_argument('--qcut', action='count', help='cut on q')
parser.add_argument('--Scut', action='count', help='cut on S')
parser.add_argument('--tsigcut', action='count', help='cut on tsig')
parser.add_argument('--risecut', action='count', help='cut on rise')
parser.add_argument('--outf', help='path of the output file', default=None)
parser.add_argument('runs', nargs='+', help='run(s)')

args = parser.parse_args()
cuts = fcn.cuts.select('dtlim')
for x in sys.argv:
	if x in ['--hcut', '--qcut', '--Scut', '--tsigcut', '--risecut']:
		dic = {x[2:-3]+'lim': getattr(fcn.cuts, x[2:-3]+'lim')}
		cuts.update(**dic)

# if bool(args.risecut): cuts.update(riselim=fcn.cuts.riselim)
# if bool(args.Scut): cuts.update(Slim=fcn.cuts.Slim)
# if bool(args.tsigcut): cuts.update(tsiglim=fcn.cuts.tsiglim)
# if bool(args.qcut): cuts.update(qlim=fcn.cuts.qlim)
# if bool(args.hcut): cuts.update(hlim=fcn.cuts.hlim)

dump_ss(runs = args.runs,
			  diff = bool(args.diff),
			  trig = bool(args.trig),
			  spu  = bool(args.spu),
			  cuts = cuts,
			  outfile = args.outf,
			  buff = True)