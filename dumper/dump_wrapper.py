
#!usr/bin/env python3
"""
wrapper for dump_funk_multi_run_pandas 
save the output of the root files into npz format
and optionally into plain ascii text

e.g command:
python dump_wrapper.py -h
python dump_wrapper.py v06
python dump_wrapper.py v09
python dump_wrapper.py --pulse_par='-4,3.5,1.5,7' v18 v20

"""

import __pardir__
import os
import argparse
import numpy as np
import subprocess as sub
import warnings

import mod.funk_io as fio 
import mod.funk_consts as fcn

from time import time
from tempfile import TemporaryFile

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# prog = os.environ['HOME'] +\
# 			 "/Dropbox/funk/r761/anal/dump_funk_multi_run_pandas" # office


def savez(indir, ascii=False, buff=False):
	""" ascii and buff are so far only implemented for normal runs
			see funk_io.npz_to_ascii()
	"""

	start = time()

	try:
		if not (os.path.exists(indir) and len(fio.find_all_files(indir))):
			raise IOError('IOError', 'savez: the directory ' + indir +\
															 ' does not exist or is empty.')
		
		if not os.path.exists(outpath):
			os.makedirs(outpath)

		fname = os.path.join(
							outpath,
							os.path.basename(indir) + '_(%s).npz'%args.pulse_par )
		if os.path.isfile(fname):
			raise Exception('Exception', 'savez: existing file ' + fname)


	except IOError as io:
		# do not sys.exit(1) because may loop on a set of directories
		print('[{}] {}'.format(*io.args))
	
	
	except Exception as ex:
		print('[{}] {}'.format(*ex.args))
	
		if ascii:
			fio.npz_to_ascii(
						fname, outf='auto', buff=buff,
						rows=0, which=0, label='pmt' )
			print('exit success - time elapsed: {:.2} seconds'\
						.format(time() - start) )
	

	else:
		print('compressing', fname)
		rootfiles = fio.find_all_files(indir)
		rootfiles.sort()
		
		if not flasher:

			ls = []	
			for rootf in rootfiles:
				# fast but memory consuming
				with TemporaryFile() as out:

					out.write(
						sub.Popen([prog, "-P", args.pulse_par, "-i", rootf], stdout=sub.PIPE)\
							 .stdout.read()
						)
					out.seek(0)

					temp_arr = np.genfromtxt(
												out, skip_header=1, delimiter=None,
												dtype=dtype, usecols=range(len(dtype)),
												invalid_raise=False, encoding=None) # need encoding (py3)
					"""	other alternatives but
							- slow:
								temp_ls = [row.strip().split(' ') for row in out][1:]
							- memory consuming:
									temp_arr = np.rec.array(
											[row for row in temp_ls if len(row)==22],
											dtype=dtype_pmt )
							- not efficient, if test inside loop:
									temp_arr = np.array(
											[row for row in temp_ls if len(row)==22])\
											.astype(np.float)
					"""

					if len(temp_arr)!=0: # some root files has no output....
						ls.append(temp_arr)	
					# sub.call(["echo", out.read()])
		
			outf = open(fname, 'wb')
			np.savez_compressed(outf, *ls)
			outf.close()
		
			if ascii:
				npz_to_ascii(
					fname, outf='auto', buff=buff,
					rows=0, which=0, label='pmt')
			

		else:
	
			dic = {}
			for rootf in rootfiles:
				with TemporaryFile() as out:
					
					out.write(
						sub.Popen([prog, "-P", args.pulse_par, "-i", rootf], stdout=sub.PIPE)\
							 .stdout.read()
						)
					out.seek(0)

					temp_arr = np.genfromtxt(
												out, skip_header=1, delimiter=None,
												dtype=dtype, usecols=range(len(dtype)),
											 	invalid_raise=False, encoding=None)
					
					# if len(temp_arr)!=0: # some root files has no output.... --> dump all
					# dic[rootf.split('_')[-2][-4:].replace('-','')] = temp_arr
					dic[os.path.basename(rootf).split('.')[0]] = np.array(temp_arr, ndmin=1)
		
			outf = open(fname, 'wb')
			np.savez_compressed(outf, **dic)
			outf.close()


		end = time()
		print('exit success - time elapsed: {:.2} seconds'.format(end - start), '\n')



parser = argparse.ArgumentParser()
parser.add_argument(
	'-f', '--flasher',
	action="count",
	help="use flasher dumper otherwise normal dumper" )
# parser.add_argument(
# 	"--inpath",
# 	help="path /to/the/directory/ containing the 'data' folder",
# 	default=fcn.prefix+'pmt_run/')
parser.add_argument(
	'--pulse_par',
	help='pulse finder paramters',
	type=str, default=fcn.pulse_par[1:-1] )
parser.add_argument(
	'data', nargs='+',
	help='basename of the directories containing the root files data ' +\
			 '(prefix is added by default)' )

args = parser.parse_args()


flasher = bool(args.flasher)
if flasher:
	prefix = os.path.join(fcn.prefix, 'flasher_run/')
	dtype  = fcn.dtype_flasher
	prog 	 = os.path.join(
						os.environ['HOME'],
						'FUNK/r1701/spe/anal/dump_flasher_run_pandas' # default
					 )

else:
	prefix = os.path.join(fcn.prefix, 'pmt_run/')
	dtype  = fcn.dtype_pmt
	prog 	 = os.path.join(
						os.environ['HOME'],
						'FUNK/r1701/funk/anal/dump_funk_multi_run_pandas' # default
					 )

indirs 	= [ os.path.join(prefix, d) for d in args.data ]
outpath	= os.path.join(
						prefix, '-'.join(args.pulse_par.split(',')[1:]) )


with warnings.catch_warnings():
	""" generated by genfromtxt(invalid_raise=False)
			for missing data in columns but allows to skip the latter
	"""
	warnings.filterwarnings(
		action="ignore", category=UserWarning,
		message="Some errors were detected")
	
	for indir in indirs:
		savez(indir, ascii=False, buff=False)