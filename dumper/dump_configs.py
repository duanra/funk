#!usr/bin/env python3
""" dump return of fma.load_counts() """

import __pardir__
import os
import sys
import argparse
import mod.funk_manip as fma
import mod.funk_consts as fcn
from mod.funk_utils import Namespace, pickle_dump,\
														iterover

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def dump(runs, cols='', trig=False, spu=False, cuts=Namespace()):

	outp = fcn.prefix_configs +\
				 '-'.join(fcn.pulse_par[1:-1].split(',')[1:]) + '/'
	if not(os.path.exists(outp)):
		os.makedirs(outp)

	infiles = fma.parse_runs(runs, on_error=True)

	for infile in infiles:
		run_v = os.path.basename(infile)[:3]
		cpars = '_'.join((x[0][:-3]+'cut' for x in list(cuts.items())[1:] if x[1]))
		fname = run_v + '_configs' +\
						('_trig' if trig else '') +\
						('_spu' if spu else '') +\
						('_%s'%cpars if cpars else '')
		fconfigs = os.path.join(outp, fname)
		outfX = []

		if cols:
			_cols  = cols.split(',')

			for var in iterover(_cols):
				fname = run_v + '_%s'%var +\
								('_trig' if trig else '') +\
								('_spu' if spu else '') +\
								('_%s'%cpars if cpars else '')
				outf 	= os.path.join(outp, fname)

				if os.path.exists(outf):
					print('[Exception] the file {} already exists'.format(outf))	
					continue

				else:
					outfX.append(outf)

		if os.path.exists(fconfigs):
			print('[Exception] the file {} already exists'.format(fconfigs))
			fconfigs = None
			if not(len(outfX)): continue

		print('infile:', infile)
		configs = fma.load_counts(infile, trig=trig, spu=spu, cuts=cuts)

		if fconfigs is not None:
			pickle_dump(configs, *os.path.split(fconfigs)[::-1])

		if len(outfX):
			for outf in outfX:
				outp, fname = os.path.split(outf)
				var = fname.split('_')[1]
				df 	= fma.reset_configs(configs)
				df_var = df[['time', 'in', 'open', var]].groupby(['in', 'open'])
				pickle_dump(df_var, fname, outp)

		print()


parser = argparse.ArgumentParser()
parser.add_argument('--trig', action='count',
										help='count trigger rate (instead of pulse rate)')
parser.add_argument('--spu', action='count',
										help='cut on traces which contain more than one pulse')
parser.add_argument('--hcut', action='count', help='cut on h')
parser.add_argument('--qcut', action='count', help='cut on q')
parser.add_argument('--Scut', action='count', help='cut on S')
parser.add_argument('--tsigcut', action='count', help='cut on tsig')
parser.add_argument('--risecut', action='count', help='cut on rise')
parser.add_argument('--cols', default='', type=str, 
										help='vars to dump: str comma separated')
parser.add_argument('runs', nargs='+', help='run(s)')

args = parser.parse_args()
cuts = fcn.cuts.select('dtlim')
for x in sys.argv:
	if x in ['--hcut', '--qcut', '--Scut', '--tsigcut', '--risecut']:
		dic = {x[2:-3]+'lim': getattr(fcn.cuts, x[2:-3]+'lim')}
		cuts.update(**dic)

dump(runs = args.runs,
		 cols = args.cols,
		 trig = bool(args.trig),
		 spu  = bool(args.spu),
		 cuts = cuts)