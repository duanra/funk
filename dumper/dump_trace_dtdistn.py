import os
import sys
import pandas as pd


outpath = os.path.join(os.path.dirname(os.path.realpath(__file__)),
											 os.pardir + os.sep + 'data')
infiles = sys.argv[1:]

for infile in infiles:

	with open(infile, 'rt') as f:
		print('reading', infile)
		# outtxt = os.path.join(outpath, 'trace_ttag_' + \
		# 											os.path.basename(infile) \
		# 														 .split('_')[-1][:-4] + '.txt')

		outpkl = os.path.join(outpath, 'trace_ttag_' + \
													os.path.basename(infile) \
																 .split('_')[-1][:-4] + '.sdf')

		# with open(outtxt, 'wt+') as out:
		# 	print('writing', outtxt)
		s  = ''
		ls = []

		for line in f:
			
			if line[0]=='#':
				# save
				if len(s):
					s = s.strip()
					# out.write(s + '\n')
					templs = s.split(' ')[1:]
					ls.extend(map(lambda x: (templs[0], float(x)), templs[1:]))

				# init string
				s = 'trace ' + line.split(' ')[1] + ' '
			
			if line[:4]=='INFO': # add trace info
				s += line.split(' ')[1] + ' '


		df = pd.DataFrame(ls, columns=['time', 'ttag'])
		df = df.set_index('time')
		df.index = df.index.map(int)

		print('saving', outpkl)
		df.to_pickle(outpkl)

print('::end')