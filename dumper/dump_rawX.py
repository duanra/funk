#!usr/bin/env python3
""" dump raw_var (configs-like) using return of fma.load_raw() """
import __pardir__
import os
import sys
import argparse
import mod.funk_manip as fma
import mod.funk_consts as fcn
from mod.funk_utils import Namespace, pickle_dump,\
														iterover

################################################################################

def dump(runs, cols='q', cuts=Namespace()):
	_cols = cols.split(',')
	try:
		assert _cols[0] != ''

	except:
		print('[ArgError] specify --cols')
		parser.print_help()

	else:
		outp = fcn.prefix_configs +\
				 	 '-'.join(fcn.pulse_par[1:-1].split(',')[1:]) + '/'
		if not(os.path.exists(outp)):
			os.makedirs(outp)

		infiles = fma.parse_runs(runs, on_error=True)

		for infile in infiles:
			run_v = os.path.basename(infile)[:3]
			cpars = '_'.join((x[0][:-3]+'cut' for x in list(cuts.items())[1:] if x[1]))
			outfs = []

			for var in iterover(_cols):
				fname = run_v + '_raw%s'%var +\
								('_%s'%cpars if cpars else '')
				outf 	= os.path.join(outp, fname)

				if os.path.exists(outf):
					print('[Exception] the file {} already exists'.format(outf))	
					continue

				else:
					outfs.append(outf)

			if len(outfs):
				print('infile:', infile)
				raw_configs = fma.load_raw(infile, cols=_cols, cuts=cuts)

				for outf in outfs:
					outp, fname = os.path.split(outf)
					var = fname.split('_')[1][3:]
					df 	= fma.reset_configs(raw_configs)
					raw = df[['time', 'in', 'open', 'capture', var]].groupby(['in', 'open'])
					pickle_dump(raw, fname, outp)

				print()


parser = argparse.ArgumentParser()
parser.add_argument('--hcut', action='count', help='cut on h')
parser.add_argument('--qcut', action='count', help='cut on q')
parser.add_argument('--Scut', action='count', help='cut on S')
parser.add_argument('--tsigcut', action='count', help='cut on tsig')
parser.add_argument('--risecut', action='count', help='cut on rise')
parser.add_argument('--cols', default='', type=str, 
										help='vars to dump: str comma separated')
parser.add_argument('runs', nargs='+', help='run(s)')

args = parser.parse_args()
cuts = fcn.cuts.select('dtlim')
for x in sys.argv:
	if x in ['--hcut', '--qcut', '--Scut', '--tsigcut', '--risecut']:
		dic = {x[2:-3]+'lim': getattr(fcn.cuts, x[2:-3]+'lim')}
		cuts.update(**dic)

dump(runs = args.runs,
		 cols = args.cols,
		 cuts = cuts)
