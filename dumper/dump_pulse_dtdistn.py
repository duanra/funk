import os
import sys
import pandas as pd
from numpy import sqrt


outpath 	= os.path.join(os.path.dirname(os.path.realpath(__file__)),
												 os.pardir + os.sep + 'data')
infiles 	= sys.argv[1:]

ix_select = [	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,  1, # Pulse.h
							1, 0, 0, 0, 1, 0, 0, 1, 1 ] # CalibPulse.h
n 				= len([x for x in ix_select if x])


for infile in infiles:
	with open(infile, 'rt') as f:
		print('reading', infile)
		# outtxt = os.path.join(outpath, 'pulse_ttag_' +\
		# 											os.path.basename(infile)\
		# 														 .split('_')[-1][:-4] + '.txt')
		outpkl = os.path.join(outpath, 'pulse_ttag_' +\
														os.path.basename(infile)\
																	 .split('_')[-1][:-4] + '.sdf')

		# with open(outtxt, 'wt+') as out:
		# 	print('writing into', outtxt)
		s = ''
		ls = []

		for line in f:

			if line[0]=='#':
				# save
				if len(s):
					s = s.strip()
					# out.write(s + '\n')
					templs = s.split(' ')[1:]
					for i in range(0, len(templs[1:]), n):
						ls.extend([(templs[0], *templs[1+i:n+1+i])])
						# ls.extend(map(lambda x: (templs[0], float(x)), templs[1:]))

				# init string
				s = 'pulse ' + line.split(' ')[1] + ' '

			if line[:5]=='pulse': 
				s += ' '.join( 
					[ val for ix, val in zip(ix_select, line.strip().split(None)[1:]) if ix ])
				s += ' '


		df = pd.DataFrame(ls, columns=['time', 'tvar', 'S', 'H', 'q', 'rise', 'ttag'])
		df = df.apply(pd.to_numeric)
		df = df.set_index('time')
		df.index = df.index.map(int)
		df.loc[:, 'tvar'] = sqrt(df['tvar'])
		df.rename(columns={'tvar': 'tsig'}, inplace=True)

		print('saving', outpkl)
		df.to_pickle(outpkl)

print('::end')