""" funk event only """
#!usr/bin/env python3

import __pardir__
import os
import sys
import argparse
import numpy as np
import subprocess as sub 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from functools import wraps
from mod.funk_io import find_all_files
from mod.funk_plt import PlotIndexer
from mod.funk_run import Capture
from mod.funk_utils import positive_index

plt.rcParams['savefig.directory'] = './'
plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (165/72, 145/72)

plt.rcParams['savefig.format'] = 'png'
plt.rcParams['savefig.dpi'] 	 = 300

# plt.rcParams['font.size'] = 10
# plt.rcParams['figure.figsize'] = (6,4)
# plt.rcParams['savefig.format'] = 'pdf'
# plt.rcParams.update(**fcn.rcPaper)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# these are globals
SAVE 			 = [ None for k in range(15) ] # save some captures for plot purpose
CI 	 			 = 0 # loop indexer on SAVE
NEXT_TRACE = None # needed because of the readline process


class iPlot(PlotIndexer):

	def __init__(self, X, i, reader, setup_plot):

		super(iPlot, self).__init__(X, i, loop=False, direc=0)
		setup_plot()

		capture = self.fX[self.fi]
		capture.print_()
		capture.plot()

		self.icapt_save = capture.icapt
		self.fi_stop 		= None
		self.fplot			= True
		self.fpause  		= 1e-3

		self.reader 		= reader
		self.setup_plot = setup_plot


	def _next_event(self):

			capture = self.reader(proc) # read next event and update globals
		
			if capture is False:
				print('::EOF')
				sys.exit(0)

			elif capture is None: # end of 60 sec
				return self._next_event()

			else:
				self.fX  = SAVE
				self.fi  = CI
				
				return capture


	# do not bind static otherwise not callable in body
	def _override(method):
		""" add/overload some functionality before calling action()
				note: pause the loop before enabling or disabling plotting
				otherwise it changes the value of self.fkey and affects action()
		"""

		@wraps(method)
		def wrapper(self, event):

			if event.key=='escape' or event.key=='ctrl+left':
				return # no action
			
			else:
				if event.key == 'p':
					self.fplot = not(self.fplot)
					print('::plotting', 'enabled' if self.fplot else 'disabled')

				return method(self, event)

		return wrapper

	# class body including methods are executed/loaded once (like module)
	on_key = _override(PlotIndexer.on_key)


	def action(self):

		##### get captured event
		capture = self.fX[self.fi]
		if capture is None: # SAVE is initially filled with None
			capture = self._next_event()

		if (self.fkey == 'right' or self.fkey == 'ctrl+right') and \
			 (capture.icapt < self.icapt_save):

			capture = self._next_event()
			self.fi_save = None
			self.fi_stop = None

		elif (self.fkey == 'left') and \
				 (capture is None or capture.icapt > self.icapt_save):

			print('[Info] max saved captures')
			if self.fi_stop is None:
				self.fi_stop = positive_index(self.fi-1, len(SAVE))
			else:
				self.fi = positive_index(self.fi_stop+1, len(SAVE))

			return
		
		else:
			pass

		##### print info #######
		capture.print_()
		##### plotting  ########
		if self.fplot:
			self.setup_plot()
			capture.plot()

		##### disable if not ###
		if self.floop:
			self.run_check(capture)

		self.icapt_save = capture.icapt


	def run_check(self, capture):
		""" implement as needed """

		# if len(capture.pulses)>=4:
		# 	self.fkey = ' '
		# 	plt.pause(0) # the indexer would not work properly

		# fRiseTime = [ pulse[-1] for pulse in capture.pulses ]
		# if any([ rise<=0 for rise in fRiseTime ]):
		# 	self.fkey = ' '


		# fTruncStd = capture.baseline[10] 
		# if fTruncStd > 0.7:
		# 	self.fkey = ' '


		fTimeSig		 = [ 0.8 * np.sqrt(pulse[-10]) for pulse in capture.pulses ]
		fTimeEntropy = [ pulse[-9] for pulse in capture.pulses ]
		
		if any([ (14 <= tsig <= 16) and (3.5 <= S <= 3.7)
						 for tsig, S in zip(fTimeSig, fTimeEntropy) ]):
			self.fkey = ' '

		# if any([ (3.5 <= tsig <= 4.5) and (2.6 <= S <= 2.8)
		# 				 for tsig, S in zip(fTimeSig, fTimeEntropy) ]):
		# 	self.fkey = ' '


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def read_event_funk(proc):
	""" proc: piping on rootf """
	global CI, SAVE, NEXT_TRACE

	capture = Capture()

	if NEXT_TRACE is not None:
		capture.traces.append(NEXT_TRACE)
		NEXT_TRACE = None

	while True:

		line = proc.stdout.readline().rstrip().decode('utf-8')

		if line.startswith('#'):
			print('rootf: {}'.format(os.path.basename(rootf)))
			print('       {}'.format(line[2:]))


		elif 's:' in line: # sensors --> new 60 secs run
			print('event: {}'.format(line), '\n')


		elif len(line) > 1000: # trace

			if capture.baseline is not None:
				CI 			 	 = positive_index(capture.icapt, len(SAVE))
				SAVE[CI] 	 = capture

				try:
					trace 		 = np.fromstring(line, dtype=np.int16, sep=' ')
					NEXT_TRACE = trace
				except:
					pass # check if EOF??

				return capture

			trace 	= np.fromstring(line, dtype=np.int16, sep=' ')
			capture.traces.append(trace)


		elif line.startswith('  '):
			if line[:4] != '    ': # baseline
				baseline = np.fromstring(line, dtype=np.float, sep=' ')
				capture.baseline = baseline

			else: # pulse
				pulse = np.fromstring(line, dtype=np.float, sep=' ')
				capture.pulses.append(pulse)
				# embed()

		
		elif not(line): # end of 60 sec
			print('-'*80, '\n')
			NEXT_TRACE = None
			Capture.ncapt -= 1 # because constructor was already called
			
			return None

		else:
			# TODO: how to break at end of pipe stdout and continue to next rootf`
			return False

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

parser = argparse.ArgumentParser()

parser.add_argument(
	'-P', '--pulse_par',
	help='pulse finder parameters',
	type=str, default=fcn.pulse_par[1:-1]
	)

parser.add_argument(
	'rootfiles', nargs='+',
	help='/path/to/the/rootfile' )

args = parser.parse_args()
pulse_par = args.pulse_par
prog = os.path.join(
	os.environ['HOME'], 'FUNK/r1701/funk/anal/dump_funk_multi_run')


# this is a class member needed for plotting
# Capture  is instantiated in read_event()
Capture.pulse_par = np.fromstring(pulse_par, dtype=np.float, sep=',')


# overwrite class method, but it should never be done this way!
def plot_funk(self, fig=None, ax=None):

	if fig is None:
		fig = plt.gcf()
	if ax is None:
		ax = plt.gca()

	# PMT trace
	trace = Capture.dU * (self.traces[0] - Capture.aOffset)
	tbins = Capture.dt * np.arange(len(trace))
	ax.plot(
		tbins, trace, color='k', lw=1, label='trace',
		zorder=0	
	)

	# baseline and thresholds
	base_mu = Capture.dU * (self.baseline[9] - Capture.aOffset)
	ax.axhline(
		base_mu, color='r', lw=0.6, ls='--',
		label=r'$\mu_\mathrm{trunc}$'
	)

	base_sig	= Capture.dU * (self.baseline[10] - Capture.aOffset)

	siglow 	 	= Capture.pulse_par[2] # relative threshold in unit sigma
	threshlow = Capture.dU * siglow * self.baseline[10] 
	ax.axhline(
		base_mu - threshlow, color='c', lw=0.6, ls='--',
		label=r'$%s\,\sigma_\mathrm{trunc}$'%siglow
	)
	ax.axhline(base_mu + threshlow, color='c', lw=0.6, ls='--')

	sigup 		= Capture.pulse_par[1] # absolute threshold in unit adc
	threshup	= Capture.dU * sigup
	ax.axhline(
		base_mu - threshup, color='y', lw=0.6, ls='--',
		label= r'$%d\,\mathrm{ADC}$'%abs(sigup)
	)
	ax.axhline(base_mu + threshup, color='y', lw=0.6, ls='--')


	# pulses
	for pulse in self.pulses:

		begin = Capture.dt * pulse[0]
		end 	= begin + Capture.dt * pulse[1]
		tp 		= begin + Capture.dt * pulse[8] - 0.69

		ax.axvspan(begin, end, color='b', lw=0, alpha=0.1)
		ax.axvline(tp, color='b', lw=0.6, ls='-', label=r'$t_\mathrm{trigger}$')
	
		# # check_cuts:
		# riselim = Capture.cuts['riselim']
		# Slim		= Capture.cuts['Slim']
		# tsiglim = Capture.cuts['tsiglim']

		# fTimeSig 		 = Capture.dt * np.sqrt(pulse[11])
		# fTimeEntropy = pulse[12] 
		# fRiseTime 	 = pulse[20]
		# fy = base_mu + 0.5 * (abs(threshup) + abs(threshlow))

		# if (riselim[0] < fRiseTime ) and\
		# 	 (tsiglim[0] <= fTimeSig <= tsiglim[1]) and\
		# 	 (Slim[0] <= fTimeEntropy <= Slim[1]):

		# 	ax.scatter(
		# 		tp+8, fy, s=180, lw=0,
		# 		marker=r'$\checkmark$', color='deeppink'
		# 	)

		# else:
		# 	ax.scatter(
		# 		tp+8, fy, s=60, lw=0,
		# 		marker='X', color='deeppink'
		# 	)

	fig.tight_layout(pad=0.05)

Capture.plot = plot_funk


def setup_plot_funk():

	ax = plt.gca()
	ax.clear()
	ax.set_xlabel('time/ns')
	ax.set_ylabel('output voltage/mV')

	ax.set_xticks([800, 1000])#, 1200])
	ax.set_xlim(700, 1100)#1400)
	ax.set_ylim(-275, 75)
	# ax.tick_params(labelleft=False)


plt.figure()
plt.ion()
plt.show()

# https://stackoverflow.com/questions/2804543/\
# read-subprocess-stdout-line-by-line
for rootf in args.rootfiles:

	with sub.Popen(
				[prog, '-pt', '-P', pulse_par, rootf], stdout=sub.PIPE)\
		as proc:

		try:
			print('+'*len(prog))
			print(prog, '\n', 'pulse_par = (%s)'%pulse_par)
			print('+'*len(prog))

			read_event_funk(proc) # read first event and update globals

		except:
			print('[Exception] in event reader')
			sys.exit(0)

		else:
			# TODO: implement check for empty rootf
			plot = iPlot(
							SAVE, CI,
							reader=read_event_funk,
							setup_plot=setup_plot_funk
						 )
			plt.gcf().canvas.mpl_connect('key_press_event', plot.on_key)
			input()