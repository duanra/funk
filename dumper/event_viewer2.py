""" flasher event only """
#!usr/bin/env python3

import __pardir__
import os
import sys
import argparse
import numpy as np
import subprocess as sub 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from functools import wraps
from mod.funk_io import find_all_files
from mod.funk_plt import PlotIndexer, order_legend
from mod.funk_run import Capture
from mod.funk_utils import positive_index

plt.rcParams['savefig.directory'] = './'
plt.rcParams.update(fcn.rcThesis)
# plt.rcParams['figure.figsize'] = (275/72, 180/72)
# plt.rcParams['figure.figsize'] = (300/72, 120/72)
plt.rcParams['figure.figsize'] = (165/72, 145/72)

plt.rcParams['savefig.format'] = 'png'
plt.rcParams['savefig.dpi'] 	 = 300

# plt.rcParams['font.size'] = 10
# plt.rcParams['figure.figsize'] = (6,4)
# plt.rcParams['savefig.format'] = 'pdf'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# these are globals
SAVE 			 = [ None for k in range(15) ] # save some captures for plot purpose
CI 	 			 = 0 # loop indexer on SAVE
NEXT_TRACE = None # needed because of the readline process


class iPlot(PlotIndexer):

	def __init__(self, X, i, reader, setup_plot):

		super(iPlot, self).__init__(X, i, loop=False, direc=0)
		setup_plot()

		capture = self.fX[self.fi]
		capture.print_()
		capture.plot()

		self.icapt_save = capture.icapt
		self.fi_stop 		= None
		self.fplot			= True
		self.fpause  		= 1e-3

		self.reader 		= reader
		self.setup_plot = setup_plot


	def _next_event(self):

			capture = self.reader(proc) # read next event and update globals
		
			if capture is False:
				print('::EOF')
				sys.exit(0)

			elif capture is None: # end of 60 sec
				return self._next_event()

			else:
				self.fX  = SAVE
				self.fi  = CI
				
				return capture


	# do not bind static otherwise not callable in body
	def _override(method):
		""" add/overload some functionality before calling action()
				note: pause the loop before enabling or disabling plotting
				otherwise it changes the value of self.fkey and affects action()
		"""

		@wraps(method)
		def wrapper(self, event):

			if event.key=='escape' or event.key=='ctrl+left':
				return # no action
			
			else:
				if event.key == 'p':
					self.fplot = not(self.fplot)
					print('::plotting', 'enabled' if self.fplot else 'disabled')

				return method(self, event)

		return wrapper

	# class body including methods are executed/loaded once (like module)
	on_key = _override(PlotIndexer.on_key)


	def action(self):

		##### get captured event
		capture = self.fX[self.fi]
		if capture is None: # SAVE is initially filled with None
			capture = self._next_event()

		if (self.fkey == 'right' or self.fkey == 'ctrl+right') and \
			 (capture.icapt < self.icapt_save):

			capture = self._next_event()
			self.fi_save = None
			self.fi_stop = None

		elif (self.fkey == 'left') and \
				 (capture is None or capture.icapt > self.icapt_save):

			print('[Info] max saved captures')
			if self.fi_stop is None:
				self.fi_stop = positive_index(self.fi-1, len(SAVE))
			else:
				self.fi = positive_index(self.fi_stop+1, len(SAVE))

			return
		
		else:
			pass

		##### print info #######
		capture.print_()
		##### plotting  ########
		if self.fplot:
			self.setup_plot()
			capture.plot()

		##### disable if not ###
		if self.floop:
			self.run_check(capture)

		self.icapt_save = capture.icapt


	def run_check(self, capture):
		""" implement as needed """

		# if len(capture.pulses)>=2:
		# 	self.fkey = ' '
		# 	plt.pause(0) # the indexer would not work properly

		# fRiseTime = [ pulse[-1] for pulse in capture.pulses ]
		# if any([ rise<=0 for rise in fRiseTime ]):
		# 	self.fkey = ' '


		# fTruncStd = capture.baseline[10] 
		# if fTruncStd > 0.7:
		# 	self.fkey = ' '


		fTimeSig		 = [ 0.8 * np.sqrt(pulse[-10]) for pulse in capture.pulses ]
		fTimeEntropy = [ pulse[-9] for pulse in capture.pulses ]
		fLgCharge 	 = [ pulse[-4] for pulse in capture.pulses ]
		
		if any([ 8.4<=q<=8.6 for q in fLgCharge ]):
			self.fkey = ' '

		# if any([ 3.5<=S<=3.8 for S in fTimeEntropy ]):
		# 	self.fkey = ' '

		# if any([ (14 <= tsig <= 16) and (3.5 <= S <= 3.7)
		# 				 for tsig, S in zip(fTimeSig, fTimeEntropy) ]):
		# 	self.fkey = ' '

		# if any([ (3.5 <= tsig <= 4.5) and (2.6 <= S <= 2.8)
		# 				 for tsig, S in zip(fTimeSig, fTimeEntropy) ]):
		# 	self.fkey = ' '


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def read_event_flasher(proc):
	""" proc: piping on rootf """
	global CI, SAVE, NEXT_TRACE

	capture = Capture()

	if NEXT_TRACE is not None:
		capture.traces.append(NEXT_TRACE)
		NEXT_TRACE = None

	while True:

		line = proc.stdout.readline().rstrip().decode('utf-8')

		if line.startswith('#'):
			print('rootf: {}'.format(os.path.basename(rootf)))
			print('       {}'.format(line[2:]))


		elif (len(line) > 1000) and (line[0] != ' '): # trace

			if capture.baseline is not None:
				CI 			 	 = positive_index(capture.icapt, len(SAVE))
				SAVE[CI] 	 = capture

				try:
					trace 		 = np.fromstring(line, dtype=np.int16, sep=' ')
					NEXT_TRACE = trace
				except:
					pass # check if EOF??

				return capture

			trace 	= np.fromstring(line, dtype=np.int16, sep=' ')
			capture.traces.append(trace)


		elif (len(line) > 1000) and line.startswith('  '):

			trace = np.fromstring(line, dtype=np.int16, sep=' ')
			capture.traces.append(trace)


		elif line.startswith('    '):
			if line[:6] != '      ': # baseline
				baseline = np.fromstring(line, dtype=np.float, sep=' ')
				capture.baseline = baseline

			else: # pulse
				pulse = np.fromstring(line, dtype=np.float, sep=' ')
				capture.pulses.append(pulse)

		
		elif not(line): # EOF
			print('-'*80, '\n')
			NEXT_TRACE = None
			Capture.ncapt -= 1 # because constructor was already called

			return False

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

parser = argparse.ArgumentParser()

parser.add_argument(
	'-P', '--pulse_par',
	help='pulse finder parameters',
	type=str, default=fcn.pulse_par[1:-1]
	)

parser.add_argument(
	'rootfiles', nargs='+',
	help='/path/to/the/rootfile'
	)

args = parser.parse_args()
pulse_par = args.pulse_par
prog = os.path.join(
	os.environ['HOME'], 'FUNK/r1701/spe/anal/dump_flasher_run')


# this is a class member needed for plotting
# Capture  is instantiated in read_event()
Capture.pulse_par = np.fromstring(pulse_par, dtype=np.float, sep=',')


# overwrite class method, but it should never be done this way!
def plot_flasher(self, fig=None, ax=None):

	if fig is None:
		fig = plt.gcf()
	if ax is None:
		ax = plt.gca()


	# PMT trace
	trace = Capture.dU * (self.traces[0] - Capture.aOffset)
	tbins = Capture.dt * np.arange(len(trace))
	ax.plot(
		tbins, trace, color='k', lw=1, label='trace',
		zorder=0	
	)

	# # TTL trigger
	# trace = Capture.dU * self.traces[1] / 10
	# tbins = Capture.dt * np.arange(len(trace))
	# ax.plot(
	# 	tbins, trace, color='grey', lw=1, label='TTL trigger',
	# 	zorder=0
	# )

	# baseline and thresholds
	base_mu = Capture.dU * (self.baseline[9] - Capture.aOffset)
	# ax.axhline(
	# 	base_mu, color='r', lw=0.6, ls='--',
	# 	label=r'$\mu_\mathrm{trunc}$'
	# )

	base_sig	= Capture.dU * (self.baseline[10] - Capture.aOffset)

	siglow 	 	= Capture.pulse_par[2] # relative threshold in unit sigma
	threshlow = Capture.dU * siglow * self.baseline[10] 
	# ax.axhline(
	# 	base_mu - threshlow, color='c', lw=0.6, ls='--',
	# 	label=r'$%s\,\sigma_\mathrm{trunc}$'%siglow
	# )
	# ax.axhline(base_mu + threshlow, color='c', lw=0.6, ls='--')

	sigup 		= Capture.pulse_par[1] # absolute threshold in unit adc
	threshup	= Capture.dU * sigup
	# ax.axhline(
	# 	base_mu - threshup, color='y', lw=0.6, ls='--',
	# 	label= r'$%d\,\mathrm{ADC}$'%abs(sigup)
	# )
	# ax.axhline(base_mu + threshup, color='y', lw=0.6, ls='--')


	# pulses
	for pulse in self.pulses:

		begin = Capture.dt * pulse[0]
		end 	= begin + Capture.dt * pulse[1]
		tp 		= begin + Capture.dt * pulse[8] - 0.69
		# tmean = begin + Capture.dt * pulse[10]

		# ax.axvspan(begin, end, color='b', lw=0, alpha=0.1)
		# ax.axvline(tp, color='b', lw=0.6, ls='-', label=r'$t_\mathrm{trigger}$')
		# ax.axvline(tmean, color='lime', lw=0.6, ls='-', label=r'$t_\mathrm{mean}$')

	# try:
	# 	lgd = ax.legend(
	# 					*order_legend(r'$t_\mathrm{trigger}$', r'$t_\mathrm{mean}$'),
	# 					loc='lower center', handlelength=1.2
	# 				)
	# 	for line in lgd.get_lines():
	# 		line.set_linewidth(1.5)
	
	# except:
	# 	pass
	
	# lgd = ax.legend(
	# 				*order_legend('trace', 'trigger', 'mu', 'sigma', 'ADC'),
	# 				bbox_to_anchor=(1.02, 0.25), loc='lower left',
	# 				borderaxespad=0, handlelength=1.2, frameon=False
	# 			)
	# for line in lgd.get_lines():
	# 	line.set_linewidth(1.5)

	fig.tight_layout(pad=0.05)
	# fig.canvas.draw()

Capture.plot = plot_flasher


def setup_plot_flasher():

	ax = plt.gca()
	ax.clear()
	# ax.set_xlabel('time/ns')
	# ax.set_ylabel('output voltage/mV')

	# ax.set_xlim(650, 800)
	# ax.set_ylim(-225, 75)

	# ax.set_xlim(200, 1600)
	# ax.set_ylim(-175, 125)
	# ax.set_ylim(-525, 125)
	# ax.set_yticks([-400, -200, 0])

	# ax.set_xlim(600, 1400)
	# ax.set_xlim(650, 850)
	# ax.set_ylim(-125, 25)
	# ax.tick_params(labelleft=False)

	ax.set_xlim(650, 800)
	ax.set_ylim(-375, 75)
	plt.axis('off')
	# ax.tick_params(labelleft=False, labelbottom=False)

plt.figure()
plt.ion()
plt.show()

for rootf in args.rootfiles:

	with sub.Popen(
				[prog, '-pt', '-P', pulse_par, rootf], stdout=sub.PIPE
		)	as proc:

		try:
			print('+'*len(prog))
			print(prog, '\n', 'pulse_par = (%s)'%pulse_par)
			print('+'*len(prog))
			read_event_flasher(proc) # read first event and update globals

		except:
			print('[Exception] in event reader')
			sys.exit(0)

		else:
			# TODO: implement check for empty rootf
			plot = iPlot(
							SAVE, CI,
							reader=read_event_flasher,
							setup_plot=setup_plot_flasher
						 )
			plt.gcf().canvas.mpl_connect('key_press_event', plot.on_key)
			input()