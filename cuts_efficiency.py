
#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from mod.funk_plt import show, add_toplegend
from mod.funk_manip import apply_cuts
from mod.funk_run import Run

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (165/72, 157/72)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

cuts = fcn.cuts.select('dtlim' , 'riselim', 'Slim', 'tsiglim')
trig = False

run  = Run.raw_configs(
				run_v='v35',
				cols=['S', 'h', 'q', 'tvar', 'rise']
			 )
df  = run.get_df('out_open').copy()
df2 = apply_cuts(df, cuts=cuts)

def get_total_counts(df):
	""" tot_trigs, tot_pulses """
	return len(df.groupby(['time', 'capture']).head(1)), len(df)

tot_trigs, tot_pulses = get_total_counts(df)
print(
	'before cuts: ntriggers={:,}, npulses={:,}'.format(tot_trigs, tot_pulses)
)

tot_trigs2, tot_pulses2 = get_total_counts(df2)
print(
	'after cuts : ntriggers={:,}, npulses={:,}'.format(tot_trigs2, tot_pulses2)
)
print(
	'overall efficiency: triggers={:.2%}, pulses={:.2%}'.format(
		tot_trigs2/tot_trigs, tot_pulses2/tot_pulses)
)


def get_captures(df):

	captures = df.groupby(['time', 'capture'])\
					 		 .size().reset_index(name='counts')

	return captures


def get_ntraces(captures, n=1):
	""" return number of traces having n pulses per trace
			note: divide by n since captures are repeated n times
	"""
	captures_ = captures[ captures['counts']==int(n) ]

	return captures_['counts'].sum() / n


def step(x, y, label=None, **kwargs):
	""" add tweaks to default mpl step """

	dx = x[1] - x[0]

	plt.step(x, y, where='mid', label=label, **kwargs)
	
	plt.hlines(y[0], x[0]-0.5*dx, x[0], **kwargs)
	plt.hlines(y[-1], x[-1]+0.5*dx, x[-1], **kwargs)

	plt.vlines(x[0]-0.5*dx, 0, y[0], **kwargs) 
	plt.vlines(x[-1]+0.5*dx, 0, y[-1], **kwargs)


# raw data
captures 	= get_captures(df)
nmax 			= captures['counts'].max()
xs 				= np.asarray(range(1, nmax+1))
ntraces 	= np.array([ get_ntraces(captures, n) for n in xs ])
ntot_traces = len(df.groupby(['time', 'capture']).head(1))

# selected data
captures2 = get_captures(df2)
nmax2 		= captures2['counts'].max()
xs2 			= np.asarray(range(1, nmax2+1) )
ntraces2  = np.array([ get_ntraces(captures2, n) for n in xs2 ])
ntot_traces2 = len(df2.groupby(['time', 'capture']).head(1))


step(
	xs[1:], ntraces[1:]/ntot_traces*100,
	label='all pulses', color='k', lw=1.2
)
step(
	xs2[1:], ntraces2[1:]/ntot_traces2*100,
	label='events', color='crimson', lw=1.2
)

plt.xlabel('multiple pulses per trace')
plt.xticks(range(2, max(nmax, nmax2)+1, 4))
plt.ylabel('amount of traces (\%)')
plt.yscale('log')
plt.grid(axis='x', lw=0.5, ls=':')
add_toplegend(labels=['all', 'events'], ncol=2)

show(pad=0.05)
# show(save='./plt/12/v35_npulsesPerTraces', pad=0.05)

embed()