#!usr/bin/env python3
import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from mod.funk_manip import load_configsX, get_df, apply_cuts
from mod.funk_plt import show, add_toplegend, order_legend
from mod.funk_stats import bin_centers, bin_widths, integer_binning
from mod.funk_utils import Namespace, posix2utc, fn_over
from mod.funk_ROOTy import TH1Wrapper, plot_tf1, get_histogram, get_rchi2

from ROOT import TF1

# plt.rcParams.update(fcn.rcPaper)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def make_plot(counts, hrange, label=None, color=None):

	nbins, xlow, xup = integer_binning(*hrange)
	th1 = TH1Wrapper(counts, nbins, xlow, xup, density=True)

	fnroot = '[0]*ROOT::Math::negative_binomial_pdf(x,[1],[2])'
	tf1_nbinom = TF1('tf1_nbinom', fnroot, *hrange)
	tf1_nbinom.SetParNames('N', 'p', 'n')
	tf1_nbinom.SetParameters(1, 0.1, 10)
	tf1_nbinom.SetParLimits(1, 0, 1)
	# tf1_nbinom.FixParameter(0, 1)

	th1.Fit(tf1_nbinom, 'RE0')
	print('rchi2 = %.3f'%get_rchi2(tf1_nbinom))

	hist = get_histogram(th1, return_yerr=True)

	plt.errorbar(
		bin_centers(hist[2]), hist[0], hist[1],
		fmt='s', ms=1, elinewidth=.5,
		color=color, alpha=0.8, label=label
	)
	xs = np.arange(*hrange)
	plot_tf1(tf1_nbinom, xs, ls='--', lw=1, color=color, zorder=0)


# old data set (PICO)
pulse_par = "(-4,3.5,1.5,7)"
cuts1   = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
trig 	 = False
key 	 = 'in_closed'

df1 = get_df(load_configsX(
				'v31', var='counts', trig=trig,
				cuts=cuts1, pulse_par=pulse_par
			), key)
counts1 = df1['counts'].values.astype(np.int)


# new data set (CAEN)
cuts2 = Namespace([
	('riselim', (0,)),
	('Slim', (2.08, 3.56)),
	('tsiglim', (3.34, 10.44))
	])

df2 = pd.read_pickle('./data/timetag/pulse_ttag_13-26.sdf')
counts2 = df2.groupby('time').size().values
time 	 = df2.index.unique().values
trunc  = counts2[:600].sum() # truncate 10 hours out
df2 = apply_cuts(df2.iloc[trunc:], cuts2)

counts2 = df2.groupby('time').size().values


# new data set (PICO)
df3 = get_df(load_configsX(
				'v36', var='counts', trig=trig,
				cuts=cuts1, pulse_par=pulse_par
			), key)
counts3 = df3['counts'].values.astype(np.int)



plt.figure('time', figsize=(5,3))

d1 = fn_over(counts1, fn=np.mean, m=10)/60
d2 = fn_over(counts2, fn=np.mean, m=10)/60
d3 = fn_over(counts3, fn=np.mean, m=10)/60
n1 = len(d1)
n2 = len(d2)
n3 = len(d3)

plt.plot(np.arange(0,n1), d1, lw=0.8, color='k', label='old (PICO)')
plt.plot(np.arange(n1,n1+n2), d2, lw=0.8, color='r', label='new (CAEN)')
plt.plot(np.arange(n1+n2,n1+n2+n3), d3, lw=0.8, color='b', label='new (PICO)')
plt.xlabel('dummy time')
plt.ylabel('SPE rates')
add_toplegend(ncol=3, lw=2)
plt.grid(which='major', ls=':', lw=0.5)
plt.tight_layout(pad=0)

plt.savefig('./plt/SPE_rates')


plt.figure('distribution', figsize=(4,2))

make_plot(counts1, hrange=(50,200), label='old (PICO)', color='k')
make_plot(counts2, hrange=(50,200), label='new (CAEN)', color='r')
make_plot(counts3, hrange=(50,200), label='new (PICO)', color='b')

plt.xlabel('counts / min')
plt.ylabel('frequency')
plt.legend()
plt.grid(which='major', ls=':', lw=0.5)

plt.tight_layout(pad=0)
plt.savefig('./plt/counts_distn')

show()
# embed()