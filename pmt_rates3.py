#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from IPython import embed
from datetime import timedelta
from matplotlib.gridspec import GridSpec

from mod.funk_manip import get_muons, reset_configs
from mod.funk_run import Run
from mod.funk_plt import autofmt_xdate, show, add_toplegend
from mod.funk_utils import posix2utc, fn_over

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (270/72, 420/72)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def get_data(run, pos, begin=None, dt=None):
	""" dt is timedelta object (duration to retrieve) """

	df = run.get_df(pos)[['time', 'rates']]
	df = df.assign(
					timedelta = posix2utc(df['time']) - \
											posix2utc(df['time'].iloc[0])
					)

	if (begin is not None) and (dt is not None):
		return df[ (df['timedelta']>= begin) &\
							 (df['timedelta'] <= begin+dt) ]

	elif begin is not None:
		return df[ (df['timedelta']>= begin) ]

	elif dt is not None:
		return df[ (df['timedelta'] <= dt) ]

	else:
		return df


cuts  = fcn.cuts.select('dtlim')
trig 	= True
run_v = 'v35'
run 	= Run.configs(run_v, trig=trig, cuts=cuts)


dfenv = reset_configs(run.configs)[['time', 'T1', 'T2', 'p']]
dfenv['T1'] = dfenv['T1'].interpolate()
dfenv['T2'] = dfenv['T2'].interpolate()
dfenv['p'] 	= dfenv['p'].interpolate()

dfmuons = get_muons(
						label='vcoinc', tperiod=run.period,
						timestamps=dfenv['time'].values
					)


fig = plt.figure()
gs 	= GridSpec(4, 1, hspace=0, height_ratios=[1.6, 1, 1, 1])

axpmt 	= fig.add_subplot(gs[0])
axtemp 	= fig.add_subplot(gs[1], sharex=axpmt)
axpress = fig.add_subplot(gs[2], sharex=axpmt)
axmuons = fig.add_subplot(gs[3], sharex=axpmt)

axpmt.tick_params(bottom=False)
axtemp.tick_params(bottom=False)
axpress.tick_params(bottom=False)

for ax in (axpmt, axtemp, axpress):
	ax.tick_params(labelbottom=False)
	# ax.tick_params(axis='x', direction='out')
del ax


m = 10

for pos, label, color in zip(
		['in_closed', 'out_closed', 'in_open', 'out_open'],
		['in/closed', 'out/closed', 'in/open', 'out/open'],
		['blue', 'grey', 'red', 'black']):

	try:
		df = get_data(run, pos, begin=None, dt=None)

		axpmt.plot(
			posix2utc(fn_over(df['time'].values, fn=np.mean, m=m)),
			fn_over(df['rates'].values, fn=np.mean, m=m),
			lw=0.5, color=color, label=label
		)

	except:
		print('[Info] nan in df[{}]'.format(pos))

del df

axpmt.axhline(y=1.55, lw=1.2, ls='--', color='darkslategrey')

axpmt.set_ylabel('$r$/Hz')
axpmt.set_yticks(range(0,12,2))
axpmt.set_ylim(-0.5, 10.5)
add_toplegend(
	labels=['in/open', 'in/closed', 'out/open', 'out/closed'],
	bbox_to_anchor=[0, 1.006, 1, 1], ncol=2,
	lw=2, handlelength=1.2, ax=axpmt
)

m *= 4

time = posix2utc(fn_over(dfenv['time'].values, fn=np.mean, m=m))
axtemp.plot(
	time, fn_over(dfenv['T1'].values, fn=np.mean, m=m),
	lw=1, color='teal', label='T1'
)
axtemp.plot(
	time, fn_over(dfenv['T2'].values, fn=np.mean, m=m),
	lw=1, color='olive', label='T2'
)
axtemp.set_ylabel('$T_{1,2}/^\circ$C')
axtemp.set_yticks([17.5, 18., 18.5, 19.])
axtemp.set_ylim(17.2, 19.3)
# axtemp.legend(loc='upper left', ncol=2)


axpress.plot(
	time, fn_over(dfenv['p'].values, fn=np.mean, m=m),
	lw=1, color='brown', label='p'
)
axpress.set_ylabel('$P/$hPa')
axpress.set_yticks([990, 1000, 1010, 1020])
axpress.set_ylim(985, 1025)
# axpress.legend(loc='upper left')


axmuons.plot(
	time, fn_over(dfmuons['rates'].values, fn=np.mean, m=m),
	lw=0.5, label='vert. coinc.'
)
axmuons.set_ylabel('$r_\mu^\mathrm{v}$/Hz')
axmuons.set_yticks([19.5, 20, 20.5, 21])
axmuons.set_ylim(19.2, 21.3)
# axmuons.legend(loc='upper left')


autofmt_xdate(daily=7, datefmt='%b-%d', ax=axmuons) # rotation=None, ha='center')
plt.figtext(0.925, 0.001, 2019)

plt.subplots_adjust(
	top=0.92, bottom=0.065,
	left=0.145, right=0.99
)

plt.savefig('./plt/12/v35_summaryplot')
plt.close()

# show()
# embed()