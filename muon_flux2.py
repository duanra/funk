#!usr/bin/env python3

import os
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from iminuit import Minuit
from mod.funk_plt import show, add_toplegend
from mod.funk_utils import handle_warnings

plt.rcParams.update(fcn.rcThesis)

pd.set_option('display.max_rows', 10)
plt.rcParams['figure.figsize'] = (280/72, 235/72)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

muon_mass	= 105.66e-3 # GeV
csv_props = dict(delim_whitespace=True, dtype=np.float)

cd = os.getcwd()
os.chdir('data/muons/')

muon_estimated2 = pd.read_csv('muon_estimated2.txt', **csv_props)

muon_alkofer = pd.read_csv('muon_alkofer1971b.txt', **csv_props)
muon_alkofer['E'] = np.sqrt(muon_alkofer['p']**2 + muon_mass**2)

muon_rastin  = pd.read_csv('muon_rastin1984a.txt', **csv_props)
muon_rastin['E'] = np.sqrt(muon_rastin['p']**2 + muon_mass**2) 

muon_ivanenko  = pd.read_csv('muon_ivanenko85.txt', **csv_props)

muon_caprice = pd.read_csv('muon_caprice97.txt', **csv_props)
muon_caprice['E']  = np.sqrt(muon_caprice['p']**2 + muon_mass**2) 
muon_caprice['jv'] = muon_caprice['jvminus'] + muon_caprice['jvplus']
muon_caprice['jv_err'] = np.sqrt( muon_caprice['jvminus_err']**2 +
																	muon_caprice['jvplus_err']**2 )

os.chdir(cd)


# def fit_spectrum(df):
# 	""" fitting function:  c*E**(b + a*log(E)) """

# 	lnE  = np.log(df['E'].values)
# 	lnjv = np.log(df['jv'].values)
	
# 	# @handle_warnings
# 	def fitfn(a, b, c):
# 		y = c + b*lnE + a*lnE**2
# 		return np.sum( (y - lnjv)**2 )

# 	m = Minuit(
# 				fitfn, a=-0.5, b=-0.5, c=-1,
# 				error_a=1e-2, error_b=1e-2, error_c=1e-2,
# 				limit_a=(None,0), limit_b=(None,0), limit_c=(None,0),
# 				errordef=1, print_level=1
# 			)
# 	m.migrad()

# 	a, b, c = list(m.args)
# 	c = np.exp(c)

# 	return c, b, a

# c, b, a = fit_spectrum(muon_estimated2)
# print(c, b, a)


a, b, c, d = np.polyfit(
							np.log(muon_estimated2['E'].values),
							np.log(muon_estimated2['jv'].values),
							deg=3
						)
d = np.exp(d)
print(d, c, b, a)


plt.figure()

n = 3
def plot_spectrum(df, *args, **kwargs):

	errorbar = kwargs.pop('errorbar', False)
	x = df['E'].values
	y = np.power(x, n) * df['jv'].values

	if errorbar:
		# ignore error in energy
		yerr = np.power(x, n) * df['jv_err'].values
		plt.errorbar(x, y, yerr, *args, **kwargs)

	else:
		plt.plot(x, y, *args, **kwargs)


# Es = np.logspace(0, 4)
# ys = d * np.power(Es, c + b*np.log(Es) + a*np.log(Es)**2)
# plt.plot(Es, np.power(Es, n) * ys, color='y', lw=1)

plot_spectrum(
	muon_estimated2, color='k', lw=2, ls='--',
	label='Analytical'
)

plot_spectrum(
	muon_alkofer, errorbar=True, label='Alkofer et al. (1971)',
	color='blue', fmt='o', ms=2, capsize=1.8, elinewidth=0.8
)

plot_spectrum(
	muon_rastin, errorbar=True, label='Rastin (1984)',
	color='grey', fmt='^', ms=2, capsize=1.8, elinewidth=0.8
)

plot_spectrum(
	muon_ivanenko, errorbar=True, label='Ivanenko et al. (1985)',
	color='red', fmt='v', ms=2, capsize=1.8, elinewidth=0.8
)

plot_spectrum(
	muon_caprice, errorbar=True, label='Kremer et al. (1997)',
	color='green', fmt='s', ms=2, capsize=1.8, elinewidth=0.8
)

plt.xlabel('$E_\mu / \mathrm{GeV}$')
plt.xscale('log')
plt.xlim(1, 1e4)
plt.ylabel(
	r'$E_\mu^3 \mathcal{D}_{\mu,v}$'
	r'$/ \mathrm{cm^{-2} s^{-1} sr^{-1} GeV^2}$'
)
plt.ylim(0.5e-3, 0.5)
plt.yscale('log')

# plt.grid(which='major', ls=':', lw=0.5)
lgd=plt.legend(bbox_to_anchor=[0.35,0.05], loc='lower left', frameon=False)

plt.tight_layout(pad=0)
plt.subplots_adjust(left=0.15)
plt.savefig('plt/12/muon_spectrum.pdf')

# show()

# embed()
