import __pardir__
import sys
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as st 
from ROOT import gROOT, TFile, TFormula, TF1, TH1D
from root_numpy import hist2array, array
from mod.funk_stats import bin_centers, bin_widths


def get_fitresults(tf1):
	""" tf1 is a THF1 root obj """
	nparams = tf1.GetNpar()
	ret = {
		'nparams'	: nparams,
		'ndf' 		: tf1.GetNDF(),
		'chi2' 		: tf1.GetChisquare(),
		'prob' 		: tf1.GetProb(),
		'nfreeparams' : tf1.GetNumberFreeParameters(),
		'nfitpoints' 	: tf1.GetNumberFitPoints (),
		'formula' : tf1.GetExpFormula()
		}
		
	parnames = []
	for i in range(nparams):
		parnames.append(tf1.GetParName(i))
		ret[tf1.GetParName(i)] = tf1.GetParameter(i)
		ret['error_'+tf1.GetParName(i)] = tf1.GetParError(i)
	ret['parnames'] = parnames

	return ret

rootf = TFile("fillrandom.root")
fitfn = gROOT.FindObject('sqroot')
hist = gROOT.FindObject('h1f')
hist.Fit(fitfn)

res = get_fitresults(fitfn)
for item in res.items():
	print(item)


hist.Draw('same')
input("press enter to continue..")
sys.exit(0)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# f = TFormula('f', 'abs(sin(x))/x')
# g = TF1('g', 'x*gaus(0) + [3]*f', 0, 10)
# g.SetParameters(10, 4, 1, 20)
# hist = TH1D('hist', 'histogram test', 200, 0, 10)
# hist.FillRandom('g', 10000)
# hist_axis = hist.GetXaxis()
# hist.Draw()
# input("press enter to continue..")
# sys.exit(0)
# yval, (xedg, ) = hist2array(hist, include_overflow=False, return_edges=True)
# plt.bar(bin_centers(xedg), yval, bin_widths(xedg))
# plt.show()
# plt.close()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# def f(x, N1, mu, sig, N2):
# 	return N1*x*np.sqrt(2*np.pi)*st.norm.pdf(x, mu, sig) + N2*np.abs(np.sin(x)/x)
# x = np.linspace(0.1, 10, 1000)
# plt.plot(x, f(x, 10, 4, 1, 20))
# plt.show()
# plt.close()