import numpy as np
from draft import count_down, squares
from utils import timing

# @timing
# def count_down(n):

# 	print('start: n={:,}'.format(n))
# 	while n>0:
# 		n -= 1
# 	print('end:   n=%d'%n)

# count_down(10**8)

# timing(cd)(10**12)

arr = np.arange(10**8)
def f(arr):
	return arr**2

timing(f)(arr)
timing(squares)(arr)
