#cython: language_level=3
#distutils: language=c++

from numpy import zeros_like


def count_down(long n):

	print('start: n={:,}'.format(n))
	while n>0:
		n -= 1
	print('end:   n=%d'%n)



def squares(arr):
	
	cdef int i, n
	cdef long[:] arr_view, ret_view

	n 			 = len(arr)
	ret 		 = zeros_like(arr)
	arr_view = arr
	ret_view = ret
	
	for i in range(n):
		ret_view[i] = arr_view[i]**2

	return ret