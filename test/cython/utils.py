from functools import wraps
from time import perf_counter

def timing(fn):
	""" decorator for simple timing of a function (include sleep time) """

	@wraps(fn)
	def wrapper(*args, **kwargs):
		start = perf_counter()
		ret 	= fn(*args, **kwargs)
		dt 		= perf_counter() - start

		if dt>120:
			factor, unit = 1/60, 'mins'
		elif 0.1<=dt<=120:
			factor, unit = 1, 's'
		else:
			factor, unit = 1e3, 'ms'

		print( 'Execution time of <{}> ~ {:.3f} {}'\
						.format(fn.__name__, dt*factor, unit) )

		return ret

	return wrapper