#!python
#cython: language_level=3
def fib(int n):
	"""Print the Fibonacci series up to n."""
	cdef int a, b
	a, b = 0, 1
	while b < n:
		print(b, end=' ')
		a, b = b, a + b

	print()
