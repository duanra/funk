#cython: language_level=3
#distutils: language=c++

from libcpp.vector cimport vector
from libc.math cimport exp
from scipy.stats import poisson
from numpy import array

cdef double _pgf_poisson(double s, double mu):
	return exp(mu*(s-1))

def two_poisson(double mu1, double mu2, double norm_tol=1e-3):
	pX_frozen = poisson(mu2)
	cdef vector[double] pX, p2
	cdef double s, pt
	cdef int n = 1
	cdef int i
	
	pX.push_back(pX_frozen.pmf(0))
	p2.push_back(_pgf_poisson(_pgf_poisson(0, mu2), mu1))

	while(True):
		s = 0
		pX.push_back(pX_frozen.pmf(n))
		for i in range(0, n):
			s += (n-i) * p2[i] * pX[n-i]
		p2.push_back(mu1 * s / n)

		if sum(p2) + norm_tol > 1. :
			break
		n += 1

	return array(p2)	

# cdef double _factorial(int n):
# 	if n ==0:
# 		return 1
# 	elif n == 1:
# 		return 1
# 	else:
# 		return n * _factorial(n-1)

# cdef double _poisson(int k, double lam):
# 	return max(exp(-lam) * lam**k / _factorial(k), 10**-10)