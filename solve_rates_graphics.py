#!usr/bin/env python3

import numpy as np
import numpy.linalg as nalg
import matplotlib.pyplot as plt 

from IPython import embed
from matplotlib import rc
from scipy.interpolate import interp1d
from tabulate import tabulate

rc('font', size=10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

Bint 			= 1.48101
Bint_err 	= 0.00086

# rates 		= np.array([2.56102, 1.53676, 2.54578, 1.53497])
# rates_err = np.array([0.00888, 0.00712, 0.00886, 0.00811])

rates 		= np.array([4.44536, 3.42116, 4.43008, 3.41918])
rates_err = np.array([0.01287, 0.01042, 0.01287, 0.01108])

print(tabulate([
		['rates'] + rates.tolist() + [ Bint ],	
		['stderr'] + rates_err.tolist() + [ Bint_err ]
		],
		headers=[
		'in_open', 'in_closed', 'out_open', 'out_closed', 'Bint_ref'
		],
		numalign='right'), '\n')

def matrix(x):

	ret = np.array([
					[1, 1, 1, 0],
					[0, 1, x, 0],
					[0, 1, 0, 1],
					[0, 1, 0, x]
					])

	return ret


# solve for different values of epsilon
eps_vals = np.linspace(.01, .99, num=10000)
results  = np.zeros((len(eps_vals), 4), dtype=float)

for i, eps in enumerate(eps_vals):

	m 		= matrix(eps)
	minv 	= nalg.inv(m)
	results[i:i+4,:] = np.dot(minv, rates)

S_vals, Bint_vals, Bair_vals, Bair2_vals = results.T


# find intersection of Bint_vals with Bint
ix    = np.flatnonzero( np.diff(np.sign(Bint_vals - Bint)) != 0 ).item()
S_sol, Bint_sol, Bair_sol, Bair2_sol = results[ix]
eps 	= eps_vals[ix]


# error propagation
rates_ 		 = rates - Bint
rates_err_ = np.sqrt(rates_err**2 + Bint_err**2)

eps_err 	 = eps * np.sqrt( (rates_err_[2] / rates_[2])**2 +
													 (rates_err_[3] / rates_[3])**2 )
Bair2_err  = rates_err_[2]
Bair_err 	 = Bair_sol * np.sqrt( (eps_err / eps)**2 +
																(rates_err_[1] / rates_[1])**2 )
S_err 		 = np.sqrt( rates_err_[0]**2 + Bair_err**2 )


print(tabulate([
		['rates'] + [S_sol, Bair_sol, Bair2_sol, eps],
		['stderr'] + [S_err, Bair_err, Bair2_err, eps_err]
		],
		headers=['', 'S', 'Bair', 'Bair2', 'eps'],
		numalign='right'), '\n')


plt.figure()

plt.plot(eps_vals, S_vals, color='red', label='Signal S')
plt.plot(eps_vals, Bint_vals, color='blue', label='Bg (internal)')
plt.plot(eps_vals, Bair_vals, color='black', label='Bg (air, in)')
plt.plot(eps_vals, Bair2_vals, color='grey', label='Bg (air, out)')

plt.axhline(0, lw=.8, ls=':', color='k')
plt.axhline(Bint, ls='--', lw=.8, color='g')
plt.axvline(eps, ls='--', lw=.8, color='g')

plt.gca().set(
	xlabel='shutter transparency ($\epsilon$)',
	ylabel='SPE count rates [Hz]')
plt.ylim(-5, 10)

lgd = plt.legend(loc=(0.1, 0.68))
for line in lgd.get_lines():
	line.set_linewidth(2)

# plt.savefig('./plt/7/3.png', format='png', dpi=600)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


plt.figure()

def get_signal(Bint, Bint_err):

	rates_ 		 = rates - Bint
	rates_err_ = np.sqrt(rates_err**2 + Bint_err**2)
	X 	 	= rates_[0] - (rates_[1]*rates_[2] / rates_[3])
	X_err = np.sqrt(
						rates_err_[0]**2 +
						(rates_[1]*rates_[2] / rates_[3])**2 * (
							(rates_err_[1] / rates_[1])**2 +
							(rates_err_[2] / rates_[2])**2 +
							(rates_err_[3] / rates_[3])**2
						))

	return X, X_err

n  = 58720
r0 = np.min(rates[[1,3]]).round(1)
Bs = np.arange(0.1, r0 + 0.1, 0.1)
Xs = np.array([ get_signal(B, np.sqrt(B/60/n)) for B in Bs ])

plt.errorbar(Bs, Xs[:,0], Xs[:,1], fmt='s', ms=3, color='darkblue')
xlim 	 = (0.05, r0 + 0.05)
xticks = Bs[::3].tolist()
xticks_sh = [ round(100*(1 - (rates[3]-B)/(rates[2]-B)), 1) for B in xticks ]

plt.grid(True, ls=':')
plt.xlim(xlim)
plt.xticks(xticks)
plt.xlabel('Bint [Hz]')
plt.ylabel('Signal [Hz]')
plt.twiny()
plt.xlim(xlim)
plt.xticks(xticks, xticks_sh)
plt.xlabel('shutter opacity [%]')

plt.savefig('./plt/7/5.png', format='png', dpi=600)

plt.show()
plt.close()
embed()