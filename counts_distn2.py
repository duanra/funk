#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from mod.funk_manip import reset_configs
from mod.funk_run import Run
from mod.funk_plt import show, order_legend

plt.rcParams.update(fcn.rcPaper)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
cuts = fcn.cuts.select('dtlim')
trig = True

run_v30 = Run.configs('v30', trig=trig, cuts=cuts)
run_v31 = Run.configs('v31', trig=trig, cuts=cuts)
run_v35 = Run.configs('v35', trig=trig, cuts=cuts)

df_v30 	= run_v30.get_df('in_open')
df_v31 	= run_v31.get_df('in_closed')
df_v35 	= reset_configs(run_v35.configs).iloc[0::2]


plt.hist(
	df_v31['counts'].values, 100, None, True,
	histtype='step', color='black', lw=1.2,
	label='$\mu={:.0f}, \,\,\,\, \sigma^2={:.0f}$'
		.format(df_v31['counts'].mean(), df_v31['counts'].var())
)


plt.hist(
	df_v30['counts'].values, 100, None, True,
	histtype='step', color='darkblue', lw=1.2,
	label='$\mu={:.0f}, \, \sigma^2={:.0f}$'
		.format(df_v30['counts'].mean(), df_v30['counts'].var())
)


plt.hist(
	df_v35['counts'].values, 100, None, True,
	histtype='step', color='darkred', lw=1.2,
	label='$\mu={:.0f}, \, \sigma^2={:.0f}$'
		.format(df_v35['counts'].mean(), df_v35['counts'].var())
)


plt.xlabel('count/min')
plt.xlim(right=650)
plt.legend(handlelength=1.8, handletextpad=0.6)


show(pad=0.05)
# show(save='./plt/8/raw_triggers_distns', pad=0.05)

embed()