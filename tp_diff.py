#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from matplotlib import rc
from functools import wraps

from mod.cymodule.tp_diff import get_mask
from mod.funk_manip import load_rawX
from mod.funk_plt import show, order_legend, add_toplegend
from mod.funk_stats import bin_centers, bin_widths
from mod.funk_utils import timing


plt.rcParams.update(fcn.rcDefense)
# plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (245/72, 172/72)
# plt.rcParams['figure.figsize'] = (165/72, 145/72)

# plt.rcParams.update(fcn.rcPaper)
# plt.rcParams.update(fcn.rcPoS)
pd.set_option('display.max_rows', 20)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

class SimpleStats:
	""" conveniently store histogram and do some basic counting """

	def __init__(self):		

		self.histdf  = {}


	def _check_key(method):

		@wraps(method)
		def wrapper(self, *args, **kwargs):
			key = args[0]

			if key in self.histdf.keys():
				return method(self, *args, **kwargs)
			else:
				print('no histogram stored with key=%s, use add_hist()'%key)

		return wrapper


	def add_hist(self, h, key):
		""" h: histogram tuple (h[0]: hist values, h[1]: bin edges) """

		hdf = pd.DataFrame({
						'bce'  : bin_centers(h[1]).round(1),
						'hist' : h[0]
					})

		self.histdf[key] = hdf


	@_check_key
	def get_hist(self, key, low=None, up=None):
		""" return histogram where low <= bce <= up """ 

		hdf = self.histdf[key]
		ix  = self.get_bces_ix(key, low, up)

		if ix is True:
			return hdf
		else:
			return hdf.loc[ix]


	@_check_key
	def get_bces_ix(self, key, low=None, up=None):
		"""" return indexes of bces in (low, up) """

		hdf = self.histdf[key]
		ix  = (hdf['bce']>=low if low is not None else True) &\
					(hdf['bce']<=up if up is not None else True)

		return ix


	def count_stats(self, key, low=None, up=None):

		return self.get_hist(key, low, up)['hist'].sum()


	def get_bmax(self, key, low=None, up=None):

		hdf = self.get_hist(key, low, up)

		return hdf['bce'].loc[ hdf['hist'].idxmax() ]


	def get_integral(self, key):

		hdf = self.get_hist(key)
		bwi = hdf['bce'].iat[1] - hdf['bce'].iat[0]

		return bwi * hdf['hist'].sum()


	def get_hist_ratio(self, key1, key2, low=None, up=None, complement=False):

		hdf1 = self.get_hist(key1, low, up)
		hdf2 = self.get_hist(key2, low, up)

		ret  = pd.DataFrame({
							'bce' 	: hdf1['bce'],
							'ratio' : hdf1['hist'] / hdf2['hist']
						})

		if complement:
			ret['ratio'] = 1 - ret['ratio']

		return ret


# @timing
def get_single_captures(df):

	captures 	= df.groupby(['time', 'capture'])\
								.size()\
								.reset_index(name='counts')

	multi_captures = captures[ captures['counts']==1 ]\
										 			 [ ['time', 'capture'] ].values

	search_in = df[['time', 'capture']].values

	dims = search_in.max(axis=0) + 1
	idx  = np.isin(
						np.ravel_multi_index(search_in.T, dims),
						np.ravel_multi_index(multi_captures.T, dims)
					).nonzero()[0]
	
	df_multi = df.iloc[idx]

	return df_multi


# @timing
def get_multi_captures(df):

	captures 	= df.groupby(['time', 'capture'])\
								.size()\
								.reset_index(name='counts')

	multi_captures = captures[ captures['counts']>1 ]\
										 			 [ ['time', 'capture'] ].values

	search_in = df[['time', 'capture']].values

	dims = search_in.max(axis=0) + 1
	idx  = np.isin(
						np.ravel_multi_index(search_in.T, dims),
						np.ravel_multi_index(multi_captures.T, dims)
					).nonzero()[0]
	
	df_multi = df.iloc[idx]

	return df_multi


def get_multi_splitting(df_multi):
	""" return splitting indexes for different captures ids """

	cid  = df_multi['capture'].values
	cidx = np.flatnonzero(cid[1:] - cid[:-1]) + 1

	return cidx


# @timing
def get_tp_diff(df_multi):
	""" run on df_multi to make it faster """

	cidx 		= get_multi_splitting(df_multi)
	tp_list = np.split(df_multi['tp'].values, cidx)

	tp_diff = np.concatenate([ np.diff(x) for x in tp_list ])

	# this doesn't work because if 2 'delayed' pulses come very close,
	# they will be seen as two reflections w.r.t the 0th pulse.
	# yet it's more likely an afterpulse or coincidental pulse
	# tp_diff = np.concatenate([ x[1:] - x[0] for x tp_list ])

	return tp_diff.round(1)


@timing
def correct_multi_captures(df_multi, hist_ratio):
	""" hist_ratio: probability of NOT being a reflection event """

	cidx 		= get_multi_splitting(df_multi)
	tp_list = np.split(df_multi['tp'].values, cidx)

	# convert to dict for cython perf
	ratio 	= dict(hist_ratio['ratio'])
	idxs 		= get_mask(tp_list, ratio)

	return df_multi.iloc[idxs]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# cuts = fcn.cuts.select('dtlim')
cuts 	= fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
run_v = 'v35'
raw_configsX = load_rawX(run_v, var='tp', cuts=cuts)


fig, ax = plt.subplots()

label = {
	'in_open'			: 'in/open',
	'in_closed'		: 'in/closed',
	'out_open'		: 'out/open',
	'out_closed'	: 'out/closed',
}

color = {
	'in_open'			: 'red',
	'in_closed'		: 'blue',
	'out_open'		: 'black',
	'out_closed'	: 'grey',
}


ss = SimpleStats()
single_captures = {}
multi_captures 	= {}

bwi 		= 0.8 # sampling time in nanosecond
bins 		= 120
hrange	= (-0.5*bwi, (bins-0.5)*bwi)
density = False


# for pos in ['in_open', 'out_open']:
for	pos in ['in_closed', 'out_closed', 'in_open', 'out_open']:

	df = raw_configsX.get_group(fcn.positions[pos])

	# note: in redo_configs() pd.concat with nan upcasts numeric
	# dtypes to float, but need integers in get_multi_captures
	# otherwise element-wise comparison would be extremently slow
	if run_v=='v34':
		df['time'] 		= df['time'].astype(np.uint)
		df['capture'] = df['capture'].astype(np.uint)
	
	df_single = get_single_captures(df)
	df_multi 	= get_multi_captures(df)
	tp_diff  	= get_tp_diff(df_multi)

	alpha = 1 # if pos=='in_open' else 0 # small fig (open + mc)

	*h, _ = ax.hist(
						tp_diff, bins, hrange, density, histtype='step',
						color=color[pos], lw=1.2, label=label[pos],
						alpha=alpha
				 	)

	ss.add_hist(h, pos)
	single_captures[pos] = df_single
	multi_captures[pos]  = df_multi

bedg = h[1].round(1)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

bmax1  		= ss.get_bmax('in_open', 20, 30)
bmax2  		= ss.get_bmax('in_open', 40, 50)
low1, up1 = bmax1 - 5.5*bwi, bmax1 + 5.5*bwi
low2, up2 = bmax2 - 5.5*bwi, bmax2 + 5.5*bwi


# n1_excess = ss.count_stats('in_open', low1, up1) -\
# 						ss.count_stats('out_open', low1, up1)
# n2_excess = ss.count_stats('in_open', low2, up2) -\
# 						ss.count_stats('out_open', low2, up2)


# print(
# 	'peak1 {:.1f} ns, ({:.1f}, {:.1f}) ns, {:.3%} excess'.format(
# 	bmax1, low1, up1, n1_excess/npulses['in_open']
# 	))
# print(
# 	'peak2 {:.1f} ns, ({:.1f}, {:.1f}) ns, {:.3%} excess'.format(
# 	bmax2, low2, up2, n2_excess/npulses['in_open']
# 	))
# print(
# 	'total excess: {:.3%}'.format(
# 	(n1_excess+n2_excess)/npulses['in_open']
# 	))


# shade first peak
hdfin  	= ss.get_hist('in_open', low1, up1)
hdfout 	= ss.get_hist('out_open', low1, up1)

ax.bar(
	hdfin['bce'],
	hdfin['hist'] - hdfout['hist'],
	width=bwi, bottom=hdfout['hist'],
	color=color['in_open'], alpha=0.1
)

# shade second peak
hdfin  	= ss.get_hist('in_open', low2, up2)
hdfout 	= ss.get_hist('out_open', low2, up2)

ax.bar(
	hdfin['bce'],
	hdfin['hist'] - hdfout['hist'],
	width=bwi, bottom=hdfout['hist'],
	color=color['in_open'], alpha=0.1
)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# MC correction

if False:

	def get_counts(df):

		gb = df.groupby('time')
		df_counts = gb.head(1)[['time', 'in', 'open']]
		df_counts = df_counts.assign(
									counts = gb.size().values
								)

		return df_counts


	df_counts_00 = get_counts(raw_configsX.get_group((0,0)))
	df_counts_01 = get_counts(raw_configsX.get_group((0,1)))
	df_counts_10 = get_counts(raw_configsX.get_group((1,0)))

	df_single_11 = single_captures['in_open']
	df_multi_11  = multi_captures['in_open']
	ntot = len(df_single_11) + len(df_multi_11)
	frac_corr = []

	# do not reset index --> used as bin number
	hist_ratio = pd.concat([
			ss.get_hist_ratio('out_open', 'in_open', low1, up1, False),
			ss.get_hist_ratio('out_open', 'in_open', low2, up2, False),
		])

	for i in range(1):

		df_multi_norefl_11 = correct_multi_captures(df_multi_11, hist_ratio)
		
		df_norefl_11 = pd.concat([df_single_11, df_multi_norefl_11])
		df_norefl_11 = df_norefl_11.sort_values('index')

		df_counts_11 = get_counts(df_norefl_11)
		df_counts_norefl = pd.concat([
												df_counts_11, df_counts_10,
												df_counts_01, df_counts_00
											 ]).sort_values('index')

		# df_counts_norefl.to_pickle(
		# 	'./data/MC_noreflections/counts_norefl%d.sdf'%i
		# )

		frac_corr.append( len(df_norefl_11) / ntot )

	print(frac_corr)
	# np.save(
	# 	'./data/MC_noreflections/%s_frac_corr'%run_v,
	# 	np.asarray(frac_corr)
	# )

	# only plot the last MC correction
	tp_diff_norefl  = get_tp_diff(df_multi_norefl_11)
	ax.hist(
		tp_diff_norefl, bins, hrange, density, histtype='step',
		color='springgreen', lw=0.7
	)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

ax.set_xlabel('$\Delta t_\mathrm{ev}$/ns')
ax.set_xlim(0,100)
ax.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
offset = ax.yaxis.get_offset_text()
offset.set_visible(False)

plt.figtext(0, 0.75, r'$\times10^{3}$') # big fig (all configs)
# plt.figtext(0, 0.93, r'$\times10^{3}$') # small fig (open + mc)

if True:
	add_toplegend(
		labels = ['in/open', 'in/closed', 'out/open', 'out/closed'],
		ncol= 2, handlelength=1.62
	)

fig.tight_layout(pad=0.05)
plt.subplots_adjust(left=0.1) # big fig (all configs)
# plt.subplots_adjust(left=0.14, top=0.96) # small fig (open + mc)

# fig.savefig('./plt/12/v35_tpDiffReflection')
# fig.savefig('./plt/12/v35_tpDiffNoReflection')
fig.savefig('../talks/src/phd_defense/figs/v35_tpDiffReflection')
plt.close()

# show()
# embed()


# get tighted bbox before applying, after all other artists are drawn
# import matplotlib.tight_layout as tl
# rect = tl.get_tight_layout_figure(
# 					fig, [ax], tl.get_subplotspec_list([ax]),
# 					tl.get_renderer(fig), pad=0
# 				)
# fig.subplots_adjust(**rect)