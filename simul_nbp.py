#!usr/bin/env python3

import numpy as np 
import matplotlib.pyplot as plt

from array import array
from scipy.stats import poisson, gamma, nbinom, uniform
from mod.funk_stats import bin_centers, integer_binning, normalization,\
													 negbinom, pareto2
from mod.funk_plt import pause, show
from mod.funk_utils import table_lr
from mod.funk_ROOTy import add_fitresults, get_fitresults,\
													 get_histogram, gen_from

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def umap_expon(rvs, lam=None):
	""" probabilility integral transform of the exponential (CDF)	"""
	if lam is None:
		lam = np.mean(rvs)
	return 1 - np.exp(-lam*rvs)


def simulate_pp(lam, dt, T):
	""" return interrarrival time of a poisson process with event rate lam,
			dt and T are the counting time interval and total duration, resp.
	"""
	mu  = lam*dt
	poi = poisson(mu)

	nevents = array('L', [])
	deltas 	= array('d', [])
	t   = 0

	while(t<T):
		N = poi.rvs()
		t_arrivals 			= np.sort(np.random.uniform(low=0, high=dt, size=N))
		t_interarrivals = np.diff(t_arrivals)
	
		nevents.append(N)
		deltas.extend(t_interarrivals)
		t += dt

	return nevents, np.array(deltas)


def simulate_nbp(mu, sig, dt, T):
	""" return interrarrival time of a negative binomial process where the 
			event rate is gamma distributed with parameters (mu, sig)
	"""
	def _reparametrize(mu, sig):
		""" reparametrization of the gamma distribution """
		theta = sig**2 / mu
		a 		= mu / theta
		loc 	= 0
		return a, loc, theta
	
	nevents = array('L', [])
	deltas 	= array('d', [])
	t  = 0

	while (t<T):
		lam = gamma.rvs(*_reparametrize(mu, sig)) # resample every dt
		nev, dts = simulate_pp(lam, dt, dt) # nev is sample from poisson
		nevents.append(nev[0])
		deltas.extend(dts)
		t += dt

		# # alternative: do not sample nev from poisson but use the expected value
		# lam = gamma.rvs(*_reparametrize(mu, sig)) # resample every dt
		# nev = int(lam*dt)
		# ts  = np.sort(np.random.uniform(low=0, high=dt, size=nev))
		# dts = np.diff(ts)
		# nevents.append(nev)
		# deltas.extend(dts)
		# t += dt

	return nevents, np.array(deltas)


def cut(arr, bounds):
	if bounds is None:
		return arr
	else:
		low, up = bounds
		return arr[(arr>=low) & (arr<=up)]


dt = 60 # seconds
T  = 7*24*60*60 # total duration

fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(10,4))
bins_nevs, *range_nevs = integer_binning(200, 500)
bins_dts, range_dts 	 = int(1e2), (1.6e-6, 2)
density = False


# POISSON
lam = 5 # mean event rate
nevents_pp, deltas_pp = simulate_pp(lam, dt, T)
hist_nevs_pp, edg, _ 	= ax0.hist(nevents_pp, bins_nevs, range_nevs,
															 	 histtype='step', alpha=.8,
															 	 label='pp', zorder=1)

# hist_dts_pp, edg, _ 	= ax1.hist(deltas_pp, bins_dts, range_dts, density,
# 																 alpha=.9, label='pp')
transformed_deltas_pp = umap_expon(deltas_pp, lam)
ax1.hist(transformed_deltas_pp, 100, None, True, histtype='step', label='pp')


# NEGBINOM
mu, sig = 5, 0.6 # for the event rate
nevents_nbp, deltas_nbp = simulate_nbp(mu, sig, dt, T)
hist_nevs_nbp, edg, _ 	= ax0.hist(nevents_nbp, bins_nevs, range_nevs,
																	 histtype='step', color='r',
																	 label='nbp', zorder=0)
n = (mu / sig)**2	# params of the corresponding NBP
p = (mu / (mu + sig**2 * dt))
x = bin_centers(edg)
N = normalization(hist_nevs_nbp, edg)
ax0.plot(x, N*nbinom.pmf(x, n, p), color='k')

# hist_dts_nbp, edg, _ 		= ax1.hist(deltas_nbp, bins_dts, range_dts, density,
# 				 											 		 histtype='step', color='r',
# 				 											 		 label='nbp', zorder=10)
# theta = sig**2 / mu
# x = bin_centers(edg)
# N = normalization(deltas_nbp, edg)
# ax1.plot(x, N*pareto2(x, n, theta), color='k')
transformed_deltas_nbp = umap_expon(deltas_nbp, lam)
ax1.hist(transformed_deltas_nbp, 100, None, True, histtype='step', label='nbp')


ax0.legend()
ax1.legend()
ax1.set_ylim(0.5,1.5)
# fig.savefig('./1/simulate_nbp.png', dpi=600)
show()


# print('pp:')
# print(table_lr(
# 	[
# 		'#events generated',
# 		'mean_dts',
# 		'min_dts',
# 		'max_dts',
# 		'hist_dts underflow',
# 		'hist_dts overflow',
# 		'hist_dts cutoff'
# 	],
# 	[
# 		sum(nevents_pp), # len(deltas_pp)+len(nevents_pp)
# 		sum(deltas_pp)/len(deltas_pp),
# 		min(deltas_pp),
# 		max(deltas_pp),
# 		len(deltas_pp[deltas_pp<range_dts[0]]),
# 		len(deltas_pp[deltas_pp>range_dts[1]]),
# 		fmt(1-sum(hist_dts_pp)/len(deltas_pp), '{:.5%}')
# 	],
# 	indent=True, hlines=True))

# print('nbp:')
# print(table_lr(
# 	[
# 		'#events generated',
# 		'mean_dts',
# 		'min_dts',
# 		'max_dts',
# 		'hist_dts underflow',
# 		'hist_dts overflow',
# 		'hist_dts cutoff'
# 	],
# 	[
# 		sum(nevents_nbp),
# 		sum(deltas_nbp)/len(deltas_nbp),
# 		min(deltas_nbp),
# 		max(deltas_nbp),
# 		len(deltas_nbp[deltas_nbp<range_dts[0]]),
# 		len(deltas_nbp[deltas_nbp>range_dts[1]]),
# 		fmt(1-sum(hist_dts_nbp)/len(deltas_nbp), '{:.5%}')
# 	],
# 	indent=True, hlines=True))


# def simulate_nbp2(mu, sig, dt, T):
# 	""" return interrarrival time of a negative binomial process where the 
# 			event rate is gamma distributed with parameters (mu, sig)
# 	"""
# 	# n  = (mu / sig)**2	# params of the corresponding NBP
# 	# p  = (mu / (mu + sig**2 * dt))
# 	# nb = nbinom(n, p) # freeze pmf

# 	n 		= (mu / sig)**2 # reparametrization of the gamma
# 	theta = mu / n
# 	def _Finv(x):
# 		return cdfinv_pareto2(x, n, theta)

# 	nevents = array('L', [])
# 	deltas 	= array('d', [])
# 	t = 0

# 	while(t<T):
# 		i, tt = 0, 0

# 		while(tt<dt):
# 			dtt = _Finv(np.random.random_sample())
# 			deltas.append(dtt)
# 			tt += dtt
# 			i  += 1 # ??? jump
# 			if tt>dt: break
# 		nevents.append(i)
# 		t += dt

# 	return nevents, np.array(deltas)
