""" check distribution of the mean """
import numpy as np
import matplotlib.pyplot as plt

from scipy.stats import poisson, gamma, nbinom, norm
from mod.funk_utils import fn_over, fmt, fmt_seq, table_lr
from mod.funk_plt import show
from mod.funk_stats import bin_centers, bin_widths, normalization,\
													 negbinom

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

n   = 10000 	# size of genevrated data points
lam = 10*60 	# event rate(per min)
dt 	= 0.5 		# min
m  	= 10			# averaging (m*dt min)

mu  = lam*dt
poi = poisson(mu=mu)
poi_rdv = poi.rvs(size=n)

sig = 4*np.sqrt(mu)
nb  = negbinom(mu=mu, sig=sig)
nb_rdv  = nb.rvs(size=n)

fig, ax = plt.subplots(1, 2, figsize=(10, 6))

# COUNTS
bins 	  = 40
brange  = None
density = True
ax[0].hist(poi_rdv, bins, brange, density,
					 histtype='step', label='poisson')
hist, edg, _= ax[0].hist(nb_rdv, bins, brange, density,
												 histtype='step', label='negbinom')
N = normalization(hist, edg)
x = np.arange(int(min(nb_rdv)), int(max(nb_rdv)))
n, p = mu**2/(sig**2-mu), mu/sig**2
ax[0].plot(x, N*nbinom.pmf(x, n, p))
ax[0].set_title('#events over {} min'.format(dt))
ax[0].set_ylabel('pdf')
ax[0].legend()

# COUNT RATES
poi_evrate = fn_over(poi_rdv, m=m, fn=np.sum)/ (m*dt)
nb_evrate  = fn_over(nb_rdv, m=m, fn=np.sum)/ (m*dt)
bins 	  = 100
brange  = (min(nb_evrate), max(nb_evrate))
density = True
ax[1].hist(poi_evrate, bins, brange, density,
					 histtype='step', label='poisson',)
hist, edg, _= ax[1].hist(nb_evrate, bins, brange, density,
												 histtype='step', label='negbinom')
N = normalization(hist, edg)
x = np.linspace(min(nb_evrate), max(nb_evrate))
muG		= np.mean(nb_evrate)
sigG  = np.std(nb_evrate, ddof=1)
theta = sigG**2 / muG
a 		= muG / theta
ax[1].plot(x, N*gamma.pdf(x, a, loc=0, scale=theta), label='gamma')
ax[1].set_title('event rate (binned average), per min')
ax[1].set_ylabel('pdf')
ax[1].legend()

# fig.savefig('./0/poisson_vs_negbinom.png', dpi=600)
# show()

print('poisson')
print(table_lr(
	[
		'total_count',
	 	'mu_count',
	 	'sig_count',
	 	'mu_evrate (unbinned)',
	 	'sig_evrate (unbinned)'
	],
	[
		sum(poi_rdv),
	 	np.mean(poi_rdv),
	 	np.std(poi_rdv, ddof=1),
	 	np.mean(poi_rdv/dt),
	 	np.std(poi_rdv/dt, ddof=1)
	],
	indent=True))

muX  = np.mean(nb_rdv)
sigX = np.std(nb_rdv, ddof=1)
muGTrue  = muX / dt
sigGTrue = np.sqrt(sigX**2 - muX) / dt

print('nb')
print(table_lr(
	[
		'total_count',
		'mu_count',
		'sig_count'
	],
	[
		sum(nb_rdv),
		muX,
		sigX
	],
	indent=True))

print('gamma (form nb params estimate)')
print(table_lr(
	[
		'mu_evrate',
		'sig_evrate'
	],
	[
	 	muGTrue,
	 	sigGTrue
	],
	indent=True))

print('gamma (from binned average)')
print(table_lr(
	[
		'mu_evrate',
		'sig_evrate'
	],
	[
	 	muG,
	 	sigG,
	],
	indent=True))

# do not try to recover mean/var of the gamma from the negbinom rdv
# remember, the two distns are already 'mixed', and gamma is marginalized
# the prob is for given (conditional) gamma, not given negbinom
# if the mean event rate is calculated from nb rdv --> binning effect on std
# could consider unbinned data (m=1 here) to get an estimate but that's wrong
xx = nbinom(n=(muGTrue/sigGTrue)**2,
						p=muGTrue/(muGTrue+sigGTrue**2*dt)\
					 ).rvs(10000)
print('nb counts from gammaTrue:', fmt_seq(np.mean(xx), np.std(xx, ddof=1)))
xx = nbinom(n=a, p=1/(1+theta*dt)).rvs(10000)
print('nb counts from gammaBined:', fmt_seq(np.mean(xx), np.std(xx, ddof=1)))

# # assume that the event rate (binned) are gamma distributed, then calculate the
# # parameters of the gamma distn. cross check with ll-fit
# print(a, theta)
# pars = gamma.fit(nb_evrate, loc=0)
# print(pars[0], pars[2])

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # comparison between various distributions
# from scipy.stats import poisson, norm
# from mod.funk_stats import negbinom, polya

# fig, ax = plt.subplots()
# x = np.arange(200)
# mu  = 50
# sig = 2*np.sqrt(mu)

# poi  = poisson(mu)
# gau1 = norm(mu, np.sqrt(mu))
# gau2 = norm(mu, sig)
# nb 	 = negbinom(mu, sig)
# pol  = lambda x: polya(x, mu, (mu/sig)**2)
# ax.plot(x, poi.pmf(x), label='poisson(mu)'.format(mu))
# # ax.plot(x, gau1.pdf(x), ls='--', label='gauss(mu, np.sqrt(mu))')
# ax.plot(x, nb.pmf(x), label='negbinom(mu, sig)', color='r')
# ax.plot(x, gau2.pdf(x), label='gauss(mu, sig)', color='grey')
# ax.plot(x, pol(x), ls='--', label='polya(mu, sig)', color='g')
# ax.legend()
# show()