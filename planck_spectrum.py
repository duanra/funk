#!usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import mod.funk_consts as fcn
import scipy.constants as const
import scipy.integrate as integrate
from scipy.interpolate import interp1d
from IPython import embed

from mod.funk_plt import add_toplegend, group_legend
from mod.funk_utils import handle_warnings

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (252/72, 198/72)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

c = const.c
h = const.h
k = const.k
eV = const.eV

@handle_warnings
def planck(lam, T):

	return 2*h*c**2/lam**5 * 1/(np.exp(h*c/k/(lam*T)) - 1)


def pmt_function(infile):

	data = np.loadtxt(infile)
	x, y = data.T
	x 	*= 1e-9

	# xnew 	= np.linspace(x[0], x[-1], 500)
	yfunc = interp1d(
						x, y, kind='cubic', bounds_error=False,
						fill_value=0
					)

	return yfunc

l1, l2 = 150e-9, 630e-9
Aeff 	= 2*np.pi*1
Emean = integrate.quad(lambda lam: h*c/lam, l1, l2)[0] / (l2 - l1)
qeff 	= pmt_function('exclusion_plot/pmt/pmt_9107B_extr')

photon_rate = Aeff/Emean * integrate.quad(
	lambda lam: qeff(lam) * planck(lam, T=298), l1, l2)[0]

print('photon energy = %.5e J = %.5f eV'%(Emean, Emean/eV))
print('photon rate = %.5e Hz/m2'%photon_rate)


lams = np.linspace(500e-9, 630e-9)
plt.plot(
	lams/1e-9, qeff(lams), color='greenyellow',
	label='$q_\mathrm{eff}$',
)
plt.xlabel('wavelength/nm')
plt.xticks(np.arange(500, 650, 25))
plt.ylabel(r'quantum efficiency (\%)')
plt.yscale('log')

plt.twinx()
plt.plot(
	lams/1e-9, planck(lams, 283)*1e-9/Emean,
	color='dimgrey', label='black-body')
plt.text(516, 6e-18, 'T=283\,K', rotation=40)
plt.plot(
	lams/1e-9, planck(lams, 293)*1e-9/Emean,
	ls='--', color='dimgrey')
plt.text(510, 1.5e-15, 'T=293\,K', rotation=40)

plt.ylabel(r'photon intensity/$\mathrm{m^{-2}sr^{-1}s^{-1}nm^{-1}}$')
plt.yscale('log')
plt.yticks(
	[ 10**x for x in range(-19, -9) ],
	[ '$10^{%d}$'%x if x%2==0 else str() for x in range(-19, -9) ]
)
plt.gca().yaxis.set_minor_locator(
	plt.LogLocator(subs='all', numticks=10))
plt.gca().yaxis.set_minor_formatter(
	plt.NullFormatter())


add_toplegend(
	labels=['q', 'black'],
	lgd=group_legend(*plt.gcf().get_axes()),
	ncol=2
)
plt.tight_layout(pad=0)
# plt.savefig('plt/12/planck_spectrum_overlay')

plt.show()

# embed()