#!usr/bin/env python3
""" combine inputs, share shutter efficiency (only) """

import numpy as np
import numpy.linalg as nalg
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from copy import deepcopy
from datetime import datetime, timedelta
from sys import exit
from mod.funk_plt import pause, show, order_legend, autofmt_xdate
from mod.funk_run import Run, CombinedRun
from mod.funk_utils import posix2utc, utc2posix, fmt, table_lr

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def get_Bint(**kwargs):
	""" kw options are spu, trig and cuts """

	run = Run.configs(run_v='v31', **kwargs)
	df  = run.get_df('in_closed')[['time', 'rates']]

	return len(df), df['rates'].mean(), df['rates'].std()


def get_matrices(crun, Bint, option='wmean'):

	seq = ['in_open', 'in_closed', 'out_open', 'out_closed']

	base_a = np.array([
							[1, 1, 0,  0],
							[0, 1, 0, -1],
							[0, 0, 1,  0],
							[0, 0, 1, -1]
							], dtype=np.float)

	base_b = np.array([1, 0, 1, 0], dtype=np.float)

	runs_v 		= deepcopy(crun.runs_v)
	ndata 		=	deepcopy(crun.ndata)
	meanrates = deepcopy(crun.meanrates)
	varmeans 	= deepcopy(crun.varmeans)
	nruns			= len(runs_v)
	weights 	= {}

	for pos in seq:
		meanrates[pos] -= Bint[1]
		varmeans[pos]  += Bint[2]**2 / Bint[0]
		weights[pos] 		= 1 / varmeans[pos]

	if option=='wmean':

		rates, varns = np.zeros(4), np.zeros(4)
		for i, pos in enumerate(seq):
			rates[i] 	 = np.sum( weights[pos] * meanrates[pos] ) \
											/ np.sum( weights[pos] )
			varns[i] 	 = np.sum( weights[pos]**2 * varmeans[pos] ) \
											/ np.sum( weights[pos] )**2
			# todo: how to correct the variance in case of only two runs available ?

		a, b 		 = base_a.copy(), base_b.copy()
		a[:,-1] *= rates
		b[:] 		*= rates
		
		var_a, var_b  = np.abs(base_a.copy()), base_b.copy()
		var_a[:,:-1] *= 0
		var_a[:,-1]	 *= varns
		var_b[:]		 *= varns

	elif option=='split':

		m, n 	= 4*nruns, 3*nruns+1

		def _base():
			""" build base matrix """
			a = np.zeros((m, n))
			for i, j in zip(range(0, m, 4), range(0, n, 3)):
				a[i:i+4, j:j+3] = base_a[:4, :3].copy()
			
			b = np.zeros(m)
			return a, b

		a, b = _base()
		var_a, var_b = np.zeros_like(a), np.zeros_like(b)

		# todo: should the variances be weighted here ?
		for i, k in zip(range(0,m,4), range(nruns)):
			a[i+1,-1] 		= -meanrates['in_closed'][k]
			a[i+3,-1] 		= -meanrates['out_closed'][k]

			b[i+0] 				= meanrates['in_open'][k]
			b[i+2] 				= meanrates['out_open'][k]

			var_a[i+1,-1] = varmeans['in_closed'][k]
			var_a[i+3,-1] = varmeans['out_closed'][k]
			
			var_b[i+0] 		= varmeans['in_open'][k]
			var_b[i+2] 		= varmeans['out_open'][k]

	else:
		print('[Exception] get_matrices(): check option=arg')
		exit(0)

	return a, b, var_a, var_b


def solve_rates(a, b, var_a, var_b, method='auto'):
	""" method: auto, inverse or lsq """

	if method=='auto': method = ('inverse' if a.shape==(4,4) else 'lsq')

	def _var_inv(mInv, var_m):
		""" calculate the variances of the elements 
				of the matrix inverse
		"""
		return nalg.multi_dot([mInv**2, var_m, mInv**2])

	def _var_aTa(a, var_a):
		""" calculate the variances of the elements
				of the matrix product transpose(a).a
		""" 
		shape = a.shape
		ret 	= np.zeros((shape[1], shape[1]))

		for i in range(shape[1]):
			for j in range(shape[1]):
				for k in range(shape[0]):
					if i==j:
						ret[i,j] += (2*a[k,i])**2 * var_a[k,i]
					else:
						ret[i,j] += var_a[k,i] * a[k,j]**2 +\
												a[k,i]**2  * var_a[k,j]

		return ret

	if method=='inverse':
		print('[Info] solve_rates(inverse)')

		aInv 		 	= nalg.inv(a)
		var_aInv 	= _var_inv(aInv, var_a)

		solns 		= np.dot(aInv, b)
		var_solns = np.dot(aInv**2, var_b) + np.dot(var_aInv, b**2)

		residuals = None

	elif method=='lsq':
		print('[Info] solve_rates(lsq)')

		aT   	 		 	= np.transpose(a)
		aTa 	 		 	= np.dot(aT, a)
		aTaInv 		 	= nalg.inv(aTa)

		var_aT 	 	 	= np.transpose(var_a)
		var_aTa 	 	= _var_aTa(a, var_a)
		var_aTaInv	= _var_inv(aTaInv, var_aTa)

		solns			  = nalg.multi_dot([aTaInv, aT, b])
		var_solns	 	= nalg.multi_dot([var_aTaInv, aT**2, b**2]) +\
								 	nalg.multi_dot([aTaInv**2, var_aT, b**2]) +\
								 	nalg.multi_dot([aTaInv**2, aT**2, var_b])

		residuals 	= b - np.dot(a, solns)

	else:
		print('[Exception] solve_rates(): check method=arg')
		exit(0)

	return solns, var_solns, residuals


def get_rsolns(solns, var_solns):
	""" return solns for each run when solve_rates(method=lsq) """
	rsolns 		 = [
						 solns[0:-1:3], solns[1:-1:3],
						 solns[2:-1:3], solns[-1] 
						 ]

	var_rsolns = [
						 var_solns[0:-1:3], var_solns[1:-1:3],
						 var_solns[2:-1:3], var_solns[-1]
						 ] 

	return rsolns, var_rsolns


def average_rsolns(rsolns, var_rsolns):
	""" rsolns and var_rsolns are from get_rsolns() """

	if not( isinstance(rsolns, list) and isinstance(var_rsolns, list) ):
		print('[Exception] average_rsolns(): check input args')
		exit(0)

	weights = [1/x for x in var_rsolns[:-1]]

	Xs 	= np.array([
			np.sum( weights[0] * rsolns[0] ) / np.sum( weights[0] ),
			np.sum( weights[1] * rsolns[1] ) / np.sum( weights[1] ),
			np.sum( weights[2] * rsolns[2] ) / np.sum( weights[2] ),
			rsolns[-1],
			])

	var_Xs = np.array([
			np.sum( weights[0]**2 * var_rsolns[0] ) / np.sum( weights[0] )**2,
			np.sum( weights[1]**2 * var_rsolns[1] ) / np.sum( weights[1] )**2,
			np.sum( weights[2]**2 * var_rsolns[2] ) / np.sum( weights[2] )**2,
			var_rsolns[-1]
			])

	return Xs, var_Xs


def print_rsolns(crun, rsolns, var_rsolns):

	def _fmt(x, error_x):
		return '{:.5f} +- {:.5f}'.format(x, error_x)

	runs_v = crun.runs_v
	for i, run_v in enumerate(runs_v):
		print(run_v)
		
		left_col = [
				'signal',
				'Bair_in',
				'Bair_out'
				]

		right_col = [
				_fmt(rsolns[0][i], np.sqrt(var_rsolns[0][i])),
				_fmt(rsolns[1][i], np.sqrt(var_rsolns[1][i])),
				_fmt(rsolns[2][i], np.sqrt(var_rsolns[2][i])),
				]

		print(table_lr(left_col, right_col, hlines=False))


def print_results(solns, var_solns, residuals=None):
	""" averaged results """

	def _fmt(x, error_x):
		return '{:.5f} +- {:.5f}'.format(x, error_x)

	eps_bar 			= 1 - 1 / solns[-1]
	error_eps_bar	= np.sqrt(var_solns[-1]) / solns[-1]**2
	
	if residuals is not None:
		residual2 = nalg.norm(residuals)**2
	else:
		residual2 = None

	left_col 	= [
				'signal',
				'Bair_in',
				'Bair_out',
				'shutter',
				'residual2'
				]	

	right_col = [
				_fmt(solns[0], np.sqrt(var_solns[0])),
				_fmt(solns[1], np.sqrt(var_solns[1])),
				_fmt(solns[2], np.sqrt(var_solns[2])),
				_fmt(eps_bar, error_eps_bar),
				fmt(residual2)
				]

	print(table_lr(left_col, right_col, hlines=True))


cuts = fcn.cuts.select('dtlim')
run0 = Run.configs(run_v='v24', trig=True, cuts=cuts)
run1 = Run.configs(run_v='v25', trig=True, cuts=cuts)
Bint = get_Bint(trig=True, cuts=cuts)

crun = CombinedRun(run0, run1)

a, b, var_a, var_b = get_matrices(crun, Bint, option='wmean')
solns, var_solns, residuals = solve_rates(a, b, var_a, var_b, method='auto')
print_results(solns, var_solns, residuals)

a, b, var_a, var_b = get_matrices(crun, Bint, option='split')
solns, var_solns, residuals = solve_rates(a, b, var_a, var_b, method='auto')
rsolns, var_rsolns = get_rsolns(solns, var_solns)
print('-------------------------------')
print_rsolns(crun, rsolns, var_rsolns)
print_results(
	*average_rsolns(rsolns, var_rsolns),
	residuals)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# def plot_rsolns(crun, Bint, rsolns, var_rsolns):
# 	""" rsolns and var_rsolns are from get_rsolns() """

# 	seq = ['in_open', 'out_open', 'in_closed', 'out_closed']

# 	runs_v 		= deepcopy(crun.runs_v)
# 	ndata 		=	deepcopy(crun.ndata)
# 	meanrates = deepcopy(crun.meanrates)
# 	varmeans 	= deepcopy(crun.varmeans)
# 	nruns			= len(runs_v)
# 	weights, wmean, var_wmean, = {}, {}, {}

# 	ax = plt.gca()

# 	# inputs
# 	for pos in seq:
# 		y 	 = meanrates[pos]
# 		yerr = np.sqrt(varmeans[pos])
# 		ax.errorbar(runs_v, y, yerr, fmt='s', lw=.2,
# 							  ms=2, elinewidth=1.2, capsize=2,
# 							  color=fcn.color[pos], label=pos)

# 		weights[pos] 	 = 1 / varmeans[pos]
# 		wmean[pos] 	 	 = np.sum( weights[pos] * meanrates[pos] ) \
# 											/ np.sum( weights[pos] )
# 		var_wmean[pos] = np.sum( weights[pos]**2 * varmeans[pos] ) \
# 											/ np.sum( weights[pos] )**2
			
# 	# outputs
# 	ax.axhline(Bint[1], ls=':', lw=1.5, color='k', label='Bint')
# 	ax.errorbar(runs_v, rsolns[0], np.sqrt(var_rsolns[0]),
# 							fmt='s', lw=1.5, ms=2, elinewidth=1.2, capsize=2,
# 						  color='r', label='signal')
# 	ax.plot(runs_v, rsolns[1], color='k', lw=1.5,
# 					label='Bair_in', zorder=0)
# 	ax.plot(runs_v, rsolns[2], color='k', lw=1.5, ls='--',
# 					label='Bair_out', zorder=0)

# 	# # errorband
# 	# xlim = ax.get_xlim()
# 	# ax.axhline(wmean['in_open'], color=fcn.color['in_open'], ls=':')
# 	# ax.barh(wmean['in_open'], width=xlim[1] - xlim[0], left=xlim[0],
# 	# 				height=np.sqrt(var_wmean['in_open']*rchi2['in_open']),
# 	# 				color=fcn.color['in_open'], alpha=0.5)
	
# 	# wsolns, var_wsolns = average_daily_solns(dsolns, var_dsolns)
# 	# dsignals, var_dsignals = dsolns[0], var_dsolns[0]
# 	# wsignal, var_wsignal 	 = wsolns[0], var_wsolns[0]
# 	# rchi2_dsignals = np.sum( (dsignals - wsignal)**2 \
# 	# 												 / var_dsignals ) / (n-1)
# 	# ax.axhline(wsignal, color='r', ls=':')
# 	# ax.barh(wsignal, width=xlim[1] - xlim[0], left=xlim[0],
# 	# 				height=np.sqrt(var_wsignal*rchi2_dsignals),
# 	# 				color='r', alpha=0.5)
# 	# ax.set_xlim(xlim) # some mpl bug

# 	ax.tick_params('y', right=True, labelright=True)
# 	lgd1 = ax.legend(*order_legend(
# 						'in_open', 'out_open', 'in_closed', 'out_closed'),
# 						loc='upper left')
# 	ax.add_artist(lgd1)
# 	lgd2 = ax.legend(*order_legend(
# 						'Bair_in', 'Bair_out', 'Bint', 'signal'),
# 						loc='upper right')

# 	show()
