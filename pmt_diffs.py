#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from IPython import embed
from matplotlib.patches import Rectangle
from datetime import timedelta
from mod.funk_manip import get_muons
from mod.funk_run import Run
from mod.funk_plt import autofmt_xdate, order_legend, show
from mod.funk_utils import posix2utc, fn_over

plt.rcParams.update({
	# tex document \columnwidth = 246pt and \textwidth = 510pt
	'figure.figsize' 			: ( 246/72, 0.6*246/72 ),
	'savefig.format'			: 'pdf',
	'text.latex.preamble'	: [ r'\usepackage{mathptmx}' ],
	'text.usetex'					: True,
	'font.family'					: 'serif',
	'font.size'						:	10,
	'axes.labelsize'			: 10,
	'legend.fontsize'			: 10,
	'xtick.labelsize'			: 9,
	'xtick.labelsize'			: 9,
	# rather use plt.tight_layout(pad=0)
	# 'savefig.bbox'				:'tight'
	# 'savefig.pad_inches'	: 0.01
})

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def get_Bint(**kwargs):
	""" kw options are spu, trig and cuts """

	run = Run.configs(run_v='v31', **kwargs)
	df  = run.get_df('in_closed')[['time', 'rates']]

	return df['rates'].mean(), df['rates'].std() / np.sqrt(len(df))


def get_data(run, key, begin=None, dt=None):
	""" dt is timedelta object (duration to retrieve) """

	df = run.get_df(key)[['time', 'rates']]
	df = df.assign(
					timedelta = posix2utc(df['time']) - \
											posix2utc(df['time'].iloc[0])
					)

	if (begin is not None) and (dt is not None):
		return df[ (df['timedelta']>= begin) &\
							 (df['timedelta'] <= begin+dt) ]

	elif begin is not None:
		return df[ (df['timedelta']>= begin) ]

	elif dt is not None:
		return df[ (df['timedelta'] <= dt) ]

	else:
		return df


def get_stats(df, Bint, Bint_err, m=1):
	""" mean rates relative to Bint """

	fmean 	= lambda x: fn_over(x, m=m, fn=np.mean)
	ferr 		= lambda x: fn_over(x, m=m, fn=lambda xx: np.std(xx, ddof=1)/np.sqrt(m))

	df_mean = df[['time', 'rates']].apply( fmean )
	df_err	= df[['rates']].apply( ferr )

	time 			 = df_mean['time'].values
	rates_mean = df_mean['rates'].values / Bint
	rates_err  = np.abs(rates_mean) * np.sqrt(
											(df_err['rates'].values / df_mean['rates'].values)**2 +
											(Bint_err / Bint)**2
											)

	return time, 1e2*rates_mean, 1e2*rates_err


# cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
cuts  = fcn.cuts.select('dtlim')
trig 	= True
run_v = 'v35'
run 	= Run.configs(run_v, trig=trig, cuts=cuts)

Bint, Bint_err = get_Bint(trig=trig, cuts=cuts)

fig, ax = plt.subplots()
m 	= 10 # averaging over m*1*4 mins

df1 = get_data(run, '1101', begin=None, dt=None)
time1, rates_mean1, rates_err1 = get_stats(df1, Bint, Bint_err, m)

df2 = get_data(run, '1000', begin=None, dt=None)
time2, rates_mean2, rates_err2 = get_stats(df2, Bint, Bint_err, m)


# ax.errorbar(
# 	posix2utc(time1), rates_mean1, rates_err1,
# 	fmt='s', ms=2.2, elinewidth=1,
# 	color='r', label=r'$r_{1101}/B_\mathrm{int}$'
# 	)

# ax.errorbar(
# 	posix2utc(time2), rates_mean2, rates_err2,
# 	fmt='s', ms=2.2, elinewidth=1,
# 	color='k', label=r'$r_{1000}/B_\mathrm{int}$'
# 	)


# lgd = ax.legend(*order_legend(
# 	r'$r_{1101}/B_\mathrm{int}$', r'$r_{1000}/B_\mathrm{int}$'),
# 	bbox_to_anchor=(0., 1.005, 1., .102), loc='lower left',
# 	ncol=4, mode="expand", borderaxespad=0,
# 	)
# for line in lgd.get_lines():
# 	line.set_linewidth(3)

# ax.set_ylabel('relative differences [\%]')
# ax.grid(which='major', ls=':')
# autofmt_xdate(daily=7, datefmt='%b-%d', rotation=None, ha='center')


# fig.tight_layout(pad=0)
# fig.savefig('./plt/8/v35_reldiffs')

# show()
embed()