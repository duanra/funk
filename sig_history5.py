""" fit the difference to get the true signal in/out from funk run
		NOTE: the history has not the same normalisation as in sig_history.py
		 			and sig_history2.py, alpha' -> alpha/tau
"""

#!usr/bin/env python3

import os
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt

from array import array
from datetime import timedelta
from IPython import embed
from iminuit import Minuit
from matplotlib import rc

import mod.funk_consts as fcn
from mod.funk_manip import reset_configs
from mod.funk_plt import show, order_legend
from mod.funk_run import Run
from mod.funk_utils import  posix2utc, flatten_list, timing,\
														pickle_dump, pickle_load, Namespace

rc('font', size=10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

class Model:

	def __init__(self, data, base, alpha, tau):
		""" note: tintvals refer now to a full cycle, i.e dt=4
				varibales are (diff, out) per cycle or tbins
				and diff is already calculated at midtime
		"""

		seq 	 	= list(map(tuple, data[['in', 'open']].iloc[:4].values))
		# [ i for i,x in enumerate(seq) if x==fcn.positions['in_open'] ][0]
		ix_in  	= seq.index(fcn.positions['in_open'])
		ix_out 	= seq.index(fcn.positions['out_open'])

		m 			= 4*10
		n 			= len(data)
		tintvls = [ (i, i+4) for i in range(0, n, 4) ]
		tbins 	= [ (0+i, i+m if i+m<n else n) for i in range(0, n, m) ]

		self.ix_in 	 	= ix_in
		self.ix_out	 	= ix_out
		self.ncycle 	= len(tintvls)
		self.nclock 	= len(tbins)
		self.tintvls 	= tintvls
		self.tbins 	 	= tbins
		self.tbin_len = m

		self.events  		 = data['counts'].values
		self.events_diff = self.events[ix_in::4] - self.events[ix_out::4]

		self._init_parameters(base, alpha, tau)


	def _init_parameters(self, base, alpha, tau):

		params = Namespace(
							flatten_list([ # 200, 180
									[('diff%d'%i, 20), ('out%d'%i, 180)]\
									for i in range(len(self.tbins))
							]))

		params.low 	 = 0
		params.base  = base
		params.alpha = alpha
		params.tau 	 = tau

		self.params  = params
		self._update()


	def _set_parameters(self, *args, **kwargs):
		""" only use this to update params, do not manually assign
				params to avoid more overhead check upon update
		"""

		if len(args): # all params must be given at once and keep the order
			new_params = Namespace( zip(self.params.keys(), args) )
			if self.params != new_params:
				self.params.update(new_params)
				self._update()

		elif len(kwargs): # only for checking purpose, not prefered method
			if all([ kw in self.params.keys() for kw in kwargs.keys() ]):
				self.params.update(kwargs)
				self._update()

		else:
			print('_set_parameters(): no parameters given')
			return


	def _update(self):
		
		self._signal_diff()
		self._signal_out()
		self._do_sum()
		self._response_diff()


	def _signal_diff(self):
		
		signal_diff = np.zeros(self.ncycle)
		for i in range(self.ncycle):
			j = int(i / (self.tbin_len//4))
			signal_diff[i] = self.params['diff%d'%j]

		self.signal_diff = signal_diff


	def _signal_out(self):

		signal_out = np.zeros(self.ncycle)
		for i in range(self.ncycle):
			j = int(i / (self.tbin_len//4))
			signal_out[i] = self.params['out%d'%j]

		self.signal_out = signal_out


	def _do_sum(self):

		dt 		= 1
		alpha = self.params.alpha
		tau 	= self.params.tau

		partial_sum 	 = np.zeros(self.ncycle)
		partial_sum[1] = (
				self.signal_out[0] * np.exp(-4*dt/tau) +
				(self.signal_diff[0] + self.signal_out[0]) * np.exp(-2*dt/tau)
			)

		for i in range(2, self.ncycle):
			partial_sum[i] = (
					partial_sum[i-1] * np.exp(-4*dt/tau) +
					self.signal_out[i-1] * np.exp(-4*dt/tau) + 
					(self.signal_diff[i-1] + self.signal_out[i-1]) * np.exp(-2*dt/tau)
				)

		self.partial_sum = partial_sum


	def _response_diff(self):

		dt 			= 1
		dt_half = 0.5
		alpha 	= self.params.alpha
		tau 		= self.params.tau

		response_diff = np.zeros(self.ncycle)
		for i in range(self.ncycle):
			response_diff[i] = (
					self.signal_diff[i] * (1 + alpha*tau * (1 - np.exp(-dt_half/tau))) +
					alpha*tau * (
						self.partial_sum[i]  * (1 - np.exp(2*dt/tau)) + self.signal_out[i]
						) * np.exp(-5*dt_half/tau) * (np.exp(dt/tau) - 1)
					)

		self.response_diff = response_diff
		

	def	fit_diff(self, **kwargs):
		""" kwargs go to Minuit.migrad() """

		nclock 			 = self.nclock
		init_fitargs = self.params.copy()
		init_fitargs.update(
			Namespace(
				[ ('limit_diff%d'%i, (0, None)) for i in range(nclock) ] +
				[ ('limit_out%d'%i, (0, None)) for i in range(nclock) ] +
				[ ('limit_low', (0, None)), ('limit_base', (0, None)),
					('limit_tau', (0, None)), ('limit_alpha', (0, 1)) ] +
				[ ('error_diff%d'%i, 0.1) for i in range(nclock)] +
				[ ('error_out%d'%i, 1) for i in range(nclock) ] +
				[ ('error_low', 0.1), ('error_base', 0.1),
					('error_tau', 0.1), ('error_alpha', 0.01) ] +
				[ ('fix_low', True), ('fix_base', True),
					('fix_tau', True), ('fix_alpha', True) ] +
				[ ('errordef', 1) ]
			))

		parnames = list(self.params.keys())

		data 		 = self.events_diff
		data_err = np.sqrt(self.events[self.ix_in::4] + 
											 self.events[self.ix_out::4])
		
		def lsq(*args):

			self._set_parameters(*args)
			response_diff = self.response_diff

			return np.sum( (data - response_diff)**2 / data_err)

		mfit = Minuit(lsq, forced_parameters=parnames, **init_fitargs)

		print('running migrad...')
		timing(mfit.migrad)(**kwargs)
		print('reduced chi2: {:.3f} / {}'\
					.format(mfit.fval, len(data)-len(self.params)-4) )

		self.fitted = mfit
		self.fitarg = {
				'fval': model.fitted.fval,
				'dof' : len(data)-len(model.params)-4,
				**mfit.fitarg
				}

		self._update()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


def get_data(run, begin=None, Dt=None):
	""" begin/Dt: timedelta object (duration to retrieve) """

	df = reset_configs(run.configs)\
				[['time', 'in', 'open', 'dt', 'counts']]
	df = df.assign(
					timedelta = posix2utc(df['time']) - \
											posix2utc(df['time'].iloc[0])
					)

	if (begin is not None) and (Dt is not None):
		ret = df[ (df['timedelta']>= begin) & (df['timedelta'] <= begin+Dt) ]

	elif begin is not None:
		ret = df[ (df['timedelta']>= begin) ]

	elif Dt is not None:
		ret = df[ (df['timedelta'] <= Dt) ]

	else:
		ret = df

	return ret.iloc[:len(ret)-len(ret)%4]\
						.reset_index(drop=True)


def plot_data(data, length=100, ax=None):

	if ax is None:
		ax = plt.gca()

	counts = data['counts'].values[:length]
	time 	 = np.arange(len(counts)) + 0.5 # shift to middle of intervals
	seq 	 = list(map(tuple, data[['in', 'open']].iloc[:4].values))

	xy 		 = {
		seq[i]: [ time[i::4], counts[i::4], np.sqrt(counts[i::4]) ]\
		for i in range(4)
		}

	color  = {
		(0, 0): 'k', (1, 0): 'k',
		(0, 1): 'b', (1, 1): 'grey'
		}

	label  = {
		(0, 0): '', (1, 0): 'in/out_closed',
		(0, 1): 'out_open', (1, 1): 'in_open'
		}

	for d in xy.items():
		if d[0]==(1,1):
			ax.errorbar(
				*d[1], fmt='o', ms=1, elinewidth=0.5,
				color=color[d[0]], label=label[d[0]],
				alpha=1, zorder=0 )


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


base, alpha, tau = 96.3677, 0.0275, 52.879

# cuts  = fcn.cuts.select('dtlim')
cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
trig 	= False
run_v = 'v35'
run 	= Run.configs(run_v, trig=trig, cuts=cuts)

begin = None # timedelta(hours=5)
Dt 		= timedelta(days=1)
data 	= get_data(run, begin, Dt)


# base, alpha, tau = 100, 0.0275, 55

# data  = pd.read_pickle('./data/pmt_memory/dummy_open1_closed1.sdf')[:4*100]
# data['out'] 	 ^= 1
# data['closed'] ^= 1
# data.rename(
# 	columns={'triggers': 'counts', 'out': 'in', 'closed': 'open'},
# 	inplace=True )


model = Model(data, base, alpha, tau)

model.fit_diff()

# pickle_dump(model.fitarg, fname='fitarg0_diff.sdic', fpath='./log/')

# fitarg = pickle_load('fitarg0_diff.sdic', fpath='./log/')
# params = [ fitarg[kw] for kw in model.params.keys() ]
# model._set_parameters(*params)


plt.plot(model.events_diff)
plt.plot(model.response_diff)
show()

embed()