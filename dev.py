#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as st

import mod.funk_io as fio
import mod.funk_consts as fcn
import mod.funk_manip as fma
import mod.funk_anal as fan
import mod.funk_utils as fus

from IPython import embed
from array import array
from copy import deepcopy
from datetime import datetime, timedelta
from matplotlib.colors import LogNorm

from mod.funk_anal import profile_plot, TrueSignal
from mod.funk_plt import show, autofmt_xdate
from mod.funk_run import Run, Flasher, ConcatenatedRun
from mod.funk_stats import bin_centers, bin_widths, unique_binning
from mod.funk_utils import Namespace, fn_over
# from mod.funk_ROOTy import TH1Fitter, plot_tf1, plot_th1

pd.set_option('display.max_rows', 20)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# # MC correction factor for reflections

# # plt.rcParams.update(fcn.rcThesis)
# plt.rcParams.update(fcn.rcDefense)
# plt.rcParams['figure.figsize'] = (175/72, 155/72)
# # plt.rcParams['figure.figsize'] = (165/72, 145/72)

# arr = np.load('./data/MC_noreflections/v35_frac_corr.npy') * 1e2

# plt.hist(
# 	arr, 30, None, True, histtype='step',
# 	lw=1.6, color='grey'#'springgreen'
# )


# # plt.xlabel('correction factor')
# # plt.yticks([0.5e4, 1e4, 1.5e4])
# # plt.gca().ticklabel_format(axis='y', style='sci', scilimits=(0,0))
# # offset = plt.gca().yaxis.get_offset_text()
# # offset.set_visible(False)
# # plt.figtext(0, 0.93, r'$\times10^{4}$')
# # plt.tight_layout(pad=0.05)
# # plt.subplots_adjust(left=0.14, top=0.96)
# # plt.savefig('./plt/12/MCreflection_coefficient')

# plt.xlabel('correction factor (\%)')
# plt.yticks([0.5e2, 1e2, 1.5e2])
# plt.gca().ticklabel_format(axis='y', style='sci', scilimits=(0,0))
# offset = plt.gca().yaxis.get_offset_text()
# offset.set_visible(False)
# plt.figtext(0, 0.93, r'$\times10^{2}$')
# plt.tight_layout(pad=0.2)
# plt.subplots_adjust(left=0.14, top=0.96)

# plt.savefig('../talks/src/phd_defense/figs/MCreflection_coefficient')
# plt.close()

# show()
# embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# run v34

# plt.rcParams.update(fcn.rcThesis)
# plt.rcParams['figure.figsize'] = (220/72, 145/72)

# cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
# # cuts  = fcn.cuts.select('dtlim')
# trig 	= False
# run 	= Run.configs(run_v='v34', trig=trig, cuts=cuts)

# df11 	= run.get_df('in_open')
# df01 	= run.get_df('out_open')
# dfall = pd.concat([df01, df11], axis=0).sort_values('time')

# df = dfall.iloc[:6000]

# time = (df['time'].values - df['time'].iloc[0]) / (60*60)
# rate = df['rates'].values
# plt.scatter(
# 	time, rate,
# 	s=4, marker='+', lw=0.1,
# 	color='k', alpha=0.9,
# 	label='data'
# )

# plt.axhline(y=6.3, lw=1.5, ls='--', color='grey')
# plt.xlabel('time/h')
# plt.ylabel('rate/Hz')
# plt.ylim(top=15)
# plt.tight_layout(pad=0.05)

# show()
# embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# run 	= Run.configs('v35', trig=False, cuts=fan.SPE)
# # # run = Run.configs('v35', trig=True)

# # dfall = fma.reset_configs(run.configs)
# dfopen = run.get_df('out_open')
# dfclosed = run.get_df('out_closed')

# embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# #  v31:: number of multiple pulses per traces

# plt.rcParams.update(fcn.rcThesis)
# plt.rcParams['figure.figsize'] = (165/72, 145/72)

# run  = Run.raw_configs(run_v='v31', npy=(62,'end')) #equiv to npy=10 + 1.5days
# df = run.get_df('in_closed')
# df = df.sort_index()
# df = df.astype({
# 			'time': np.uint, 'event': np.uint16,
# 			'capture': np.uint, 'pulse': np.uint16
# 		 })

# # x = df.time.values
# # print(all(x[i]<=x[i+1] for i in range(len(x)-1)))

# def get_captures(df):
# 	captures = df.groupby(['time', 'capture'])\
# 					 		 .size().reset_index(name='counts')

# 	return captures


# def get_ntraces(captures, n=1):
# 	""" return number of traces having n pulses per trace
# 			note: divide by n since captures are repeated n times
# 	"""
# 	captures_ = captures[ captures['counts']==int(n) ]

# 	return captures_['counts'].sum() / n


# def step(x, y, label=None, **kwargs):
# 	""" add tweaks to default mpl step """

# 	dx = x[1] - x[0]

# 	plt.step(x, y, where='mid', label=label, **kwargs)
	
# 	plt.hlines(y[0], x[0]-0.5*dx, x[0], **kwargs)
# 	plt.hlines(y[-1], x[-1]+0.5*dx, x[-1], **kwargs)

# 	plt.vlines(x[0]-0.5*dx, 0, y[0], **kwargs) 
# 	plt.vlines(x[-1]+0.5*dx, 0, y[-1], **kwargs)

# # raw data
# captures 	= get_captures(df)
# nmax 			= captures['counts'].max()
# xs 				= np.asarray(range(1, nmax+1))
# ntraces 	= np.array([get_ntraces(captures, n) for n in xs])
# ntot_traces = len(df.groupby(['time', 'capture']).head(1))
# # equals np.sum(ntraces)

# step(
# 	xs[1:], ntraces[1:]/ntot_traces*100,
# 	lw=1.2,  color='k'
# )

# plt.xlabel('multiple pulses per trace')
# plt.xticks(range(2, nmax+1, 2))
# plt.ylabel('amount of traces (\%)')
# plt.yscale('log')
# plt.grid(axis='x', lw=0.5, ls=':')
# plt.tight_layout(pad=0.05)
# plt.savefig('./plt/12/v31_npulsesPerTraces')
# plt.close()

# # show()
# # embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# # v31:: trigger and baseline correlation

# plt.rcParams.update(fcn.rcThesis)
# plt.rcParams['figure.figsize'] = (165/72, 145/72)

# def get_data(run, pos, begin=None, dt=None):
# 	""" dt is timedelta object (duration to retrieve) """

# 	df = run.get_df(pos)[['time', 'counts']]
# 	df = df.assign(
# 					timedelta = fus.posix2utc(df['time']) - \
# 											fus.posix2utc(df['time'].iloc[0])
# 					)

# 	if (begin is not None) and (dt is not None):
# 		return df[ (df['timedelta']>= begin) &\
# 							 (df['timedelta'] <= begin+dt) ]

# 	elif begin is not None:
# 		return df[ (df['timedelta']>= begin) ]

# 	elif dt is not None:
# 		return df[ (df['timedelta'] <= dt) ]

# 	else:
# 		return df


# run = Run.configs('v31', trig=True)
# # dfa = run.get_df('in_closed')
# dfa = get_data(run, 'in_closed', begin=timedelta(days=1.5), dt=None)
# dfa = dfa.astype({'time': np.uint})
# # dfa = dfa.reset_index(drop=True)
# triggers = dfa['counts'].values


# dfb = fma.load_rawX('v31', 'bmean').get_group((1,0))
# dfb = dfb.astype({'time': np.uint})
# dfb = dfb.groupby(['time', 'capture']).head(1)
# ix0 = np.where(dfb['time'].values==dfa['time'].iloc[0])[0][0]
# dfb = dfb.iloc[ix0:]
# average_bmean = dfb.groupby('time').mean()['bmean'].values


# plt.hist2d(
# 	triggers, average_bmean, bins=40,
# 	range=[(min(triggers), 165), None],
# 	norm=LogNorm()
# )

# plt.xlabel(r'$b_\mathrm{int}$/Hz')
# plt.ylabel(r'$\langle\mu_\mathrm{trunc}\rangle$/ADC')
# plt.tight_layout(pad=0.05)
# plt.subplots_adjust(left=0.26)
# plt.savefig('./plt/12/v31_baselineCorrelation')
# plt.close()

# show()
# embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # run 	= Run.configs('v35', trig=False, cuts=fan.SPE)
# run = Run.configs('v35', trig=True)
# dfall = fma.reset_configs(run.configs)
# dfopen = run.get_df('out_open')
# dfclosed = run.get_df('out_closed')

# pps = [
# 	"(-4,-4,1.5,7)",
# 	"(-4,-8,1.5,7)",
# 	"(-4,-12,1.5,7)",
# ]

# q4 	= fma.load_rawX('v35', 'q', cuts=fan.SPE, pulse_par=pps[0]).get_group((0,1))
# q8 	= fma.load_rawX('v35', 'q', cuts=fan.SPE, pulse_par=pps[1]).get_group((0,1))
# q12 = fma.load_rawX('v35', 'q', cuts=fan.SPE, pulse_par=pps[2]).get_group((0,1))


# embed()
# df = fma.sensors_data()
# df = fma.muon_data()

# run 	= Run.configs('v35', trig=False, cuts=fan.SPE)

# df01 = run.get_df('out_open')
# df00 = run.get_df('out_closed')

# x = (df01['time'].values + df00['time'].values) / 2
# y = (fan.PMT.alpha/2) * (df01['rates'].values - df00['rates'].values) \
# 			/ df00['rates'].values

# plt.plot(
# 	fus.posix2utc(x), y,
# 	color='b'
# )
# plt.plot(
# 	fus.posix2utc(fus.fn_over(x, m=10)),	fus.fn_over(y, m=10),
# 	color='k'
# )
# autofmt_xdate(datefmt='%b-%d', daily=4)
# plt.tight_layout(pad=0)
# plt.savefig('plt/out2.pdf')
# plt.show()


# # TODO: performance of signal reconstruction
# # data.counts_true.values - model.signal

# data 	= pd.read_pickle('./data/pmt_memory/dummy_open1_closed1.sdf')
# data['out'] 	 = 1^data['out']
# data['closed'] = 1^data['closed']
# data.rename(
# 	columns={'out': 'in', 'closed': 'open'},
# 	inplace=True
# 	)
# data  = data[['in', 'open', 'counts_true', 'counts']]
# model = TrueSignal.reconstruct(data, 1, 60, 0)

# data2 = data.copy()
# y0, y1, y2, y3 = data2['counts'].iloc[-4:]
# dx2 = y2 - y0 # i.e assume no signal

# data2.loc[0::4, 'counts'] = data2.loc[0::4, 'counts'] - 0.25*dx2
# data2.loc[1::4, 'counts'] = data2.loc[1::4, 'counts'] - 0.25*dx2
# data2.loc[2::4, 'counts'] = data2.loc[2::4, 'counts'] - 1.25*dx2
# data2.loc[3::4, 'counts'] = data2.loc[3::4, 'counts'] - 0.25*dx2

# model2 = TrueSignal.reconstruct(data2, 1, 60, 0)

# embed()