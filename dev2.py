#!usr/bin/env python3

import os
import sys
import inspect
import subprocess as sub

import numpy as np
import pandas as pd 
import scipy.constants as const
import scipy.stats as st
import scipy.interpolate as interpolate
import matplotlib.pyplot as plt
import matplotlib.tight_layout as tl
import mod.funk_consts as fcn
import mod.funk_io as fio

from bz2 import BZ2File
from collections import OrderedDict
from itertools import chain
from IPython import embed
from functools import wraps, partial
from natsort import natsorted

from mod.funk_plt import show, order_legend, add_toplegend, heatmap
from mod.funk_stats import bin_centers, xcorr, power_spectrum, fourier
from mod.funk_utils import (
	asarray, num, handle_warnings, timing,
	Namespace, handle_warnings, fn_over,
	pickle_load, pickle_dump, posix2utc )

# import ROOT
# from ROOT import TF1
# from ROOT.Math import GSLIntegrator, WrappedTF1
# from mod.funk_ROOTy import TF1_signature, fplot

plt.rcParams.update(fcn.rcThesis)
# plt.rcParams['figure.figsize'] = (165/72, 145/72)

pd.set_option('display.max_rows', 10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
embed()
# from scipy.integrate import dblquad, nquad

# # this is fucking slow small d (<5)
# a = 62.5; b=25; d=2.5;

# def f(xp, yp):
# 	return d / (xp**2 + yp**2 + d**2)**(3/2)

# def Omega(x, y):
# 	return nquad(f, [[-(a/2+x), a/2-x], [-(b/2+y), b/2-y]])[0]

# II = nquad(
# 			lambda x, y: Omega(x, y),
# 			[[-a/2, a/2], [-b/2, b/2]]
# 		)[0]
# print(II)

# def flux(xp, yp):
# 	return d**4 / (xp**2 + yp**2 + d**2)**3

# def integrand(x,y):
# 	return nquad(flux, [[-(a/2+x), a/2-x], [-(b/2+y), b/2-y]])[0]

# III = nquad(
# 				lambda x, y: integrand(x, y),
# 				[[-a/2, a/2], [-b/2, b/2]]
# 			)[0]
# print(III)

# embed()

# def bounds_yp(y):
# 	return [-(b/2+y), b/2-y]

# def bounds_xp(x):
# 	return [-(a/2+x), a/2-x]

# def bounds_y():
# 	return [-b/2, b/2]

# def bounds_x():
# 	return [-a/2, a/2]


# II = nquad(f, [bounds_yp, bounds_y, bounds_xp, bounds_x])
# print(II)

# Fs = 1 # per minute
# N  = 10000 # sample
# ts = 1/Fs * np.arange(N)
# T1, T2, T3, T4 = 15, 5*60, 10*60, 15*60 
# ys = (
# 	np.sin(2*np.pi*ts/T1) +
# 	np.sin(2*np.pi*ts/T2) +
# 	np.sin(2*np.pi*ts/T3) + 
# 	np.sin(2*np.pi*ts/T4) + 
# 	np.random.normal(0, 0, N) + 
# 	np.random.normal(5, 10, N)
# )
# # plt.plot(ts, ys, lw=0.1)
# # show()
# freqs, fvals = power_spectrum(ys-ys.mean(), Fs)
# print(60/T1, 60/T2, 60/T3, 60/T4)
# fvals_mean 	= fvals.mean()
# fvals_std 	= fvals.std(ddof=1)
# fvals_tmean = st.tmean(fvals, (None, fvals_mean+fvals_std))
# fvals_tstd  = st.tstd(fvals, (None, fvals_mean+fvals_std))
# plt.plot(freqs*60, fvals, lw=0.5)
# plt.axhline(fvals_tmean+3*fvals_tstd)
# plt.axhline(fvals_mean+3*fvals_std, color='r')
# # plt.xlabel(r'\textcolor{red}{x}')
# show()

# embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# x = np.arange(0,4,0.1)
# y1 = np.sin(x)
# y2 = np.sin(x+3)

# plt.figure('original')
# plt.plot(x, y1)
# plt.plot(x, y2)

# plt.figure('xcorr')
# lags, correls = xcorr(y1, y2, 0.1, None, True)
# imax = np.argmax(correls)
# print(lags[imax], correls[imax])
# plt.plot(lags, correls)
# plt.axvline(lags[imax], ls='--')


# plt.figure('shifted')
# yinterp1 = interpolate.interp1d(x, y1, bounds_error=False, fill_value=np.nan)
# yy1 = yinterp1(x + lags[imax])
# plt.plot(x, yy1)
# plt.plot(x, y2)

# yinterp2 = interpolate.interp1d(x, y2, bounds_error=False, fill_value=np.nan)
# yy2 = yinterp2(x+3)
# plt.plot(x, y1)
# plt.plot(x, yy2)

# show()
# embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# illustration of pulse entropy/ width

# def std_gaussian_entropy(h):
# 	""" standard deviation of a gaussian signal as function of
# 			the differential information entropy
# 	"""
# 	return np.exp(h) / np.sqrt( 2*np.pi*np.e )


# def discrete_gaussian(x, mu, sig):
# 	""" x must be regularly sampled """

# 	dx = x[1] - x[0]
# 	xx = np.arange(x[0]-dx/2, x[-1]+3*dx/2, dx)
# 	prob = st.norm.cdf((xx[1:]-mu)/sig) - st.norm.cdf((xx[:-1]-mu)/sig)

# 	return prob

# def entropy(prob):
# 	ret = 0
# 	for p in prob:
# 		if p==0:
# 			ret += 0
# 		else:
# 			ret += p * np.log(p)

# 	return -ret
# 	# return -np.sum(prob * np.log(prob))

# dx = 1
# sig = np.arange(dx, 11, dx/2) #[1, 2, 3, 4, 5]
# x = np.arange(0, 200+dx, dx)
# y = []
# S = []

# for s in sig:

# 	yy = st.norm.pdf(x, 100, s)
# 	yy = yy / np.sum(yy)
# 	SS = entropy(yy) #-np.sum(y*np.log(y))

# 	y.append(yy)
# 	S.append(SS)	

# y = np.asarray(y)
# S = np.asarray(S)


# plt.plot(S, sig, color='k', lw=1.5, label='calc1')
# plt.vlines(
# 	[S[4], S[16]], 1, [sig[4], sig[16]],
# 	colors=['crimson', 'mediumblue'], linestyles='--', lw=1
# )
# plt.hlines(
# 	[sig[4], sig[16]], 0.9, [S[4], S[16]],
# 	colors=['crimson', 'mediumblue'], linestyles='--', lw=1
# )
# plt.xlabel('$S$')
# plt.xlim(0.9,4.1)
# plt.ylabel('$\sigma_t$')
# plt.ylim(1,10.5)
# plt.tight_layout(pad=0.05)
# rect = tl.get_tight_layout_figure(
# 					plt.gcf(), [plt.gca()], tl.get_subplotspec_list([plt.gca()]),
# 					tl.get_renderer(plt.gcf()), pad=0.05
# 				)
# # plt.savefig('./plt/12/pulses_entropy_sigmas')
# show()

# # S2 = np.array([ entropy(discrete_gaussian(x, 100, s)) for s in sig ])
# # plt.plot(S2, sig, label='calc2', ls=':')

# # S3 = np.linspace(0,5)
# # plt.plot(S3, dx*std_gaussian_entropy(S3), label='diffentr', ls='--')



# plt.plot(x, -y[4], lw=1.5, color='crimson', label=sig[4])
# plt.plot(x, -y[16], lw=1.5, color='mediumblue', label=sig[16])
# plt.tick_params(labelleft=False, labelbottom=False)
# plt.xlim(60, 140)
# plt.ylim(-0.15, 0.01)
# # plt.legend()
# # plt.tight_layout(pad=0.05)
# plt.subplots_adjust(**rect)
# # plt.savefig('./plt/12/pulses_entropy_example')
# show()

# embed()	


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# from wolframclient.language import wl, wlexpr
# from wolframclient.evaluation import WolframLanguageSession

# def g(x):
# 	return x^2

# wm_kernel = '/usr/local/Wolfram/Mathematica/11.3/Executables/WolframKernel'
# wls = WolframLanguageSession(wm_kernel)

# wls.evaluate("num_integrate[f] := NIntegrate[f[x], {x, 0, 3}]")
# num_integrate = wls.function("num_integrate")

# wls.terminate()