#!usr/bin/env python3

import numpy as np 
import matplotlib.pyplot as plt

from scipy.stats import expon, gamma, powerlaw
from mod.funk_stats import bin_centers, bin_widths,\
													 pareto2
from mod.funk_plt import pause, show
from mod.funk_utils import fmt_seq, fmt_dic, table_lr
from mod.funk_ROOTy import add_fitresults, get_fitresults, get_parameters,\
													 get_histogram, gen_from,\
													 TF1_signature
from ROOT import TH1D, TF1
from root_numpy import fill_hist

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def umap_expon(rvs, lam=None):
	""" probabilility integral transform of the exponential (CDF)	"""
	if lam is None:
		lam = np.mean(rvs)
	return 1 - np.exp(-lam*rvs)


def transformed_pareto2(y, n, theta, lam):
	""" probability of Y=F(X) where F is the CDF of expon(lam) and X~pareto2
			with y in [0,1), i.e apply umap_expon transform of pareto2 rvs
	"""
	from warnings import catch_warnings, filterwarnings
	with catch_warnings(record=True) as w:
		filterwarnings('always')
		ret = n*theta / (lam*(1-y))      \
					* np.power(1 - theta/lam * np.log(1-y), -n-1)
		if len(w):
			print(y, n, theta, lam)
	
	return ret 


def transformed_gamma(y, n, theta, lam):
	from scipy.special import gamma as gf

	return 1 / (lam*(1-y) * theta**n*gf(n))\
				 * np.power(1-y, 1/(theta*lam))\
				 * np.power(-np.log(1-y)/lam, n-1)


lam, mu, sig  = 5, 5, 1
n 		= (mu / sig)**2
theta = mu / n

rvs_range = (0, 3)
size = int(1e6)
bins = 500 # for transformed plot
# fexpon = expon(scale=1/lam)
# fgamma = gamma(a=0.1, loc=0, scale=10)


rvs1 = gen_from(TF1_signature(expon.pdf), (0, 1/lam),
								*rvs_range, size=size)
transformed_rvs1 = umap_expon(rvs1, lam)
plt.hist(transformed_rvs1, bins, (0, 1), True)


rvs2 = gen_from(TF1_signature(pareto2), (n, theta),
								*rvs_range, size=size)
transformed_rvs2 = umap_expon(rvs2, lam)
hist, edg, _= plt.hist(transformed_rvs2, bins, (0, 1), True,
											 color='r', histtype='step')
x = bin_centers(edg) # np.linspace(0, 1, num=100, endpoint=False) 
plt.plot(x, transformed_pareto2(x, n, theta, lam), color='r', ls='--')


rvs3 = gen_from(TF1_signature(gamma.pdf), (0.8, 0, 0.25),
								*rvs_range, size=size)
transformed_rvs3 = umap_expon(rvs3, lam)
hist, edg, _= plt.hist(transformed_rvs3, bins, (0, 1), True,
											 color='k', histtype='step')
x = bin_centers(edg)
plt.plot(x, transformed_gamma(x, 0.8, 0.25, lam), color='k', ls='--')


plt.axhline(1, ls=':', lw=.8, color='grey')
plt.yscale('log')
# plt.savefig('./1/distns.transform.png', dpi=600)
show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# plt.hist(rvs1, 200, None, True)
# plt.hist(rvs2, 200, None, True, color='r', histtype='step')
# plt.hist(rvs3, 200, None, True, color='k', histtype='step')
# plt.yscale('log')
# show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # test fit transformed_pareto2

# lam, mu, sig  = 5, 5, 1
# n 		 = (mu / sig)**2
# theta  = mu / n

# rvs_range = (0, 3)
# size = int(1e6)
# bins = 100 # for transformed plot

# rvs2 = gen_from(TF1_signature(pareto2), (n, theta), *rvs_range, size=size)
# transformed_rvs2 = umap_expon(rvs2, lam)

# h = TH1D('h', 'transformed histo', bins, 0, 1)
# fill_hist(h, transformed_rvs2)
# h.Scale(1/h.Integral('width'))

# f = TF1('f', TF1_signature(transformed_pareto2),
# 				h.GetBinCenter(1), h.GetBinCenter(bins), 3)
# f.SetParameters(n, theta, lam)
# f.FixParameter(2, lam)
# h.Fit(f, 'RP0')

# pyhist = get_histogram(h, return_yerr=True)
# fitted = get_fitresults(f)
# x = bin_centers(pyhist[2])
# plt.errorbar(x, pyhist[0], pyhist[1], fmt='s', ms=2, elinewidth=.5)
# plt.plot(x, [f.Eval(xval) for xval in x], color='r', zorder=10)

# print(fmt_dic(fitted))
# show()


# # test fit transformed gamma

# lam, n, theta = 5, 0.8, 0.25
# rvs_range = (0, 3)
# size = int(1e6)
# bins = 100 # for transformed plot

# rvs3 = gen_from(TF1_signature(gamma.pdf), (n, 0, theta), *rvs_range, size=size)
# transformed_rvs3 = umap_expon(rvs3, lam)

# h = TH1D('h', 'transformed histo', bins, 0, 1)
# fill_hist(h, transformed_rvs3)
# h.Scale(1/h.Integral('width'))

# f = TF1('f', TF1_signature(transformed_gamma),
# 				h.GetBinCenter(1), h.GetBinCenter(bins), 3)
# f.SetParameters(n, theta, lam)
# h.Fit(f, 'RP')

# pyhist = get_histogram(h, return_yerr=True)
# fitted = get_fitresults(f)
# x = bin_centers(pyhist[2])
# plt.errorbar(x, pyhist[0], pyhist[1], fmt='_', color='k', ms=3, elinewidth=1)
# plt.plot(x, [f.Eval(xval) for xval in x], color='r', zorder=10)

# print(fmt_dic(fitted))
# show()