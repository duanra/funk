# 3d plots of the spe phase-space

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import mpl_toolkits.mplot3d.art3d as art3d
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import Ellipse

import mod.funk_consts as fcn
from mod.funk_stats import bin_centers
from mod.funk_run import Flasher


plt.rcParams.update({
	# 'figure.figsize': ( 550/72, 400/72 ),
	'font.size'			: 23,
	'text.latex.preamble'	: [r'\usepackage{newpxtext,newpxmath}'],
	'font.family'					: 'serif',
	'text.usetex'	: True
})


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

run 	= Flasher('v08f', split=True)
data 	= run.data
keys 	= sorted(data.keys())


def select_df(data, k):

	df 				 = data[k].copy() # need copy 
	df['h'] 	 = 10**df['h']
	df['q'] 	 = 10**df['q'] / 10**8 
	df['tvar'] = np.sqrt(df['tvar'])
	df.rename(columns={
							'h' 	 : 'H',
							'q'		 : 'Q',
							'tvar' : 'tsig'
							}, inplace=True)

	g 		= df.groupby('event')
	ntrig = g.head(1)['pulse'].sum() # this works only for each rootfile
	npu 	= g.size().sum()

	return df, ntrig, npu


# ik  = 'l126'
# k   = [k for k in keys if ik in k][0]

for k in keys[74:75]:

	fig = plt.figure()
	ax  = Axes3D(fig, rect=(-0.1, 0.1, 1.11, 1.12))
	
	ik  = k.split('_')[-2]
	df, ntrig, npu = select_df(data, k)

	hist, S_edg, H_edg = np.histogram2d(
												df['S'].values,  df['H'].values,
												range=[(1,5), (0,1.8)], bins=100
											 )

	S_bce = bin_centers(S_edg)
	H_bce = bin_centers(H_edg)

	x, y 	= np.meshgrid(S_bce, H_bce)
	# ax.plot_surface(
	# 	y, x, hist, color='grey',
	# 	linewidth=0, antialiased=False)

	x  = x.flatten('F')
	y  = y.flatten('F')
	z  = np.zeros_like(x)
	
	dx = (S_edg[1] - S_edg[0]) * np.ones_like(x)
	dy = (H_edg[1] - H_edg[0]) * np.ones_like(x)
	dz = hist.flatten()
	# print x, y, z, dx, dy, dz
	# print x.shape, y.shape, z.shape, dx.shape, dy.shape, dz.shape


	print('plotting (key=%s)'%k)

	fracs = dz / dz.max()
	norm  = mcolors.Normalize(fracs.min(), fracs.max())
	color_values = plt.get_cmap('Blues')(norm(fracs)) 

	ax.bar3d(
		x, y, z, dx, dy, dz,
		zsort='min', color=color_values, #'grey',
		# linewidth=0.1
	)
	# ax.text(4, 0, 16, ik)


	# p1 = Ellipse(
	# 			(2.5, 0.25), 3, 0.8, lw=2, ls='--',
	# 			facecolor=(0,0,0,0), edgecolor='crimson',
	# 		 )
	# ax.add_patch(p1)
	# art3d.pathpatch_2d_to_3d(p1, z=0, zdir="z")
	# ax.text(
	# 	0.8, 0.5, 20, 'SPE events',
	# 	zdir=None, color='crimson')


	# p2 = Ellipse(
	# 			(4.1, 1.5), 2.5, 1.5, lw=2, ls='--',
	# 			facecolor=(0,0,0,0), edgecolor='k',
	# 		 )
	# ax.add_patch(p2)
	# art3d.pathpatch_2d_to_3d(p2, z=0, zdir="z")
	# ax.text(
	# 	0.5, 3.5, 30, 'Multiple hits',
	# 	zdir=None, color='k')


	ax.set_xlabel('Information entropy', labelpad=20)
	ax.w_xaxis.set_pane_color((1., 1., 1., 1.))
	ax.set_xticks((1.5, 2.5, 3.5, 4.5))
	

	ax.set_ylabel('Pulse height/V', labelpad=20)
	# ax.yaxis._axinfo['label']['space_factor'] = 2.0
	ax.w_yaxis.set_pane_color((1., 1., 1., 1.))
	# ax.set_yticks([5, 10, 15, 20, 25])
	ax.set_yticks((0, 0.5, 1, 1.5)) 

	ax.w_zaxis.line.set_lw(0.)
	ax.set_zticks([])
	ax.set_zbound(0)

	ax.grid(False)

	fig.savefig('./plt/13/flasher_%s.png'%ik, dpi=300)
	# fig.savefig('./plt/13/flasher_%s.eps'%ik)
	# fig.savefig('./plt/13/flasher_%s.pdf'%ik)

	# plt.show()
	plt.close()