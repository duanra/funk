#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from IPython import embed
from matplotlib.patches import Rectangle
from datetime import timedelta
from mod.funk_run import Run, DailyStats
from mod.funk_plt import autofmt_xdate, order_legend, show
from mod.funk_utils import order_unzip, fn_over_df

plt.rcParams.update({
	# tex document \columnwidth = 246pt and \textwidth = 510pt
	'figure.figsize' 			: ( 246/72, 0.6*246/72 ),
	'savefig.format'			: 'pdf',
	'text.latex.preamble'	: [ r'\usepackage{mathptmx}' ],
	'text.usetex'					: True,
	'font.family'					: 'serif',
	'font.size'						:	10,
	'axes.labelsize'			: 10,
	'legend.fontsize'			: 10,
	'xtick.labelsize'			: 9,
	'xtick.labelsize'			: 9,
	# rather use plt.tight_layout(pad=0)
	# 'savefig.bbox'				:'tight'
	# 'savefig.pad_inches'	: 0.01
})

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def get_Bint(**kwargs):
	""" kw options are spu, trig and cuts """

	run = Run.configs(run_v='v31', **kwargs)
	df  = run.get_df('in_closed')[['time', 'rates']]

	return len(df), df['rates'].mean(), df['rates'].var() / len(df)


def get_data(Bint, run, key):
	"""" get daily relative differences """

	mpos = { key: val for val, key in fcn.positions.items() }
	pos0 = mpos[ (int(key[0]), int(key[1])) ]
	pos1 = mpos[ (int(key[2]), int(key[3])) ]

	dates 				 = run.daily_dates
	rates0, varns0 = run.daily_meanrates[pos0], run.daily_varmeans[pos0]
	rates1, varns1 = run.daily_meanrates[pos1], run.daily_varmeans[pos1]

	print('get_data({} - {})'.format(pos0, pos1))
	# print( (rates0 - rates1))
	rel_rates = (rates0 - rates1) / Bint[1]
	varns = rel_rates**2 * ( (varns0 + varns1) / (rates0 - rates1)**2 +
													  Bint[2] / Bint[1]**2 ) 

	return dates, 1e2*rel_rates, 1e2*np.sqrt(varns)
	# return dates, rates0 - rates1, np.sqrt(varns0 + varns1)


cuts  = fcn.cuts.select('dtlim')
run_v = 'v35'
run 	= DailyStats(run_v, trig=True, cuts=cuts)
Bint  = get_Bint(trig=True, cuts=cuts)
print(Bint)

label = {
	'1101'	: r'r$_{1101}$/B$_\mathrm{int}$',
	'1000'	: r'r$_{1000}$/B$_\mathrm{int}$',
	'1110'	: r'r$_{1110}$/B$_\mathrm{int}$',
	'0100'	: r'r$_{0100}$/B$_\mathrm{int}$',
}


fig, ax = plt.subplots()

for key in ['1101', '1000', '1110', '0100']:
	dates, diffs, stderrs = get_data(Bint, run, key)
	ax.errorbar(
		x=dates, y=diffs, yerr=stderrs,
		fmt='s', ms=2.2, elinewidth=1,
		color=fcn.color[key], label=label[key]
		)
# ax.axhline(0, ls=':', lw=.5, color='k')


ax.legend(
	*order_legend(r'r$_{1101}$/B$_\mathrm{int}$', r'r$_{1000}$/B$_\mathrm{int}$',
								r'r$_{1110}$/B$_\mathrm{int}$', r'r$_{0100}$/B$_\mathrm{int}$'),
	bbox_to_anchor=(0., 1.005, 1., .102), loc='lower left',
	ncol=4, mode="expand", borderaxespad=0
	)
ax.set_ylabel('relative differences [\%]')
ax.grid(which='major', ls=':')
autofmt_xdate(daily=7, datefmt='%b-%d', rotation=None, ha='center')


fig.tight_layout(pad=0)
fig.savefig('./plt/8/v35_reldiffs')

# embed()
# show()


# ax0.tick_params(labeltop=False, bottom=False, top=True) 
# ax0.spines['bottom'].set_visible(False)
# ax1.spines['top'].set_visible(False)
# d = .005  # how big to make the diagonal lines in axes coordinates
# # arguments to pass to plot, just so we don't keep repeating them
# kwargs = dict(transform=ax0.transAxes, color='k', clip_on=False)
# ax0.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
# ax0.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal
# kwargs.update(transform=ax1.transAxes)  # switch to the bottom axes
# ax1.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
# ax1.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal