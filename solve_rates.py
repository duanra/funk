#!usr/bin/env python3

import numpy as np
import numpy.linalg as nalg
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from copy import deepcopy
from datetime import datetime, timedelta
from sys import exit
from mod.funk_plt import pause, show, order_legend, autofmt_xdate
from mod.funk_run import Run, DailyStats, ConcatenatedRun
from mod.funk_utils import posix2utc, utc2posix, fmt, table_lr

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def get_Bint(**kwargs):
	""" kw options are spu, trig and cuts """

	run = Run.configs(run_v='v31', **kwargs)
	df  = run.get_df('in_closed')[['time', 'rates']]

	return len(df), df['rates'].mean(), df['rates'].std()


def get_matrices(run, Bint, option='umean'):
	""" return a, b such that a X = b where
						 X = (S, Bair_in, Bair_out, ...,  eps_inv)
			
			option: 'umean' simply average over the whole data set (single run)
							'wmean' split the data then take the weighted average
							'split' treat daily measurements
	"""

	seq = ['in_open', 'in_closed', 'out_open', 'out_closed']

	base_a = np.array([
							[1, 1, 0,  0],
							[0, 1, 0, -1],
							[0, 0, 1,  0],
							[0, 0, 1, -1]
							], dtype=np.float)

	base_b = np.array([1, 0, 1, 0], dtype=np.float)

	if option=='umean':

		rates, varns = np.zeros(4), np.zeros(4)
		for i, pos in enumerate(seq):
			df = run.get_df(pos)['rates']
			rates[i] = df.mean() - Bint[1]
			varns[i] = df.var() / len(df) + Bint[2]**2 / Bint[0]

		a, b 		 = base_a.copy(), base_b.copy()
		a[:,-1] *= rates
		b[:] 		*= rates

		var_a, var_b  = np.abs(base_a.copy()), base_b.copy()
		var_a[:,:-1] *= 0 # constants
		var_a[:,-1]	 *= varns
		var_b[:]		 *= varns

	else:	

		# !!! note: shallow copy doesn't create copy of nested object
		# !!! i.e not recursive, even 1st level (the dict[key] here points to
		# !!! a mutable array, would have no issue with immutable object)
		ndata 		=	deepcopy(run.daily_ndata)
		meanrates = deepcopy(run.daily_meanrates)
		varmeans 	= deepcopy(run.daily_varmeans)
		ndays 		= len(ndata[seq[0]])
		weights 	= {}
		# rchi2 		= {}

		# substract Bint and get standard errors
		for pos in seq:
			meanrates[pos] -= Bint[1]
			varmeans[pos]  += Bint[2]**2 / Bint[0]
			weights[pos] 		= 1 / varmeans[pos] # ndata[pos] / sum(ndata[pos])

		if option=='wmean':

			rates, varns = np.zeros(4), np.zeros(4)
			for i, pos in enumerate(seq):
				rates[i] 	 = np.sum( weights[pos] * meanrates[pos] ) \
												/ np.sum( weights[pos] )
				varns[i] 	 = np.sum( weights[pos]**2 * varmeans[pos] ) \
												/ np.sum( weights[pos] )**2
				# rchi2[pos] = np.sum( (meanrates[pos] - rates[i])**2 
				# 								/ varmeans[pos] ) / (ndays-1)

			a, b 		 = base_a.copy(), base_b.copy()
			a[:,-1] *= rates
			b[:] 		*= rates
			
			var_a, var_b  = np.abs(base_a.copy()), base_b.copy()
			var_a[:,:-1] *= 0 # constants
			var_a[:,-1]	 *= varns
			var_b[:]		 *= varns

		elif option=='split':

			m, n 	= 4*ndays, 3*ndays+1 # nrows, ncols

			def _base():
				""" build base matrix """
				a = np.zeros((m, n))
				for i, j in zip(range(0, m, 4), range(0, n, 3)):
					a[i:i+4, j:j+3] = base_a[:4, :3].copy()
					# a[1::2,-1] = -1
				
				b = np.zeros(m)
				# b[0::2] = 1
				return a, b

			a, b = _base()
			var_a, var_b = np.zeros_like(a), np.zeros_like(b)

			for i, k in zip(range(0,m,4), range(ndays)):
				a[i+1,-1] 		= -meanrates['in_closed'][k]
				a[i+3,-1] 		= -meanrates['out_closed'][k]

				b[i+0] 				= meanrates['in_open'][k]
				b[i+2] 				= meanrates['out_open'][k]

				var_a[i+1,-1] = varmeans['in_closed'][k]
				var_a[i+3,-1] = varmeans['out_closed'][k]
				
				var_b[i+0] 		= varmeans['in_open'][k]
				var_b[i+2] 		= varmeans['out_open'][k]

		else:
			print('[Exception] get_matrices(): check option=arg')
			exit(0)

	return a, b, var_a, var_b


def solve_rates(a, b, var_a, var_b, method='auto'):
	""" method: auto, inverse or lsq """

	if method=='auto': method = ('inverse' if a.shape==(4,4) else 'lsq')

	def _var_inv(mInv, var_m):
		""" calculate the variances of the elements 
				of the matrix inverse
		"""
		return nalg.multi_dot([mInv**2, var_m, mInv**2])

	def _var_aTa(a, var_a):
		""" calculate the variances of the elements
				of the matrix product transpose(a).a
		""" 
		shape = a.shape
		ret 	= np.zeros((shape[1], shape[1]))

		for i in range(shape[1]):
			for j in range(shape[1]):
				for k in range(shape[0]):
				# ret[i,j] += a[k,i] * a[k,j]
					if i==j:
						ret[i,j] += (2*a[k,i])**2 * var_a[k,i]
					else:
						ret[i,j] += var_a[k,i] * a[k,j]**2 +\
												a[k,i]**2  * var_a[k,j]

		return ret

	if method=='inverse':
		print('[Info] solve_rates(inverse)')

		aInv 		 	= nalg.inv(a)
		var_aInv 	= _var_inv(aInv, var_a)

		solns 		= np.dot(aInv, b)
		var_solns = np.dot(aInv**2, var_b) + np.dot(var_aInv, b**2)

		residuals = None

	elif method=='lsq':
		print('[Info] solve_rates(lsq)')

		aT   	 		 	= np.transpose(a)
		aTa 	 		 	= np.dot(aT, a)
		aTaInv 		 	= nalg.inv(aTa)

		var_aT 	 	 	= np.transpose(var_a)
		var_aTa 	 	= _var_aTa(a, var_a)
									# np.dot(var_aT, a**2) + np.dot(aT**2, var_a)
		var_aTaInv	= _var_inv(aTaInv, var_aTa)

		# print(m_inv.shape, aT.shape, b.shape)
		solns			  = nalg.multi_dot([aTaInv, aT, b])
		# this is not totally correct (correlation in the matrix operation)
		var_solns	 	= nalg.multi_dot([var_aTaInv, aT**2, b**2]) +\
								 	nalg.multi_dot([aTaInv**2, var_aT, b**2]) +\
								 	nalg.multi_dot([aTaInv**2, aT**2, var_b])

		residuals 	= b - np.dot(a, solns)

	else:
		print('[Exception] solve_rates(): check method=arg')
		exit(0)

	return solns, var_solns, residuals


def get_daily_solns(solns, var_solns):

	return ([
			solns[0:-1:3], solns[1:-1:3],
			solns[2:-1:3], solns[-1]
			], [
			var_solns[0:-1:3], var_solns[1:-1:3],
			var_solns[2:-1:3], var_solns[-1]
			])


def average_daily_solns(dsolns, var_dsolns):
	""" dsolns and var_dsolns are from get_daily_solns() """

	if not( isinstance(dsolns, list) and isinstance(var_dsolns, list) ):
		print('[Exception] average_daily_solns(): check input args')
		exit(0)

	weights = [1/x for x in var_dsolns[:-1]]

	Xs 	= np.array([
					np.sum( weights[0] * dsolns[0] ) / np.sum( weights[0] ),
					np.sum( weights[1] * dsolns[1] ) / np.sum( weights[1] ),
					np.sum( weights[2] * dsolns[2] ) / np.sum( weights[2] ),
					dsolns[-1],
					])

	var_Xs = np.array([
					np.sum( weights[0]**2 * var_dsolns[0] ) / np.sum( weights[0] )**2,
					np.sum( weights[1]**2 * var_dsolns[1] ) / np.sum( weights[1] )**2,
					np.sum( weights[2]**2 * var_dsolns[2] ) / np.sum( weights[2] )**2,
					var_dsolns[-1]
					])


	return Xs, var_Xs


def print_results(solns, var_solns, residuals=None):
	""" solns and var_solns are from average_daily_solns() if daily """ 

	def _fmt(x, error_x):
		return '{:.5f} +- {:.5f}'.format(x, error_x)

	eps_bar 			= 1 - 1 / solns[-1]
	error_eps_bar	= np.sqrt(var_solns[-1]) / solns[-1]**2
	
	if residuals is not None:
		residual2 = nalg.norm(residuals)**2
	else:
		residual2 = None

	left_col 	= [
			'signal',
			'Bair_in',
			'Bair_out',
			'shutter',
			'residual2'
			]	

	right_col = [
			_fmt(solns[0], np.sqrt(var_solns[0])),
			_fmt(solns[1], np.sqrt(var_solns[1])),
			_fmt(solns[2], np.sqrt(var_solns[2])),
			_fmt(eps_bar, error_eps_bar),
			fmt(residual2)
			]

	print(table_lr(left_col, right_col, hlines=True))


def plot_daily_results(run, Bint, dsolns, var_dsolns):
	""" dsolns and var_dsolns are from get_daily_solns() """

	seq = ['in_open', 'out_open', 'in_closed', 'out_closed']
	# seq = ['in_open', 'in_closed', 'out_open', 'out_closed']

	dates 		= deepcopy(run.daily_dates)
	ndata 		=	deepcopy(run.daily_ndata)
	meanrates = deepcopy(run.daily_meanrates)
	varmeans 	= deepcopy(run.daily_varmeans)
	n 				= len(ndata[seq[0]])
	weights, wmean, var_wmean, rchi2 = {}, {}, {}, {}

	ax = plt.gca()

	# inputs
	for pos in seq:
		y 	 = meanrates[pos]
		yerr = np.sqrt(varmeans[pos])
		ax.errorbar(dates, y, yerr, fmt='s-', lw=.2,
							  ms=2, elinewidth=1.2, capsize=2,
							  color=fcn.color[pos], label=pos)

		weights[pos] 	 = 1 / varmeans[pos]
		wmean[pos] 	 	 = np.sum( weights[pos] * meanrates[pos] ) \
											/ np.sum( weights[pos] )
		var_wmean[pos] = np.sum( weights[pos]**2 * varmeans[pos] ) \
											/ np.sum( weights[pos] )**2
		rchi2[pos] 		 = np.sum( (meanrates[pos] - wmean[pos])**2 
														 / varmeans[pos] ) / (n-1)
	
	# outputs
	ax.axhline(Bint[1], ls=':', lw=1.5, color='k', label='Bint')
	ax.errorbar(dates, dsolns[0], np.sqrt(var_dsolns[0]),
							fmt='s-', lw=1.5, ms=2, elinewidth=1.2, capsize=2,
						  color='r', label='signal')
	ax.plot(dates, dsolns[1], color='k', lw=1.5,
					label='Bair_in', zorder=0)
	ax.plot(dates, dsolns[2], color='k', lw=1.5, ls='--',
					label='Bair_out', zorder=0)

	# errorband
	xlim = ax.get_xlim()
	ax.axhline(wmean['in_open'], color=fcn.color['in_open'], ls=':')
	ax.barh(wmean['in_open'], width=xlim[1] - xlim[0], left=xlim[0],
					height=np.sqrt(var_wmean['in_open']*rchi2['in_open']),
					color=fcn.color['in_open'], alpha=0.5)
	
	wsolns, var_wsolns = average_daily_solns(dsolns, var_dsolns)
	dsignals, var_dsignals = dsolns[0], var_dsolns[0]
	wsignal, var_wsignal 	 = wsolns[0], var_wsolns[0]
	rchi2_dsignals = np.sum( (dsignals - wsignal)**2 \
													 / var_dsignals ) / (n-1)
	ax.axhline(wsignal, color='r', ls=':')
	ax.barh(wsignal, width=xlim[1] - xlim[0], left=xlim[0],
					height=np.sqrt(var_wsignal*rchi2_dsignals),
					color='r', alpha=0.5)
	ax.set_xlim(xlim) # some mpl bug

	autofmt_xdate(datefmt='%b-%d')
	ax.tick_params('y', right=True, labelright=True)
	lgd1 = ax.legend(*order_legend(
						'in_open', 'out_open', 'in_closed', 'out_closed'),
						loc='upper left')
	ax.add_artist(lgd1)
	lgd2 = ax.legend(*order_legend(
						'Bair_in', 'Bair_out', 'Bint', 'signal'),
						loc='upper right')

	# plt.savefig('./2/dailys.pdf', format='pdf')
	show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

cuts  = fcn.cuts.select('dtlim')
# cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim', 'qlim')
run_v = 'v25'
run   = DailyStats(run_v, trig=True, cuts=cuts)
Bint  = get_Bint(trig=True, cuts=cuts)
print(run.mseq)

print(run_v)
a, b, var_a, var_b = get_matrices(run, Bint, option='umean')
solns, var_solns, _= solve_rates(a, b, var_a, var_b, method='inverse')
print_results(solns, var_solns)


# a, b, var_a, var_b = get_matrices(run, Bint, option='wmean')
# solns, var_solns, residuals = solve_rates(a, b, var_a, var_b, method='inverse')
# print_results(solns, var_solns, residuals)


# a, b, var_a, var_b = get_matrices(run, Bint, option='split')
# solns, var_solns, residuals = solve_rates(a, b, var_a, var_b, method='auto')
# dsolns, var_dsolns = get_daily_solns(solns, var_solns)
# print_results(
# 	*average_daily_solns(dsolns, var_dsolns),
# 	residuals)
# plot_daily_results(
# 	run, Bint, dsolns, var_dsolns)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# cuts = fcn.cuts.select('dtlim')
# run0 = Run.configs(run_v='v24', trig=True, cuts=cuts)
# run1 = Run.configs(run_v='v25', trig=True, cuts=cuts)
# crun = ConcatenatedRun(run0, run1)
# Bint = get_Bint(trig=True, cuts=cuts)

# print(crun.run_v)
# print(crun.duration)
# print(run0.mseq)
# print(run1.mseq)
# a, b, var_a, var_b = get_matrices(crun, Bint, option='umean')
# solns, var_solns, _= solve_rates(a, b, var_a, var_b, method='inverse')
# print_results(solns, var_solns)