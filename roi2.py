#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from IPython import embed
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import LogNorm
from matplotlib.patches import Rectangle
from mod.funk_stats import bin_centers
from mod.funk_run import Run
from mod.funk_manip import apply_cuts
from mod.funk_plt import show, order_legend, group_legend

plt.rcParams.update({
	'text.latex.preamble': [
			r'\usepackage{times}',
			r'\usepackage{newpxmath}',
		],
	'text.usetex': True,
})


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


def std_gaussian_entropy(h):
	""" standard deviation of a gaussian signal as function of
			the differential information entropy
	"""
	return np.exp(h) / np.sqrt( 2*np.pi*np.e )


cuts = fcn.cuts.select('dtlim')
run  = Run.raw_configs(
				run_v='v35', npy=0,#'auto',
				cols=['S', 'h', 'q', 'tvar', 'rise'],
				cuts=cuts)

df = run.get_df('out_open').copy() # do not copy if out of memory
df['h'] = 10**df['h'].values
df.rename(columns={'h': 'H'}, inplace=True)


fig = plt.figure()
ax  = Axes3D(fig)

hist, S_edg, tsig_edg = np.histogram2d(
													df['S'].values,  df['tsig'].values,
													range=[(1,5), (0,30)], bins=100,
													density=True
												)

S_bce 	 = bin_centers(S_edg)
tsig_bce = bin_centers(tsig_edg)

x, y 	= np.meshgrid(S_bce, tsig_bce)
x  = x.flatten('F')
y  = y.flatten('F')
z  = np.zeros_like(x)

dx = (S_edg[1] - S_edg[0]) * np.ones_like(x)
dy = (tsig_edg[1] - tsig_edg[0]) * np.ones_like(y)
dz = hist.ravel()


ax.bar3d(x, y, z, dx, dy, dz, color='grey', zsort='min')

# ax.text(4, 0, 16, ik)
ax.w_xaxis.set_pane_color((1., 1., 1., 1.))
ax.w_yaxis.set_pane_color((1., 1., 1., 1.))
# ax.w_zaxis.set_pane_color((1., 1., 1., 1.))

ax.set_xlabel('Shanon entropy')
ax.set_ylabel('Pulse-width [ns]')
# ax.set_zlabel('Histogram')

ax.set_xticks([1.5, 2.5, 3.5, 4.5])
ax.set_yticks([5, 10, 15, 20, 25])
ax.w_zaxis.line.set_lw(0.)
ax.set_zticks([])
ax.set_zbound(0)

ax.grid(False)

print('saving figure...')
fig.savefig('./plt/9/roi3d.pdf')
# fig.savefig('./plt/9/roi3d.png', format='png', dpi=600)
# plt.show()
plt.close()