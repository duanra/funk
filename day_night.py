#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as st
import mod.funk_consts as fcn 

from IPython import embed
from datetime import datetime
from matplotlib.patches import Rectangle

from mod.funk_manip import muon_data, reset_configs
from mod.funk_run import Run, ConcatenatedRun
from mod.funk_plt import show, add_toplegend
from mod.funk_stats import bin_centers
from mod.funk_utils import posix2utc

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (260/72, 165/72)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def binned(df, col='rates'):

	tbins 	= [0] + [3599 + i*3600 for i in range(24)]
	daytime = df['daytime'].values
	ys = df[col].values
	ymeans, tedges = st.binned_statistic(
										daytime, ys, bins=tbins, statistic=np.mean
									 )[:2]
	yerrs = st.binned_statistic(
						daytime, ys, bins=tbins,
						statistic=lambda z: np.std(z, ddof=1)
					)[0] /\
					np.sqrt(st.binned_statistic(
						daytime, ys, bins=tbins,
						statistic='count'
					)[0])

	return tedges, ymeans, yerrs


def second2hour(arr):
	return np.asarray(arr) / (60*60)


def shift(binned_stats):
	""" shit the return of binned() from 0h-24h to 6h-24h-6h """	

	tedges, ymeans, yerrs = binned_stats
	thours = second2hour(bin_centers(tedges))

	thours = np.concatenate((thours[6:], thours[0:6]+24))
	ymeans = np.concatenate((ymeans[6:], ymeans[0:6]))
	yerrs  = np.concatenate((yerrs[6:], yerrs[0:6]))

	return 	thours, ymeans, yerrs

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def get_pmt_data(run, pos):

	df = run.get_df(pos)[['time', 'rates']]

	df = df.assign(
				# seconds elapsed since midnight
				daytime = [
					(datetime.strptime(d, '%H:%M:%S') - datetime(1900,1,1,0)).seconds
					for d in posix2utc(df['time'].values, fmt='%H:%M:%S')
			 ])

	return df


# cuts = fcn.cuts.select('dtlim')
# trig = True
# run0 = Run.configs(run_v='v24', trig=trig, cuts=cuts)
# run1 = Run.configs(run_v='v25', trig=trig, cuts=cuts)
# run  = ConcatenatedRun(run0, run1)


cuts = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
trig = False

run_v31 = Run.configs(run_v='v31', trig=trig, cuts=cuts)
run_v35 = Run.configs(run_v='v35', trig=trig, cuts=cuts)

df = run_v31.get_df('in_closed')
bint_mean = df['rates'].mean()
bint_err 	= df['rates'].std() / np.sqrt(len(df))
# bint_mean, bint_err = 1.376, 0.194 # this is actually std


keys = ['1110', '0100', '1101', '1000']
colors 	= ['blue', 'grey', 'red', 'black', ]
zorders = [0, 1, 0, 1]
labels 	= [
	'$r_\mathrm{in/open} - r_\mathrm{in/closed}$',
	'$r_\mathrm{out/open} - r_\mathrm{out/closed}$',
	'$r_\mathrm{in/open} - r_\mathrm{out/open}$',
	'$r_\mathrm{in/closed} - r_\mathrm{out/closed}$',
] 



for key, color, label, zorder in zip(keys, colors, labels, zorders):

	df = get_pmt_data(run_v35, key)
	thours, ymeans, yerrs = shift(binned(df, col='rates'))

	ymeans_rel = ymeans / bint_mean
	yerrs_rel  = ymeans_rel * np.sqrt(
								(yerrs / ymeans)**2 + (bint_err / bint_mean)**2
							 )

	plt.errorbar(
		thours, ymeans_rel, yerrs_rel,
		fmt='s', ms=1.5, mew=0, elinewidth=0.6,
		color=color, label=label, zorder=zorder
	)

	# plt.step(
	# 	thours, ymeans_rel, where='mid',
	# 	lw=0.1, color=color, alpha=0.1
	# )


ymin, ymax = plt.ylim()
plt.gca().add_patch(
	Rectangle(
		xy=(6, ymin), 
		width=12, # hours
	  height=ymax-ymin,
	  facecolor='yellow', alpha=.3
))

plt.axhline(y=0, color='#b0b0b0', lw=0.5, ls=':')

plt.xlabel('daily hour/utc')
plt.xticks(
	[6, 10, 14, 18, 22, 26, 30],
	['6h', '10h', '14h', '18h', '22h', '2h', '6h']
)
plt.ylabel('$\Delta r_\mathrm{ev}/b_\mathrm{int}$')

add_toplegend(
	labels=[
	'$r_\mathrm{in/open} - r_\mathrm{out/open}$',
	'$r_\mathrm{in/closed} - r_\mathrm{out/closed}$',
	'$r_\mathrm{in/open} - r_\mathrm{in/closed}$',
	'$r_\mathrm{out/open} - r_\mathrm{out/closed}$',
	],
	ncol= 2, handletextpad=0.1
)



plt.tight_layout(pad=0.05)
plt.savefig('./plt/12/v35_daynight')
plt.close()

# show()
# embed()




# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # !!!! above code has been modifed, so whatever below is likely to break !!!
# plot muons

# def get_muons(muons, label, tperiod=None):
# 	from funk_manip import get_muons

# 	df = get_muons(muons=muons, label=label, tperiod=tperiod)
# 	df = df.assign(
# 		daytime = [ (datetime.strptime(d, '%H:%M:%S') - datetime(1900,1,1,0)).seconds \
# 								for d in posix2utc(df['time'].values, fmt='%H:%M:%S') ]
# 		# seconds elapsed since midnight
# 		)

# 	return df

# muons	= muon_data()
# tperiod_v25 = ( datetime(2018, 3, 2, 14, 52, 49),
# 						  	datetime(2018, 4, 18, 9, 21, 11) )

# tperiod_v32 = ( datetime(2018, 10, 26, 10, 17, 26),
# 								datetime(2018, 11, 14, 9, 28, 39) )


# fig, (ax0, ax1) = plt.subplots(2, 1, sharex=True, figsize=(10,6))


# df = get_muons(muons, 'vcoinc', None)
# bhours, bstats, bstats_err = shift(binned(df))
# ax0.errorbar(
# 	bhours, bstats, bstats_err,
# 	fmt='s', ms=2.2, elinewidth=1,
# 	color=fcn.color['vmuons'], label='vertical coincidence'
# 	)


# df = get_muons(muons, 'hcoinc', None)
# bhours, bstats, bstats_err = shift(binned(df))
# ax1.errorbar(
# 	bhours, bstats, bstats_err,
# 	fmt='s', ms=2.2, elinewidth=1,
# 	color=fcn.color['hmuons'], label='horizontal coincidence'
# 	)


# ax0.spines['bottom'].set_visible(False)
# ax0.tick_params(bottom=False) 
# ax1.spines['top'].set_visible(False)

# d = .01  # how big to make the diagonal lines in axes coordinates
# # arguments to pass to plot, just so we don't keep repeating them
# kwargs = dict(transform=ax0.transAxes, color='k', clip_on=False)
# ax0.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
# ax0.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal
# kwargs.update(transform=ax1.transAxes)  # switch to the bottom axes
# ax1.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
# ax1.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal


# ax1.set_xticks([6, 10, 14, 18, 22, 26, 30])
# ax1.set_xticklabels(['6h', '10h', '14h', '18h', '22h', '2h', '6h'])
# ax1.set_xlabel('daytime [utc]')

# ax0.grid(which='major', ls=':')
# ax1.grid(which='major', ls=':')

# ax0.legend(
# 	*group_legend(ax0, ax1),
# 	bbox_to_anchor=(0., 1.005, 1., .102), loc='lower left',
# 	ncol=2, mode="expand", borderaxespad=0
# 	)
# fig.text(0.01, .7, 'muon rates [Hz]', rotation=90)

# fig.tight_layout()
# fig.subplots_adjust(left=0.09)
# fig.savefig('./3/print/day_night_muons.svg', format='svg')
# show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# plot sensors

# def get_sensors(run):
# 	df = reset_configs(crun.configs)[['time', 'T1', 'T2', 'p']]
# 	df = df.assign(
# 		daytime = [ (datetime.strptime(d, '%H:%M:%S') - datetime(1900,1,1,0)).seconds \
# 								for d in posix2utc(df['time'].values, fmt='%H:%M:%S') ]
# 		# seconds elapsed since midnight
# 		)

# 	return df

# cuts  = fcn.cuts.select('dtlim')
# run0 	= Run.configs(run_v='v24', trig=True, cuts=cuts)
# run1 	= Run.configs(run_v='v25', trig=True, cuts=cuts)
# crun 	= ConcatenatedRun(run0, run1)
# # crun = Run.configs(run_v='v32', trig=True, cuts=cuts)


# fig, ax = plt.subplots()

# df 		= get_sensors(crun)

# label = {
# 	'T1'	: r'T$_1$',
# 	'T2'	: r'T$_2$',
# 	'p'		: r'p'
# }

# bhours, bstats, bstats_err = shift(binned(df, stats='T1'))
# ax.errorbar(
# 	bhours, bstats, bstats_err,
# 	fmt='s', ms=2.2, elinewidth=1, capsize=2,
# 	color=fcn.color['T1'], label=label['T1']
# 	)


# # bhours, bstats, bstats_err = shift(binned(df, stats='T2'))
# # ax.errorbar(
# # 	bhours, bstats, bstats_err,
# # 	fmt='s', ms=2.2, elinewidth=1,
# # 	color=fcn.color['T2'], label=label['T2']
# # 	)


# axr = ax.twinx()
# bhours, bstats, bstats_err = shift(binned(df, stats='p'))
# axr.errorbar(
# 	bhours, bstats, bstats_err,
# 	fmt='s', ms=2.2, elinewidth=1, capsize=2,
# 	color=fcn.color['p'], label=label['p']
# 	)

# ax.set_xticks([6, 10, 14, 18, 22, 26, 30])
# ax.set_xticklabels(['6h', '10h', '14h', '18h', '22h', '2h', '6h'])
# ax.set_xlabel('daytime [utc]')
# ax.set_ylabel('temperatures [Hz]')
# ax.grid(which='major', ls=':')

# axr.set_ylabel('pressures [hPa]')
# axr.legend(
# 	*group_legend(ax, axr),
# 	bbox_to_anchor=(0., 1.005, 1., .102), loc='lower left',
# 	ncol=2, mode="expand", borderaxespad=0
# 	)

# fig.tight_layout()
# fig.savefig('./3/print/v25_day_night_sensors.svg', format='svg')
# show()