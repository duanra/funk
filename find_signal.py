#!usr/bin/env python3

import os
import numpy as np
import numpy.linalg as nalg
import pandas as pd 
import matplotlib.pyplot as plt
import scipy.stats as st
import mod.funk_consts as fcn

from IPython import embed
from matplotlib.patches import Rectangle
from natsort import natsorted
from scipy.interpolate import interp1d

from mod.funk_anal import *
from mod.funk_io import find_all_files
from mod.funk_plt import show, autofmt_xdate, add_toplegend,\
	group_legend, cornertext
from mod.funk_manip import get_muons
from mod.funk_run import Run
from mod.funk_stats import mean_stderr, mean_varn, integer_binning, xcorr
from mod.funk_utils import posix2utc, fn_over

# plt.rcParams.update(fcn.rcDefense)
# plt.rcParams.update(fcn.rcThesis)
plt.rcParams.update(fcn.rcPaper)
# plt.rcParams.update(fcn.rcPoS)

pd.set_option('display.max_rows', 12)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


run 	= Run.configs('v35', trig=False, cuts=SPE)
begin = None # timedelta(hours=5)
Dt 		= None # timedelta(days=5)
data 	= get_data(run, begin, Dt)
data2 = data.copy()

model = TrueSignal.reconstruct(
					data, PMT.alpha, PMT.tau, base=0, # on top of base
					print_level=0 # no verbosity
				)
data 	= data.assign(counts_rec=model.signal)
data  = data.iloc[4*50:] # truncate 50 cycles ~ 3.5 hours


mseq 	= model.seq
ixs 	= dict(map(
					lambda x: (POS[x], slice(mseq.index(x), None, 4)),
					mseq
				))

dt 					= data['dt'].values
time 				= data['time'].values
counts 			= data['counts'].values
counts_rec 	= data['counts_rec'].values
counts_hist = counts - counts_rec


rates, rates_rec, rates_hist, trends, rates_detr = {}, {}, {}, {}, {}

for pos, ix in ixs.items():

	rates[pos] 			= counts[ix] / dt[ix]
	rates_rec[pos] 	= counts_rec[ix] / dt[ix]
	rates_hist[pos] = counts_hist[ix] / dt[ix]
	trends[pos] 		= get_trend(rates_rec[pos], time[ix], fup=6)
	rates_detr[pos]	= rates_rec[pos] - trends[pos]


signal_rec = rates_rec['in_open'] - rates_rec['out_open']
print('*'*80)
print(
	'HP signal [before detrending] : {:.5f} +- {:.5f}'
	.format(*mean_stderr(signal_rec))
)

signal_detr = rates_detr['in_open'] - rates_detr['out_open']
print(
	'HP signal [after detrending]  : {:.5f} +- {:.5f}'
	.format(*mean_stderr(signal_detr))
)

bgdiff_detr = rates_detr['in_closed'] - rates_detr['out_closed']
print(
	'Background diff. (closed sh.) : {:.5f} +- {:.5f}'
	.format(*mean_stderr(bgdiff_detr))
)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


if False:

	plt.figure('reconstruction')
	# plt.figure('reconstruction', figsize=(260/72, 165/72))
	# plt.figure('reconstruction', figsize=(300/72, 165/72))

	ix = slice(1, 9000, 4) # sample from out closed
	m  = 1
	ts = posix2utc(fn_over(time[ix], m))

	plt.plot(
		ts, fn_over(rates['out_closed'], m)[:len(ts)],
		lw=0.5, color='k', label='events')
	plt.plot(
		ts, fn_over(rates_rec['out_closed'], m)[:len(ts)],
		lw=0.5, color='crimson', label='rec. events')
	plt.plot(
		ts, fn_over(rates_hist['out_closed'], m)[:len(ts)],
		lw=0.8, color='#00cd00', label='past contribution')

	autofmt_xdate(hourly=40, datefmt='%b-%d')
	plt.figtext(0.91, 0.02, 2019)
	plt.ylabel('rate/Hz')
	plt.yticks(range(0,10,2))
	plt.ylim(0,9)
	add_toplegend(
		labels=['events', 'rec', 'past'], ncol=3, lw=2,
		# handlelength=1.2, handletextpad=0.5
	)

	show(pad=0.05)
	# show(save='./plt/8/v35_reconstruction_outclosed', pad=0.05)
	# show(save='./plt/12/v35_reconstruction', pad=0.05)
	# show(save='../talks/src/phd_defense/figs/v35_reconstruction', pad=0.05)



if False:

	plt.figure('power spectrum', figsize=(280/72, 175/72))

	pos = 'out_open'
	ix 	= ixs[pos]
	freqs, powers, Fs = get_power_spectrum(rates_rec[pos], time[ix])
	powers_mean = powers.mean()
	powers_std 	= powers.std(ddof=1)
	powers_tmean = st.tmean(powers, limits=(None, powers_mean+powers_std))
	powers_tstd  = st.tstd(powers, limits=(None, powers_mean+powers_std))

	plt.step(freqs, powers, where='mid', color='k', lw=0.5)
	plt.axhline(
		powers_tmean + 2*powers_tstd, ls='--',
		lw=0.8, color='r', zorder=1
	)

	ymin, ymax = plt.ylim(-100, 5)
	plt.gca().add_patch(
		Rectangle(xy=(0,ymin), 
		width= 1/(6*60*60) if hertz2cpd is None else hertz2cpd(1/(6*60*60)), # T=6h
		height=ymax-ymin,
		facecolor='yellow', alpha=0.5, zorder=0
	))

	plt.xlabel('frequency/cycles per day')
	plt.ylabel('power spectrum/dB')


	# # # # # # zoom # # # # #
	plt.tight_layout(pad=0.05)
	bbox = plt.gca().get_position().extents.tolist() # (x0, y0, x1, y1)
	rect = (0.4, 0.74, bbox[2]-0.4, bbox[3]-0.74 ) # (x0, y0, width height)
	plt.gcf().add_axes(rect)

	plt.step(freqs, powers, where='mid', color='k', lw=0.5)
	plt.axhline(
		powers_tmean + 2*powers_tstd, ls='--',
		lw=0.8, color='r', zorder=1
	)

	plt.xticks(fontsize=8)
	plt.yticks([-30, -60], fontsize=8)
	xmin, xmax = plt.xlim(-1e-6, 9)
	ymin, ymax = plt.ylim(-80, -20)

	plt.gca().add_patch(
		Rectangle(
			xy=(0,ymin),
			width= 1/(6*60*60) if hertz2cpd is None else hertz2cpd(1/(6*60*60)),
			height=ymax-ymin,
			facecolor='yellow', alpha=0.5, zorder=0
	))


	show()
	# show(save='./plt/12/v35_power_spectrum')



if False:

	plt.figure('low-freq detrending', figsize=(260/72, 165/72))

	pos = 'out_open'
	ix 	= ixs[pos]
	xs 	= rates_rec[pos]
	ts 	= time[ix]
	trend_low = get_trend(xs, ts, fup=6)
	residual  = get_trend(xs, ts, flow=6)

	xmax = 2250 # 9000/4 (~1week)
	ts 	 = posix2utc(ts[:xmax])
	plt.plot(
		ts, xs[:xmax], color='crimson',
		lw=0.5, label='rec. events')
	plt.plot(
		ts, trend_low[:xmax] + np.mean(xs), color='yellow',
		lw=1, label=r'LF + DC')
	plt.plot(
		ts, residual[:xmax], color='k', # darkslatedgrey
		lw=0.5, label='HF')


	autofmt_xdate(hourly=40, datefmt='%b-%d')
	plt.figtext(0.92, 0.015, 2019)
	plt.ylabel('$r(t)$/Hz')
	plt.ylim(-2,10)
	plt.yticks(range(0, 10, 4))
	add_toplegend(labels=['rec', 'LF', 'HF'], ncol=3, lw=2)

	show(pad=0.05)
	# show(save='./plt/12/v35_detrending', pad=0.05)



if False:

	plt.figure('signal in/out distns', figsize=(200/72, 155/72))
	nbins 	= 100
	hrange 	= (0, 10)
	density = True

	plt.hist(
		rates_detr['in_open'], nbins, hrange, density,
		histtype='step', color='red', lw=1.1,
		label='in/open'
	)

	plt.hist(
		rates_detr['out_open'], nbins, hrange, density,
		histtype='step', color='blue', lw=1.1,
		label='out/open'
	)

	plt.hist(
		rates_rec['in_open'], nbins, hrange, density,
		histtype='step', color='red', lw=0.3,
		label='(not detrended)', zorder=10
	)

	plt.hist(
		rates_rec['out_open'], nbins, hrange, density,
		histtype='step', color='blue',  lw=0.3,
		label='(not detrended)', zorder=10
	)

	plt.xlabel('rate/Hz')
	plt.xticks(range(0,12,2))
	add_toplegend(
		labels=['in', 'out'], ncol=2, handlelength=1.6#1.8
	)

	show(pad=0.05)
	# show(save='./plt/12/signal_InOutDistn', pad=0.05)
	# show(save='../talks/src/phd_defense/figs/signal_InOutDistn', pad=0.05)



if False:

	plt.figure('detrending effect')
	nbins 	= 100
	hrange 	= (0, 10)
	density = True

	xs = rates_rec['out_open']
	ts = time[ixs['out_open']]

	for fup in range(0, 10):

		trend 	= get_trend(xs, ts, fup=fup)
		xs_detr = xs - trend
		print(trend.mean())

		mu, std = xs_detr.mean(), xs_detr.std(ddof=1)

		plt.hist(
			xs_detr, nbins, hrange, density, histtype='step',
			label='fup={:d}, $\mu={:.4f}$, $\sigma={:.4f}$'
						.format(fup, mu, std)
		)

	# plt.legend()
	show(pad=0.05)



if False:
	# systematics check

	r = np.abs(np.mean(rates['in_open'] - rates['out_open']))

	T = data2['T1'].values
	Tout 	= (T[ixs['out_open']] + T[ixs['out_closed']])/2
	Tin 	= (T[ixs['in_open']] + T[ixs['in_closed']])/2
	Tdiff = Tin - Tout
	Tdiff_mu 	= Tdiff.mean()
	Tdiff_std = Tdiff.std(ddof=1)

	Tcoeff = 0.03 # Hz/Celcius
	print(Tdiff_mu, Tdiff_std)
	print(Tdiff_std*Tcoeff/r*100)
	print(Tdiff_mu*Tcoeff/r*100)

	print()
	P = data2['p'].values
	Pout 	= (P[ixs['out_open']] + P[ixs['out_closed']])/2
	Pin 	= (P[ixs['in_open']] + P[ixs['in_closed']])/2
	Pdiff = Pin - Pout
	Pdiff_mu 	= Pdiff.mean()
	Pdiff_std = Pdiff.std(ddof=1)
	Pcoeff = 0.508e3 # KHz/hPa
	ref = 300e3

	print(Pdiff_mu, Pdiff_std)
	print(Pdiff_std*Pcoeff/r*100)
	print(Pdiff_mu*Pcoeff/r*100)


embed()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# apply reconstruction on MC-reflection-corrected dataset, but no detrending 
# the following is not totally correct: the excess should not be removed before
# historical reconstruction because it contains both instantaneous 
# reflections and the historical parts

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# MC_files = natsorted(find_all_files(
# 						'./data/MC_noreflections', pattern='norefl'
# 					 ))
# signal_hp_norefl = []


# for fname in MC_files[:10]:

# 	print('file', fname)
# 	data_norefl  = pd.read_pickle(fname).reset_index(drop=True)

# 	model_norefl = TrueSignal.reconstruct(
# 									data_norefl, PMT.alpha, PMT.tau, base=0,
# 									print_level=0
# 								 )

# 	data_norefl  = data_norefl.assign(counts_rec=model_norefl.signal)

# 	# truncate 50 cycles ~ 3.5 hours and add env data
# 	data_norefl  = data_norefl.iloc[4*50:]
# 	data_norefl  = data_norefl.assign(
# 									timedelta=data['timedelta'].values,
# 									dt=data['dt'].values,
# 									T1=data['T1'].values,
# 									T2=data['T2'].values,
# 									p=data['p'].values,
# 								 )

# 	rates_norefl_rec = data_norefl['counts_rec'].values \
# 											/ data_norefl['dt'].values

# 	signal_hp_norefl.append((
# 		rates_norefl_rec[ixs['in_open']] - rates_norefl_rec[ixs['out_open']]
# 	).mean())

# signal_hp_norefl = np.asarray(signal_hp_norefl)


# plt.figure()
# plt.hist(
# 	signal_hp_norefl, 100, None, True,
# 	color='b', histtype='step',
# 	label='$\mu={:4f}$'.format(signal_hp_norefl.mean())
# )
# plt.axvline(
# 	signal_hp.mean(), color='r',
# 	label='$\mu={:4f}$'.format(signal_hp.mean())
# )
# plt.xlabel('signal rate / Hz')
# plt.locator_params(axis='x', nbins=5)
# plt.ylabel('pdf')
# plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
# plt.legend(loc='upper center')
# plt.tight_layout(pad=0)
# plt.subplots_adjust(top=0.9)
# # plt.savefig('plt/plot4')
# # show()

# embed()

# # frequency of corrections (to be further checked)
# plt.figure()
# d1 	= data['counts'].values[ixs['in_open']]
# d2 	= data_norefl['counts'].values[ixs['in_open']]
# dtc = np.diff(np.where(d1!=d2)[0]) * 4 # min
# nbins, xlow, xup = integer_binning(min(dtc), max(dtc))
# plt.hist(dtc, nbins, (xlow, xup))
# plt.xticks(range(0, int(xup)+4, 4))
# plt.yscale('log')
# plt.tight_layout(pad=0)
# # plt.savefig('plt/plot5')
# show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# see reflections.nb for the calculations
# here dx2 is the excess in instantaneous count. at worst this equals y2-y0

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# dx2 = 0.0032 
# data2.loc[0::4, 'counts'] = data2.loc[0::4, 'counts'] - PMT.alpha/4*dx2
# data2.loc[1::4, 'counts'] = data2.loc[1::4, 'counts'] - PMT.alpha/4*dx2
# data2.loc[2::4, 'counts'] = data2.loc[2::4, 'counts'] - (1+PMT.alpha/4)*dx2
# data2.loc[3::4, 'counts'] = data2.loc[3::4, 'counts'] - PMT.alpha/4*dx2

# model2 = TrueSignal.reconstruct(
# 					data2, PMT.alpha, PMT.tau, base=0,
# 					print_level=0
# 				)
# data2  = data2.assign(counts_rec=model2.signal)
# data2  = data2.iloc[4*50:] # truncate 50 cycles ~ 3.5 hours

# print(np.mean(
# 	(data2['counts_rec'].values[2::4] - data2['counts_rec'].values[0::4])
# ))

# print('*'*80)

# embed()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# # some correlation checks

# T1 	 	 = data['T1'].values
# p  	 	 = data['p'].values
# muons  = get_muons(tperiod=run.period, timestamps=time)

# at 		 = 'out_open'
# y  		 = rates_rec[ixs[at]]

# plt.figure('vs_muons')
# x  		 = muons['rates'][ixs[at]]
# bins_x = 50
# bins_y = 50

# profile_plot(x, y, bins_x, bins_y, legend=True)
# plt.xlabel('muon rates [Hz]')
# plt.ylabel('event rates [Hz]')


# plt.figure('vs_pressures')
# x  		 = p[ixs[at]]
# bins_x = 50
# bins_y = 50

# profile_plot(x, y, bins_x, bins_y, legend=True)
# plt.xlabel('pressure [hPa]')
# plt.ylabel('event rates [Hz]')

# show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# # correlation: counts_hist at the end of each cycle vs. average temperature 

# utc = posix2utc(fn_over(data['time'].values, m=4))
# y1 	= fn_over(data['T2'].values, m=4)

# # muons  = get_muons(tperiod=run.period, timestamps=time)
# # y1  = fn_over(muons['rates'], m=4)

# y2 	= fn_over(counts_hist, m=4, fn=np.mean)
# dt 	= 4.4
# tdummy = dt * np.arange(len(y1))


# maxlags = 400
# tlags, correls = xcorr(y1, y2, dt, maxlags) # shift temperature
# imax = np.argmax(correls) # look for correl, not anticorrel
# tlag_max = tlags[imax]
# correl_max = correls[imax]
# print(tlag_max, correl_max)


# plt.figure('xcorr', figsize=(184/72, 170/72))

# plt.scatter(tlags/60, correls, s=1, marker='+', color='#ff7f0e')
# # plt.axvline(tlag_max/60, ls='--', lw=1, color='#b0b0b0')
# plt.xlabel(r'$t_\mathrm{lag}$/h')
# plt.ylabel(r'$\kappa_{\tilde{r}, T_2}$')

# plt.tight_layout(pad=0.05)
# plt.subplots_adjust(left=0.2)
# plt.savefig('./plt/12/history_xcorrelation.pdf')
# plt.close()


# plt.figure('noshift', figsize=(260/72, 165/72))
# yinterp1 = interp1d(tdummy, y1, bounds_error=False, fill_value=np.nan)
# yy1 = yinterp1(tdummy + 0)#+ tlag_max)

# plt.plot(
# 	utc, y2/60, lw=0.5, color='mediumblue',
# 	label='historical events'
# )
# # plt.plot(utc, counts_rec[0::4]/60, lw=0.5,zorder=0)
# plt.ylabel(r'$\tilde{r}$/Hz')
# autofmt_xdate(daily=7, datefmt='%b-%d')

# plt.twinx()
# plt.plot(
# 	utc, yy1, lw=0.5,  color='olive',
# 	label='temperature'
# )
# plt.ylabel('$T_2/^\circ$C')

# add_toplegend(
# 	labels=['hist', 'temp'], ncol=2, lw=2,
# 	lgd=group_legend(*plt.gcf().get_axes())
# )
# autofmt_xdate(daily=7, datefmt='%b-%d')
# plt.figtext(0.78, 0.02, 2019)

# plt.tight_layout(pad=0.05)
# plt.subplots_adjust(left=0.1)
# # plt.savefig('./plt/12/history_correlnoshift.pdf')
# # plt.close()

# show()

# yinterp1 = interp1d(
# 							t, y1, kind='quadratic',
# 							bounds_error=False, fill_value=0
# 					 ) 
# yinterp2 = interp1d(
# 							t, y2, kind='quadratic',
# 							bounds_error=False, fill_value=0
# 					 ) 
# yy1 = yinterp1(t)
# yy2 = yinterp2(t + ilag)
# ix 	= (yy1*yy2 != 0)
# plt.scatter(y1, y2)
# plt.scatter(yy1[ix], yy2[ix])


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
 
# n  = len(signal_hp)
# dn = n // 10
# ts = list(range(dn, n+dn, dn))
# xs, xerrs = [], []
# ys, yerrs = [], []

# for k in ts:
# 	x, xerr = mean_stderr(signal_hp[:k])
# 	xs.append(x)
# 	xerrs.append(xerr)

# 	y, yerr = mean_stderr(bg_diff[:k])
# 	ys.append(y)
# 	yerrs.append(yerr)

# ts = np.array(ts)/n
# plt.errorbar(
# 	ts, xs, xerrs, color='r', capsize=3, marker='o', ms=5,
# 	label='in/open - out/open'
# )
# plt.errorbar(
# 	ts, ys, yerrs, color='b', capsize=3, marker='o', ms=5,
# 	label='in/closed - out/closed'
# )
# plt.axhline(0, color='grey', ls=':')
# plt.ylabel('rate/Hz')
# plt.xlabel('fraction of data')
# plt.legend()
# # plt.savefig('./plt/rate_difference.png', dpi=300)
# show()

# # diffferential 
# n  = len(signal_hp)
# dn = n // 20
# xs, xerrs = [], []
# ys, yerrs = [], []

# for i in range(20):
# 	x, xerr = mean_stderr(signal_hp[i*dn:(i+1)*dn])
# 	xs.append(x)
# 	xerrs.append(xerr)

# 	y, yerr = mean_stderr(bg_diff[i*dn:(i+1)*dn])
# 	ys.append(y)
# 	yerrs.append(yerr)

# ts = [r'$t_{%d}$'%i for i in range(20)]
# plt.errorbar(
# 	ts, xs, xerrs, color='r', capsize=3, marker='o', ms=5,
# 	label='in/open - out/open', ls='None'
# )
# plt.errorbar(
# 	ts, ys, yerrs, color='b', capsize=3, marker='o', ms=5,
# 	label='in/closed - out/closed', ls='None'
# )
# plt.axhline(0, color='grey', ls=':')
# plt.ylabel('rate/Hz')
# plt.xlabel('time bins')
# plt.legend()
# # plt.savefig('./plt/rate_difference3.png', dpi=300)
# show()


# plt.figure(figsize=(5.5,5.5))
# plt.scatter(ys, xs, s=40, marker='x', color='k')
# plt.xlim(-0.30, 0.30)
# plt.ylim(-0.30, 0.30)
# plt.xlabel('in/closed - out/closed [Hz]')
# plt.ylabel('in/open - out/open [Hz]')
# cornertext(
# 	'pearson correl = %.2f'%pearsonr(xs, ys)[0],
# 	frameon=True)
# plt.axhline(0, color='grey', ls=':')
# plt.axvline(0, color='grey', ls=':')
# # plt.savefig('./plt/rate_difference4.png', dpi=300)
# show()



# plt.figure(figsize=(5.5,5.5))
# plt.errorbar(
# 	ys, xs, yerr=xerrs, xerr=yerrs, color='k', marker='o', ms=5,
# 	ls='None', alpha=0.8
# )
# plt.xlim(-0.30, 0.30)
# plt.ylim(-0.30, 0.30)
# plt.xlabel('in/closed - out/closed [Hz]')
# plt.ylabel('in/open - out/open [Hz]')
# cornertext(
# 	'pearson correl = %.2f'%pearsonr(xs, ys)[0],
# 	frameon=True)
# plt.axhline(0, color='grey', ls=':')
# plt.axvline(0, color='grey', ls=':')
# # plt.savefig('./plt/rate_difference5.png', dpi=300)
# show()


# embed()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # old resolution method (maybe still usefull)

# def get_Bint():

# 	run = Run.configs(run_v='v31', trig=False, cuts=SPE)
# 	df  = run.get_df('in_closed')[['time', 'rates']]

# 	return df['rates'].mean(), df['rates'].std() / np.sqrt(len(df))


# def get_matrices(rates, trends=None):

# 	seq 	 = ['in_open', 'in_closed', 'out_open', 'out_closed']		

# 	a_base = np.array([
# 							[1, 1, 0,  0],
# 							[0, 1, 0, -1],
# 							[0, 0, 1,  0],
# 							[0, 0, 1, -1]
# 							], dtype=np.float)

# 	b_base = np.array([1, 0, 1, 0], dtype=np.float)


# 	rs, varns 	 	 = np.zeros(4), np.zeros(4)
# 	Bint, Bint_err = get_Bint()
# 	if trends is None:
# 		trends = np.zeros_like(rates)


# 	for i, pos in enumerate(seq):

# 		tseries  = rates[ixs[pos]] - trends[ixs[pos]]
# 		rs[i] 	 = np.mean(tseries) - Bint
# 		varns[i] = np.var(tseries, ddof=1) / len(tseries) + Bint_err**2

# 	a, b 		 = a_base.copy(), b_base.copy()
# 	a[:,-1] *= rs
# 	b[:] 		*= rs

# 	a_varns, b_varns  = np.abs(a_base.copy()), b_base.copy()
# 	a_varns[:,:-1] *= 0 # constants
# 	a_varns[:,-1]	 *= varns
# 	b_varns[:]		 *= varns

# 	return a, b, a_varns, b_varns


# def variance_inverse(m_inv, m_varn):
# 	# http://cds.cern.ch/record/400631/files/9909031.pdf (eq. 10)
# 	m_inv2 = m_inv**2

# 	return nalg.multi_dot([m_inv2, m_varn, m_inv2])


# def solve_rates(a, b, a_varns, b_varns):

# 	a_inv 		 	= nalg.inv(a)
# 	a_inv_varns = variance_inverse(a_inv, a_varns)

# 	solns 			= np.dot(a_inv, b)
# 	solns_varns	= np.dot(a_inv**2, b_varns) + np.dot(a_inv_varns, b**2)

# 	return solns, np.sqrt(solns_varns)


# def print_results(solns, solns_err):
# 	""" solns and solns_err are from average_daily_solns() if daily """ 

# 	table 	= [
# 		['signal', solns[0], solns_err[0]],
# 		['Bair_in', solns[1], solns_err[1]],

# 		['Bair_out', solns[2], solns_err[2]],
# 		['shutter', 1 - 1/solns[3], solns_err[3] / solns[3]**2]
# 		]

# 	print(tabulate(table, tablefmt='grid', numalign='right'))


# a, b, a_varns, b_varns = get_matrices(rates_rec, trends=None)
# solns, solns_err = solve_rates(a, b, a_varns, b_varns)
# print_results(solns, solns_err)