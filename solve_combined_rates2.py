#!usr/bin/env python3
""" combine inputs, share both signal and shutter efficiency """

import numpy as np
import numpy.linalg as nalg
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from copy import deepcopy
from datetime import datetime, timedelta
from sys import exit
from mod.funk_plt import pause, show, order_legend, autofmt_xdate
from mod.funk_run import Run, CombinedRun
from mod.funk_utils import posix2utc, utc2posix, fmt, table_lr

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def get_Bint(**kwargs):
	""" kw options are spu, trig and cuts """

	run = Run.configs(run_v='v31', **kwargs)
	df  = run.get_df('in_closed')[['time', 'rates']]

	return len(df), df['rates'].mean(), df['rates'].std()


def get_matrices(Bint, *runs):

	seq = ['in_open', 'in_closed', 'out_open', 'out_closed']

	def _base(m, n):
		""" build base matrix """
		base_a = np.array([
								[1, 0, 1,  0],
								[1, 0, 0, -1],
								[0, 1, 0,  0],
								[0, 1, 0, -1]
								], dtype=np.float)

		base_b = np.array([1, 0, 1, 0], dtype=np.float)

		if (m==n==4):
			return base_a, base_b

		else: # (n<m>4):
			a = np.zeros((m, n))
			for i, j in zip(range(0, m, 4), range(0, n, 2)):
				a[i:i+4, j:j+2] = base_a[:4, :2].copy()
			a[0::4,-2] = 1		
			a[1::2,-1] = -1

			b = np.zeros(m)
			for i in range(0, m, 4):
				b[i:i+4] = base_b.copy()

			return a, b


	nruns = len(runs)
	if nruns==1:
		run  	= runs[0]
		m, n 	= 4, 4
		a, b 	= _base(m, n)
		var_a, var_b 	= _base(m, n)
		var_a 				= np.abs(a)
		var_a[:,:-1] *= 0
		
		rates = np.array([
							run.meanrates[pos] - Bint[1] \
							for pos in seq] )
		varns = np.array([
							run.varmeans[pos] + Bint[2]**2 / Bint[0] \
							for pos in seq] )

		a[:,-1] 		 *= rates
		b[:]				 *= rates

		var_a[:, -1] *= varns
		var_b[:]		 *= varns


	else: # split and lsq, use solve_combined_rates.py for wmean
		m, n 	= 4*nruns, 2*nruns+2
		a, b 	= _base(m, n)
		var_a, var_b 	= _base(m, n)
		var_a 				= np.abs(a)
		var_a[:,:-1] *= 0

		for i, run in zip(range(0, m, 4), runs):
			rates = np.array([
								 run.meanrates[pos] - Bint[1] \
								 for pos in seq] )
			varns = np.array([
								 run.varmeans[pos] + Bint[2]**2 / Bint[0] \
								 for pos in seq] )
			
			a[i:i+4,-1] 		*= rates
			b[i:i+4]				*= rates
			
			var_a[i:i+4,-1] *= varns
			var_b[i:i+4]	  *= varns


	return a, b, var_a, var_b


def solve_rates(a, b, var_a, var_b, method='auto'):
	""" method: auto, inverse or lsq """

	if method=='auto': method = ('inverse' if a.shape==(4,4) else 'lsq')

	def _var_inv(mInv, var_m):
		return nalg.multi_dot([mInv**2, var_m, mInv**2])

	def _var_aTa(a, var_a):
		shape = a.shape
		ret 	= np.zeros((shape[1], shape[1]))

		for i in range(shape[1]):
			for j in range(shape[1]):
				for k in range(shape[0]):
					if i==j:
						ret[i,j] += (2*a[k,i])**2 * var_a[k,i]
					else:
						ret[i,j] += var_a[k,i] * a[k,j]**2 +\
												a[k,i]**2  * var_a[k,j]

		return ret

	if method=='inverse':
		print('[Info] solve_rates(inverse)')

		aInv 		 	= nalg.inv(a)
		var_aInv 	= _var_inv(aInv, var_a)

		solns 		= np.dot(aInv, b)
		var_solns = np.dot(aInv**2, var_b) + np.dot(var_aInv, b**2)

		residuals = None

	elif method=='lsq':
		print('[Info] solve_rates(lsq)')

		aT   	 		 	= np.transpose(a)
		aTa 	 		 	= np.dot(aT, a)
		aTaInv 		 	= nalg.inv(aTa)

		var_aT 	 	 	= np.transpose(var_a)
		var_aTa 	 	= _var_aTa(a, var_a)
		var_aTaInv	= _var_inv(aTaInv, var_aTa)

		solns			  = nalg.multi_dot([aTaInv, aT, b])
		var_solns	 	= nalg.multi_dot([var_aTaInv, aT**2, b**2]) +\
								 	nalg.multi_dot([aTaInv**2, var_aT, b**2]) +\
								 	nalg.multi_dot([aTaInv**2, aT**2, var_b])

		residuals 	= b - np.dot(a, solns)

	else:
		print('[Exception] solve_rates(): check method=arg')
		exit(0)

	return solns, var_solns, residuals


cuts  = fcn.cuts.select('dtlim')
run1  = Run.configs(run_v='v24', trig=True, cuts=cuts)
run2  = Run.configs(run_v='v25', trig=True, cuts=cuts)
Bint 	= get_Bint(trig=True, cuts=cuts)

a, b, var_a, var_b = get_matrices(Bint, run1, run2)
# np.set_printoptions(linewidth=150)
# print(var_a, '\n')
# print(var_b, '\n')
solns, var_solns, residuals = solve_rates(a, b, var_a, var_b)
print(solns)
print(np.sqrt(var_solns))