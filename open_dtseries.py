#!usr/bin/env python3

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from datetime import timedelta
from matplotlib import rc
from mod.funk_plt import show
from mod.funk_run import Run
from mod.funk_utils import posix2utc, fn_over

from IPython import embed
rc('font', size=8)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def get_data(run, pos):
	
	if run.run_v=='v34':
		# merge adjacent timestamp for open/open

		df = run.get_df(pos)[['time', 'dt', 'counts']].reset_index(drop=True)

		time 	 = df['time'].iloc[1::2].values # use the second timestamp

		dt 	 	 = fn_over(df['dt'].values, fn=np.sum, m=2, on_error=True)
		counts = fn_over(df['counts'].values, fn=np.sum, m=2, on_error=True)
		rates  = counts / dt

		df = pd.DataFrame({
							'time'	: time,
							'dt' 		: dt,
							'counts': counts,
							'rates' : rates
							})

		df = df.assign(
							timedelta = posix2utc(df['time']) -\
													posix2utc(df['time'].iloc[0]) +\
													timedelta(minutes=2)
							# note: this is relative to the starting of the measurement at pos
							)

	else:

		df = run.get_df(pos)[['time', 'rates']].reset_index(drop=True)

		df = df.assign(
							timedelta = posix2utc(df['time']) - \
													posix2utc(df['time'].iloc[0])
							# note: this is relative to the starting of the measurement at pos
							)


	return df


def get_signal(run):

	hduration = int(np.ceil(run.duration.total_seconds() / (60 * 60)))
	dtseries 	= [timedelta(hours=6+h) for h in range(0, hduration, 6)]

	positions = ['in_open', 'out_open']
	data 			= {pos : get_data(run, pos) for pos in positions}
	n 				= len(dtseries)
	signal 		= np.zeros((n,2))

	df_in  	= data['in_open']
	df_out 	= data['out_open']
	df_diff	= pd.concat([
							df_in[['rates']] - df_out[['rates']],
							df_out['time'],
							df_out['timedelta']
							], axis=1)

	for i, dt in enumerate(dtseries):

		subdf = df_diff[ df_diff['timedelta']<=dt ]

		signal[i,0] = subdf['rates'].mean()
		signal[i,1] = subdf['rates'].std() / np.sqrt(len(subdf))


	mdtseries = [dt.total_seconds() / (60*60*24) for dt in dtseries] # in days

	return mdtseries, signal



def add_errorband(X, ax=None, **kwargs):
	kwargs.setdefault('alpha', 0.3)
	kwargs.setdefault('lw', 0)

	if ax is None: ax = plt.gca()

	ax.fill_between(mdtseries,
									X[:,0] - X[:,1],
									X[:,0] + X[:,1], **kwargs)



# cuts 	= fcn.cuts.select('dtlim')
cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
trig 	= False

run_v25 = Run.configs('v25', trig=trig, cuts=cuts)
run_v32 = Run.configs('v32', trig=trig, cuts=cuts)
run_v34 = Run.configs('v34', trig=trig, cuts=cuts)


fig, ax 	= plt.subplots(figsize=(10,6))


mdtseries, signal = get_signal(run_v25)
ax.plot(mdtseries, signal[:,0], color='g', lw=0.8, label='v25')
add_errorband(signal, color='g')
reflected = 0.49e-2 * run_v25.get_df('in_open')['rates'].mean()
ax.vlines(mdtseries[-1], 0, reflected, color='k', lw=1.5)


mdtseries, signal = get_signal(run_v32)
ax.plot(mdtseries, signal[:,0], color='b', lw=0.8, label='v32')
add_errorband(signal, color='b')
reflected = 0.64e-2 * run_v32.get_df('in_open')['rates'].mean()
ax.vlines(mdtseries[-1], 0, reflected, color='k', lw=1.5)


mdtseries, signal = get_signal(run_v34)
ax.plot(mdtseries, signal[:,0], color='r', lw=0.8, label='v34')
add_errorband(signal, color='r')
reflected = 0.26e-2 * run_v34.get_df('in_open')['rates'].mean()
ax.vlines(mdtseries[-1], 0, reflected, color='k', lw=1.5)


ax.axhline(0, color='k', lw=1.5, ls='--')
ax.grid(which='major', ls=':')
ax.tick_params(right=True, labelright=True)
ax.set_xlabel('duration (days)')
ax.set_ylim(-0.2, 0.3)
ax.set_ylabel('signal (open-open) [Hz]')
ax.legend(loc='upper right')

fig.tight_layout()
# fig.savefig('./4/0.pdf', format='pdf')
show()