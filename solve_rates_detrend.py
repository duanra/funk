#!usr/bin/env python3

import numpy as np
import pandas as pd
import numpy.linalg as nalg
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from copy import deepcopy
from datetime import datetime, timedelta
from scipy.interpolate import interp1d
from sys import exit
from mod.funk_plt import pause, show, order_legend, autofmt_xdate
from mod.funk_run import Run, ConcatenatedRun
from mod.funk_utils import posix2utc, utc2posix, fmt, table_lr, pickle_load,\
													 Namespace

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def get_Bint(**kwargs):
	""" kw options are spu, trig and cuts """

	run = Run.configs(run_v='v31', **kwargs)
	df  = run.get_df('in_closed')[['time', 'rates']]

	return len(df), df['rates'].mean(), df['rates'].std()


def get_trend(run, df_trends, fcut):

	idxcut  = np.argmin(np.abs( df_trends['fcut'].values - fcut ))
	# print(df_trends['fcut'].iloc[idxcut])
	
	ifft_lf = interp1d(
							df_trends['time'].iloc[idxcut],
							df_trends['ifft_lf'].iloc[idxcut],
							kind='quadratic',
							fill_value='extrapolate')

	ret = {}
	for pos in ['in_open', 'in_closed', 'out_open', 'out_closed']:

		time 		 = run.get_df(pos)['time'].values
		ret[pos] = pd.DataFrame({'time': time, 'rates' : ifft_lf(time)})

	return ret


def get_matrices(run, Bint, trend=None):

	seq = ['in_open', 'in_closed', 'out_open', 'out_closed']

	base_a = np.array([
							[1, 1, 0,  0],
							[0, 1, 0, -1],
							[0, 0, 1,  0],
							[0, 0, 1, -1]
							], dtype=np.float)

	base_b = np.array([1, 0, 1, 0], dtype=np.float)

	rates, varns = np.zeros(4), np.zeros(4)

	for i, pos in enumerate(seq):
		tseries  	= run.get_df(pos)['rates'].reset_index(drop=True)

		if trend is not None:
			tseries -= trend[pos]['rates']

		rates[i] 	= tseries.mean() - Bint[1]
		varns[i] 	= tseries.var() / len(tseries) + Bint[2]**2 / Bint[0]

	a, b 		 = base_a.copy(), base_b.copy()
	a[:,-1] *= rates
	b[:] 		*= rates

	var_a, var_b  = np.abs(base_a.copy()), base_b.copy()
	var_a[:,:-1] *= 0 # constants
	var_a[:,-1]	 *= varns
	var_b[:]		 *= varns

	return a, b, var_a, var_b


def solve_rates(a, b, var_a, var_b):

	def _var_inv(mInv, var_m):
		return nalg.multi_dot([mInv**2, var_m, mInv**2])

	aInv 		 	= nalg.inv(a)
	var_aInv 	= _var_inv(aInv, var_a)

	solns 		= np.dot(aInv, b)
	var_solns = np.dot(aInv**2, var_b) + np.dot(var_aInv, b**2)

	return solns, var_solns


def print_results(solns, var_solns, residuals=None):
	""" solns and var_solns are from average_daily_solns() if daily """ 

	def _fmt(x, error_x):
		return '{:.5f} +- {:.5f}'.format(x, error_x)

	eps_bar 			= 1 - 1 / solns[-1]
	error_eps_bar	= np.sqrt(var_solns[-1]) / solns[-1]**2
	
	if residuals is not None:
		residual2 = nalg.norm(residuals)**2
	else:
		residual2 = None

	left_col 	= [
			'signal',
			'Bair_in',
			'Bair_out',
			'shutter',
			'residual2'
			]	

	right_col = [
			_fmt(solns[0], np.sqrt(var_solns[0])),
			_fmt(solns[1], np.sqrt(var_solns[1])),
			_fmt(solns[2], np.sqrt(var_solns[2])),
			_fmt(eps_bar, error_eps_bar),
			fmt(residual2)
			]

	print(table_lr(left_col, right_col, hlines=True))


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# cuts = fcn.cuts.select('dtlim')
cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
trig 	= False
run_v = 'v32'
run 	= Run.configs(run_v, trig=trig, cuts=cuts)


# # cuts = fcn.cuts.select('dtlim')
# cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
# trig 	= False
# run_v = 'v25'
# run0 	= Run.configs(run_v='v24', trig=trig, cuts=cuts)
# run1 	= Run.configs(run_v='v25', trig=trig, cuts=cuts)
# run  	= ConcatenatedRun(run0, run1)


Bint  		= get_Bint(trig=trig, cuts=cuts)
df_trends = pickle_load('{}_trends.sdf'.format(run_v))
trend 		= None #get_trend(run, df_trends, fcut=8)


a, b, var_a, var_b = get_matrices(run, Bint, trend=trend)
solns, var_solns = solve_rates(a, b, var_a, var_b)
print_results(solns, var_solns)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# print(crun.run_v)
# print(crun.duration)
# print(run0.mseq)
# print(run1.mseq)
# a, b, var_a, var_b = get_matrices(crun, Bint, option='umean')
# solns, var_solns, _= solve_rates(a, b, var_a, var_b, method='inverse')
# print_results(solns, var_solns)