#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed

from mod.funk_anal import profile_plot
from mod.funk_plt import show
from mod.funk_run import Run
from mod.funk_stats import unique_binning

# plt.rcParams.update(fcn.rcPaper)
# plt.rcParams['figure.figsize'] = (205/72, 148/72)

plt.rcParams.update(fcn.rcPaper)
pd.set_option('display.max_rows', 10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

cuts = fcn.cuts.select('dtlim' , 'riselim', 'Slim', 'tsiglim')
trig = False

run_v31  = Run.configs('v31', trig=trig, cuts=cuts)
df = run_v31.get_df('in_closed')[50:]
y  = df['rates'].values

plt.figure('vs_T1')
ix = (y<3) # Hz
x  = df['T1'].values[ix]
y  = y[ix]

bins_x = unique_binning(x)
bins_y = 30
profile_plot(x, y, bins_x, bins_y)


plt.xticks(np.arange(18.8, 20, 0.2))
plt.yticks(np.arange(1,3,0.5))
plt.ylim(0.5, 3)

plt.xlabel('temperature/$\mathrm{^\circ C}$')
plt.ylabel('rate/Hz')
# plt.xlabel(r'$T_1/\mathrm{^\circ C}$')
# plt.ylabel(r'$b_\mathrm{int}$/Hz')


plt.tight_layout(pad=0.05)
plt.subplots_adjust(left=0.165)
plt.savefig('./plt/12/internalBg_vs_T1')
# plt.savefig('./plt/12/v31_temperatureCoeff')
# plt.close()

show()
embed()