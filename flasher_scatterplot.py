""" use for flasher l-scan """
#!usr/bin/env python3

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from matplotlib.colors import LogNorm
from mod.funk_run import Flasher
from mod.funk_plt import PlotIndexer, setup_scatterplot, show

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

NFLASHES = 5000

def get_df(dic, key):
	
	df = dic[key].copy()

	df['h'] 	 = 10**df['h'].values
	df['q'] 	 = 10**df['q'] / 1e8 
	df['tvar'] = np.sqrt(df['tvar'].values)
	df.rename(
		columns={'h': 'H', 'q': 'Q', 'tvar': 'tsig'},
		inplace=True
	)

	return df


def print_stats(df):
	""" df should correspond to one rootfile """

	gb 		= df.groupby('event')
	ntrig = gb.head(1)['pulse'].sum()
	npu 	= gb.size().sum()

	ncaptures = len(df['event'].unique())
	nspu = len(df['event'].drop_duplicates(keep=False))
	ntot = len(df) # pulses 
	print('total captures: %d, total pulses: %d'%(ncaptures, ntot))
	print('relative ncaptures: {:.2%}'.format(ncaptures/NFLASHES))
	print('relative nspu: {:.2%}'.format(nspu/ncaptures))



class iPlot(PlotIndexer):

	def __init__(self, X, i, dic, labels, figsize=(10,10)):

		super(iPlot, self).__init__(X, i, loop=False, direc=0)

		grid   = 2*(len(labels), )
		labels_grid = np.array([
										'%s-%s'%(a,b) for a in labels for b in labels
									]).reshape(*grid).T

		fig, axes, caxes = setup_scatterplot(grid, figsize)
		
		ki = X[i]
		df = get_df(dic, ki)
		print('::%s'%ki)
		print_stats(df)

		self.dic 	 = dic
		self.grid  = grid
		self.labels_grid = labels_grid

		self.fig 	 = fig
		self.axes  = axes
		self.caxes = caxes

		self.scatterplot(df)
		self.fig.canvas.set_window_title(ki)


	def scatterplot(self, df):
		
		for (i,j), label in np.ndenumerate(self.labels_grid):

			if j > i: # upper triangle
				continue

			elif j == i:
				ax = self.axes[(i,i)]
				
				xvar, _ = label.split('-')
				xs 	= df[xvar].values
				xpp = plot_props[xvar]

				ax.hist(
					xs, 100, xpp['histrange'], False,
					histtype='step', color='k', lw=1.5
				)
				
				ax.set_xlim(*xpp['axislim'])
				ax.set_xticks(xpp['axisticks'])
				
				ax.set_ylabel('histogram', labelpad=12, rotation=-90)
				ax.set_ylim(0, 1e3)
				# ax.set_yticks(ax.get_yticks()[1:-1])
				ax.yaxis.set_label_position('right')
				ax.ticklabel_format(
					axis='y', style='sci', scilimits=(0,0), useMathText=True)
				ax.yaxis.get_offset_text().set_position((0.84,0))	

				if i == self.grid[0]-1: # last row
					ax.set_xlabel(xpp['label'])			

			else: # j<i
				ax 	= self.axes[(i,j)]
				cax = self.caxes[(i,j)]

				xvar, yvar = label.split('-')
				xs, ys 		 = df[xvar].values, df[yvar].values
				xpp, ypp	 = plot_props[xvar], plot_props[yvar]

				h2d = ax.hist2d(
								xs, ys, bins=100, range=(xpp['histrange'], ypp['histrange']),
								cmap='viridis_r', norm=LogNorm(vmax=1e2)
							)
				cbar = ax.figure.colorbar(
								mappable=h2d[3], cax=cax, orientation='vertical',
								ticklocation='left'
							 )
				# cbar.ax.tick_params(which='minor', direction='in')

				ax.set_xlim(*xpp['axislim'])
				ax.set_xticks(xpp['axisticks'])

				ax.set_ylim(*ypp['axislim'])
				ax.set_yticks(ypp['axisticks'])

				if i == self.grid[0]-1: # last row
					ax.set_xlabel(xpp['label'])

				if j==0: # first column
					ax.set_ylabel(ypp['label'])

		print('::plot updated')


	def action(self):

		for k, ax in self.axes.items():
			ax.clear()
		for k, cax in self.caxes.items():
			cax.clear()

		ki = self.fX[self.fi]
		df = get_df(self.dic, ki)
		print('::%s'%ki)
		print_stats(df)

		self.scatterplot(df)
		self.fig.canvas.set_window_title(ki)


plot_props = {
	
	'S' : {
		'axislim'		: (1, 5),
		'histrange'	: (1, 5),
		'label'			: 'Shanon entropy',
		'axisticks'	: (1.5, 2.5, 3.5, 4.5)
	},

	'H' : {
		'axislim' 	: (-0.2, 1.8),
		'histrange'	: (0, 1.8),
		'label'			: 'H/V',
		'axisticks'	: (0, 0.5, 1, 1.5)
	},

	'Q' : {
		'axislim' 	: (-5, 35),
		'histrange'	: (0, 35),
		'label'			: 'Q/$10^8$e',
		'axisticks'	: (0, 10, 20, 30)
	},

	'tsig' : {
		'axislim' 	: (-5, 55),
		'histrange'	: (0, 55),
		'label'			: 'tsig/ns',
		'axisticks'	: (0, 20, 40)
	},

}


run = Flasher('v08f', split=True)
dic = run.data
for key in tuple(dic.keys()):
	dic[ key[-7:-3] ] = dic.pop(key)


# interactive plots
keys = sorted(dic.keys())#[26:27]
i 	 = keys.index('l137') # initial index
labels = ['S', 'H', 'Q', 'tsig']

plt.ion()
plot = iPlot(keys, i, dic, labels, figsize=(10,10))
plot.fpause = 0.1
plt.gcf().canvas.mpl_connect('key_press_event', plot.on_key)

plt.show(block=True)
plt.close()

embed()