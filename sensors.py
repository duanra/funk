#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from IPython import embed
from matplotlib.dates import DateFormatter
from matplotlib.gridspec import GridSpec

from mod.funk_manip import sensors_data, select_data
from mod.funk_plt import autofmt_xdate, show, group_legend, add_toplegend
from mod.funk_utils import posix2utc, utc2posix, fn_over
from mod.funk_stats import bin_centers, bin_widths

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (360/72, 195/72)

# plt.rcParams.update(fcn.rcPaper)

# plt.rcParams.update(fcn.rcPoster)
# plt.rcParams['figure.figsize'] = (960/72, 350/72)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

sensors = sensors_data()
tperiod = (1507342519, 1558476000)

df = select_data(sensors, tperiod)
df['T1'] = df['T1'].interpolate()
df['T2'] = df['T2'].interpolate()
df['p']  = df['p'].interpolate()

utctime = posix2utc(df['time'].values)

fig = plt.figure()
gs 	= GridSpec(2, 2, hspace=0, wspace=0, width_ratios=[2.35,1])

axtopleft  = fig.add_subplot(gs[0])
axtopright = fig.add_subplot(gs[1], sharey=axtopleft)
axbotleft  = fig.add_subplot(gs[2], sharex=axtopleft)
axbotright = fig.add_subplot(gs[3], sharey=axbotleft)

axtopleft.tick_params(labelbottom=False, bottom=False)
axtopright.axis('off')
axbotright.axis('off')


axtopleft.plot(
	utctime, df['T1'].values,
	lw=1, color='teal', label='T1'
)
axtopleft.plot(
	utctime, df['T2'].values,
	lw=1, color='olive', label='T2'
)
axtopleft.set_ylabel('$T/^\circ$C')

hTrange = axtopleft.get_ylim()

hT1 = np.histogram( df['T1'].values, bins=20, range=hTrange)
axtopright.barh(
	bin_centers(hT1[1]),  hT1[0], bin_widths(hT1[1]),
	lw=0, color='teal', alpha=0.75
)

hT2 = np.histogram( df['T2'].values, bins=20, range=hTrange)
axtopright.barh(
	bin_centers(hT2[1]),  hT2[0], bin_widths(hT2[1]),
	lw=0, color='olive', alpha=0.75
)


axbotleft.plot(
	utctime, df['p'].values,
	lw=1, color='brown'
)
axbotleft.set_ylabel('$P/$hPa')
autofmt_xdate(monthly=3, ax=axbotleft)

hPrange = axbotleft.get_ylim()
hP = np.histogram( df['p'].values, bins=20, range=hPrange)
axbotright.barh(
	bin_centers(hP[1]),  hP[0], bin_widths(hP[1]),
	lw=0, color='brown', alpha=1
)


# plt.tight_layout(pad=0.05)
plt.subplots_adjust(
	top=0.99, bottom=0.15, left=0.12, right=0.995
)
plt.savefig('./plt/12/temperature_pressure')
show()


embed()