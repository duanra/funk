#!usr/bin/env python3
""" test interactive matplotlib """

import numpy as np
import matplotlib.pyplot as plt

from inspect import stack
from mod.funk_plt import PlotIndexer


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


class iPlot(PlotIndexer):

	def __init__(self, ax, X, i=0, loop=False, direc=0):
		super(iPlot, self).__init__(X, i, loop, direc)


	def action(self): # whatever you want
		print('i =', self.fi)
		ax.clear() 
		ax.plot(X[self.fi])
		# fig.canvas.draw_idle()


fig, ax = plt.subplots()
plt.ion()
# plt.show()

X = [ np.random.randn(50) for i in range(1000) ]

myplot = iPlot(ax, X, i=-1)
myplot.fpause = 1e-3
fig.canvas.mpl_connect('key_press_event', myplot.on_key)

plt.show(block=True) 
plt.close()
# input()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# NOTE: interactive matplotlib 
# - plt.ion()  --- plt.show(block=True) (no need to 're-draw', in general)
# - plt.ioff() --- plt.show(), re-draw with fig.canvas.draw_idle() 
# - in both case, update plt on mpl_connect events (see also mpl widgets)
# - plt.ion()  --- block plt using input()

# see:
# https://stackoverflow.com/questions/6697259/\
# interactive-matplotlib-plot-with-two-sliders