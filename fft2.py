#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from IPython import embed
from matplotlib import rc
from scipy.fftpack import ifft
from mod.funk_manip import redo_configs
from mod.funk_run import Run
from mod.funk_plt import show, order_legend, autofmt_xdate
from mod.funk_stats import fourier
from mod.funk_utils import posix2utc, fn_over

rc('font', size=10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def dB(arr, ref_value):
	return 10 * np.log10(arr / ref_value)


def hertz2cpd(arr):
	""" cpd: cycles per day """
	return np.asarray(arr) * (60 * 60 * 24)


def cpd2hertz(arr):
	return np.asarray(arr) / (60 * 60 * 24)


def get_data(run, pos):
	""" rework configs to open/closed only at 'in' position """

	configs = redo_configs(run.configs, mode=1)

	return configs.get_group(fcn.positions[pos])


def get_transform(df):

	rates = df['rates'].values
	time  = df['time'].values

	dt 		= np.mean(time[1:] - time[:-1])
	Fs 		= 1 / dt # sampling frequency

	freqs, fftvals = fourier(rates, Fs, None, False, False, False)
	freqs = hertz2cpd(freqs).round(8)
	Fs 		= hertz2cpd(Fs).round(8)

	return freqs, fftvals, Fs


# def get_trends(fft_df, time, fthr=2):

# 	idxthr = np.argmin(np.abs( fft_df['freq'].abs().values - fthr ))
# 	fmin 	 = fft_df['freq'].iloc[1]
# 	ls 	 	 = []

# 	for fcut in fft_df['freq'].iloc[1:idxthr+1]:
# 		ifft_lf = select_freqs(fft_df, (fmin, fcut) )

# 		ifft_lf = np.real(ifft( ifft_lf['fval'].values ))

# 		ls.append( (fcut, time, ifft_lf) )

# 	return pd.DataFrame(ls, columns=['fcut', 'time', 'ifft_lf'])


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


cuts  = fcn.cuts.select('dtlim')
# cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
run_v = 'v25'
run 	= Run.configs(run_v, trig=True, cuts=cuts)
df 		= get_data(run, 'in_closed') # in and out are combined --> in


freqs, fftvals, Fs = get_transform(df)
DC 		= np.abs(fftvals[0])
N 		= len(fftvals)
fabs 	= np.abs(freqs)


# ixcut = np.argmin(np.abs( fabs - 2 ))
# fcut 	= freqs[ixcut]
fcut 	= 2 # cpd
fmin 	= freqs[1]
fmax 	= max(fabs)


flow 					= (fmin, fcut)
ixlow  				= (fabs >= flow[0]) & (fabs <= flow[1])
fftvals_low		= fftvals.copy()
fftvals_low[ np.logical_not(ixlow) ] = 0


fhigh 	 		 	= (fcut, fmax)
ixhigh 				= (fabs > fhigh[0]) & (fabs <= fhigh[1])
fftvals_high 	= fftvals.copy()
fftvals_high[ np.logical_not(ixhigh) ] = 0


ifftvals_low  = np.real(ifft( fftvals_low ))
ifftvals_high = np.real(ifft( fftvals_high ))


# plt.figure()
# plt.plot(freqs[1:], np.abs(fftvals_low[1:]), '+')
# plt.plot(freqs[1:], np.abs(fftvals_high[1:]), '+')
# plt.xlim(-10,10)


plt.figure()
m 	 	= 1
time 	= posix2utc(fn_over(df['time'].values, m=m))
rates = df['rates'].values
print('check: ', np.allclose(rates, DC/N + ifftvals_low + ifftvals_high))
plt.plot(
	time, fn_over(rates, m=m) - DC/N,
	lw=0.8, color='k', label='data')
plt.plot(
	time, fn_over(ifftvals_high, m=m),
	lw=0.8, color='g', label='residual')
plt.plot(
	time, fn_over(ifftvals_low, m=m),
	lw=0.8, color='r', label='trend')

autofmt_xdate(daily=4)
plt.ylabel('event rates - mean [Hz]')
lgd = plt.legend(
	bbox_to_anchor=(0., 1.005, 1., .102), loc='lower left',
		ncol=3, mode="expand", borderaxespad=0 )
for line in lgd.get_lines():
	line.set_linewidth(2)

show()
# embed()