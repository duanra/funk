#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as st
import mod.funk_consts as fcn

from IPython import embed
from matplotlib.legend_handler import HandlerTuple

from mod.funk_anal import SPE
from mod.funk_plt import show, autofmt_xdate
from mod.funk_run import Run, Flasher, ConcatenatedRun
from mod.funk_utils import Namespace, fn_over
from mod.funk_stats import bin_centers

plt.rcParams.update(fcn.rcDefense)
plt.rcParams['figure.figsize'] = (220/72, 165/72)

# plt.rcParams.update(fcn.rcThesis)
# plt.rcParams['figure.figsize'] = (165/72, 145/72)
# plt.rcParams['figure.figsize'] = (118/72, 108/72)

pd.set_option('display.max_rows', 20)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# reg_coeffs = []
# ropen_means = []
# rclosed_means = []
# bexts = []
# opas = [] # hutter opacities

# bint = 1.55

# run_vs = ['v26', 'v27', 'v28', 'v29', 'v30']
# colors = ['dimgrey', 'grey', 'lightslategrey', 'slategrey', 'k']

# for run_v, color in zip(run_vs, colors):

# 	run = Run.configs(run_v, trig=True)
# 	dfopen 	 = run.get_df('in_open') 
# 	dfclosed = run.get_df('in_closed')

# 	ropen 	= dfopen['rates'].values
# 	rclosed = dfclosed['rates'].values
# 	rdiff = dfopen['rates'].values - dfclosed['rates'].values
	
# 	# ix = (rdiff>=0)
# 	# ropen = ropen[ix]
# 	# rclosed = rclosed[ix]
# 	# rdiff = rdiff[ix]

# 	bins = 15
# 	statrange = (-0.5,2)

# 	ys, xedgs = st.binned_statistic(
# 								rdiff, ropen, statistic=np.mean,
# 								bins=bins, range=statrange
# 							)[:2]

# 	xs = bin_centers(xedgs)

# 	xerrs = st.binned_statistic(
# 						rdiff, rdiff, statistic=lambda z: np.std(z, ddof=1),
# 						bins=bins, range=statrange
# 					)[0]
# 					# / np.sqrt(st.binned_statistic(
# 					# 	rdiff, rdiff, statistic='count',
# 					# 	bins=bins, range=statrange
# 					# )[0])

# 	yerrs = st.binned_statistic(
# 						rdiff, ropen, statistic=lambda z: np.std(z, ddof=1),
# 						bins=bins, range=statrange
# 					)[0]
# 					# / np.sqrt(st.binned_statistic(
# 					# 	rdiff, ropen, statistic='count',
# 					# 	bins=bins, range=statrange
# 					# )[0])

# 	ix = (rdiff>=0)
# 	reg = st.linregress(rdiff[ix], ropen[ix])
# 	m, n = reg[0], reg[1]
# 	ropen_mean = ropen[ix].mean()
# 	rclosed_mean = rclosed[ix].mean()
# 	rdiff_mean = rdiff[ix].mean()

# 	bext = m * rdiff_mean + n - bint
# 	opa = rdiff_mean / bext

# 	reg_coeffs.append(reg)
# 	ropen_means.append(ropen_mean)
# 	rclosed_means.append(rclosed_mean)
# 	bexts.append(bext)
# 	opas.append(opa)

# 	# plt.scatter(rdiff, ropen, s=1)
# 	# plt.scatter(xs, ys, s=10)

# 	plt.errorbar(
# 		xs, ys, yerrs, xerrs,
# 		fmt='s', ms=1.5, mew=0,
# 		elinewidth=0.5, color=color
# 	)

# plt.axhline(y=1.55, lw=1.2, ls='--', color='darkslategrey')
# plt.axvline(x=0, color='#b0b0b0', lw=0.5, ls=':')
# # plt.plot(xs, reg['v30'][0]*xs+reg['v30'][1])

# plt.xlabel('$r_\mathrm{open} - r_\mathrm{closed}$/Hz')
# plt.ylabel('$r_\mathrm{open}$/Hz')
# plt.tight_layout(pad=0.05)
# # plt.savefig('./plt/12/ctruns_totRateVsDiff')
# # plt.close()

# # show()
# embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

run_vs = ['v25', 'v26', 'v27', 'v28', 'v29', 'v30']

ropen 	= []
rclosed = []

# run = Run.configs(run_vs[0], trig=True)
run = Run.configs(run_vs[0], trig=False, cuts=SPE)
dfopen 	 = run.get_df('out_open') 
dfclosed = run.get_df('out_closed')

ropen_mean = dfopen['rates'].mean()
ropen_std  = dfopen['rates'].std()
rclosed_mean = dfclosed['rates'].mean()
rclosed_std  = dfclosed['rates'].std()

ropen.append([ropen_mean, ropen_std])
rclosed.append([rclosed_mean, rclosed_std])

for run_v in run_vs[1:]:

	# run = Run.configs(run_v, trig=True)
	run = Run.configs(run_v, trig=False, cuts=SPE)
	dfopen 	 = run.get_df('in_open') 
	dfclosed = run.get_df('in_closed')

	ropen_mean = dfopen['rates'].mean()
	ropen_std  = dfopen['rates'].std()	
	rclosed_mean = dfclosed['rates'].mean()
	rclosed_std  = dfclosed['rates'].std()

	ropen.append([ropen_mean, ropen_std])
	rclosed.append([rclosed_mean, rclosed_std])


ropen = np.asarray(ropen)
rclosed = np.asarray(rclosed)

# plotkwargs = {
# 	'fmt': 's', 'ms': 1.8, 'mew':0,
# 	'elinewidth': 0.5
# }

plotkwargs = {
	'fmt': 's', 'ms': 2, #'mew':0,
	'elinewidth': 1, 'capsize': 2,
}

eb1 = plt.errorbar(
			[run_vs[0]], [ropen[0,0]], [ropen[0,1]],
			color='limegreen', label='v25open', **plotkwargs
		)

eb2 = plt.errorbar(
			[run_vs[0]], [rclosed[0,0]], [rclosed[0,1]],
			color='lightgreen', label='v25closed', **plotkwargs
		)

eb3 = plt.errorbar(
			run_vs[1:], ropen[1:,0], ropen[1:,1],
			color='k', label='vxxopen', **plotkwargs
		)

eb4 = plt.errorbar(
				run_vs[1:], rclosed[1:,0], rclosed[1:,1],
				color='grey', label='vxxclosed', **plotkwargs
			)

# plt.axhline(y=1.55, lw=1.2, ls='--', color='darkslategrey')
plt.axhline(y=1.38, lw=1.2, ls='--', color='darkslategrey')

plt.xlabel('run number')
# plt.ylabel('trigger rate/Hz')
plt.ylabel('rate/Hz')
plt.ylim(bottom=0)
plt.legend(
	[(eb1, eb3), (eb2, eb4)],
	[r'$\langle r_\mathrm{open} \rangle$', r'$\langle r_\mathrm{closed} \rangle$'],
	handler_map={tuple: HandlerTuple(ndivide=None)},
	loc='upper right'
)
plt.tight_layout(pad=0.05)

# plt.savefig('./plt/12/ctruns_triggerRate')
plt.savefig('../talks/src/phd_defense/figs/ctruns_rate')
plt.close()

# show()
# embed()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# run_vs = ['v26', 'v27', 'v28', 'v29', 'v30']
# volume = np.linspace(1/500, 1/2500, num=5)

# ropen_means  = []

# for run_v in run_vs:

# 	run = Run.configs(run_v, trig=True)
# 	dfopen 	 = run.get_df('in_open') 

# 	ropen_means.append(dfopen['rates'].mean())


# plt.scatter(volume, ropen_means)
# show()

# embed()

