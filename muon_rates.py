#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from IPython import embed
from matplotlib.dates import DateFormatter
from mod.funk_manip import muon_data, get_muons
from mod.funk_plt import autofmt_xdate, show, group_legend, add_toplegend
from mod.funk_utils import posix2utc, utc2posix, fn_over

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (295/72, 185/72)

# plt.rcParams.update(fcn.rcPaper)

# plt.rcParams.update(fcn.rcPoster)
# plt.rcParams['figure.figsize'] = (960/72, 350/72)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def average(x):
	return fn_over(x, fn=np.mean, m=3*60)


area_pad = 62.5*25*1e-4 # m^2
muons 	 = muon_data()
tperiod  = posix2utc(1507342519, 1558476000) # all
# tperiod  = posix2utc([1551966232, 1554340746]) # v35

df_vert  = get_muons('vcoinc', tperiod, muons=muons)
df_horiz = get_muons('hcoinc', tperiod, muons=muons)


fig, (axtop, axbot) = plt.subplots(2, 1, sharex=True)

axbot.spines['top'].set_visible(False)
axbot.tick_params(top=False)
axtop.spines['bottom'].set_visible(False)
axtop.tick_params(bottom=False)


utctime = posix2utc(average(df_vert['time'].values))
axtop.plot(
	utctime, average(df_vert['rates'].values)/area_pad,
	color='#1f77b4', lw=0.5, label='vertical coincidence'
) # color='#d62728'

axtop.set_ylabel('muon flux/(Hz/m$^2$)')
axtop.yaxis.set_label_coords(-0.11, 0)
axtop.set_yticks([125, 130, 135, 140])
axtop.set_ylim(122.5, 142.5)

# axtop.set_ylabel('muon rate/Hz')
# axtop.yaxis.set_label_coords(-0.11, 0)
# axtop.set_yticks([20, 21, 22])
# axtop.set_ylim(19.35, 22.5)


axbot.plot(
	utctime, average(df_horiz['rates'].values)/area_pad,
	color='#dda0dd', lw=0.5, label='horizontal coincidence'
) # color='#1f77b4'

axbot.set_yticks([6, 6.25, 6.5])
axbot.set_ylim(5.75, 6.75)
# axbot.set_yticks([0.9, 1, 1.1])
# axbot.set_ylim(0.85, 1.17)

# axbot.set_xticks(
# 	posix2utc(
# 		utc2posix(
# 			['2019-Mar-11', '2019-Mar-18', '2019-Mar-25', '2019-Apr-01'],
# 			fmt='%Y-%b-%d'
# 	))
# )

# axbot.xaxis.set_major_formatter(DateFormatter('%b-%d'))
# plt.setp(axbot.get_xticklabels(), ha='right', rotation=30)
# plt.figtext(0.9, 0.02, 2019)

# autofmt_xdate(daily=7, datefmt='%b-%d')
autofmt_xdate(monthly=3, ax=axbot)


# axes style
d = 0.025
props = dict(
	transform=axtop.transAxes, color='k',
	clip_on=False, linewidth=0.8
)
axtop.plot((-d, +d), (-d, +d), **props)
axtop.plot((1 - d, 1 + d), (-d, +d), **props)
props.update(transform=axbot.transAxes)
axbot.plot((-d, +d), (1 - d, 1 + d), **props)
axbot.plot((1 - d, 1 + d), (1 - d, 1 + d), **props)


# legend style
add_toplegend(
	labels=['vert', 'horiz'],
	lgd=group_legend(axtop, axbot),
	bbox_to_anchor=[0, 1.012, 1, 1],
	handlelength=1.6,
	ncol=2, lw=2, ax=axtop, fig=fig
)
fig.subplots_adjust(
	left=0.135, bottom=0.2, hspace=0.05,
	top=0.892, right=0.98
)

# lgdtop = axtop.legend(loc='upper right', borderaxespad=0, frameon=False)
# lgdtop.get_lines()[0].set_linewidth(2)

# lgdbot = axbot.legend(loc='upper right', borderaxespad=0, frameon=False)
# lgdbot.get_lines()[0].set_linewidth(2)

# fig.subplots_adjust(
# 	left=0.14, bottom=0.2, hspace=0.05,
# 	top=0.99, right=0.985
# )

# fig.savefig('./plt/12/muon_flux')
show()
embed()