#!/usr/bin/env python3

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# from IPython import embed

plt.rcParams.update({
    # tex document \columnwidth = 246pt and \textwidth = 510pt
    # save with plt.tight_layout(pad=0.05)
    'figure.figsize'      : ( 246/72, 0.5*246/72 ),
    'savefig.format'      : 'pdf',
    'text.latex.preamble' : [
        # temporary fix for times font: https://github.com/matplotlib/matplotlib/issues/9118
        # r'\PassOptionsToPackage{full}{textcomp}',
        # r'\usepackage{newtxtext,newtxmath}',
        r'\usepackage{newpxtext,newpxmath}',
        r'\usepackage{upgreek}',
        r'\usepackage{amssymb}'
    ],
    'text.usetex'         : True,
    'font.family'         : 'serif',
    'font.size'           : 9, #10
    'axes.labelsize'      : 9, #10
    'legend.fontsize'     : 9, #10
    'xtick.labelsize'     : 9,
    'ytick.labelsize'     : 9,
    'axes.grid'           : True,
    'grid.linestyle'      : ':',
    'grid.linewidth'      : 0.4
})

blue  = '#1f77b4'
green = '#2ca02c'
red   = '#d62728'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

infile  = '/cr/data02/Funk/data/pmt_run/hv1050-FUNKDAQ2-v08.pandas.txt.bz2' # run v35
# '/home/schaefer-c/funk_data/pandas_3p5-1p5-7_FUNKDAQ2/hv1050-FUNKDAQ2-v02-8adc.pandas.txt.bz2', # run v25
#  'funkdata/pandas_3p5-1p5-7_FUNKDAQ2/hv1050-FUNKDAQ2-v03-newShutter.pandas.txt.bz2'


def pickle_dump(infile, outfile=None):
    """ redump infile for faster loading """

    if outfile is None:
        # cd      = os.path.dirname(os.path.realpath(__file__))
        cd      = '/home/tmp/data/Funk/data/pmt_run/'
        fname   = os.path.basename(infile)[:-15] + '.sdf' 
        outfile = os.path.join(cd, fname)

    dtype = {
        't1'    : np.float, # large time 
        't2'    : np.float,
        'event' : np.uint,
        'q'     : np.float  # is needed to identify empty traces
    }

    print('loading', infile)
    df = pd.read_csv(
            infile, delim_whitespace=True, header=0,
            usecols=dtype.keys(), dtype=dtype
        )

    print('saving', outfile)
    df.to_pickle(outfile)


infile  = '/home/tmp/data/Funk/data/pmt_run/hv1050-FUNKDAQ2-v08.sdf'
if not os.path.isfile(infile):
    pickle_dump(infile)

outfile = 'v35_timing_accuracy.pdf' 


print('loading', infile)
data   = pd.read_pickle(infile)
data   = data.groupby('event').head(1)

tLarge = data['t1'].values
tSmall = data['t2'].values
# tcut = 60.0042            # for funkdaq
# tcut = 60.0038              # for funkdaq2
print('tLarge: ', tLarge.min(), tLarge.max())
print('tSmall: ', tSmall.min(), tSmall.max())


fig, (ax1, ax2) = plt.subplots(1, 2)

bins    = np.arange(60, 60.006, 1e-4)
density = False

# h_tLarge, binedges = np.histogram(tLarge, bins=bins, density=density)
# ax1.step(bins[:-1], h_tLarge, where='post', label=r'$t_1$', color=blue, lw=1)
h_tLarge, binedges, _= ax1.hist(
    tLarge, bins=bins, density=density,
    color=blue, lw=1, histtype='step',
    label=r'$t_1$', zorder=10
    )

# h_tSmall, binedges = np.histogram(tSmall, bins=bins, density=density)
# ax1.step(bins[:-1], h_tSmall, where='post', label=r'$t_2$', color=green, lw=1)
h_tSmall, binedges, _= ax1.hist(
    tSmall, bins=bins, density=density,
    color=green, lw=1, histtype='step',
    label=r'$t_2$', zorder=10
    )

ax1.set_xlabel('$t$/s')
ax1.set_xticks([60., 60.003, 60.006])
ax1.get_xaxis().get_major_formatter().set_useOffset(False)
ax1.set_ylabel('Number of events')
ax1.set_yscale('log')
lgd1 = ax1.legend(
    bbox_to_anchor=(0., 1.005, 1., .102), loc='lower left',
    ncol=2, mode='expand', borderaxespad=0, borderpad=0.3,
    handlelength=1.2, handleheight=0.5, handletextpad=0.6
)


tDelta = (tLarge - tSmall) * 1e3
print('tDelta = tLarge - tSmall: ', np.mean(tDelta), ' +- ',  np.std(tDelta))
print('tDelta: ', tDelta.min(), tDelta.max())

binsDelta = np.arange(0, 0.006 * 1e3, 0.0002 * 1e3)
density   = False

# h_tDelta, binedgesDelta = np.histogram(tDelta, bins=binsDelta, density=density)
# ax2.step(binsDelta[:-1], h_tDelta, where='post', label=r'$\Delta t = t_1 - t_2$', color=red, lw=1)
h_tDelta, binedgesDelta, _= ax2.hist(
    tDelta, bins=binsDelta, density=density,
    color=red, lw=1, histtype='step',
    label='$\Delta t = t_1 - t_2$', zorder=10
    )

ax2.set_xlabel('$\Delta t$/ms')    
ax2.set_yscale('log')
lgd2 = ax2.legend(
    bbox_to_anchor=(0., 1.005, 1., .102), loc='lower left',
    ncol=1, mode='expand', borderaxespad=0, borderpad=0.3,
    handlelength=1.2, handleheight=0.5, handletextpad=0.6
)
for line in lgd2.get_lines():
    line.set_linewidth(3)


fig.tight_layout(pad=0.05)
fig.savefig(outfile)

tMeas = (tLarge + tSmall) / 2.
print('tMeas = (tLarge + tSmall) / 2.')
print('np.min(tMeas), np.mean(tMeas), np.max(tMeas)')
print(np.min(tMeas), np.mean(tMeas), np.max(tMeas))