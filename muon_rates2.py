#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from IPython import embed
from datetime import datetime
from matplotlib.dates import DateFormatter
from matplotlib.gridspec import GridSpec

from mod.funk_manip import muon_data, get_muons
from mod.funk_plt import autofmt_xdate, show, group_legend, add_toplegend
from mod.funk_utils import posix2utc, utc2posix, fn_over
from mod.funk_stats import bin_centers

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (360/72, 220/72)

# plt.rcParams.update(fcn.rcPaper)

# plt.rcParams.update(fcn.rcPoster)
# plt.rcParams['figure.figsize'] = (960/72, 350/72)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def average(x):
	return fn_over(x, fn=np.mean, m=5*60)


area_pad = 62.5*25*1e-4 # m^2
muons 	 = muon_data()
tperiod  = (posix2utc(1507342519), datetime.now())
# tperiod  = posix2utc([1551966232, 1554340746])
df_vert  = get_muons('vcoinc', tperiod, muons=muons)
df_horiz = get_muons('hcoinc', tperiod, muons=muons)


fig = plt.figure()
gs 	= GridSpec(2, 2, hspace=0.05, wspace=0.05, width_ratios=[2,1])

axtopleft  = fig.add_subplot(gs[0])
axtopright = fig.add_subplot(gs[1], sharey=axtopleft)
axbotleft  = fig.add_subplot(gs[2], sharex=axtopleft)
axbotright = fig.add_subplot(gs[3], sharey=axbotleft)

axtopleft.tick_params(labelbottom=False, bottom=False)
axtopleft.spines['bottom'].set_visible(False)
axbotleft.tick_params(top=False)
axbotleft.spines['top'].set_visible(False)
axtopright.tick_params(
	labelleft=False, labelbottom=False,
	top=False, bottom=False, right=False, left=False
)
axbotright.set_zorder(0)
axbotright.tick_params(
	labelleft=False, labelbottom=False,
	top=False, bottom=False, right=False, left=False
)

for sp in ['top', 'right', 'bottom']:
	axtopright.spines[sp].set_visible(False)
	axbotright.spines[sp].set_visible(False)


utctime = posix2utc(average(df_vert['time'].values))
y_vert 	= average(df_vert['rates'].values) / area_pad
axtopleft.plot(
	utctime, y_vert, color='#1f77b4', lw=1,
	label='vertical coincidence'
)
axtopleft.set_ylabel('muon flux/(Hz/m$^2$)', labelpad=0.6)
axtopleft.yaxis.set_label_coords(-0.11, 0)
axtopleft.set_ylim(123, 142)


# histogram of the averaged one
h = np.histogram(y_vert, bins=50, density=True)
axtopright.step(
	h[0], bin_centers(h[1]), color='#1f77b4', where='mid'
)

y_horz = average(df_horiz['rates'].values) / area_pad
axbotleft.plot(
	utctime, y_horz, color='#ff7f0e', lw=1,
	label='horizontal coincidence'
)
autofmt_xdate(monthly=3, ax=axbotleft)
# axbotright.set_ylim(5.5, 7)

h = np.histogram(y_horz, bins=50, density=True)
axbotright.step(
	h[0], bin_centers(h[1]), color='#ff7f0e', where='mid'
)


d = .02
axprops = dict(
	transform=axtopleft.transAxes, color='k',
	clip_on=False, linewidth=0.8
)
axtopleft.plot((-d, +d), (-d, +d), **axprops)
axtopleft.plot((1 - d, 1 + d), (-d, +d), **axprops)
axprops.update(transform=axbotleft.transAxes)
axbotleft.plot((-d, +d), (1 - d, 1 + d), **axprops)
axbotleft.plot((1 - d, 1 + d), (1 - d, 1 + d), **axprops)


plt.subplots_adjust(
	top=0.995, bottom=0.2, left=0.1, right=0.995
)
# plt.savefig('./plt/12/muon_flux')

show()
# embed()