#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from datetime import datetime
from matplotlib.patches import Rectangle
from matplotlib import rc
from scipy.stats import binned_statistic
from mod.funk_manip import get_vmuons
from mod.funk_run import Run, ConcatenatedRun
from mod.funk_plt import show, order_legend
from mod.funk_stats import average_power_spectrum, peakfreqs
from mod.funk_utils import posix2utc

rc('font', size=10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def dB(arr, ref_value):
	return 10 * np.log10( arr/ref_value )


def seconds2days(arr, decimals=1):
	return arr / (60 * 60 * 24)


def hours2seconds(arr, decimals=1):
	return arr * 60 * 60


def get_transform(df):
	rates 		 = df['rates'].values
	timestamps = df['time'].values

	dt 				 = np.mean(timestamps[1:] - timestamps[:-1])
	Fs 				 = 1 / dt # sampling frequency

	return len(timestamps)
	fft_rates  = power_spectrum(rates, Fs)
	yref  		 = fft_rates.loc[0, 'power'] # DC component
	power 		 = dB(fft_rates['power'].iloc[1:].values, yref)
	ffreq 		 = fft_rates['freq'].iloc[1:].values
	# print(table_lr(['Fs', 'freq_step'],
	# 							 [Fs, Fs/len(timestamps)]))

	return ffreq, power, Fs


cuts  = fcn.cuts.select('dtlim')
run_v = 'v25'
run 	= Run.configs(run_v, trig=True, cuts=cuts)

print(get_transform(run.get_df('in_open')))


# split in 3days measurement
# take rate diff
# do fft --> average


# see 
# https://en.wikipedia.org/wiki/Spectral_density_estimation
# https://en.wikipedia.org/wiki/Bartlett%27s_method

# do multi_fourier, just split rates array instead of the run

# add hmuons
