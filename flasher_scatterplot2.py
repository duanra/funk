""" use for flasher optimal l-setting """
#!usr/bin/env python3

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from matplotlib.colors import LogNorm
from matplotlib.patches import Rectangle
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from mod.funk_run import Flasher
from mod.funk_plt import setup_scatterplot, show
from mod.funk_manip import apply_cuts
from mod.funk_utils import Namespace

plt.rcParams.update(fcn.rcThesis)

pd.set_option('display.max_rows', 10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

run = Flasher('v09f', split=False)
df 	= run.data

df['h'] = 10**df['h'].values
df['q'] = 10**df['q'] / 1e8 
df.rename(columns={'h': 'H', 'q': 'Q'}, inplace=True)


print('preliminary cuts')
df2 = df.copy()
df2 = apply_cuts(df2, cuts=Namespace(riselim=(0,)))
df2 = apply_cuts(df2, cuts=Namespace(tplim=(280., 300.)))


# define SPE phase-space
S_mu 	= df2['S'].mean()
S_std = df2['S'].std()
S_lim = (round(S_mu - 3*S_std,3), round(S_mu + 3*S_std,3))

tsig 	= df2['tsig']
tsig 	= tsig[(tsig>0.) & (tsig<10.)]
tsig_mu  = tsig.mean() 
tsig_std = tsig.std()
tsig_lim = (round(tsig_mu - 3*tsig_std, 3), round(tsig_mu + 3*tsig_std, 3))

print('spe cuts')
print('S_lim    = ({:.3f}, {:.3f})'.format(*S_lim))
print('tsig_lim = ({:.3f}, {:.3f})'.format(*tsig_lim))
spe = Namespace([('Slim', S_lim), ('tsiglim', tsig_lim)])
df3 = apply_cuts(df2, spe)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# # riselim distribution

# plt.rcParams['figure.figsize'] = (165/72, 145/72)

# plt.figure()
# bwi 		= 0.8 # sampling time in nanosecond
# bins		= np.arange((-7-0.5)*bwi, (28-0.5)*bwi, bwi)

# plt.hist(
# 	df['rise'].values, bins, None, True,
# 	histtype='step', color='k', lw=1.5
# )
# plt.axvline(0, lw=0.8, ls='--', color='crimson')
# plt.xlabel(r'$t_\mathrm{rise}$/ns')
# plt.xticks(range(-5,25,5))
# plt.yscale('log')
# plt.tight_layout(pad=0.05)
# plt.savefig('./plt/12/flasher_tRise_distn')
# plt.close()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# # tp distn

# plt.rcParams['figure.figsize'] = (165/72, 145/72)

# plt.figure()
# bwi 		= 0.8 # sampling time in nanosecond
# bins		= np.arange((310-0.5)*bwi, (570-0.5)*bwi, bwi)

# plt.hist(
# 	df['tp'].values, bins, None, True,
# 	histtype='step', color='k', lw=1.5
# )
# plt.axvline(280, lw=0.8, ls='--', color='crimson', zorder=0)
# plt.axvline(300, lw=0.8, ls='--', color='crimson')
# plt.xlabel(r'$t_\mathrm{trigger}$/ns')
# plt.xticks(range(250,500,50))
# plt.yscale('log')
# plt.tight_layout(pad=0.05)
# plt.savefig('./plt/12/flasher_tTrigger_distn')
# plt.close()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# scatterplot

labels = ['S', 'H', 'Q', 'tsig']
grid   = 2*(len(labels), )
labels_grid = np.array([
								'%s-%s'%(a,b) for a in labels for b in labels
							]).reshape(*grid).T

plot_props = {
	
	'S' : {
		'axislim'		: (1, 5),
		'histrange'	: (1, 5),
		'label'			: '$S$',
		'axisticks'	: (1.5, 2.5, 3.5, 4.5)
	},

	'H' : {
		'axislim' 	: (-0.2, 1.8),
		'histrange'	: (0, 1.8),
		'label'			: '$H$/V',
		'axisticks'	: (0, 0.5, 1, 1.5)
	},

	'Q' : {
		'axislim' 	: (-5, 35),
		'histrange'	: (0, 35),
		'label'			: '$Q/10^8e$',
		'axisticks'	: (0, 10, 20, 30)
	},

	'tsig' : {
		'axislim' 	: (-5, 55),
		'histrange'	: (0, 55),
		'label'			: '$\sigma_t$/ns',
		'axisticks'	: (0, 20, 40)
	},

}


fig, axes, caxes = setup_scatterplot(grid, figsize=(360/72, 360/72))
fig.canvas.set_window_title('flasher run')

# note: we use df, not df2 here
for (i,j), label in np.ndenumerate(labels_grid):

	if j > i: # upper triangle
		pass

	elif j == i:
		ax = axes[(i,i)]
		
		xvar, _ = label.split('-')
		xpp = plot_props[xvar]

		ax.hist(
			df[xvar].values, 100, xpp['histrange'], False,
			histtype='step', color='k', lw=1.2
		)
		ax.hist(
			df3[xvar].values, 100, xpp['histrange'], False,
			histtype='step', color='r', lw=0.8
		)		
		
		ax.set_xlim(*xpp['axislim'])
		ax.set_xticks(xpp['axisticks'])
		
		# ax.set_ylabel('histogram', labelpad=12, rotation=-90)
		ax.yaxis.set_label_position('right')
		ax.set_yscale('log')
		ax.set_yticks([1e1, 1e3, 1e5])
		ax.set_ylim(top=2e5)
		ax.tick_params(left=False, top=False)

		if i == grid[0]-1: # last row
			ax.set_xlabel(xpp['label'])			

	else: # j<i
		ax 	= axes[(i,j)]
		cax = caxes[(i,j)]
		cax.remove() # do not create axes manually --> use inset_axes

		xvar, yvar = label.split('-')
		xs, ys 		 = df[xvar].values, df[yvar].values
		xpp, ypp	 = plot_props[xvar], plot_props[yvar]

		h2d = ax.hist2d(
						xs, ys, bins=100, range=(xpp['histrange'], ypp['histrange']),
						cmap='viridis', norm=LogNorm()
					)
		ax.tick_params(right=False)

		cax  = inset_axes(
						ax, width='5%', height='75%',
						loc='center right',
						axes_kwargs={'zorder':100},
						borderpad=0
					 )
		cax.tick_params(
			axis='both', direction='in', length=2,
			left=False, labelleft=False,
			bottom=False, labelbottom=False,
			labelsize='smaller'
		)
		cax.tick_params(
			which='minor', direction='in', length=1.5
		)

		cbar = ax.figure.colorbar(
						mappable=h2d[3], cax=cax, orientation='vertical',
						ticklocation='left', ticks=[1e1, 1e2, 1e3, 1e4]
					 )
		cax.yaxis.set_minor_locator(plt.NullLocator())


		ax.set_xlim(*xpp['axislim'])
		ax.set_xticks(xpp['axisticks'])

		ax.set_ylim(*ypp['axislim'])
		ax.set_yticks(ypp['axisticks'])

		if i == grid[0]-1: # last row
			ax.set_xlabel(xpp['label'])

		if j==0: # first column
			ax.set_ylabel(ypp['label'])


for i in range(1,grid[0]):
	axes[(i,0)].axvline(S_lim[0], lw=0.8, ls='--', color='r')
	axes[(i,0)].axvline(S_lim[1], lw=0.8, ls='--', color='r')
# axes[(0,0)].axvline(S_lim[0], lw=1.5, color='r')
# axes[(0,0)].axvline(S_lim[1], lw=1.5, color='r')


for j in range(grid[1]-1):
	axes[(grid[0]-1,j)].axhline(tsig_lim[0], lw=0.8, ls='--', color='r')
	axes[(grid[0]-1,j)].axhline(tsig_lim[1], lw=0.8, ls='--', color='r')
# axes[(grid[0]-1,grid[1]-1)].axvline(tsig_lim[0], lw=1.5, color='r')
# axes[(grid[0]-1,grid[1]-1)].axvline(tsig_lim[1], lw=1.5, color='r')


axes[(grid[0]-1,0)].add_patch(
	Rectangle(
		xy=(S_lim[0], tsig_lim[0]),
		width=S_lim[1] - S_lim[0],
		height=tsig_lim[1] - tsig_lim[0],
		fill=False, edgecolor='r', lw=0.8
))

fig.subplots_adjust(
	top=0.99, bottom=0.078,
	left=0.085, right=0.95,
	wspace=0, hspace=0
)
# fig.savefig('./plt/12/flasher_scatterplotSPE.pdf')

# show()
embed()