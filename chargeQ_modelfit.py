#!usr/bin/env python3

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import mod.funk_consts as fcn

from IPython import embed
from mod.funk_run import Flasher
from mod.funk_manip import apply_cuts
from mod.funk_plt import show, add_toplegend
from mod.funk_utils import Namespace, handle_warnings
from mod.funk_ROOTy import TH1Wrapper, TH1Fitter, plot_th1

plt.rcParams.update(fcn.rcThesis)
plt.rcParams['figure.figsize'] = (190/72, 185/72)

pd.set_option('display.max_rows', 10)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


run = Flasher('v09f', split=False)
df 	= run.data

df['h'] = 10**df['h'].values
df['q'] = 10**df['q'] / 1e8 
df.rename(columns={'h': 'H', 'q': 'Q'}, inplace=True)


print('preliminary cuts')
df2 = df.copy()
df2 = apply_cuts(df2, cuts=Namespace(riselim=(0,)))
df2 = apply_cuts(df2, cuts=Namespace(tplim=(280., 300.)))


Q = df2['Q'].values
th1Q = TH1Wrapper(Q, 100, min(Q), 4.5, True)

def gaussian(x, N, mu, sig):
	return N * np.exp(-0.5*np.power((x-mu)/sig, 2))

fitter = TH1Fitter(th1Q, gaussian, 0.8, 1.5)
fitter.set_parameters(1, 1, 1)
fitter.fit('R')
N, mu, sig = fitter.get_parameters().values()


plot_th1(th1Q, lw=1.5, color='k', label='data')

xlow = np.linspace(0.4, 0.8)
plt.plot(
	xlow, gaussian(xlow, N, mu, sig),
	color='r', lw=1.5, ls=':'
)
xfit = np.linspace(0.8, 1.5)
plt.plot(
	xfit, gaussian(xfit, N, mu, sig),
	color='r', lw=1.5, label='Gaussian'
)
xhigh = np.linspace(1.5, 2.5)
plt.plot(
	xhigh, gaussian(xhigh, N, mu, sig),
	color='r', lw=1.5, ls=':'
)

plt.xlabel('$Q/10^8e$')
add_toplegend(labels=['data', 'Gaussian'], ncol=2)


show()
# show(save='./plt/12/flasher_QDistn', pad=0.05)

embed()


# hist = get_histogram(th1Q, return_yerr=True)
# plt.errorbar(
# 	bin_centers(hist[2]), hist[0], hist[1],
# 	fmt='s', ms=1.5, mew=0, elinewidth=.5,
# 	color='k', alpha=1, label='data',
# 	zorder=0
# )
# show()


# def spe_gain(x, N, muDyn):
# 	""" x: number of photoelectrons from a single electron
# 			N: normalisation
# 			muDyn: average amplification per dynonde
# 	"""

# 	k = 11
# 	a = np.power(muDyn, k)	
# 	b = 0.5 * (a - 1) / (muDyn - 1)

# 	ret = np.exp(-a/b) * (
# 					dirac(x,0) +
# 					np.exp(-x/b) * np.sqrt(a/x) / b
# 						* iv(1, 2*np.sqrt(a*x)/b) 
# 				)

# 	return ret


# def spe_gainrel(xrel, N0, muDyn):
	
# 	xref = 1e8

# 	def prob(xrel):
# 		if xrel<0:
# 			return 0
# 		elif xrel==0:
# 			# follows from conservation of differential pdf
# 			return spe_gain(0, N0, muDyn)
# 		else:
# 			return xref * spe_gain(xref * xrel, N0, muDyn)

# 	# xrel = np.asarray(x / xref)
# 	xrel = np.array(xrel, ndmin=1)

# 	return np.asarray([prob(xx) for xx in xrel])