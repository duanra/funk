#!usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mod.funk_consts as fcn 

from IPython import embed
from matplotlib.patches import Rectangle
from datetime import timedelta
from mod.funk_manip import get_muons
from mod.funk_run import Run
from mod.funk_plt import autofmt_xdate, add_toplegend, show
from mod.funk_utils import posix2utc, fn_over

plt.rcParams.update(fcn.rcThesis)
# plt.rcParams.update(fcn.rcPaper)
# plt.rcParams.update(fcn.rcPoS)

# plt.rcParams.update(fcn.rcPoster)
# plt.rcParams['figure.figsize'] = (960/72, 350/72)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def get_data(run, pos, begin=None, dt=None):
	""" dt is timedelta object (duration to retrieve) """

	df = run.get_df(pos)[['time', 'rates']]
	df = df.assign(
					timedelta = posix2utc(df['time']) - \
											posix2utc(df['time'].iloc[0])
					)

	if (begin is not None) and (dt is not None):
		return df[ (df['timedelta']>= begin) &\
							 (df['timedelta'] <= begin+dt) ]

	elif begin is not None:
		return df[ (df['timedelta']>= begin) ]

	elif dt is not None:
		return df[ (df['timedelta'] <= dt) ]

	else:
		return df


# # cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
# cuts  = fcn.cuts.select('dtlim')
# trig 	= True
# run_v = 'v35'
# run 	= Run.configs(run_v, trig=trig, cuts=cuts)
# m 		= 10 # averaging over m*1*4 mins


# label = {
# 	'in_open'			: 'in/open',
# 	'in_closed'		: 'in/closed',
# 	'out_open'		: 'out/open',
# 	'out_closed'	: 'out/closed',
# }

# color = {
# 	'in_open'			: 'red',
# 	'in_closed'		: 'black',
# 	'out_open'		: 'blue',
# 	'out_closed'	: 'grey',
# }


# fig, ax = plt.subplots()

# for pos in ['in_closed', 'out_closed', 'in_open', 'out_open']:

# 	try:
# 		df = get_data(run, pos, begin=None, dt=None)
# 		df = df[['time', 'rates']].apply(
# 						lambda z: fn_over(z, fn=np.mean, m=m))

# 		ax.plot(
# 			posix2utc(df['time'].values),
# 			df['rates'].values,
# 			color=color[pos], lw=0.5,
# 			label=label[pos]
# 			)

# 	except:
# 		print('[Info] nan in df[{}]'.format(pos))

# ax.axhline(y=1.55, lw=1.2, ls='--', color='k')


# autofmt_xdate(daily=7, datefmt='%b-%d') # rotation=None, ha='center')
# plt.figtext(0.9, 0.02, 2019)

# ax.set_ylabel('trigger rate/Hz')
# ax.set_yticks(range(0, 12, 2))
# ax.set_ylim(0, 10)

# add_toplegend(
# 	labels=['in/open', 'in/closed', 'out/open', 'out/closed'],
# 	ncol=2, lw=2, labelspacing=0.2
# )

# fig.tight_layout(pad=0.05)
# fig.savefig('./plt/8/v35_raw_triggers')

# # # show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# internal background

plt.rcParams['figure.figsize'] = (260/72, 165/72)

# cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
cuts  = fcn.cuts.select('dtlim')
trig 	= True
run_v = 'v31'
run 	= Run.configs(run_v, trig=trig, cuts=cuts)

df = get_data(run, 'in_closed', begin=timedelta(days=1.5), dt=None)

plt.plot(
	posix2utc(df['time'].values),
	df['rates'].values,
	lw=0.5, color='darkslategrey',
	label='raw'
)

df2 = df[['time', 'rates']].apply(
				lambda z: fn_over(z, fn=np.mean, m=10))

plt.plot(
	posix2utc(df2['time'].values),
	df2['rates'].values,
	lw=0.5, color='khaki', # '#bcbd22'
	label='10-event average'
)

plt.axhline(y=6.46, lw=1.2, ls='--', color='k')

autofmt_xdate(daily=7, datefmt='%b-%d') # rotation=None, ha='center')
plt.figtext(0.9, 0.02, 2018)

plt.ylabel(r'$b_\mathrm{int}$/Hz')
plt.yticks(range(0,7))
plt.ylim(top=6.9)

add_toplegend(labels=['raw', 'average'], ncol=2, lw=2)

plt.tight_layout(pad=0.05)
plt.subplots_adjust(left=0.1)
plt.savefig('./plt/12/v31_triggerRate')
plt.close()

# show()
# embed()