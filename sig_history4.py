""" reconstruct (exact) true signal in/out from funk run
		NOTE: the history has not the same normalisation as in sig_history.py
		 			and sig_history2.py, alpha' -> alpha/tau
"""
#!usr/bin/env python3

import os
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt

from array import array
from datetime import timedelta
from IPython import embed
from iminuit import Minuit

import mod.funk_consts as fcn
from mod.funk_manip import reset_configs
from mod.funk_plt import show, order_legend, autofmt_xdate
from mod.funk_run import Run
from mod.funk_utils import  (
	posix2utc, timing, pickle_dump, pickle_load, Namespace )

plt.rcParams.update(**fcn.rcPaper)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

class Model:

	def __init__(self, data, base, alpha, tau):

		seq 	 	= list(map(tuple, data[['in', 'open']].iloc[:4].values))
		ix_in  	= seq.index(fcn.positions['in_open'])
		ix_out 	= seq.index(fcn.positions['out_open'])

		n 			= len(data)
		tintvls = [ (i, i+1) for i in range(n) ]

		self.dt 		 = 1
		self.ix_in 	 = ix_in
		self.ix_out	 = ix_out
		self.seq 		 = seq
		self.midtime = np.mean(tintvls, axis=1)
		self.tintvls = tintvls

		self.events  = data['counts'].values
		self.params  = Namespace(base=base, alpha=alpha, tau=tau)

		self.signal  = np.zeros(n)
		self.history = np.zeros(n)
		self.xneg 	 = 0
		self._initial_values()


	def _initial_values(self):

		base 		= self.params.base
		alpha 	= self.params.alpha
		tau 		= self.params.tau
		dt 			= self.dt
		dt_half = dt / 2


		# # known initial values
		# y0 	 = self.events[0] - base
		# den  = 1 + alpha * tau * (1 - np.exp(-dt_half/tau))
		# xneg = 50
		# hneg = xneg * alpha * tau * np.exp(-dt_half/tau) 
		# x0 	 = (y0 - hneg) / den
		# self.xneg 		 = xneg
		# self.signal[0] = x0


		# fit initial values
		ndebut 			 = 4*10
		events_debut = self.events[:ndebut]
		xneg = 50
		xs 	 = 100 * np.ones(ndebut)
		ix0  = min(self.seq.index((0,0)), self.seq.index((1,0)))
		xs[ix0::2] = 50

		parnames = ['xneg'] + ['x%d'%i for i in range(ndebut)]
		fitarg 	 = Namespace(
			[ ('xneg', xneg) ] +
			[ ('x%d'%i, xs[i]) for i in range(ndebut) ] +
			[ ('limit_xneg', (0, None)) ] +
			[ ('limit_x%d'%i, (0, None)) for i in range(ndebut) ] +
			[ ('error_xneg', 0.1) ] +
			[ ('error_x%d'%i, 0.1) for i in range(ndebut) ] +
			[ ('fix_x%d'%i, False) for i in range(ix0, ndebut, 2)] +	
			# [ ('base', base), ('alpha', alpha), ('tau', tau) ] +
			# [ ('error_base', 0.1), ('error_0.1', 0.1), ('error_tau', 0.01)] +
			# [ ('fix_base', True), ('fix_alpha', True), ('fix_tau', True)] + 
			[ ('errordef', 1) ]
			)

		def lsq(*args):

			xneg, *xs = args
			ys = np.zeros(ndebut)
			hs = np.zeros(ndebut)

			coeff = 1 + alpha*tau * (1 - np.exp(-dt_half/tau))
			ys[0] = base + xs[0]*coeff + alpha*tau*xneg * np.exp(-dt_half/tau)

			for i in range(1, ndebut):
				hs[i] = hs[i-1] * np.exp(-dt/tau) +\
								xs[i-1] * np.exp(-dt_half/tau) * (1 - np.exp(-dt/tau))
				hneg 	= xneg * np.exp(-(i+dt_half)/tau) 
				ys[i] = base + xs[i]*coeff + alpha*tau * (hneg + hs[i])

			# error is assumed to be proportional to the poissonian one or constant
			return np.sum( (events_debut - ys)**2 )


		mfit = Minuit(lsq, forced_parameters=parnames, **fitarg)
		print('fitting initial values::')
		timing(mfit.migrad)()
		# mfit.minos('xneg')
		# mfit.minos('x0')

		self.xneg 		 = mfit.values['xneg']
		self.signal[0] = mfit.values['x0']


	def reconstruct(self):

		nevents = len(self.events)
		base 		= self.params.base
		alpha 	= self.params.alpha
		tau 		= self.params.tau
		dt 			= self.dt
		dt_half = dt / 2
		den 		= 1 + alpha * tau * (1 - np.exp(-dt_half/tau))


		for i in range(1, nevents):

			yi 	 = self.events[i] - base
			hi 	 = self.history[i-1] * np.exp(-dt/tau) +\
					 	 self.signal[i-1] * np.exp(-dt_half/tau) * (1 - np.exp(-dt/tau))
			hneg = self.xneg *  np.exp(-(i+dt_half)/tau) 
			xi 	 = (yi  - alpha*tau * (hi + hneg) ) / den

			self.history[i] = hi
			self.signal[i] 	= xi

		print('::reconstruction done!')

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


def get_data(run, begin=None, Dt=None):
	""" begin/Dt: timedelta object (duration to retrieve) """

	df = reset_configs(run.configs)\
				[['time', 'in', 'open', 'dt', 'counts', 'T1']]
	df = df.assign(
					timedelta = posix2utc(df['time']) - \
											posix2utc(df['time'].iloc[0])
					)

	if (begin is not None) and (Dt is not None):
		ret = df[ (df['timedelta']>= begin) & (df['timedelta'] <= begin+Dt) ]

	elif begin is not None:
		ret = df[ (df['timedelta']>= begin) ]

	elif Dt is not None:
		ret = df[ (df['timedelta'] <= Dt) ]

	else:
		ret = df

	return ret.iloc[:len(ret)-len(ret)%4]\
						.reset_index(drop=True)


def plot_data(data, length=100, ax=None):

	if ax is None:
		ax = plt.gca()

	counts = data['counts'].values[:length]
	time 	 = np.arange(len(counts)) + 0.5 # shift to middle of intervals
	seq 	 = list(map(tuple, data[['in', 'open']].iloc[:4].values))

	xy 		 = {
		seq[i]: [ time[i::4], counts[i::4], np.sqrt(counts[i::4]) ]\
		for i in range(4)
		}

	color  = {
		(0, 0): 'k', (1, 0): 'k',
		(0, 1): 'b', (1, 1): 'grey'
		}

	label  = {
		(0, 0): '', (1, 0): 'in/out_closed',
		(0, 1): 'out_open', (1, 1): 'in_open'
		}

	for d in xy.items():
		if d[0]==(1,1):
			ax.errorbar(
				*d[1], fmt='o', ms=1, elinewidth=0.5,
				color=color[d[0]], label=label[d[0]],
				alpha=1, zorder=0 )


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# data
# base, alpha, tau = 96.3677, 0.0275, 52.879
base, alpha, tau = 96.5991, 0.0170406, 54.1243

# cuts  = fcn.cuts.select('dtlim')
cuts  = fcn.cuts.select('dtlim', 'riselim', 'Slim', 'tsiglim')
trig 	= False
run_v = 'v35'
run 	= Run.configs(run_v, trig=trig, cuts=cuts)


begin = None # timedelta(hours=5)
Dt 		= timedelta(weeks=1)
data 	= get_data(run, begin, Dt)

model = Model(data, 0, alpha, tau)
model.reconstruct()


plt.figure()
ix 	 = slice(0, None, 4)
time = posix2utc(data['time'].values)
dts  = data['dt'].values

plt.plot(
	time[ix], model.events[ix]/dts[ix], color='k',
	lw=0.5, label='SPE events')
plt.plot(
	time[ix], model.signal[ix]/dts[ix], color='dimgrey',
	lw=0.5, label='true signal')
plt.plot(
	time[ix], (model.events[ix]-model.signal[ix])/dts[ix], color='purple',
	lw=1, label='history')

plt.ylabel('pulse-event rates [Hz]')

autofmt_xdate(hourly=40, datefmt='%b-%d', rotation=30, ha='right')
lgd = plt.legend(
	*order_legend('SPE', 'signal', 'history'),
	bbox_to_anchor=(0., 1.005, 1., .102), loc='lower left',
	ncol=3, mode="expand", borderaxespad=0,
	handlelength=1.5, handletextpad=0.5
	)
for line in lgd.get_lines():
	line.set_linewidth(2)

plt.grid(which='major', ls=':')

plt.tight_layout(pad=0)
plt.savefig('./plt/8/reconstruction2')


# plt.figure()
# plt.hist(model.events[ix], 100, histtype='step')
# plt.hist(model.signal[ix], 100, histtype='step')
# print(np.mean(model.events[ix]), np.var(model.events[ix], ddof=1))
# print(np.mean(model.signal[ix]), np.var(model.signal[ix], ddof=1))

# show()



# base, alpha, tau = 100, 0.0275, 55

# data  = pd.read_pickle('./data/pmt_memory/dummy_open1_closed1.sdf')#[:4*100]
# data['out'] 	 = 1^data['out']
# data['closed'] = 1^data['closed']
# data.rename(
# 	columns={'out': 'in', 'closed': 'open'},
# 	inplace=True )


# model = Model(data, 100, alpha, tau)
# model.reconstruct()

# # MC
# plt.plot(model.signal - data['counts_true'].values)
# plt.axhline(0, ls='--', color='k', zorder=0)
# show()

# embed()