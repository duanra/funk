""" check open/closed distributions """
#!usr/bin/env python3

import os
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt

from IPython import embed
from iminuit import Minuit
from matplotlib import rc
from contextlib import suppress
from warnings import catch_warnings, filterwarnings
# from inspect import signature
# from functools import wraps

from mod.funk_plt import show
from mod.funk_utils import islistlike, timing, pickle_dump, Namespace

rc('font', size=10)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def read_data(infile):

	dtype = [
		('time', np.uint),
		('triggers', np.uint),
		('dt_long', np.float),
		('dt_short', np.float),
		('closed', np.uint8)
		]

	with catch_warnings():
		filterwarnings('ignore', category=UserWarning)
		data = pd.DataFrame(
							np.genfromtxt(
								infile, dtype=dtype, usecols=(0, 3, 6, 7, 10),
								invalid_raise=False, encoding=None, max_rows=None	))

	data = data.assign( dt=(data['dt_long']+data['dt_short'])/2 )
	data = data.assign( rates=data['triggers']/data['dt'] )
	data = data[['time', 'dt', 'triggers', 'rates', 'closed']]

	return data


def spl_index(nopen, nclosed):
	""" splitting indices """

	so = [ [i, i+nopen] for i in range(nclosed, length, nopen+nclosed) ]
	with suppress(IndexError):
		# correct for when the shutter didn't work
		del so[14]; so[13][1] = 3450
		del so[32]; so[31][1] = 7820

	return [ ix for s in so for ix in s ]


def get_status(data, status, concat=False):

	idx 	= spl_index(nopen, nclosed)
	data_ = np.split(data, idx)
	data_ = data_[0::2] if status=='closed' else data_[1::2]

	return pd.concat(data_, axis=0) if concat else data_



infile 	= './data/a.log'
data 		= read_data(infile)#[:200+3*230]
nopen 	= 30
nclosed = 200
length 	= len(data)


#### check splitting
# data_open 	= get_status(data, 'open')
# data_closed = get_status(data, 'closed')
# for d in data_open:
# 	plt.plot(d['triggers'], lw=0.5)
# for d in data_closed:
# 	plt.plot(d['triggers'], color='k', lw=0.5)
# show()


#### check distn
# counts_open 	= get_status(data, 'open', True)['triggers']
# counts_closed = get_status(data, 'closed', True)['triggers']

# fig, ax = plt.subplots(figsize=(5,4))
# ax.hist(counts_open, bins=100, range=None,
# 				color='r', histtype='step',
# 				label='shutter open:\n'
# 							'mu = {:.5}, var = {:.5}'\
# 							.format(counts_open.mean(), counts_open.var()))
# ax.hist(counts_closed, bins=100, range=None,
# 				color='b', histtype='step',
# 				label='shutter closed:\n'
# 							'mu = {:.5}, var = {:.5}'\
# 							.format(counts_closed.mean(), counts_closed.var()))

# ax.legend()
# fig.tight_layout()
# show()


#	### check truncation
# ls_closed = get_status(data, 'closed')

# counts 		= pd.concat([ d[100:]['triggers'] for d in ls_closed ], axis=0)

# plt.hist(counts, bins=100, 
# 				 label='shutter closed:\n'
# 				 'mu = {:.5}, var = {:.5}'\
# 				 .format(counts.mean(), counts.var())	)

# plt.legend()
# show()

embed()