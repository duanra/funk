#!usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt 
import seaborn as sns


arr = np.array([
	[0.2, 0.4, 0.7],
	[0.1, 0.5, 0.3],
	[0.8, 0.9, 0.2],
	])

fig, ax = plt.subplots()

sns.heatmap(
	arr, annot=True, fmt='.3f',
	cmap='RdYlBu_r', square=True,
	vmin=0, vmax=1,
	cbar_kws={'pad': .005, 'aspect': 30, 'shrink': 1},
	ax=ax)

ax.set_xticklabels(['a', 'b', 'c'])

plt.show()
plt.close()			
